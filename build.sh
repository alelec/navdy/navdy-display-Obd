#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR"

bash ./set_version.sh $(git describe --tags | cut -d'-' -f1)
java -Xmx6g -jar ../apktool_2.3.2.jar b . -o Obd.apk
zipalign -f -p 4 Obd.apk Obda.apk
apksigner sign --ks ../navdy_cert.jks --ks-pass=env:JP --out Obd.apk Obda.apk
rm -rf Obda.apk

popd
