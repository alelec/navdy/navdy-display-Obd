#!/bin/bash
# Set version 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" > /dev/null

export VNUM=${1}
export VERS=1.0.${1}-$(git rev-parse --short HEAD)
echo $VERS

sed -r -i "s#versionCode: .*#versionCode: \x27${VNUM}\x27#" apktool.yml
sed -r -i "s#versionName: .*#versionName: ${VERS}#" apktool.yml
dos2unix apktool.yml

sed -r -i "s#android\:versionCode=\"[0-9]+\"#android:versionCode=\"${VNUM}\"#" AndroidManifest.xml
sed -r -i "s#android\:versionName=\"\S+?\"#android:versionName=\"${VERS}\"#" AndroidManifest.xml
dos2unix AndroidManifest.xml

sed -r -i "s#VERSION_NAME:Ljava/lang/String; = .*#VERSION_NAME:Ljava/lang/String; = \"${VERS}\"#" smali/com/navdy/obd/app/BuildConfig.smali
dos2unix smali/com/navdy/obd/app/BuildConfig.smali

popd > /dev/null