.class public Lparsii/tokenizer/ParseError;
.super Ljava/lang/Object;
.source "ParseError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lparsii/tokenizer/ParseError$Severity;
    }
.end annotation


# instance fields
.field private message:Ljava/lang/String;

.field private pos:Lparsii/tokenizer/Position;

.field private final severity:Lparsii/tokenizer/ParseError$Severity;


# direct methods
.method protected constructor <init>(Lparsii/tokenizer/Position;Ljava/lang/String;Lparsii/tokenizer/ParseError$Severity;)V
    .locals 0
    .param p1, "pos"    # Lparsii/tokenizer/Position;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "severity"    # Lparsii/tokenizer/ParseError$Severity;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lparsii/tokenizer/ParseError;->pos:Lparsii/tokenizer/Position;

    .line 37
    iput-object p2, p0, Lparsii/tokenizer/ParseError;->message:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lparsii/tokenizer/ParseError;->severity:Lparsii/tokenizer/ParseError$Severity;

    .line 39
    return-void
.end method

.method public static error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;
    .locals 5
    .param p0, "pos"    # Lparsii/tokenizer/Position;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 70
    move-object v0, p1

    .line 71
    .local v0, "message":Ljava/lang/String;
    invoke-interface {p0}, Lparsii/tokenizer/Position;->getLine()I

    move-result v1

    if-lez v1, :cond_0

    .line 72
    const-string v1, "%3d:%2d: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p0}, Lparsii/tokenizer/Position;->getLine()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p0}, Lparsii/tokenizer/Position;->getPos()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_0
    new-instance v1, Lparsii/tokenizer/ParseError;

    sget-object v2, Lparsii/tokenizer/ParseError$Severity;->ERROR:Lparsii/tokenizer/ParseError$Severity;

    invoke-direct {v1, p0, v0, v2}, Lparsii/tokenizer/ParseError;-><init>(Lparsii/tokenizer/Position;Ljava/lang/String;Lparsii/tokenizer/ParseError$Severity;)V

    return-object v1
.end method

.method public static warning(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;
    .locals 5
    .param p0, "pos"    # Lparsii/tokenizer/Position;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 52
    move-object v0, p1

    .line 53
    .local v0, "message":Ljava/lang/String;
    invoke-interface {p0}, Lparsii/tokenizer/Position;->getLine()I

    move-result v1

    if-lez v1, :cond_0

    .line 54
    const-string v1, "%3d:%2d: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p0}, Lparsii/tokenizer/Position;->getLine()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p0}, Lparsii/tokenizer/Position;->getPos()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 56
    :cond_0
    new-instance v1, Lparsii/tokenizer/ParseError;

    sget-object v2, Lparsii/tokenizer/ParseError$Severity;->WARNING:Lparsii/tokenizer/ParseError$Severity;

    invoke-direct {v1, p0, v0, v2}, Lparsii/tokenizer/ParseError;-><init>(Lparsii/tokenizer/Position;Ljava/lang/String;Lparsii/tokenizer/ParseError$Severity;)V

    return-object v1
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lparsii/tokenizer/ParseError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Lparsii/tokenizer/Position;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lparsii/tokenizer/ParseError;->pos:Lparsii/tokenizer/Position;

    return-object v0
.end method

.method public getSeverity()Lparsii/tokenizer/ParseError$Severity;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lparsii/tokenizer/ParseError;->severity:Lparsii/tokenizer/ParseError$Severity;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 106
    const-string v0, "%s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lparsii/tokenizer/ParseError;->severity:Lparsii/tokenizer/ParseError$Severity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lparsii/tokenizer/ParseError;->message:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
