.class public Lparsii/tokenizer/Token;
.super Ljava/lang/Object;
.source "Token.java"

# interfaces
.implements Lparsii/tokenizer/Position;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lparsii/tokenizer/Token$TokenType;
    }
.end annotation


# instance fields
.field private contents:Ljava/lang/String;

.field private internTrigger:Ljava/lang/String;

.field private line:I

.field protected pos:I

.field private source:Ljava/lang/String;

.field private trigger:Ljava/lang/String;

.field private type:Lparsii/tokenizer/Token$TokenType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public static create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;
    .locals 2
    .param p0, "type"    # Lparsii/tokenizer/Token$TokenType;
    .param p1, "pos"    # Lparsii/tokenizer/Position;

    .prologue
    .line 77
    new-instance v0, Lparsii/tokenizer/Token;

    invoke-direct {v0}, Lparsii/tokenizer/Token;-><init>()V

    .line 78
    .local v0, "result":Lparsii/tokenizer/Token;
    iput-object p0, v0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    .line 79
    invoke-interface {p1}, Lparsii/tokenizer/Position;->getLine()I

    move-result v1

    iput v1, v0, Lparsii/tokenizer/Token;->line:I

    .line 80
    invoke-interface {p1}, Lparsii/tokenizer/Position;->getPos()I

    move-result v1

    iput v1, v0, Lparsii/tokenizer/Token;->pos:I

    .line 82
    return-object v0
.end method

.method public static createAndFill(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;
    .locals 2
    .param p0, "type"    # Lparsii/tokenizer/Token$TokenType;
    .param p1, "ch"    # Lparsii/tokenizer/Char;

    .prologue
    .line 94
    new-instance v0, Lparsii/tokenizer/Token;

    invoke-direct {v0}, Lparsii/tokenizer/Token;-><init>()V

    .line 95
    .local v0, "result":Lparsii/tokenizer/Token;
    iput-object p0, v0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    .line 96
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getLine()I

    move-result v1

    iput v1, v0, Lparsii/tokenizer/Token;->line:I

    .line 97
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getPos()I

    move-result v1

    iput v1, v0, Lparsii/tokenizer/Token;->pos:I

    .line 98
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getStringValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getStringValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 101
    return-object v0
.end method


# virtual methods
.method public addToContent(C)Lparsii/tokenizer/Token;
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;
    .locals 1
    .param p1, "ch"    # Lparsii/tokenizer/Char;

    .prologue
    .line 135
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Token;->addToContent(C)Lparsii/tokenizer/Token;

    move-result-object v0

    return-object v0
.end method

.method public addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;
    .locals 2
    .param p1, "ch"    # Lparsii/tokenizer/Char;

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 125
    return-object p0
.end method

.method public addToTrigger(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;
    .locals 2
    .param p1, "ch"    # Lparsii/tokenizer/Char;

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 114
    return-object p0
.end method

.method public getContents()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    return-object v0
.end method

.method public getLine()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lparsii/tokenizer/Token;->line:I

    return v0
.end method

.method public getPos()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lparsii/tokenizer/Token;->pos:I

    return v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    .line 170
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lparsii/tokenizer/Token$TokenType;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    return-object v0
.end method

.method public hasContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 311
    if-nez p1, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "content must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public is(Lparsii/tokenizer/Token$TokenType;)Z
    .locals 1
    .param p1, "type"    # Lparsii/tokenizer/Token$TokenType;

    .prologue
    .line 324
    iget-object v0, p0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDecimal()Z
    .locals 1

    .prologue
    .line 456
    sget-object v0, Lparsii/tokenizer/Token$TokenType;->DECIMAL:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v0

    return v0
.end method

.method public isEnd()Z
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->EOI:Lparsii/tokenizer/Token$TokenType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs isIdentifier([Ljava/lang/String;)Z
    .locals 5
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 382
    array-length v4, p1

    if-nez v4, :cond_0

    .line 383
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v4

    .line 390
    :goto_0
    return v4

    .line 385
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 386
    .local v3, "value":Ljava/lang/String;
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4, v3}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 387
    const/4 v4, 0x1

    goto :goto_0

    .line 385
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 390
    .end local v3    # "value":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isInteger()Z
    .locals 1

    .prologue
    .line 447
    sget-object v0, Lparsii/tokenizer/Token$TokenType;->INTEGER:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v0

    return v0
.end method

.method public varargs isKeyword([Ljava/lang/String;)Z
    .locals 5
    .param p1, "keywords"    # [Ljava/lang/String;

    .prologue
    .line 360
    array-length v4, p1

    if-nez v4, :cond_0

    .line 361
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v4

    .line 368
    :goto_0
    return v4

    .line 363
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v2, v0, v1

    .line 364
    .local v2, "keyword":Ljava/lang/String;
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4, v2}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 365
    const/4 v4, 0x1

    goto :goto_0

    .line 363
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 368
    .end local v2    # "keyword":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isNotEnd()Z
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lparsii/tokenizer/Token;->type:Lparsii/tokenizer/Token$TokenType;

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->EOI:Lparsii/tokenizer/Token$TokenType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNumber()Z
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0}, Lparsii/tokenizer/Token;->isInteger()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Token;->isDecimal()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs isSpecialIdentifier([Ljava/lang/String;)Z
    .locals 5
    .param p1, "triggers"    # [Ljava/lang/String;

    .prologue
    .line 404
    array-length v4, p1

    if-nez v4, :cond_0

    .line 405
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v4

    .line 412
    :goto_0
    return v4

    .line 407
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 408
    .local v3, "trigger":Ljava/lang/String;
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4, v3}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409
    const/4 v4, 0x1

    goto :goto_0

    .line 407
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 412
    .end local v3    # "trigger":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public varargs isSpecialIdentifierWithContent(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 7
    .param p1, "trigger"    # Ljava/lang/String;
    .param p2, "contents"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 427
    sget-object v6, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v6, p1}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 438
    :cond_0
    :goto_0
    return v4

    .line 430
    :cond_1
    array-length v6, p2

    if-nez v6, :cond_2

    move v4, v5

    .line 431
    goto :goto_0

    .line 433
    :cond_2
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 434
    .local v1, "content":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v4, v5

    .line 435
    goto :goto_0

    .line 433
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public isString()Z
    .locals 1

    .prologue
    .line 474
    sget-object v0, Lparsii/tokenizer/Token$TokenType;->STRING:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v0

    return v0
.end method

.method public varargs isSymbol([Ljava/lang/String;)Z
    .locals 5
    .param p1, "symbols"    # [Ljava/lang/String;

    .prologue
    .line 338
    array-length v4, p1

    if-nez v4, :cond_0

    .line 339
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v4

    .line 346
    :goto_0
    return v4

    .line 341
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 342
    .local v3, "symbol":Ljava/lang/String;
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {p0, v4, v3}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 343
    const/4 v4, 0x1

    goto :goto_0

    .line 341
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 346
    .end local v3    # "symbol":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Lparsii/tokenizer/Token$TokenType;
    .param p2, "trigger"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 274
    invoke-virtual {p0, p1}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 281
    :cond_0
    :goto_0
    return v0

    .line 277
    :cond_1
    if-nez p2, :cond_2

    .line 278
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trigger must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_2
    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getTrigger()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 232
    iput-object p1, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 244
    iput-object p1, p0, Lparsii/tokenizer/Token;->source:Ljava/lang/String;

    .line 245
    return-void
.end method

.method public setTrigger(Ljava/lang/String;)V
    .locals 1
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    .line 219
    iput-object p1, p0, Lparsii/tokenizer/Token;->trigger:Ljava/lang/String;

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lparsii/tokenizer/Token;->internTrigger:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public silentAddToContent(C)Lparsii/tokenizer/Token;
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lparsii/tokenizer/Token;->contents:Ljava/lang/String;

    .line 158
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getType()Lparsii/tokenizer/Token$TokenType;

    move-result-object v1

    invoke-virtual {v1}, Lparsii/tokenizer/Token$TokenType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lparsii/tokenizer/Token;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lparsii/tokenizer/Token;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs wasTriggeredBy([Ljava/lang/String;)Z
    .locals 7
    .param p1, "triggers"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 291
    array-length v5, p1

    if-nez v5, :cond_1

    .line 300
    :cond_0
    :goto_0
    return v4

    .line 294
    :cond_1
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 295
    .local v0, "aTrigger":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lparsii/tokenizer/Token;->getTrigger()Ljava/lang/String;

    move-result-object v6

    if-ne v5, v6, :cond_2

    .line 296
    const/4 v4, 0x1

    goto :goto_0

    .line 294
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
