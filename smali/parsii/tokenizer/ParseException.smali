.class public Lparsii/tokenizer/ParseException;
.super Ljava/lang/Exception;
.source "ParseException.java"


# instance fields
.field private errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "errors":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 46
    iput-object p2, p0, Lparsii/tokenizer/ParseException;->errors:Ljava/util/List;

    .line 47
    return-void
.end method

.method public static create(Ljava/util/List;)Lparsii/tokenizer/ParseException;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;)",
            "Lparsii/tokenizer/ParseException;"
        }
    .end annotation

    .prologue
    .local p0, "errors":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 34
    new-instance v1, Lparsii/tokenizer/ParseException;

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/ParseError;

    invoke-virtual {v0}, Lparsii/tokenizer/ParseError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lparsii/tokenizer/ParseException;-><init>(Ljava/lang/String;Ljava/util/List;)V

    move-object v0, v1

    .line 40
    :goto_0
    return-object v0

    .line 35
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_1

    .line 36
    new-instance v1, Lparsii/tokenizer/ParseException;

    const-string v2, "%d errors occured. First: %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/ParseError;

    invoke-virtual {v0}, Lparsii/tokenizer/ParseError;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lparsii/tokenizer/ParseException;-><init>(Ljava/lang/String;Ljava/util/List;)V

    move-object v0, v1

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lparsii/tokenizer/ParseException;

    const-string v1, "An unknown error occured"

    invoke-direct {v0, v1, p0}, Lparsii/tokenizer/ParseException;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public getErrors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lparsii/tokenizer/ParseException;->errors:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 60
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lparsii/tokenizer/ParseException;->errors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/ParseError;

    .line 62
    .local v0, "error":Lparsii/tokenizer/ParseError;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 63
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 68
    .end local v0    # "error":Lparsii/tokenizer/ParseError;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
