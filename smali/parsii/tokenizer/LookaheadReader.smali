.class public Lparsii/tokenizer/LookaheadReader;
.super Lparsii/tokenizer/Lookahead;
.source "LookaheadReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lparsii/tokenizer/Lookahead",
        "<",
        "Lparsii/tokenizer/Char;",
        ">;"
    }
.end annotation


# instance fields
.field private input:Ljava/io/Reader;

.field private line:I

.field private pos:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 40
    invoke-direct {p0}, Lparsii/tokenizer/Lookahead;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lparsii/tokenizer/LookaheadReader;->input:Ljava/io/Reader;

    .line 45
    return-void
.end method


# virtual methods
.method protected bridge synthetic endOfInput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lparsii/tokenizer/LookaheadReader;->endOfInput()Lparsii/tokenizer/Char;

    move-result-object v0

    return-object v0
.end method

.method protected endOfInput()Lparsii/tokenizer/Char;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lparsii/tokenizer/Char;

    const/4 v1, 0x0

    iget v2, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    iget v3, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-direct {v0, v1, v2, v3}, Lparsii/tokenizer/Char;-><init>(CII)V

    return-object v0
.end method

.method protected bridge synthetic fetch()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lparsii/tokenizer/LookaheadReader;->fetch()Lparsii/tokenizer/Char;

    move-result-object v0

    return-object v0
.end method

.method protected fetch()Lparsii/tokenizer/Char;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 55
    :try_start_0
    iget-object v3, p0, Lparsii/tokenizer/LookaheadReader;->input:Ljava/io/Reader;

    invoke-virtual {v3}, Ljava/io/Reader;->read()I

    move-result v0

    .line 56
    .local v0, "character":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 67
    .end local v0    # "character":I
    :goto_0
    return-object v2

    .line 59
    .restart local v0    # "character":I
    :cond_0
    const/16 v3, 0xa

    if-ne v0, v3, :cond_1

    .line 60
    iget v3, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    .line 61
    const/4 v3, 0x0

    iput v3, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    .line 63
    :cond_1
    iget v3, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    .line 64
    new-instance v3, Lparsii/tokenizer/Char;

    int-to-char v4, v0

    iget v5, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    iget v6, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-direct {v3, v4, v5, v6}, Lparsii/tokenizer/Char;-><init>(CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_0

    .line 65
    .end local v0    # "character":I
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/io/IOException;
    iget-object v3, p0, Lparsii/tokenizer/LookaheadReader;->problemCollector:Ljava/util/List;

    new-instance v4, Lparsii/tokenizer/Char;

    iget v5, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    iget v6, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-direct {v4, v7, v5, v6}, Lparsii/tokenizer/Char;-><init>(CII)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lparsii/tokenizer/LookaheadReader;->itemBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Buffer empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 76
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/LookaheadReader;->itemBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lparsii/tokenizer/LookaheadReader;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
