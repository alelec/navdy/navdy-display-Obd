.class public Lparsii/tokenizer/Tokenizer;
.super Lparsii/tokenizer/Lookahead;
.source "Tokenizer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lparsii/tokenizer/Lookahead",
        "<",
        "Lparsii/tokenizer/Token;",
        ">;"
    }
.end annotation


# instance fields
.field private blockCommentEnd:Ljava/lang/String;

.field private blockCommentStart:Ljava/lang/String;

.field private brackets:[C

.field private decimalSeparator:C

.field private effectiveDecimalSeparator:C

.field private groupingSeparator:C

.field protected input:Lparsii/tokenizer/LookaheadReader;

.field private keywords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private keywordsCaseSensitive:Z

.field private lineComment:Ljava/lang/String;

.field private specialIdStarters:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private specialIdTerminators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private stringDelimiters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private treatSinglePipeAsBracket:Z


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 3
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    const/16 v0, 0x2e

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Lparsii/tokenizer/Lookahead;-><init>()V

    .line 50
    iput-char v0, p0, Lparsii/tokenizer/Tokenizer;->decimalSeparator:C

    .line 54
    iput-char v0, p0, Lparsii/tokenizer/Tokenizer;->effectiveDecimalSeparator:C

    .line 58
    const/16 v0, 0x5f

    iput-char v0, p0, Lparsii/tokenizer/Tokenizer;->groupingSeparator:C

    .line 62
    const-string v0, "//"

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->lineComment:Ljava/lang/String;

    .line 66
    const-string v0, "/*"

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentStart:Ljava/lang/String;

    .line 70
    const-string v0, "*/"

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentEnd:Ljava/lang/String;

    .line 75
    const/4 v0, 0x6

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->brackets:[C

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lparsii/tokenizer/Tokenizer;->treatSinglePipeAsBracket:Z

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->specialIdStarters:Ljava/util/Set;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->specialIdTerminators:Ljava/util/Set;

    .line 92
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->keywords:Ljava/util/Map;

    .line 96
    iput-boolean v2, p0, Lparsii/tokenizer/Tokenizer;->keywordsCaseSensitive:Z

    .line 101
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    .line 110
    new-instance v0, Lparsii/tokenizer/LookaheadReader;

    invoke-direct {v0, p1}, Lparsii/tokenizer/LookaheadReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    .line 111
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/LookaheadReader;->setProblemCollector(Ljava/util/List;)V

    .line 114
    const/16 v0, 0x22

    const/16 v1, 0x5c

    invoke-virtual {p0, v0, v1}, Lparsii/tokenizer/Tokenizer;->addStringDelimiter(CC)V

    .line 115
    const/16 v0, 0x27

    invoke-virtual {p0, v0, v2}, Lparsii/tokenizer/Tokenizer;->addStringDelimiter(CC)V

    .line 116
    return-void

    .line 75
    :array_0
    .array-data 2
        0x28s
        0x5bs
        0x7bs
        0x7ds
        0x5ds
        0x29s
    .end array-data
.end method


# virtual methods
.method public varargs addError(Lparsii/tokenizer/Position;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "pos"    # Lparsii/tokenizer/Position;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "parameters"    # [Ljava/lang/Object;

    .prologue
    .line 808
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809
    return-void
.end method

.method public addKeyword(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 578
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->keywords:Ljava/util/Map;

    iget-boolean v0, p0, Lparsii/tokenizer/Tokenizer;->keywordsCaseSensitive:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    return-void

    .line 578
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public addSpecialIdStarter(C)V
    .locals 2
    .param p1, "character"    # C

    .prologue
    .line 588
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->specialIdStarters:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 589
    return-void
.end method

.method public addSpecialIdTerminator(C)V
    .locals 2
    .param p1, "character"    # C

    .prologue
    .line 598
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->specialIdTerminators:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 599
    return-void
.end method

.method public addStringDelimiter(CC)V
    .locals 3
    .param p1, "stringDelimiter"    # C
    .param p2, "escapeCharacter"    # C

    .prologue
    .line 620
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    return-void
.end method

.method public addUnescapedStringDelimiter(C)V
    .locals 3
    .param p1, "stringDelimiter"    # C

    .prologue
    .line 629
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    return-void
.end method

.method public varargs addWarning(Lparsii/tokenizer/Position;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "pos"    # Lparsii/tokenizer/Position;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "parameters"    # [Ljava/lang/Object;

    .prologue
    .line 825
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lparsii/tokenizer/ParseError;->warning(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 826
    return-void
.end method

.method public atEnd()Z
    .locals 1

    .prologue
    .line 790
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    invoke-virtual {v0}, Lparsii/tokenizer/Token;->isEnd()Z

    move-result v0

    return v0
.end method

.method protected canConsumeThisString(Ljava/lang/String;)Z
    .locals 6
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 245
    if-nez p1, :cond_0

    move v1, v2

    .line 254
    :goto_0
    return v1

    .line 248
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 249
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1, v0}, Lparsii/tokenizer/LookaheadReader;->next(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    new-array v4, v3, [C

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, v4, v2

    invoke-virtual {v1, v4}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 250
    goto :goto_0

    .line 248
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 253
    :cond_2
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/LookaheadReader;->consume(I)V

    move v1, v3

    .line 254
    goto :goto_0
.end method

.method public clearStringDelimiters()V
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 610
    return-void
.end method

.method public consumeExpectedKeyword(Ljava/lang/String;)V
    .locals 5
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 837
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {v0, v1, p1}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 842
    :goto_0
    return-void

    .line 840
    :cond_0
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Position;

    const-string v2, "Unexpected token: \'%s\'. Expected: \'%s\'"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Token;

    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object p1, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lparsii/tokenizer/Tokenizer;->addError(Lparsii/tokenizer/Position;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public consumeExpectedSymbol(Ljava/lang/String;)V
    .locals 5
    .param p1, "symbol"    # Ljava/lang/String;

    .prologue
    .line 829
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {v0, v1, p1}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 830
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 834
    :goto_0
    return-void

    .line 832
    :cond_0
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Position;

    const-string v2, "Unexpected token: \'%s\'. Expected: \'%s\'"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Token;

    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object p1, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lparsii/tokenizer/Tokenizer;->addError(Lparsii/tokenizer/Position;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic endOfInput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->endOfInput()Lparsii/tokenizer/Token;

    move-result-object v0

    return-object v0
.end method

.method protected endOfInput()Lparsii/tokenizer/Token;
    .locals 2

    .prologue
    .line 126
    sget-object v1, Lparsii/tokenizer/Token$TokenType;->EOI:Lparsii/tokenizer/Token$TokenType;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-static {v1, v0}, Lparsii/tokenizer/Token;->createAndFill(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic fetch()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetch()Lparsii/tokenizer/Token;

    move-result-object v0

    return-object v0
.end method

.method protected fetch()Lparsii/tokenizer/Token;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 132
    :goto_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isWhitepace()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    goto :goto_0

    .line 137
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x0

    .line 189
    :goto_1
    return-object v0

    .line 142
    :cond_1
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfLineComment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->skipToEndOfLine()V

    .line 144
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetch()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 148
    :cond_2
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfBlockComment()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->skipBlockComment()V

    .line 150
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetch()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 154
    :cond_3
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetchNumber()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 159
    :cond_4
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfIdentifier()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 160
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetchId()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 164
    :cond_5
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->getValue()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 165
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetchString()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 170
    :cond_6
    invoke-virtual {p0, v5}, Lparsii/tokenizer/Tokenizer;->isAtBracket(Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 171
    sget-object v1, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-static {v1, v0}, Lparsii/tokenizer/Token;->createAndFill(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 175
    :cond_7
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfSpecialId()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 176
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetchSpecialId()Lparsii/tokenizer/Token;

    move-result-object v0

    goto :goto_1

    .line 180
    :cond_8
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->isSymbolCharacter(Lparsii/tokenizer/Char;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 181
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetchSymbol()Lparsii/tokenizer/Token;

    move-result-object v0

    goto/16 :goto_1

    .line 184
    :cond_9
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Position;

    const-string v3, "Invalid character in input: \'%s\'"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {v1}, Lparsii/tokenizer/Char;->getStringValue()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    .line 189
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->fetch()Lparsii/tokenizer/Token;

    move-result-object v0

    goto/16 :goto_1
.end method

.method protected fetchId()Lparsii/tokenizer/Token;
    .locals 4

    .prologue
    .line 400
    sget-object v3, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Position;

    invoke-static {v3, v2}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v0

    .line 401
    .local v0, "result":Lparsii/tokenizer/Token;
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v2}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 402
    :goto_0
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {p0, v2}, Lparsii/tokenizer/Tokenizer;->isIdentifierChar(Lparsii/tokenizer/Char;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v2}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto :goto_0

    .line 405
    :cond_0
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v2}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->specialIdTerminators:Ljava/util/Set;

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v2}, Lparsii/tokenizer/Char;->getValue()C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    invoke-static {v2, v0}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v1

    .line 407
    .local v1, "specialId":Lparsii/tokenizer/Token;
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v2}, Lparsii/tokenizer/Char;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->setTrigger(Ljava/lang/String;)V

    .line 408
    invoke-virtual {v0}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->setContent(Ljava/lang/String;)V

    .line 409
    invoke-virtual {v0}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->setSource(Ljava/lang/String;)V

    .line 410
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 411
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    .line 412
    invoke-virtual {p0, v1}, Lparsii/tokenizer/Tokenizer;->handleKeywords(Lparsii/tokenizer/Token;)Lparsii/tokenizer/Token;

    move-result-object v2

    .line 414
    .end local v1    # "specialId":Lparsii/tokenizer/Token;
    :goto_1
    return-object v2

    :cond_1
    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->handleKeywords(Lparsii/tokenizer/Token;)Lparsii/tokenizer/Token;

    move-result-object v2

    goto :goto_1
.end method

.method protected fetchNumber()Lparsii/tokenizer/Token;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 519
    sget-object v3, Lparsii/tokenizer/Token$TokenType;->INTEGER:Lparsii/tokenizer/Token$TokenType;

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Position;

    invoke-static {v3, v2}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v1

    .line 520
    .local v1, "result":Lparsii/tokenizer/Token;
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 521
    :goto_0
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v2}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    const/4 v3, 0x2

    new-array v3, v3, [C

    iget-char v4, p0, Lparsii/tokenizer/Tokenizer;->decimalSeparator:C

    aput-char v4, v3, v5

    iget-char v4, p0, Lparsii/tokenizer/Tokenizer;->groupingSeparator:C

    aput-char v4, v3, v6

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v2}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 523
    :cond_0
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    new-array v3, v6, [C

    iget-char v4, p0, Lparsii/tokenizer/Tokenizer;->groupingSeparator:C

    aput-char v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 524
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto :goto_0

    .line 525
    :cond_1
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    new-array v3, v6, [C

    iget-char v4, p0, Lparsii/tokenizer/Tokenizer;->decimalSeparator:C

    aput-char v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 526
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->DECIMAL:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 527
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Position;

    const-string v4, "Unexpected decimal separators"

    invoke-static {v2, v4}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    :goto_1
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto/16 :goto_0

    .line 529
    :cond_2
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->DECIMAL:Lparsii/tokenizer/Token$TokenType;

    invoke-static {v2, v1}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v0

    .line 530
    .local v0, "decimalToken":Lparsii/tokenizer/Token;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-char v3, p0, Lparsii/tokenizer/Tokenizer;->effectiveDecimalSeparator:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lparsii/tokenizer/Token;->setContent(Ljava/lang/String;)V

    .line 531
    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lparsii/tokenizer/Token;->setSource(Ljava/lang/String;)V

    .line 532
    move-object v1, v0

    goto :goto_1

    .line 536
    .end local v0    # "decimalToken":Lparsii/tokenizer/Token;
    :cond_3
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v2}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto/16 :goto_0

    .line 540
    :cond_4
    return-object v1
.end method

.method protected fetchSpecialId()Lparsii/tokenizer/Token;
    .locals 3

    .prologue
    .line 460
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Position;

    invoke-static {v2, v1}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v0

    .line 461
    .local v0, "result":Lparsii/tokenizer/Token;
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/Token;->addToTrigger(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 462
    :goto_0
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {p0, v1}, Lparsii/tokenizer/Tokenizer;->isIdentifierChar(Lparsii/tokenizer/Char;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto :goto_0

    .line 465
    :cond_0
    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->handleKeywords(Lparsii/tokenizer/Token;)Lparsii/tokenizer/Token;

    move-result-object v1

    return-object v1
.end method

.method protected fetchString()Lparsii/tokenizer/Token;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 325
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v3}, Lparsii/tokenizer/Char;->getValue()C

    move-result v2

    .line 326
    .local v2, "separator":C
    iget-object v4, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v3}, Lparsii/tokenizer/Char;->getValue()C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Character;

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 327
    .local v0, "escapeChar":C
    sget-object v4, Lparsii/tokenizer/Token$TokenType;->STRING:Lparsii/tokenizer/Token$TokenType;

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Position;

    invoke-static {v4, v3}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v1

    .line 328
    .local v1, "result":Lparsii/tokenizer/Token;
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v3}, Lparsii/tokenizer/Token;->addToTrigger(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 329
    :cond_0
    :goto_0
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v3}, Lparsii/tokenizer/Char;->isNewLine()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    new-array v4, v9, [C

    aput-char v2, v4, v8

    invoke-virtual {v3, v4}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v3}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v3

    if-nez v3, :cond_2

    .line 330
    if-eqz v0, :cond_1

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    new-array v4, v9, [C

    aput-char v0, v4, v8

    invoke-virtual {v3, v4}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 331
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v3}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 332
    invoke-virtual {p0, v2, v0, v1}, Lparsii/tokenizer/Tokenizer;->handleStringEscape(CCLparsii/tokenizer/Token;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 333
    iget-object v5, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Position;

    const-string v6, "Cannot use \'%s\' as escaped character"

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v4}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lparsii/tokenizer/Char;

    invoke-virtual {v4}, Lparsii/tokenizer/Char;->getStringValue()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 339
    :cond_1
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v3}, Lparsii/tokenizer/Token;->addToContent(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto/16 :goto_0

    .line 342
    :cond_2
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    new-array v4, v9, [C

    aput-char v2, v4, v8

    invoke-virtual {v3, v4}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 343
    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Char;

    invoke-virtual {v1, v3}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 347
    :goto_1
    return-object v1

    .line 345
    :cond_3
    iget-object v4, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v3}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Position;

    const-string v5, "Premature end of string constant"

    invoke-static {v3, v5}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected fetchSymbol()Lparsii/tokenizer/Token;
    .locals 3

    .prologue
    .line 478
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Position;

    invoke-static {v2, v1}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v0

    .line 479
    .local v0, "result":Lparsii/tokenizer/Token;
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/Token;->addToTrigger(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    .line 480
    :goto_0
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {p0, v1}, Lparsii/tokenizer/Tokenizer;->isSymbolCharacter(Lparsii/tokenizer/Char;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v1}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Char;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/Token;->addToTrigger(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    goto :goto_0

    .line 483
    :cond_0
    return-object v0
.end method

.method public getBlockCommentEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getBlockCommentStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentStart:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimalSeparator()C
    .locals 1

    .prologue
    .line 641
    iget-char v0, p0, Lparsii/tokenizer/Tokenizer;->decimalSeparator:C

    return v0
.end method

.method public getEffectiveDecimalSeparator()C
    .locals 1

    .prologue
    .line 664
    iget-char v0, p0, Lparsii/tokenizer/Tokenizer;->effectiveDecimalSeparator:C

    return v0
.end method

.method public getGroupingSeparator()C
    .locals 1

    .prologue
    .line 687
    iget-char v0, p0, Lparsii/tokenizer/Tokenizer;->groupingSeparator:C

    return v0
.end method

.method public getLineComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->lineComment:Ljava/lang/String;

    return-object v0
.end method

.method protected handleKeywords(Lparsii/tokenizer/Token;)Lparsii/tokenizer/Token;
    .locals 4
    .param p1, "idToken"    # Lparsii/tokenizer/Token;

    .prologue
    .line 424
    iget-boolean v2, p0, Lparsii/tokenizer/Tokenizer;->keywordsCaseSensitive:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->keywords:Ljava/util/Map;

    invoke-virtual {p1}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v0, v2

    .line 428
    .local v0, "keyword":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_1

    .line 429
    sget-object v2, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    invoke-static {v2, p1}, Lparsii/tokenizer/Token;->create(Lparsii/tokenizer/Token$TokenType;Lparsii/tokenizer/Position;)Lparsii/tokenizer/Token;

    move-result-object v1

    .line 430
    .local v1, "keywordToken":Lparsii/tokenizer/Token;
    invoke-virtual {v1, v0}, Lparsii/tokenizer/Token;->setTrigger(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p1}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->setContent(Ljava/lang/String;)V

    .line 432
    invoke-virtual {p1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lparsii/tokenizer/Token;->setSource(Ljava/lang/String;)V

    .line 437
    .end local v1    # "keywordToken":Lparsii/tokenizer/Token;
    :goto_1
    return-object v1

    .line 424
    .end local v0    # "keyword":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lparsii/tokenizer/Tokenizer;->keywords:Ljava/util/Map;

    invoke-virtual {p1}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v0, v2

    goto :goto_0

    .restart local v0    # "keyword":Ljava/lang/String;
    :cond_1
    move-object v1, p1

    .line 437
    goto :goto_1
.end method

.method protected handleStringEscape(CCLparsii/tokenizer/Token;)Z
    .locals 5
    .param p1, "separator"    # C
    .param p2, "escapeChar"    # C
    .param p3, "stringToken"    # Lparsii/tokenizer/Token;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 363
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    aput-char p1, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-virtual {p3, p1}, Lparsii/tokenizer/Token;->addToContent(C)Lparsii/tokenizer/Token;

    .line 365
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move v0, v1

    .line 380
    :goto_0
    return v0

    .line 367
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    aput-char p2, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    invoke-virtual {p3, p2}, Lparsii/tokenizer/Token;->silentAddToContent(C)Lparsii/tokenizer/Token;

    .line 369
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move v0, v1

    .line 370
    goto :goto_0

    .line 371
    :cond_1
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    const/16 v4, 0x6e

    aput-char v4, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    const/16 v0, 0xa

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->silentAddToContent(C)Lparsii/tokenizer/Token;

    .line 373
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move v0, v1

    .line 374
    goto :goto_0

    .line 375
    :cond_2
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    const/16 v4, 0x72

    aput-char v4, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 376
    const/16 v0, 0xd

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->silentAddToContent(C)Lparsii/tokenizer/Token;

    .line 377
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {p3, v0}, Lparsii/tokenizer/Token;->addToSource(Lparsii/tokenizer/Char;)Lparsii/tokenizer/Token;

    move v0, v1

    .line 378
    goto :goto_0

    :cond_3
    move v0, v2

    .line 380
    goto :goto_0
.end method

.method protected isAtBracket(Z)Z
    .locals 5
    .param p1, "inSymbol"    # Z

    .prologue
    const/16 v4, 0x7c

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 227
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->brackets:[C

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 234
    :goto_0
    return v0

    .line 230
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lparsii/tokenizer/Tokenizer;->treatSinglePipeAsBracket:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    aput-char v4, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v1, [C

    aput-char v4, v3, v2

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 231
    goto :goto_0

    :cond_1
    move v0, v2

    .line 234
    goto :goto_0
.end method

.method protected isAtEndOfBlockComment()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentEnd:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->canConsumeThisString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isAtStartOfBlockComment()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->blockCommentStart:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->canConsumeThisString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isAtStartOfIdentifier()Z
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isLetter()Z

    move-result v0

    return v0
.end method

.method protected isAtStartOfLineComment()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->lineComment:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->lineComment:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Tokenizer;->canConsumeThisString(Ljava/lang/String;)Z

    move-result v0

    .line 269
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isAtStartOfNumber()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    new-array v3, v2, [C

    const/16 v4, 0x2d

    aput-char v4, v3, v1

    invoke-virtual {v0, v3}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected isAtStartOfSpecialId()Z
    .locals 2

    .prologue
    .line 201
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->specialIdStarters:Ljava/util/Set;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->getValue()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isIdentifierChar(Lparsii/tokenizer/Char;)Z
    .locals 4
    .param p1, "current"    # Lparsii/tokenizer/Char;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 450
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isLetter()Z

    move-result v2

    if-nez v2, :cond_0

    new-array v2, v1, [C

    const/16 v3, 0x5f

    aput-char v3, v2, v0

    invoke-virtual {p1, v2}, Lparsii/tokenizer/Char;->is([C)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public isKeywordsCaseSensitive()Z
    .locals 1

    .prologue
    .line 552
    iget-boolean v0, p0, Lparsii/tokenizer/Tokenizer;->keywordsCaseSensitive:Z

    return v0
.end method

.method protected isSymbolCharacter(Lparsii/tokenizer/Char;)Z
    .locals 5
    .param p1, "ch"    # Lparsii/tokenizer/Char;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 496
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isDigit()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isLetter()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->isWhitepace()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 509
    :cond_0
    :goto_0
    return v1

    .line 500
    :cond_1
    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v0

    .line 501
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v3

    if-nez v3, :cond_0

    .line 505
    invoke-virtual {p0, v2}, Lparsii/tokenizer/Tokenizer;->isAtBracket(Z)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfBlockComment()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfLineComment()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfNumber()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtStartOfIdentifier()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lparsii/tokenizer/Tokenizer;->stringDelimiters:Ljava/util/Map;

    invoke-virtual {p1}, Lparsii/tokenizer/Char;->getValue()C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 509
    goto :goto_0
.end method

.method public more()Z
    .locals 1

    .prologue
    .line 781
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    invoke-virtual {v0}, Lparsii/tokenizer/Token;->isNotEnd()Z

    move-result v0

    return v0
.end method

.method public setBlockCommentEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "blockCommentEnd"    # Ljava/lang/String;

    .prologue
    .line 759
    iput-object p1, p0, Lparsii/tokenizer/Tokenizer;->blockCommentEnd:Ljava/lang/String;

    .line 760
    return-void
.end method

.method public setBlockCommentStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "blockCommentStart"    # Ljava/lang/String;

    .prologue
    .line 738
    iput-object p1, p0, Lparsii/tokenizer/Tokenizer;->blockCommentStart:Ljava/lang/String;

    .line 739
    return-void
.end method

.method public setDecimalSeparator(C)V
    .locals 0
    .param p1, "decimalSeparator"    # C

    .prologue
    .line 650
    iput-char p1, p0, Lparsii/tokenizer/Tokenizer;->decimalSeparator:C

    .line 651
    return-void
.end method

.method public setEffectiveDecimalSeparator(C)V
    .locals 0
    .param p1, "effectiveDecimalSeparator"    # C

    .prologue
    .line 675
    iput-char p1, p0, Lparsii/tokenizer/Tokenizer;->effectiveDecimalSeparator:C

    .line 676
    return-void
.end method

.method public setGroupingSeparator(C)V
    .locals 0
    .param p1, "groupingSeparator"    # C

    .prologue
    .line 696
    iput-char p1, p0, Lparsii/tokenizer/Tokenizer;->groupingSeparator:C

    .line 697
    return-void
.end method

.method public setKeywordsCaseSensitive(Z)V
    .locals 0
    .param p1, "keywordsCaseSensitive"    # Z

    .prologue
    .line 565
    iput-boolean p1, p0, Lparsii/tokenizer/Tokenizer;->keywordsCaseSensitive:Z

    .line 566
    return-void
.end method

.method public setLineComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "lineComment"    # Ljava/lang/String;

    .prologue
    .line 717
    iput-object p1, p0, Lparsii/tokenizer/Tokenizer;->lineComment:Ljava/lang/String;

    .line 718
    return-void
.end method

.method public setProblemCollector(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "problemCollector":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    invoke-super {p0, p1}, Lparsii/tokenizer/Lookahead;->setProblemCollector(Ljava/util/List;)V

    .line 121
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0, p1}, Lparsii/tokenizer/LookaheadReader;->setProblemCollector(Ljava/util/List;)V

    .line 122
    return-void
.end method

.method protected skipBlockComment()V
    .locals 3

    .prologue
    .line 310
    :goto_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->isAtEndOfBlockComment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    :goto_1
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    goto :goto_0

    .line 316
    :cond_1
    iget-object v1, p0, Lparsii/tokenizer/Tokenizer;->problemCollector:Ljava/util/List;

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Position;

    const-string v2, "Premature end of block comment"

    invoke-static {v0, v2}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected skipToEndOfLine()V
    .locals 1

    .prologue
    .line 277
    :goto_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Char;

    invoke-virtual {v0}, Lparsii/tokenizer/Char;->isNewLine()Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->input:Lparsii/tokenizer/LookaheadReader;

    invoke-virtual {v0}, Lparsii/tokenizer/LookaheadReader;->consume()Ljava/lang/Object;

    goto :goto_0

    .line 280
    :cond_0
    return-void
.end method

.method public throwOnError()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 864
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/ParseError;

    .line 865
    .local v0, "e":Lparsii/tokenizer/ParseError;
    invoke-virtual {v0}, Lparsii/tokenizer/ParseError;->getSeverity()Lparsii/tokenizer/ParseError$Severity;

    move-result-object v2

    sget-object v3, Lparsii/tokenizer/ParseError$Severity;->ERROR:Lparsii/tokenizer/ParseError$Severity;

    if-ne v2, v3, :cond_0

    .line 866
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lparsii/tokenizer/ParseException;->create(Ljava/util/List;)Lparsii/tokenizer/ParseException;

    move-result-object v2

    throw v2

    .line 869
    .end local v0    # "e":Lparsii/tokenizer/ParseError;
    :cond_1
    return-void
.end method

.method public throwOnErrorOrWarning()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 850
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 851
    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->getProblemCollector()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lparsii/tokenizer/ParseException;->create(Ljava/util/List;)Lparsii/tokenizer/ParseException;

    move-result-object v0

    throw v0

    .line 853
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->itemBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 767
    const-string v0, "No Token fetched..."

    .line 772
    :goto_0
    return-object v0

    .line 769
    :cond_0
    iget-object v0, p0, Lparsii/tokenizer/Tokenizer;->itemBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 770
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 772
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    invoke-virtual {v0}, Lparsii/tokenizer/Token;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Next: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lparsii/tokenizer/Tokenizer;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    invoke-virtual {v0}, Lparsii/tokenizer/Token;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
