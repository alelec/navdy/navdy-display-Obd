.class public abstract Lparsii/tokenizer/Lookahead;
.super Ljava/lang/Object;
.source "Lookahead.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected endOfInputIndicator:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected endReached:Z

.field protected itemBuffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected problemCollector:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lparsii/tokenizer/Lookahead;->endReached:Z

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lparsii/tokenizer/Lookahead;->problemCollector:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public consume()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    invoke-virtual {p0}, Lparsii/tokenizer/Lookahead;->current()Ljava/lang/Object;

    move-result-object v0

    .line 135
    .local v0, "result":Ljava/lang/Object;, "TT;"
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lparsii/tokenizer/Lookahead;->consume(I)V

    .line 136
    return-object v0
.end method

.method public consume(I)V
    .locals 4
    .param p1, "numberOfItems"    # I

    .prologue
    .line 148
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    if-gez p1, :cond_4

    .line 149
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "numberOfItems < 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 151
    .end local p1    # "numberOfItems":I
    .local v1, "numberOfItems":I
    :goto_0
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "numberOfItems":I
    .restart local p1    # "numberOfItems":I
    if-lez v1, :cond_1

    .line 152
    iget-object v2, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    iget-object v2, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v1, p1

    .end local p1    # "numberOfItems":I
    .restart local v1    # "numberOfItems":I
    goto :goto_0

    .line 155
    .end local v1    # "numberOfItems":I
    .restart local p1    # "numberOfItems":I
    :cond_0
    iget-boolean v2, p0, Lparsii/tokenizer/Lookahead;->endReached:Z

    if-eqz v2, :cond_2

    .line 165
    :cond_1
    return-void

    .line 158
    :cond_2
    invoke-virtual {p0}, Lparsii/tokenizer/Lookahead;->fetch()Ljava/lang/Object;

    move-result-object v0

    .line 159
    .local v0, "item":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_3

    .line 160
    const/4 v2, 0x1

    iput-boolean v2, p0, Lparsii/tokenizer/Lookahead;->endReached:Z

    :cond_3
    move v1, p1

    .line 162
    .end local p1    # "numberOfItems":I
    .restart local v1    # "numberOfItems":I
    goto :goto_0

    .end local v0    # "item":Ljava/lang/Object;, "TT;"
    .end local v1    # "numberOfItems":I
    .restart local p1    # "numberOfItems":I
    :cond_4
    move v1, p1

    .end local p1    # "numberOfItems":I
    .restart local v1    # "numberOfItems":I
    goto :goto_0
.end method

.method public current()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Lookahead;->next(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract endOfInput()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract fetch()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getProblemCollector()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    iget-object v0, p0, Lparsii/tokenizer/Lookahead;->problemCollector:Ljava/util/List;

    return-object v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lparsii/tokenizer/Lookahead;->next(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public next(I)Ljava/lang/Object;
    .locals 3
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    if-gez p1, :cond_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "offset < 0"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_0
    :goto_0
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_2

    iget-boolean v1, p0, Lparsii/tokenizer/Lookahead;->endReached:Z

    if-nez v1, :cond_2

    .line 92
    invoke-virtual {p0}, Lparsii/tokenizer/Lookahead;->fetch()Ljava/lang/Object;

    move-result-object v0

    .line 93
    .local v0, "item":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_1

    .line 94
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lparsii/tokenizer/Lookahead;->endReached:Z

    goto :goto_0

    .line 99
    .end local v0    # "item":Ljava/lang/Object;, "TT;"
    :cond_2
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_4

    .line 100
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->endOfInputIndicator:Ljava/lang/Object;

    if-nez v1, :cond_3

    .line 101
    invoke-virtual {p0}, Lparsii/tokenizer/Lookahead;->endOfInput()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lparsii/tokenizer/Lookahead;->endOfInputIndicator:Ljava/lang/Object;

    .line 103
    :cond_3
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->endOfInputIndicator:Ljava/lang/Object;

    .line 105
    :goto_1
    return-object v1

    :cond_4
    iget-object v1, p0, Lparsii/tokenizer/Lookahead;->itemBuffer:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1
.end method

.method public setProblemCollector(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p0, "this":Lparsii/tokenizer/Lookahead;, "Lparsii/tokenizer/Lookahead<TT;>;"
    .local p1, "problemCollector":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    iput-object p1, p0, Lparsii/tokenizer/Lookahead;->problemCollector:Ljava/util/List;

    .line 184
    return-void
.end method
