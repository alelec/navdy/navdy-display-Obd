.class public final enum Lparsii/tokenizer/ParseError$Severity;
.super Ljava/lang/Enum;
.source "ParseError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lparsii/tokenizer/ParseError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Severity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lparsii/tokenizer/ParseError$Severity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lparsii/tokenizer/ParseError$Severity;

.field public static final enum ERROR:Lparsii/tokenizer/ParseError$Severity;

.field public static final enum WARNING:Lparsii/tokenizer/ParseError$Severity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lparsii/tokenizer/ParseError$Severity;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v2}, Lparsii/tokenizer/ParseError$Severity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/ParseError$Severity;->WARNING:Lparsii/tokenizer/ParseError$Severity;

    new-instance v0, Lparsii/tokenizer/ParseError$Severity;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lparsii/tokenizer/ParseError$Severity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/ParseError$Severity;->ERROR:Lparsii/tokenizer/ParseError$Severity;

    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Lparsii/tokenizer/ParseError$Severity;

    sget-object v1, Lparsii/tokenizer/ParseError$Severity;->WARNING:Lparsii/tokenizer/ParseError$Severity;

    aput-object v1, v0, v2

    sget-object v1, Lparsii/tokenizer/ParseError$Severity;->ERROR:Lparsii/tokenizer/ParseError$Severity;

    aput-object v1, v0, v3

    sput-object v0, Lparsii/tokenizer/ParseError$Severity;->$VALUES:[Lparsii/tokenizer/ParseError$Severity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lparsii/tokenizer/ParseError$Severity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lparsii/tokenizer/ParseError$Severity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/ParseError$Severity;

    return-object v0
.end method

.method public static values()[Lparsii/tokenizer/ParseError$Severity;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lparsii/tokenizer/ParseError$Severity;->$VALUES:[Lparsii/tokenizer/ParseError$Severity;

    invoke-virtual {v0}, [Lparsii/tokenizer/ParseError$Severity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lparsii/tokenizer/ParseError$Severity;

    return-object v0
.end method
