.class public final enum Lparsii/tokenizer/Token$TokenType;
.super Ljava/lang/Enum;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lparsii/tokenizer/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lparsii/tokenizer/Token$TokenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lparsii/tokenizer/Token$TokenType;

.field public static final enum DECIMAL:Lparsii/tokenizer/Token$TokenType;

.field public static final enum EOI:Lparsii/tokenizer/Token$TokenType;

.field public static final enum ID:Lparsii/tokenizer/Token$TokenType;

.field public static final enum INTEGER:Lparsii/tokenizer/Token$TokenType;

.field public static final enum KEYWORD:Lparsii/tokenizer/Token$TokenType;

.field public static final enum SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

.field public static final enum STRING:Lparsii/tokenizer/Token$TokenType;

.field public static final enum SYMBOL:Lparsii/tokenizer/Token$TokenType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "ID"

    invoke-direct {v0, v1, v3}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "SPECIAL_ID"

    invoke-direct {v0, v1, v4}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v5}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->STRING:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "DECIMAL"

    invoke-direct {v0, v1, v6}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->DECIMAL:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "INTEGER"

    invoke-direct {v0, v1, v7}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->INTEGER:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "SYMBOL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "KEYWORD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    new-instance v0, Lparsii/tokenizer/Token$TokenType;

    const-string v1, "EOI"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lparsii/tokenizer/Token$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->EOI:Lparsii/tokenizer/Token$TokenType;

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Lparsii/tokenizer/Token$TokenType;

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    aput-object v1, v0, v3

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->SPECIAL_ID:Lparsii/tokenizer/Token$TokenType;

    aput-object v1, v0, v4

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->STRING:Lparsii/tokenizer/Token$TokenType;

    aput-object v1, v0, v5

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->DECIMAL:Lparsii/tokenizer/Token$TokenType;

    aput-object v1, v0, v6

    sget-object v1, Lparsii/tokenizer/Token$TokenType;->INTEGER:Lparsii/tokenizer/Token$TokenType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lparsii/tokenizer/Token$TokenType;->KEYWORD:Lparsii/tokenizer/Token$TokenType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lparsii/tokenizer/Token$TokenType;->EOI:Lparsii/tokenizer/Token$TokenType;

    aput-object v2, v0, v1

    sput-object v0, Lparsii/tokenizer/Token$TokenType;->$VALUES:[Lparsii/tokenizer/Token$TokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lparsii/tokenizer/Token$TokenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lparsii/tokenizer/Token$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token$TokenType;

    return-object v0
.end method

.method public static values()[Lparsii/tokenizer/Token$TokenType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lparsii/tokenizer/Token$TokenType;->$VALUES:[Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {v0}, [Lparsii/tokenizer/Token$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lparsii/tokenizer/Token$TokenType;

    return-object v0
.end method
