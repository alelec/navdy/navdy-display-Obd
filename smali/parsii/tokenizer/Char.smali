.class public Lparsii/tokenizer/Char;
.super Ljava/lang/Object;
.source "Char.java"

# interfaces
.implements Lparsii/tokenizer/Position;


# instance fields
.field private line:I

.field private pos:I

.field private value:C


# direct methods
.method constructor <init>(CII)V
    .locals 0
    .param p1, "value"    # C
    .param p2, "line"    # I
    .param p3, "pos"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-char p1, p0, Lparsii/tokenizer/Char;->value:C

    .line 29
    iput p2, p0, Lparsii/tokenizer/Char;->line:I

    .line 30
    iput p3, p0, Lparsii/tokenizer/Char;->pos:I

    .line 31
    return-void
.end method


# virtual methods
.method public getLine()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lparsii/tokenizer/Char;->line:I

    return v0
.end method

.method public getPos()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lparsii/tokenizer/Char;->pos:I

    return v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const-string v0, ""

    .line 132
    :goto_0
    return-object v0

    :cond_0
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue()C
    .locals 1

    .prologue
    .line 39
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    return v0
.end method

.method public varargs is([C)Z
    .locals 5
    .param p1, "tests"    # [C

    .prologue
    .line 115
    move-object v0, p1

    .local v0, "arr$":[C
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-char v3, v0, v1

    .line 116
    .local v3, "test":C
    iget-char v4, p0, Lparsii/tokenizer/Char;->value:C

    if-ne v3, v4, :cond_0

    if-eqz v3, :cond_0

    .line 117
    const/4 v4, 0x1

    .line 120
    .end local v3    # "test":C
    :goto_1
    return v4

    .line 115
    .restart local v3    # "test":C
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    .end local v3    # "test":C
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public isDigit()Z
    .locals 1

    .prologue
    .line 58
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    return v0
.end method

.method public isEndOfInput()Z
    .locals 1

    .prologue
    .line 96
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLetter()Z
    .locals 1

    .prologue
    .line 67
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    return v0
.end method

.method public isNewLine()Z
    .locals 2

    .prologue
    .line 86
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWhitepace()Z
    .locals 1

    .prologue
    .line 77
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lparsii/tokenizer/Char;->isEndOfInput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "<End Of Input>"

    .line 104
    :goto_0
    return-object v0

    :cond_0
    iget-char v0, p0, Lparsii/tokenizer/Char;->value:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
