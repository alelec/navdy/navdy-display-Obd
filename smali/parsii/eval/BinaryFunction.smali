.class public abstract Lparsii/eval/BinaryFunction;
.super Ljava/lang/Object;
.source "BinaryFunction.java"

# interfaces
.implements Lparsii/eval/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract eval(DD)D
.end method

.method public eval(Ljava/util/List;)D
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/eval/Expression;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "args":Ljava/util/List;, "Ljava/util/List<Lparsii/eval/Expression;>;"
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lparsii/eval/Expression;

    invoke-virtual {v4}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    .line 30
    .local v0, "a":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    .end local v0    # "a":D
    :goto_0
    return-wide v0

    .line 33
    .restart local v0    # "a":D
    :cond_0
    const/4 v4, 0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lparsii/eval/Expression;

    invoke-virtual {v4}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v2

    .line 34
    .local v2, "b":D
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_1

    move-wide v0, v2

    .line 35
    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {p0, v0, v1, v2, v3}, Lparsii/eval/BinaryFunction;->eval(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getNumberOfArguments()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x2

    return v0
.end method

.method public isNaturalFunction()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method
