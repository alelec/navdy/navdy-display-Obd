.class public Lparsii/eval/Constant;
.super Lparsii/eval/Expression;
.source "Constant.java"


# static fields
.field public static final EMPTY:Lparsii/eval/Constant;


# instance fields
.field private value:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lparsii/eval/Constant;

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-direct {v0, v2, v3}, Lparsii/eval/Constant;-><init>(D)V

    sput-object v0, Lparsii/eval/Constant;->EMPTY:Lparsii/eval/Constant;

    return-void
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 25
    invoke-direct {p0}, Lparsii/eval/Expression;-><init>()V

    .line 26
    iput-wide p1, p0, Lparsii/eval/Constant;->value:D

    .line 27
    return-void
.end method


# virtual methods
.method public evaluate()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lparsii/eval/Constant;->value:D

    return-wide v0
.end method

.method public isConstant()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lparsii/eval/Constant;->value:D

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
