.class public Lparsii/eval/BinaryOperation;
.super Lparsii/eval/Expression;
.source "BinaryOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lparsii/eval/BinaryOperation$Op;
    }
.end annotation


# static fields
.field public static final EPSILON:D = 1.0E-10


# instance fields
.field private left:Lparsii/eval/Expression;

.field private final op:Lparsii/eval/BinaryOperation$Op;

.field private right:Lparsii/eval/Expression;

.field private sealed:Z


# direct methods
.method public constructor <init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V
    .locals 1
    .param p1, "op"    # Lparsii/eval/BinaryOperation$Op;
    .param p2, "left"    # Lparsii/eval/Expression;
    .param p3, "right"    # Lparsii/eval/Expression;

    .prologue
    .line 52
    invoke-direct {p0}, Lparsii/eval/Expression;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lparsii/eval/BinaryOperation;->sealed:Z

    .line 53
    iput-object p1, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    .line 54
    iput-object p2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    .line 55
    iput-object p3, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    .line 56
    return-void
.end method


# virtual methods
.method public evaluate()D
    .locals 12

    .prologue
    const-wide v10, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    const-wide/16 v6, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 120
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v8}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    .line 121
    .local v0, "a":D
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-virtual {v8}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v2

    .line 122
    .local v2, "b":D
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_1

    .line 123
    add-double v4, v0, v2

    .line 149
    :cond_0
    :goto_0
    return-wide v4

    .line 124
    :cond_1
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->SUBTRACT:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_2

    .line 125
    sub-double v4, v0, v2

    goto :goto_0

    .line 126
    :cond_2
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_3

    .line 127
    mul-double v4, v0, v2

    goto :goto_0

    .line 128
    :cond_3
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->DIVIDE:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_4

    .line 129
    div-double v4, v0, v2

    goto :goto_0

    .line 130
    :cond_4
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->POWER:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_5

    .line 131
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    goto :goto_0

    .line 132
    :cond_5
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->MODULO:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_6

    .line 133
    rem-double v4, v0, v2

    goto :goto_0

    .line 134
    :cond_6
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->LT:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_7

    .line 135
    cmpg-double v8, v0, v2

    if-ltz v8, :cond_0

    move-wide v4, v6

    goto :goto_0

    .line 136
    :cond_7
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->LT_EQ:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_a

    .line 137
    cmpg-double v8, v0, v2

    if-ltz v8, :cond_8

    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    cmpg-double v8, v8, v10

    if-gez v8, :cond_9

    :cond_8
    move-wide v6, v4

    :cond_9
    move-wide v4, v6

    goto :goto_0

    .line 138
    :cond_a
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->GT:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_b

    .line 139
    cmpl-double v8, v0, v2

    if-gtz v8, :cond_0

    move-wide v4, v6

    goto :goto_0

    .line 140
    :cond_b
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->GT_EQ:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_e

    .line 141
    cmpl-double v8, v0, v2

    if-gtz v8, :cond_c

    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    cmpg-double v8, v8, v10

    if-gez v8, :cond_d

    :cond_c
    move-wide v6, v4

    :cond_d
    move-wide v4, v6

    goto :goto_0

    .line 142
    :cond_e
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->EQ:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_f

    .line 143
    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    cmpg-double v8, v8, v10

    if-ltz v8, :cond_0

    move-wide v4, v6

    goto/16 :goto_0

    .line 144
    :cond_f
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->NEQ:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_10

    .line 145
    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_0

    move-wide v4, v6

    goto/16 :goto_0

    .line 146
    :cond_10
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->AND:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_12

    .line 147
    cmpl-double v8, v0, v4

    if-nez v8, :cond_11

    cmpl-double v8, v2, v4

    if-eqz v8, :cond_0

    :cond_11
    move-wide v4, v6

    goto/16 :goto_0

    .line 148
    :cond_12
    iget-object v8, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v9, Lparsii/eval/BinaryOperation$Op;->OR:Lparsii/eval/BinaryOperation$Op;

    if-ne v8, v9, :cond_15

    .line 149
    cmpl-double v8, v0, v4

    if-eqz v8, :cond_13

    cmpl-double v8, v2, v4

    if-nez v8, :cond_14

    :cond_13
    move-wide v6, v4

    :cond_14
    move-wide v4, v6

    goto/16 :goto_0

    .line 152
    :cond_15
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    iget-object v5, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getLeft()Lparsii/eval/Expression;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    return-object v0
.end method

.method public getOp()Lparsii/eval/BinaryOperation$Op;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    return-object v0
.end method

.method public getRight()Lparsii/eval/Expression;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    return-object v0
.end method

.method public isSealed()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lparsii/eval/BinaryOperation;->sealed:Z

    return v0
.end method

.method public seal()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lparsii/eval/BinaryOperation;->sealed:Z

    .line 105
    return-void
.end method

.method public setLeft(Lparsii/eval/Expression;)V
    .locals 0
    .param p1, "left"    # Lparsii/eval/Expression;

    .prologue
    .line 82
    iput-object p1, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    .line 83
    return-void
.end method

.method public simplify()Lparsii/eval/Expression;
    .locals 10

    .prologue
    .line 157
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->simplify()Lparsii/eval/Expression;

    move-result-object v2

    iput-object v2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    .line 158
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->simplify()Lparsii/eval/Expression;

    move-result-object v2

    iput-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    .line 160
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    new-instance v2, Lparsii/eval/Constant;

    invoke-virtual {p0}, Lparsii/eval/BinaryOperation;->evaluate()D

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lparsii/eval/Constant;-><init>(D)V

    .line 201
    :goto_0
    return-object v2

    .line 164
    :cond_0
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v3, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v3, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    if-ne v2, v3, :cond_5

    .line 167
    :cond_1
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    iget-object v1, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    .line 169
    .local v1, "tmp":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    iput-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    .line 170
    iput-object v1, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    .line 173
    .end local v1    # "tmp":Lparsii/eval/Expression;
    :cond_2
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    instance-of v2, v2, Lparsii/eval/BinaryOperation;

    if-eqz v2, :cond_5

    .line 174
    iget-object v0, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    check-cast v0, Lparsii/eval/BinaryOperation;

    .line 175
    .local v0, "childOp":Lparsii/eval/BinaryOperation;
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    iget-object v3, v0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    if-ne v2, v3, :cond_5

    .line 177
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 180
    iget-object v2, v0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 181
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v3, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    if-ne v2, v3, :cond_3

    .line 182
    new-instance v2, Lparsii/eval/BinaryOperation;

    iget-object v3, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    new-instance v4, Lparsii/eval/Constant;

    iget-object v5, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v5}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v6

    iget-object v5, v0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v5}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v8

    add-double/2addr v6, v8

    invoke-direct {v4, v6, v7}, Lparsii/eval/Constant;-><init>(D)V

    iget-object v5, v0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-direct {v2, v3, v4, v5}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v2, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    sget-object v3, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    if-ne v2, v3, :cond_5

    .line 187
    new-instance v2, Lparsii/eval/BinaryOperation;

    iget-object v3, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    new-instance v4, Lparsii/eval/Constant;

    iget-object v5, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v5}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v6

    iget-object v5, v0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v5}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-direct {v4, v6, v7}, Lparsii/eval/Constant;-><init>(D)V

    iget-object v5, v0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-direct {v2, v3, v4, v5}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    goto :goto_0

    .line 192
    :cond_4
    iget-object v2, v0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 195
    new-instance v2, Lparsii/eval/BinaryOperation;

    iget-object v3, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    iget-object v4, v0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    new-instance v5, Lparsii/eval/BinaryOperation;

    iget-object v6, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    iget-object v7, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    iget-object v8, v0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-direct {v5, v6, v7, v8}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    invoke-direct {v2, v3, v4, v5}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    goto/16 :goto_0

    .line 201
    .end local v0    # "childOp":Lparsii/eval/BinaryOperation;
    :cond_5
    invoke-super {p0}, Lparsii/eval/Expression;->simplify()Lparsii/eval/Expression;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lparsii/eval/BinaryOperation;->left:Lparsii/eval/Expression;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lparsii/eval/BinaryOperation;->op:Lparsii/eval/BinaryOperation$Op;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lparsii/eval/BinaryOperation;->right:Lparsii/eval/Expression;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
