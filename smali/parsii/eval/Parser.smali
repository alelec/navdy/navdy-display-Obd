.class public Lparsii/eval/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# static fields
.field private static functionTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lparsii/eval/Function;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lparsii/tokenizer/ParseError;",
            ">;"
        }
    .end annotation
.end field

.field private final scope:Lparsii/eval/Scope;

.field private tokenizer:Lparsii/tokenizer/Tokenizer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lparsii/eval/Parser;->functionTable:Ljava/util/Map;

    .line 75
    const-string v0, "sin"

    sget-object v1, Lparsii/eval/Functions;->SIN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 76
    const-string v0, "cos"

    sget-object v1, Lparsii/eval/Functions;->COS:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 77
    const-string v0, "tan"

    sget-object v1, Lparsii/eval/Functions;->TAN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 78
    const-string v0, "sinh"

    sget-object v1, Lparsii/eval/Functions;->SINH:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 79
    const-string v0, "cosh"

    sget-object v1, Lparsii/eval/Functions;->COSH:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 80
    const-string v0, "tanh"

    sget-object v1, Lparsii/eval/Functions;->TANH:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 81
    const-string v0, "asin"

    sget-object v1, Lparsii/eval/Functions;->ASIN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 82
    const-string v0, "acos"

    sget-object v1, Lparsii/eval/Functions;->ACOS:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 83
    const-string v0, "atan"

    sget-object v1, Lparsii/eval/Functions;->ATAN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 84
    const-string v0, "atan2"

    sget-object v1, Lparsii/eval/Functions;->ATAN2:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 85
    const-string v0, "deg"

    sget-object v1, Lparsii/eval/Functions;->DEG:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 86
    const-string v0, "rad"

    sget-object v1, Lparsii/eval/Functions;->RAD:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 87
    const-string v0, "abs"

    sget-object v1, Lparsii/eval/Functions;->ABS:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 88
    const-string v0, "round"

    sget-object v1, Lparsii/eval/Functions;->ROUND:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 89
    const-string v0, "ceil"

    sget-object v1, Lparsii/eval/Functions;->CEIL:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 90
    const-string v0, "floor"

    sget-object v1, Lparsii/eval/Functions;->FLOOR:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 91
    const-string v0, "exp"

    sget-object v1, Lparsii/eval/Functions;->EXP:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 92
    const-string v0, "ln"

    sget-object v1, Lparsii/eval/Functions;->LN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 93
    const-string v0, "log"

    sget-object v1, Lparsii/eval/Functions;->LOG:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 94
    const-string v0, "sqrt"

    sget-object v1, Lparsii/eval/Functions;->SQRT:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 95
    const-string v0, "min"

    sget-object v1, Lparsii/eval/Functions;->MIN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 96
    const-string v0, "max"

    sget-object v1, Lparsii/eval/Functions;->MAX:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 97
    const-string v0, "rnd"

    sget-object v1, Lparsii/eval/Functions;->RND:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 98
    const-string v0, "sign"

    sget-object v1, Lparsii/eval/Functions;->SIGN:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 99
    const-string v0, "if"

    sget-object v1, Lparsii/eval/Functions;->IF:Lparsii/eval/Function;

    invoke-static {v0, v1}, Lparsii/eval/Parser;->registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V

    .line 100
    return-void
.end method

.method private constructor <init>(Ljava/io/Reader;Lparsii/eval/Scope;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "scope"    # Lparsii/eval/Scope;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    .line 151
    iput-object p2, p0, Lparsii/eval/Parser;->scope:Lparsii/eval/Scope;

    .line 152
    new-instance v0, Lparsii/tokenizer/Tokenizer;

    invoke-direct {v0, p1}, Lparsii/tokenizer/Tokenizer;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    .line 153
    iget-object v0, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    iget-object v1, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Lparsii/tokenizer/Tokenizer;->setProblemCollector(Ljava/util/List;)V

    .line 154
    return-void
.end method

.method public static parse(Ljava/io/Reader;)Lparsii/eval/Expression;
    .locals 2
    .param p0, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lparsii/eval/Parser;

    invoke-static {}, Lparsii/eval/Scope;->create()Lparsii/eval/Scope;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lparsii/eval/Parser;-><init>(Ljava/io/Reader;Lparsii/eval/Scope;)V

    invoke-virtual {v0}, Lparsii/eval/Parser;->parse()Lparsii/eval/Expression;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/io/Reader;Lparsii/eval/Scope;)Lparsii/eval/Expression;
    .locals 1
    .param p0, "input"    # Ljava/io/Reader;
    .param p1, "scope"    # Lparsii/eval/Scope;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, Lparsii/eval/Parser;

    invoke-direct {v0, p0, p1}, Lparsii/eval/Parser;-><init>(Ljava/io/Reader;Lparsii/eval/Scope;)V

    invoke-virtual {v0}, Lparsii/eval/Parser;->parse()Lparsii/eval/Expression;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lparsii/eval/Expression;
    .locals 3
    .param p0, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lparsii/eval/Parser;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lparsii/eval/Scope;->create()Lparsii/eval/Scope;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lparsii/eval/Parser;-><init>(Ljava/io/Reader;Lparsii/eval/Scope;)V

    invoke-virtual {v0}, Lparsii/eval/Parser;->parse()Lparsii/eval/Expression;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;Lparsii/eval/Scope;)Lparsii/eval/Expression;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "scope"    # Lparsii/eval/Scope;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lparsii/eval/Parser;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lparsii/eval/Parser;-><init>(Ljava/io/Reader;Lparsii/eval/Scope;)V

    invoke-virtual {v0}, Lparsii/eval/Parser;->parse()Lparsii/eval/Expression;

    move-result-object v0

    return-object v0
.end method

.method private reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;
    .locals 3
    .param p1, "left"    # Lparsii/eval/Expression;
    .param p2, "right"    # Lparsii/eval/Expression;
    .param p3, "op"    # Lparsii/eval/BinaryOperation$Op;

    .prologue
    .line 306
    instance-of v1, p2, Lparsii/eval/BinaryOperation;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 307
    check-cast v0, Lparsii/eval/BinaryOperation;

    .line 308
    .local v0, "rightOp":Lparsii/eval/BinaryOperation;
    invoke-virtual {v0}, Lparsii/eval/BinaryOperation;->isSealed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lparsii/eval/BinaryOperation;->getOp()Lparsii/eval/BinaryOperation$Op;

    move-result-object v1

    invoke-virtual {v1}, Lparsii/eval/BinaryOperation$Op;->getPriority()I

    move-result v1

    invoke-virtual {p3}, Lparsii/eval/BinaryOperation$Op;->getPriority()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 309
    invoke-direct {p0, v0, p1, p3}, Lparsii/eval/Parser;->replaceLeft(Lparsii/eval/BinaryOperation;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)V

    .line 313
    .end local v0    # "rightOp":Lparsii/eval/BinaryOperation;
    .end local p2    # "right":Lparsii/eval/Expression;
    :goto_0
    return-object p2

    .restart local p2    # "right":Lparsii/eval/Expression;
    :cond_0
    new-instance v1, Lparsii/eval/BinaryOperation;

    invoke-direct {v1, p3, p1, p2}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    move-object p2, v1

    goto :goto_0
.end method

.method public static registerFunction(Ljava/lang/String;Lparsii/eval/Function;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "function"    # Lparsii/eval/Function;

    .prologue
    .line 66
    sget-object v0, Lparsii/eval/Parser;->functionTable:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method private replaceLeft(Lparsii/eval/BinaryOperation;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)V
    .locals 3
    .param p1, "target"    # Lparsii/eval/BinaryOperation;
    .param p2, "newLeft"    # Lparsii/eval/Expression;
    .param p3, "op"    # Lparsii/eval/BinaryOperation$Op;

    .prologue
    .line 317
    invoke-virtual {p1}, Lparsii/eval/BinaryOperation;->getLeft()Lparsii/eval/Expression;

    move-result-object v1

    instance-of v1, v1, Lparsii/eval/BinaryOperation;

    if-eqz v1, :cond_0

    .line 318
    invoke-virtual {p1}, Lparsii/eval/BinaryOperation;->getLeft()Lparsii/eval/Expression;

    move-result-object v0

    check-cast v0, Lparsii/eval/BinaryOperation;

    .line 319
    .local v0, "leftOp":Lparsii/eval/BinaryOperation;
    invoke-virtual {v0}, Lparsii/eval/BinaryOperation;->isSealed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lparsii/eval/BinaryOperation;->getOp()Lparsii/eval/BinaryOperation$Op;

    move-result-object v1

    invoke-virtual {v1}, Lparsii/eval/BinaryOperation$Op;->getPriority()I

    move-result v1

    invoke-virtual {p3}, Lparsii/eval/BinaryOperation$Op;->getPriority()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 320
    invoke-direct {p0, v0, p2, p3}, Lparsii/eval/Parser;->replaceLeft(Lparsii/eval/BinaryOperation;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)V

    .line 325
    .end local v0    # "leftOp":Lparsii/eval/BinaryOperation;
    :goto_0
    return-void

    .line 324
    :cond_0
    new-instance v1, Lparsii/eval/BinaryOperation;

    invoke-virtual {p1}, Lparsii/eval/BinaryOperation;->getLeft()Lparsii/eval/Expression;

    move-result-object v2

    invoke-direct {v1, p3, p2, v2}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    invoke-virtual {p1, v1}, Lparsii/eval/BinaryOperation;->setLeft(Lparsii/eval/Expression;)V

    goto :goto_0
.end method


# virtual methods
.method protected atom()Lparsii/eval/Expression;
    .locals 11

    .prologue
    .line 356
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "-"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 357
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 358
    new-instance v2, Lparsii/eval/BinaryOperation;

    sget-object v6, Lparsii/eval/BinaryOperation$Op;->SUBTRACT:Lparsii/eval/BinaryOperation$Op;

    new-instance v7, Lparsii/eval/Constant;

    const-wide/16 v8, 0x0

    invoke-direct {v7, v8, v9}, Lparsii/eval/Constant;-><init>(D)V

    invoke-virtual {p0}, Lparsii/eval/Parser;->atom()Lparsii/eval/Expression;

    move-result-object v8

    invoke-direct {v2, v6, v7, v8}, Lparsii/eval/BinaryOperation;-><init>(Lparsii/eval/BinaryOperation$Op;Lparsii/eval/Expression;Lparsii/eval/Expression;)V

    .line 359
    .local v2, "result":Lparsii/eval/BinaryOperation;
    invoke-virtual {v2}, Lparsii/eval/BinaryOperation;->seal()V

    .line 445
    .end local v2    # "result":Lparsii/eval/BinaryOperation;
    :goto_0
    return-object v2

    .line 362
    :cond_0
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "("

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 363
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 364
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v2

    .line 365
    .local v2, "result":Lparsii/eval/Expression;
    instance-of v6, v2, Lparsii/eval/BinaryOperation;

    if-eqz v6, :cond_1

    move-object v6, v2

    .line 366
    check-cast v6, Lparsii/eval/BinaryOperation;

    invoke-virtual {v6}, Lparsii/eval/BinaryOperation;->seal()V

    .line 368
    :cond_1
    sget-object v6, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v7, ")"

    invoke-virtual {p0, v6, v7}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    .end local v2    # "result":Lparsii/eval/Expression;
    :cond_2
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "{"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 372
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 373
    new-instance v0, Lparsii/eval/FunctionCall;

    invoke-direct {v0}, Lparsii/eval/FunctionCall;-><init>()V

    .line 374
    .local v0, "call":Lparsii/eval/FunctionCall;
    sget-object v6, Lparsii/eval/Functions;->BIT_EXTRACT:Lparsii/eval/Function;

    invoke-virtual {v0, v6}, Lparsii/eval/FunctionCall;->setFunction(Lparsii/eval/Function;)V

    .line 375
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isIdentifier([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 376
    new-instance v7, Lparsii/eval/VariableReference;

    iget-object v8, p0, Lparsii/eval/Parser;->scope:Lparsii/eval/Scope;

    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lparsii/eval/Scope;->getVariable(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v6

    invoke-direct {v7, v6}, Lparsii/eval/VariableReference;-><init>(Lparsii/eval/Variable;)V

    invoke-virtual {v0, v7}, Lparsii/eval/FunctionCall;->addParameter(Lparsii/eval/Expression;)V

    .line 383
    :goto_1
    sget-object v6, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v7, ":"

    invoke-virtual {p0, v6, v7}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    .line 384
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->isNumber()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 385
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 386
    .local v4, "value":D
    new-instance v6, Lparsii/eval/Constant;

    invoke-direct {v6, v4, v5}, Lparsii/eval/Constant;-><init>(D)V

    invoke-virtual {v0, v6}, Lparsii/eval/FunctionCall;->addParameter(Lparsii/eval/Expression;)V

    .line 393
    .end local v4    # "value":D
    :goto_2
    sget-object v6, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v7, "}"

    invoke-virtual {p0, v6, v7}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    move-object v2, v0

    .line 394
    goto/16 :goto_0

    .line 378
    :cond_3
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    .line 379
    .local v3, "token":Lparsii/tokenizer/Token;
    iget-object v6, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v7, "Unexpected token: \'%s\'. Expected a valid identifier."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 388
    .end local v3    # "token":Lparsii/tokenizer/Token;
    :cond_4
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    .line 389
    .restart local v3    # "token":Lparsii/tokenizer/Token;
    iget-object v6, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v7, "Unexpected token: \'%s\'. Expected a bit offset (number)."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 396
    .end local v0    # "call":Lparsii/eval/FunctionCall;
    .end local v3    # "token":Lparsii/tokenizer/Token;
    :cond_5
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "|"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 397
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 398
    new-instance v0, Lparsii/eval/FunctionCall;

    invoke-direct {v0}, Lparsii/eval/FunctionCall;-><init>()V

    .line 399
    .restart local v0    # "call":Lparsii/eval/FunctionCall;
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v6

    invoke-virtual {v0, v6}, Lparsii/eval/FunctionCall;->addParameter(Lparsii/eval/Expression;)V

    .line 400
    sget-object v6, Lparsii/eval/Functions;->ABS:Lparsii/eval/Function;

    invoke-virtual {v0, v6}, Lparsii/eval/FunctionCall;->setFunction(Lparsii/eval/Function;)V

    .line 401
    sget-object v6, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v7, "|"

    invoke-virtual {p0, v6, v7}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    move-object v2, v0

    .line 402
    goto/16 :goto_0

    .line 404
    .end local v0    # "call":Lparsii/eval/FunctionCall;
    :cond_6
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isIdentifier([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 405
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "("

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 406
    invoke-virtual {p0}, Lparsii/eval/Parser;->functionCall()Lparsii/eval/Expression;

    move-result-object v2

    goto/16 :goto_0

    .line 408
    :cond_7
    new-instance v2, Lparsii/eval/VariableReference;

    iget-object v7, p0, Lparsii/eval/Parser;->scope:Lparsii/eval/Scope;

    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lparsii/eval/Scope;->getVariable(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v6

    invoke-direct {v2, v6}, Lparsii/eval/VariableReference;-><init>(Lparsii/eval/Variable;)V

    goto/16 :goto_0

    .line 410
    :cond_8
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->isNumber()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 411
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 412
    .restart local v4    # "value":D
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    sget-object v7, Lparsii/tokenizer/Token$TokenType;->ID:Lparsii/tokenizer/Token$TokenType;

    invoke-virtual {v6, v7}, Lparsii/tokenizer/Token;->is(Lparsii/tokenizer/Token$TokenType;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 413
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/tokenizer/Token;

    invoke-virtual {v6}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    .line 414
    .local v1, "quantifier":Ljava/lang/String;
    const-string v6, "n"

    if-ne v6, v1, :cond_a

    .line 415
    const-wide v6, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v4, v6

    .line 416
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 439
    .end local v1    # "quantifier":Ljava/lang/String;
    :cond_9
    :goto_3
    new-instance v2, Lparsii/eval/Constant;

    invoke-direct {v2, v4, v5}, Lparsii/eval/Constant;-><init>(D)V

    goto/16 :goto_0

    .line 417
    .restart local v1    # "quantifier":Ljava/lang/String;
    :cond_a
    const-string v6, "u"

    if-ne v6, v1, :cond_b

    .line 418
    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    .line 419
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    goto :goto_3

    .line 420
    :cond_b
    const-string v6, "m"

    if-ne v6, v1, :cond_c

    .line 421
    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    .line 422
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    goto :goto_3

    .line 423
    :cond_c
    const-string v6, "K"

    if-eq v6, v1, :cond_d

    const-string v6, "k"

    if-ne v6, v1, :cond_e

    .line 424
    :cond_d
    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v6

    .line 425
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    goto :goto_3

    .line 426
    :cond_e
    const-string v6, "M"

    if-ne v6, v1, :cond_f

    .line 427
    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v6

    .line 428
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    goto :goto_3

    .line 429
    :cond_f
    const-string v6, "G"

    if-ne v6, v1, :cond_10

    .line 430
    const-wide v6, 0x41cdcd6500000000L    # 1.0E9

    mul-double/2addr v4, v6

    .line 431
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    goto :goto_3

    .line 433
    :cond_10
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    .line 434
    .restart local v3    # "token":Lparsii/tokenizer/Token;
    iget-object v6, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v7, "Unexpected token: \'%s\'. Expected a valid quantifier."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 441
    .end local v1    # "quantifier":Ljava/lang/String;
    .end local v3    # "token":Lparsii/tokenizer/Token;
    .end local v4    # "value":D
    :cond_11
    iget-object v6, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v6}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    .line 442
    .restart local v3    # "token":Lparsii/tokenizer/Token;
    iget-object v6, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v7, "Unexpected token: \'%s\'. Expected an expression."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    sget-object v2, Lparsii/eval/Constant;->EMPTY:Lparsii/eval/Constant;

    goto/16 :goto_0
.end method

.method protected expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V
    .locals 6
    .param p1, "type"    # Lparsii/tokenizer/Token$TokenType;
    .param p2, "trigger"    # Ljava/lang/String;

    .prologue
    .line 495
    iget-object v0, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Token;

    invoke-virtual {v0, p1, p2}, Lparsii/tokenizer/Token;->matches(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v0}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 503
    :goto_0
    return-void

    .line 498
    :cond_0
    iget-object v2, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    iget-object v0, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v0}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/tokenizer/Position;

    const-string v3, "Unexpected token \'%s\'. Expected: \'%s\'"

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v1}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Token;

    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected expression()Lparsii/eval/Expression;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 186
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v0

    .line 187
    .local v0, "left":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "&&"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 189
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v1

    .line 190
    .local v1, "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->AND:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    .line 196
    .end local v0    # "left":Lparsii/eval/Expression;
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_0
    :goto_0
    return-object v0

    .line 191
    .restart local v0    # "left":Lparsii/eval/Expression;
    :cond_1
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "||"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 193
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v1

    .line 194
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->OR:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0
.end method

.method protected functionCall()Lparsii/eval/Expression;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 454
    new-instance v0, Lparsii/eval/FunctionCall;

    invoke-direct {v0}, Lparsii/eval/FunctionCall;-><init>()V

    .line 455
    .local v0, "call":Lparsii/eval/FunctionCall;
    iget-object v3, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v3}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    .line 456
    .local v2, "funToken":Lparsii/tokenizer/Token;
    sget-object v3, Lparsii/eval/Parser;->functionTable:Ljava/util/Map;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/eval/Function;

    .line 457
    .local v1, "fun":Lparsii/eval/Function;
    if-nez v1, :cond_0

    .line 458
    iget-object v3, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v4, "Unknown function: \'%s\'"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_0
    invoke-virtual {v0, v1}, Lparsii/eval/FunctionCall;->setFunction(Lparsii/eval/Function;)V

    .line 461
    iget-object v3, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v3}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 462
    :goto_0
    iget-object v3, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v3}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ")"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v3}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lparsii/tokenizer/Token;

    invoke-virtual {v3}, Lparsii/tokenizer/Token;->isNotEnd()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 463
    invoke-virtual {v0}, Lparsii/eval/FunctionCall;->getParameters()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 464
    sget-object v3, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v4, ","

    invoke-virtual {p0, v3, v4}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    .line 466
    :cond_1
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v3

    invoke-virtual {v0, v3}, Lparsii/eval/FunctionCall;->addParameter(Lparsii/eval/Expression;)V

    goto :goto_0

    .line 468
    :cond_2
    sget-object v3, Lparsii/tokenizer/Token$TokenType;->SYMBOL:Lparsii/tokenizer/Token$TokenType;

    const-string v4, ")"

    invoke-virtual {p0, v3, v4}, Lparsii/eval/Parser;->expect(Lparsii/tokenizer/Token$TokenType;Ljava/lang/String;)V

    .line 469
    if-nez v1, :cond_4

    .line 470
    sget-object v0, Lparsii/eval/Constant;->EMPTY:Lparsii/eval/Constant;

    .line 481
    .end local v0    # "call":Lparsii/eval/FunctionCall;
    :cond_3
    :goto_1
    return-object v0

    .line 472
    .restart local v0    # "call":Lparsii/eval/FunctionCall;
    :cond_4
    invoke-virtual {v0}, Lparsii/eval/FunctionCall;->getParameters()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1}, Lparsii/eval/Function;->getNumberOfArguments()I

    move-result v4

    if-eq v3, v4, :cond_3

    invoke-interface {v1}, Lparsii/eval/Function;->getNumberOfArguments()I

    move-result v3

    if-ltz v3, :cond_3

    .line 473
    iget-object v3, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v4, "Number of arguments for function \'%s\' do not match. Expected: %d, Found: %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-interface {v1}, Lparsii/eval/Function;->getNumberOfArguments()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    invoke-virtual {v0}, Lparsii/eval/FunctionCall;->getParameters()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    sget-object v0, Lparsii/eval/Constant;->EMPTY:Lparsii/eval/Constant;

    goto :goto_1
.end method

.method protected parse()Lparsii/eval/Expression;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 163
    invoke-virtual {p0}, Lparsii/eval/Parser;->expression()Lparsii/eval/Expression;

    move-result-object v2

    invoke-virtual {v2}, Lparsii/eval/Expression;->simplify()Lparsii/eval/Expression;

    move-result-object v0

    .line 164
    .local v0, "result":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->isNotEnd()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/tokenizer/Token;

    .line 166
    .local v1, "token":Lparsii/tokenizer/Token;
    iget-object v2, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    const-string v3, "Unexpected token: \'%s\'. Expected an expression."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lparsii/tokenizer/Token;->getSource()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    .end local v1    # "token":Lparsii/tokenizer/Token;
    :cond_0
    iget-object v2, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 171
    iget-object v2, p0, Lparsii/eval/Parser;->errors:Ljava/util/List;

    invoke-static {v2}, Lparsii/tokenizer/ParseException;->create(Ljava/util/List;)Lparsii/tokenizer/ParseException;

    move-result-object v2

    throw v2

    .line 173
    :cond_1
    return-object v0
.end method

.method protected power()Lparsii/eval/Expression;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 336
    invoke-virtual {p0}, Lparsii/eval/Parser;->atom()Lparsii/eval/Expression;

    move-result-object v0

    .line 337
    .local v0, "left":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "^"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "**"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 338
    :cond_0
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 339
    invoke-virtual {p0}, Lparsii/eval/Parser;->power()Lparsii/eval/Expression;

    move-result-object v1

    .line 340
    .local v1, "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->POWER:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    .line 342
    .end local v0    # "left":Lparsii/eval/Expression;
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_1
    return-object v0
.end method

.method protected product()Lparsii/eval/Expression;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 282
    invoke-virtual {p0}, Lparsii/eval/Parser;->power()Lparsii/eval/Expression;

    move-result-object v0

    .line 283
    .local v0, "left":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "*"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 284
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 285
    invoke-virtual {p0}, Lparsii/eval/Parser;->product()Lparsii/eval/Expression;

    move-result-object v1

    .line 286
    .local v1, "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    .line 298
    .end local v0    # "left":Lparsii/eval/Expression;
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_0
    :goto_0
    return-object v0

    .line 288
    .restart local v0    # "left":Lparsii/eval/Expression;
    :cond_1
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "/"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 290
    invoke-virtual {p0}, Lparsii/eval/Parser;->product()Lparsii/eval/Expression;

    move-result-object v1

    .line 291
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->DIVIDE:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0

    .line 293
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_2
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "%"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 295
    invoke-virtual {p0}, Lparsii/eval/Parser;->product()Lparsii/eval/Expression;

    move-result-object v1

    .line 296
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->MODULO:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0
.end method

.method protected relationalExpression()Lparsii/eval/Expression;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 209
    invoke-virtual {p0}, Lparsii/eval/Parser;->term()Lparsii/eval/Expression;

    move-result-object v0

    .line 210
    .local v0, "left":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "<"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 212
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 213
    .local v1, "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->LT:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    .line 240
    .end local v0    # "left":Lparsii/eval/Expression;
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_0
    :goto_0
    return-object v0

    .line 215
    .restart local v0    # "left":Lparsii/eval/Expression;
    :cond_1
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "<="

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 216
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 217
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 218
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->LT_EQ:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0

    .line 220
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_2
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "="

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 221
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 222
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 223
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->EQ:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0

    .line 225
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_3
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, ">="

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 226
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 227
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 228
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->GT_EQ:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0

    .line 230
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_4
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, ">"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 231
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 232
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 233
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->GT:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto/16 :goto_0

    .line 235
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_5
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "!="

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 237
    invoke-virtual {p0}, Lparsii/eval/Parser;->relationalExpression()Lparsii/eval/Expression;

    move-result-object v1

    .line 238
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->NEQ:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected term()Lparsii/eval/Expression;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 252
    invoke-virtual {p0}, Lparsii/eval/Parser;->product()Lparsii/eval/Expression;

    move-result-object v0

    .line 253
    .local v0, "left":Lparsii/eval/Expression;
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "+"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 255
    invoke-virtual {p0}, Lparsii/eval/Parser;->term()Lparsii/eval/Expression;

    move-result-object v1

    .line 256
    .local v1, "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    .line 270
    .end local v0    # "left":Lparsii/eval/Expression;
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_0
    :goto_0
    return-object v0

    .line 258
    .restart local v0    # "left":Lparsii/eval/Expression;
    :cond_1
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "-"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lparsii/tokenizer/Token;->isSymbol([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 259
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->consume()Ljava/lang/Object;

    .line 260
    invoke-virtual {p0}, Lparsii/eval/Parser;->term()Lparsii/eval/Expression;

    move-result-object v1

    .line 261
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->SUBTRACT:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0

    .line 263
    .end local v1    # "right":Lparsii/eval/Expression;
    :cond_2
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->isNumber()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 264
    iget-object v2, p0, Lparsii/eval/Parser;->tokenizer:Lparsii/tokenizer/Tokenizer;

    invoke-virtual {v2}, Lparsii/tokenizer/Tokenizer;->current()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/tokenizer/Token;

    invoke-virtual {v2}, Lparsii/tokenizer/Token;->getContents()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    invoke-virtual {p0}, Lparsii/eval/Parser;->term()Lparsii/eval/Expression;

    move-result-object v1

    .line 266
    .restart local v1    # "right":Lparsii/eval/Expression;
    sget-object v2, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    invoke-direct {p0, v0, v1, v2}, Lparsii/eval/Parser;->reOrder(Lparsii/eval/Expression;Lparsii/eval/Expression;Lparsii/eval/BinaryOperation$Op;)Lparsii/eval/Expression;

    move-result-object v0

    goto :goto_0
.end method
