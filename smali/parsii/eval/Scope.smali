.class public Lparsii/eval/Scope;
.super Ljava/lang/Object;
.source "Scope.java"


# static fields
.field private static root:Lparsii/eval/Scope;


# instance fields
.field private context:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lparsii/eval/Variable;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Lparsii/eval/Scope;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    .line 38
    return-void
.end method

.method public static create()Lparsii/eval/Scope;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lparsii/eval/Scope;

    invoke-direct {v0}, Lparsii/eval/Scope;-><init>()V

    .line 51
    .local v0, "result":Lparsii/eval/Scope;
    invoke-static {}, Lparsii/eval/Scope;->getRootScope()Lparsii/eval/Scope;

    move-result-object v1

    iput-object v1, v0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    .line 53
    return-object v0
.end method

.method public static createWithParent(Lparsii/eval/Scope;)Lparsii/eval/Scope;
    .locals 1
    .param p0, "parent"    # Lparsii/eval/Scope;

    .prologue
    .line 81
    invoke-static {}, Lparsii/eval/Scope;->create()Lparsii/eval/Scope;

    move-result-object v0

    .line 82
    .local v0, "result":Lparsii/eval/Scope;
    iput-object p0, v0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    .line 84
    return-object v0
.end method

.method private static getRootScope()Lparsii/eval/Scope;
    .locals 4

    .prologue
    .line 60
    sget-object v0, Lparsii/eval/Scope;->root:Lparsii/eval/Scope;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lparsii/eval/Scope;

    invoke-direct {v0}, Lparsii/eval/Scope;-><init>()V

    sput-object v0, Lparsii/eval/Scope;->root:Lparsii/eval/Scope;

    .line 62
    sget-object v0, Lparsii/eval/Scope;->root:Lparsii/eval/Scope;

    const-string v1, "pi"

    invoke-virtual {v0, v1}, Lparsii/eval/Scope;->getVariable(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    invoke-virtual {v0, v2, v3}, Lparsii/eval/Variable;->makeConstant(D)V

    .line 63
    sget-object v0, Lparsii/eval/Scope;->root:Lparsii/eval/Scope;

    const-string v1, "euler"

    invoke-virtual {v0, v1}, Lparsii/eval/Scope;->getVariable(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v0

    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    invoke-virtual {v0, v2, v3}, Lparsii/eval/Variable;->makeConstant(D)V

    .line 66
    :cond_0
    sget-object v0, Lparsii/eval/Scope;->root:Lparsii/eval/Scope;

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lparsii/eval/Variable;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v1, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lparsii/eval/Variable;

    .line 138
    :goto_0
    return-object v1

    .line 135
    :cond_0
    new-instance v0, Lparsii/eval/Variable;

    invoke-direct {v0, p1}, Lparsii/eval/Variable;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "result":Lparsii/eval/Variable;
    iget-object v1, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 138
    goto :goto_0
.end method

.method public find(Ljava/lang/String;)Lparsii/eval/Variable;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/eval/Variable;

    .line 103
    :goto_0
    return-object v0

    .line 100
    :cond_0
    iget-object v0, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    invoke-virtual {v0, p1}, Lparsii/eval/Scope;->find(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getLocalVariables()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lparsii/eval/Variable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lparsii/eval/Scope;->context:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getNames()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v1, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    if-nez v1, :cond_0

    .line 157
    invoke-virtual {p0}, Lparsii/eval/Scope;->getLocalNames()Ljava/util/Set;

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    .line 159
    :cond_0
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 160
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    invoke-virtual {v1}, Lparsii/eval/Scope;->getNames()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 161
    invoke-virtual {p0}, Lparsii/eval/Scope;->getLocalNames()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getVariable(Ljava/lang/String;)Lparsii/eval/Variable;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lparsii/eval/Scope;->find(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v0

    .line 115
    .local v0, "result":Lparsii/eval/Variable;
    if-eqz v0, :cond_0

    .line 118
    .end local v0    # "result":Lparsii/eval/Variable;
    :goto_0
    return-object v0

    .restart local v0    # "result":Lparsii/eval/Variable;
    :cond_0
    invoke-virtual {p0, p1}, Lparsii/eval/Scope;->create(Ljava/lang/String;)Lparsii/eval/Variable;

    move-result-object v0

    goto :goto_0
.end method

.method public getVariables()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lparsii/eval/Variable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v1, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    if-nez v1, :cond_0

    .line 181
    invoke-virtual {p0}, Lparsii/eval/Scope;->getLocalVariables()Ljava/util/Collection;

    move-result-object v0

    .line 186
    :goto_0
    return-object v0

    .line 183
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lparsii/eval/Variable;>;"
    iget-object v1, p0, Lparsii/eval/Scope;->parent:Lparsii/eval/Scope;

    invoke-virtual {v1}, Lparsii/eval/Scope;->getVariables()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 185
    invoke-virtual {p0}, Lparsii/eval/Scope;->getLocalVariables()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
