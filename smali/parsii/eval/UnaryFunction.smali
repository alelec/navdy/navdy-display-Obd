.class public abstract Lparsii/eval/UnaryFunction;
.super Ljava/lang/Object;
.source "UnaryFunction.java"

# interfaces
.implements Lparsii/eval/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract eval(D)D
.end method

.method public eval(Ljava/util/List;)D
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/eval/Expression;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "args":Ljava/util/List;, "Ljava/util/List<Lparsii/eval/Expression;>;"
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    .line 30
    .local v0, "a":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    .end local v0    # "a":D
    :goto_0
    return-wide v0

    .restart local v0    # "a":D
    :cond_0
    invoke-virtual {p0, v0, v1}, Lparsii/eval/UnaryFunction;->eval(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getNumberOfArguments()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    return v0
.end method

.method public isNaturalFunction()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method
