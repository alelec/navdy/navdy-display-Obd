.class public Lparsii/eval/Functions;
.super Ljava/lang/Object;
.source "Functions.java"


# static fields
.field public static final ABS:Lparsii/eval/Function;

.field public static final ACOS:Lparsii/eval/Function;

.field public static final ASIN:Lparsii/eval/Function;

.field public static final ATAN:Lparsii/eval/Function;

.field public static final ATAN2:Lparsii/eval/Function;

.field public static final BIT_EXTRACT:Lparsii/eval/Function;

.field public static final CEIL:Lparsii/eval/Function;

.field public static final COS:Lparsii/eval/Function;

.field public static final COSH:Lparsii/eval/Function;

.field public static final DEG:Lparsii/eval/Function;

.field public static final EXP:Lparsii/eval/Function;

.field public static final FLOOR:Lparsii/eval/Function;

.field public static final IF:Lparsii/eval/Function;

.field public static final LN:Lparsii/eval/Function;

.field public static final LOG:Lparsii/eval/Function;

.field public static final MAX:Lparsii/eval/Function;

.field public static final MIN:Lparsii/eval/Function;

.field public static final RAD:Lparsii/eval/Function;

.field public static final RND:Lparsii/eval/Function;

.field public static final ROUND:Lparsii/eval/Function;

.field public static final SIGN:Lparsii/eval/Function;

.field public static final SIN:Lparsii/eval/Function;

.field public static final SINH:Lparsii/eval/Function;

.field public static final SQRT:Lparsii/eval/Function;

.field public static final TAN:Lparsii/eval/Function;

.field public static final TANH:Lparsii/eval/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lparsii/eval/Functions$1;

    invoke-direct {v0}, Lparsii/eval/Functions$1;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->SIN:Lparsii/eval/Function;

    .line 37
    new-instance v0, Lparsii/eval/Functions$2;

    invoke-direct {v0}, Lparsii/eval/Functions$2;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->SINH:Lparsii/eval/Function;

    .line 47
    new-instance v0, Lparsii/eval/Functions$3;

    invoke-direct {v0}, Lparsii/eval/Functions$3;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->COS:Lparsii/eval/Function;

    .line 57
    new-instance v0, Lparsii/eval/Functions$4;

    invoke-direct {v0}, Lparsii/eval/Functions$4;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->COSH:Lparsii/eval/Function;

    .line 67
    new-instance v0, Lparsii/eval/Functions$5;

    invoke-direct {v0}, Lparsii/eval/Functions$5;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->TAN:Lparsii/eval/Function;

    .line 77
    new-instance v0, Lparsii/eval/Functions$6;

    invoke-direct {v0}, Lparsii/eval/Functions$6;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->TANH:Lparsii/eval/Function;

    .line 87
    new-instance v0, Lparsii/eval/Functions$7;

    invoke-direct {v0}, Lparsii/eval/Functions$7;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ABS:Lparsii/eval/Function;

    .line 97
    new-instance v0, Lparsii/eval/Functions$8;

    invoke-direct {v0}, Lparsii/eval/Functions$8;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ASIN:Lparsii/eval/Function;

    .line 107
    new-instance v0, Lparsii/eval/Functions$9;

    invoke-direct {v0}, Lparsii/eval/Functions$9;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ACOS:Lparsii/eval/Function;

    .line 117
    new-instance v0, Lparsii/eval/Functions$10;

    invoke-direct {v0}, Lparsii/eval/Functions$10;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ATAN:Lparsii/eval/Function;

    .line 127
    new-instance v0, Lparsii/eval/Functions$11;

    invoke-direct {v0}, Lparsii/eval/Functions$11;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ATAN2:Lparsii/eval/Function;

    .line 138
    new-instance v0, Lparsii/eval/Functions$12;

    invoke-direct {v0}, Lparsii/eval/Functions$12;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->ROUND:Lparsii/eval/Function;

    .line 148
    new-instance v0, Lparsii/eval/Functions$13;

    invoke-direct {v0}, Lparsii/eval/Functions$13;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->FLOOR:Lparsii/eval/Function;

    .line 158
    new-instance v0, Lparsii/eval/Functions$14;

    invoke-direct {v0}, Lparsii/eval/Functions$14;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->CEIL:Lparsii/eval/Function;

    .line 168
    new-instance v0, Lparsii/eval/Functions$15;

    invoke-direct {v0}, Lparsii/eval/Functions$15;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->SQRT:Lparsii/eval/Function;

    .line 178
    new-instance v0, Lparsii/eval/Functions$16;

    invoke-direct {v0}, Lparsii/eval/Functions$16;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->EXP:Lparsii/eval/Function;

    .line 188
    new-instance v0, Lparsii/eval/Functions$17;

    invoke-direct {v0}, Lparsii/eval/Functions$17;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->LN:Lparsii/eval/Function;

    .line 198
    new-instance v0, Lparsii/eval/Functions$18;

    invoke-direct {v0}, Lparsii/eval/Functions$18;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->LOG:Lparsii/eval/Function;

    .line 208
    new-instance v0, Lparsii/eval/Functions$19;

    invoke-direct {v0}, Lparsii/eval/Functions$19;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->MIN:Lparsii/eval/Function;

    .line 218
    new-instance v0, Lparsii/eval/Functions$20;

    invoke-direct {v0}, Lparsii/eval/Functions$20;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->MAX:Lparsii/eval/Function;

    .line 225
    new-instance v0, Lparsii/eval/Functions$21;

    invoke-direct {v0}, Lparsii/eval/Functions$21;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->BIT_EXTRACT:Lparsii/eval/Function;

    .line 235
    new-instance v0, Lparsii/eval/Functions$22;

    invoke-direct {v0}, Lparsii/eval/Functions$22;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->RND:Lparsii/eval/Function;

    .line 245
    new-instance v0, Lparsii/eval/Functions$23;

    invoke-direct {v0}, Lparsii/eval/Functions$23;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->SIGN:Lparsii/eval/Function;

    .line 255
    new-instance v0, Lparsii/eval/Functions$24;

    invoke-direct {v0}, Lparsii/eval/Functions$24;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->DEG:Lparsii/eval/Function;

    .line 265
    new-instance v0, Lparsii/eval/Functions$25;

    invoke-direct {v0}, Lparsii/eval/Functions$25;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->RAD:Lparsii/eval/Function;

    .line 279
    new-instance v0, Lparsii/eval/Functions$26;

    invoke-direct {v0}, Lparsii/eval/Functions$26;-><init>()V

    sput-object v0, Lparsii/eval/Functions;->IF:Lparsii/eval/Function;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
