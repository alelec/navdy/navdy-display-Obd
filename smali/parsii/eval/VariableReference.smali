.class public Lparsii/eval/VariableReference;
.super Lparsii/eval/Expression;
.source "VariableReference.java"


# instance fields
.field private var:Lparsii/eval/Variable;


# direct methods
.method public constructor <init>(Lparsii/eval/Variable;)V
    .locals 0
    .param p1, "var"    # Lparsii/eval/Variable;

    .prologue
    .line 26
    invoke-direct {p0}, Lparsii/eval/Expression;-><init>()V

    .line 27
    iput-object p1, p0, Lparsii/eval/VariableReference;->var:Lparsii/eval/Variable;

    .line 28
    return-void
.end method


# virtual methods
.method public evaluate()D
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lparsii/eval/VariableReference;->var:Lparsii/eval/Variable;

    invoke-virtual {v0}, Lparsii/eval/Variable;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public isConstant()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lparsii/eval/VariableReference;->var:Lparsii/eval/Variable;

    invoke-virtual {v0}, Lparsii/eval/Variable;->isConstant()Z

    move-result v0

    return v0
.end method

.method public simplify()Lparsii/eval/Expression;
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lparsii/eval/VariableReference;->isConstant()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Lparsii/eval/Constant;

    invoke-virtual {p0}, Lparsii/eval/VariableReference;->evaluate()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lparsii/eval/Constant;-><init>(D)V

    move-object p0, v0

    .line 50
    .end local p0    # "this":Lparsii/eval/VariableReference;
    :cond_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lparsii/eval/VariableReference;->var:Lparsii/eval/Variable;

    invoke-virtual {v0}, Lparsii/eval/Variable;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
