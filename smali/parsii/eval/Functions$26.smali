.class final Lparsii/eval/Functions$26;
.super Ljava/lang/Object;
.source "Functions.java"

# interfaces
.implements Lparsii/eval/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lparsii/eval/Functions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/List;)D
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lparsii/eval/Expression;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "args":Ljava/util/List;, "Ljava/util/List<Lparsii/eval/Expression;>;"
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    .line 288
    .local v0, "check":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    .end local v0    # "check":D
    :goto_0
    return-wide v0

    .line 291
    .restart local v0    # "check":D
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_1

    .line 292
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    goto :goto_0

    .line 294
    :cond_1
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lparsii/eval/Expression;

    invoke-virtual {v2}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getNumberOfArguments()I
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x3

    return v0
.end method

.method public isNaturalFunction()Z
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    return v0
.end method
