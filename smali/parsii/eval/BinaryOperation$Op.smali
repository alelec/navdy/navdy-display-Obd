.class public final enum Lparsii/eval/BinaryOperation$Op;
.super Ljava/lang/Enum;
.source "BinaryOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lparsii/eval/BinaryOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Op"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lparsii/eval/BinaryOperation$Op;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lparsii/eval/BinaryOperation$Op;

.field public static final enum ADD:Lparsii/eval/BinaryOperation$Op;

.field public static final enum AND:Lparsii/eval/BinaryOperation$Op;

.field public static final enum DIVIDE:Lparsii/eval/BinaryOperation$Op;

.field public static final enum EQ:Lparsii/eval/BinaryOperation$Op;

.field public static final enum GT:Lparsii/eval/BinaryOperation$Op;

.field public static final enum GT_EQ:Lparsii/eval/BinaryOperation$Op;

.field public static final enum LT:Lparsii/eval/BinaryOperation$Op;

.field public static final enum LT_EQ:Lparsii/eval/BinaryOperation$Op;

.field public static final enum MODULO:Lparsii/eval/BinaryOperation$Op;

.field public static final enum MULTIPLY:Lparsii/eval/BinaryOperation$Op;

.field public static final enum NEQ:Lparsii/eval/BinaryOperation$Op;

.field public static final enum OR:Lparsii/eval/BinaryOperation$Op;

.field public static final enum POWER:Lparsii/eval/BinaryOperation$Op;

.field public static final enum SUBTRACT:Lparsii/eval/BinaryOperation$Op;


# instance fields
.field private final priority:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x2

    .line 27
    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "ADD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v6}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "SUBTRACT"

    invoke-direct {v0, v1, v5, v6}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->SUBTRACT:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "MULTIPLY"

    invoke-direct {v0, v1, v3, v4}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "DIVIDE"

    invoke-direct {v0, v1, v6, v4}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->DIVIDE:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "MODULO"

    invoke-direct {v0, v1, v4, v4}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->MODULO:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "POWER"

    invoke-direct {v0, v1, v7, v7}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->POWER:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "LT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->LT:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "LT_EQ"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->LT_EQ:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "EQ"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->EQ:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "GT_EQ"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->GT_EQ:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "GT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->GT:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "NEQ"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->NEQ:Lparsii/eval/BinaryOperation$Op;

    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "AND"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->AND:Lparsii/eval/BinaryOperation$Op;

    .line 28
    new-instance v0, Lparsii/eval/BinaryOperation$Op;

    const-string v1, "OR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v5}, Lparsii/eval/BinaryOperation$Op;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->OR:Lparsii/eval/BinaryOperation$Op;

    .line 26
    const/16 v0, 0xe

    new-array v0, v0, [Lparsii/eval/BinaryOperation$Op;

    const/4 v1, 0x0

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->ADD:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    sget-object v1, Lparsii/eval/BinaryOperation$Op;->SUBTRACT:Lparsii/eval/BinaryOperation$Op;

    aput-object v1, v0, v5

    sget-object v1, Lparsii/eval/BinaryOperation$Op;->MULTIPLY:Lparsii/eval/BinaryOperation$Op;

    aput-object v1, v0, v3

    sget-object v1, Lparsii/eval/BinaryOperation$Op;->DIVIDE:Lparsii/eval/BinaryOperation$Op;

    aput-object v1, v0, v6

    sget-object v1, Lparsii/eval/BinaryOperation$Op;->MODULO:Lparsii/eval/BinaryOperation$Op;

    aput-object v1, v0, v4

    sget-object v1, Lparsii/eval/BinaryOperation$Op;->POWER:Lparsii/eval/BinaryOperation$Op;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->LT:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->LT_EQ:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->EQ:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->GT_EQ:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->GT:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->NEQ:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->AND:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lparsii/eval/BinaryOperation$Op;->OR:Lparsii/eval/BinaryOperation$Op;

    aput-object v2, v0, v1

    sput-object v0, Lparsii/eval/BinaryOperation$Op;->$VALUES:[Lparsii/eval/BinaryOperation$Op;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lparsii/eval/BinaryOperation$Op;->priority:I

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lparsii/eval/BinaryOperation$Op;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lparsii/eval/BinaryOperation$Op;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lparsii/eval/BinaryOperation$Op;

    return-object v0
.end method

.method public static values()[Lparsii/eval/BinaryOperation$Op;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lparsii/eval/BinaryOperation$Op;->$VALUES:[Lparsii/eval/BinaryOperation$Op;

    invoke-virtual {v0}, [Lparsii/eval/BinaryOperation$Op;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lparsii/eval/BinaryOperation$Op;

    return-object v0
.end method


# virtual methods
.method public getPriority()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lparsii/eval/BinaryOperation$Op;->priority:I

    return v0
.end method
