.class public Lparsii/eval/FunctionCall;
.super Lparsii/eval/Expression;
.source "FunctionCall.java"


# instance fields
.field private function:Lparsii/eval/Function;

.field private parameters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lparsii/eval/Expression;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lparsii/eval/Expression;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lparsii/eval/FunctionCall;->parameters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addParameter(Lparsii/eval/Expression;)V
    .locals 1
    .param p1, "expression"    # Lparsii/eval/Expression;

    .prologue
    .line 57
    iget-object v0, p0, Lparsii/eval/FunctionCall;->parameters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public evaluate()D
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lparsii/eval/FunctionCall;->function:Lparsii/eval/Function;

    iget-object v1, p0, Lparsii/eval/FunctionCall;->parameters:Ljava/util/List;

    invoke-interface {v0, v1}, Lparsii/eval/Function;->eval(Ljava/util/List;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getParameters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lparsii/eval/Expression;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lparsii/eval/FunctionCall;->parameters:Ljava/util/List;

    return-object v0
.end method

.method public setFunction(Lparsii/eval/Function;)V
    .locals 0
    .param p1, "function"    # Lparsii/eval/Function;

    .prologue
    .line 48
    iput-object p1, p0, Lparsii/eval/FunctionCall;->function:Lparsii/eval/Function;

    .line 49
    return-void
.end method

.method public simplify()Lparsii/eval/Expression;
    .locals 6

    .prologue
    .line 31
    iget-object v2, p0, Lparsii/eval/FunctionCall;->function:Lparsii/eval/Function;

    invoke-interface {v2}, Lparsii/eval/Function;->isNaturalFunction()Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    .end local p0    # "this":Lparsii/eval/FunctionCall;
    :goto_0
    return-object p0

    .line 34
    .restart local p0    # "this":Lparsii/eval/FunctionCall;
    :cond_0
    iget-object v2, p0, Lparsii/eval/FunctionCall;->parameters:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lparsii/eval/Expression;

    .line 35
    .local v0, "expr":Lparsii/eval/Expression;
    invoke-virtual {v0}, Lparsii/eval/Expression;->isConstant()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 39
    .end local v0    # "expr":Lparsii/eval/Expression;
    :cond_2
    new-instance v2, Lparsii/eval/Constant;

    invoke-virtual {p0}, Lparsii/eval/FunctionCall;->evaluate()D

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lparsii/eval/Constant;-><init>(D)V

    move-object p0, v2

    goto :goto_0
.end method
