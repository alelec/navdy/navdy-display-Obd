.class final Lparsii/eval/Functions$21;
.super Lparsii/eval/BinaryFunction;
.source "Functions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lparsii/eval/Functions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Lparsii/eval/BinaryFunction;-><init>()V

    return-void
.end method


# virtual methods
.method protected eval(DD)D
    .locals 3
    .param p1, "a"    # D
    .param p3, "b"    # D

    .prologue
    .line 228
    double-to-int v0, p1

    const/4 v1, 0x1

    double-to-int v2, p3

    shl-int/2addr v1, v2

    and-int/2addr v0, v1

    if-lez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
