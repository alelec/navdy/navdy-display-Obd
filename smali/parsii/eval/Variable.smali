.class public Lparsii/eval/Variable;
.super Ljava/lang/Object;
.source "Variable.java"


# instance fields
.field private constant:Z

.field private name:Ljava/lang/String;

.field private value:D


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lparsii/eval/Variable;->value:D

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lparsii/eval/Variable;->constant:Z

    .line 40
    iput-object p1, p0, Lparsii/eval/Variable;->name:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lparsii/eval/Variable;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lparsii/eval/Variable;->value:D

    return-wide v0
.end method

.method public isConstant()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lparsii/eval/Variable;->constant:Z

    return v0
.end method

.method public makeConstant(D)V
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 62
    invoke-virtual {p0, p1, p2}, Lparsii/eval/Variable;->setValue(D)V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lparsii/eval/Variable;->constant:Z

    .line 64
    return-void
.end method

.method public setValue(D)V
    .locals 5
    .param p1, "value"    # D

    .prologue
    .line 50
    iget-boolean v0, p0, Lparsii/eval/Variable;->constant:Z

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "%s is constant!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lparsii/eval/Variable;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    iput-wide p1, p0, Lparsii/eval/Variable;->value:D

    .line 54
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lparsii/eval/Variable;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lparsii/eval/Variable;->value:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withValue(D)Lparsii/eval/Variable;
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 105
    invoke-virtual {p0, p1, p2}, Lparsii/eval/Variable;->setValue(D)V

    .line 106
    return-object p0
.end method
