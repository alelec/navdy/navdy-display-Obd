.class public final Lcom/navdy/util/RunningStats;
.super Ljava/lang/Object;
.source "RunningStats.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u0006\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\nJ\u0006\u0010\u0015\u001a\u00020\u0004J\u0006\u0010\u0016\u001a\u00020\nJ\u0006\u0010\u0017\u001a\u00020\u0013J\u0006\u0010\u0018\u001a\u00020\nJ\u0006\u0010\u0019\u001a\u00020\nJ\u000e\u0010\u001a\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\nR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u000c\"\u0004\u0008\u0011\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/navdy/util/RunningStats;",
        "",
        "()V",
        "n",
        "",
        "getN",
        "()J",
        "setN",
        "(J)V",
        "newM",
        "",
        "getNewM",
        "()D",
        "setNewM",
        "(D)V",
        "newV",
        "getNewV",
        "setNewV",
        "add",
        "",
        "value",
        "count",
        "mean",
        "reset",
        "standardDeviation",
        "variance",
        "zScore",
        "obd-service_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private n:J

.field private newM:D

.field private newV:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(D)V
    .locals 9
    .param p1, "value"    # D

    .prologue
    const-wide/16 v6, 0x1

    .line 15
    iget-wide v4, p0, Lcom/navdy/util/RunningStats;->n:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/navdy/util/RunningStats;->n:J

    .line 16
    iget-wide v4, p0, Lcom/navdy/util/RunningStats;->n:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 17
    iput-wide p1, p0, Lcom/navdy/util/RunningStats;->newM:D

    .line 18
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/util/RunningStats;->newV:D

    .line 24
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->newM:D

    .line 21
    .local v0, "oldM":D
    iget-wide v2, p0, Lcom/navdy/util/RunningStats;->newV:D

    .line 22
    .local v2, "oldV":D
    sub-double v4, p1, v0

    iget-wide v6, p0, Lcom/navdy/util/RunningStats;->n:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    add-double/2addr v4, v0

    iput-wide v4, p0, Lcom/navdy/util/RunningStats;->newM:D

    .line 23
    sub-double v4, p1, v0

    iget-wide v6, p0, Lcom/navdy/util/RunningStats;->newM:D

    sub-double v6, p1, v6

    mul-double/2addr v4, v6

    add-double/2addr v4, v2

    iput-wide v4, p0, Lcom/navdy/util/RunningStats;->newV:D

    goto :goto_0
.end method

.method public final count()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->n:J

    return-wide v0
.end method

.method public final getN()J
    .locals 2

    .prologue
    .line 10
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->n:J

    return-wide v0
.end method

.method public final getNewM()D
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->newM:D

    return-wide v0
.end method

.method public final getNewV()D
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->newV:D

    return-wide v0
.end method

.method public final mean()D
    .locals 4

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->n:J

    const/4 v2, 0x0

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->newM:D

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/util/RunningStats;->n:J

    .line 49
    iput-wide v2, p0, Lcom/navdy/util/RunningStats;->newM:D

    .line 50
    iput-wide v2, p0, Lcom/navdy/util/RunningStats;->newV:D

    .line 51
    return-void
.end method

.method public final setN(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 10
    iput-wide p1, p0, Lcom/navdy/util/RunningStats;->n:J

    return-void
.end method

.method public final setNewM(D)V
    .locals 1
    .param p1, "<set-?>"    # D

    .prologue
    .line 11
    iput-wide p1, p0, Lcom/navdy/util/RunningStats;->newM:D

    return-void
.end method

.method public final setNewV(D)V
    .locals 1
    .param p1, "<set-?>"    # D

    .prologue
    .line 12
    iput-wide p1, p0, Lcom/navdy/util/RunningStats;->newV:D

    return-void
.end method

.method public final standardDeviation()D
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/navdy/util/RunningStats;->variance()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final variance()D
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 33
    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->n:J

    int-to-long v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/util/RunningStats;->newV:D

    iget-wide v2, p0, Lcom/navdy/util/RunningStats;->n:J

    int-to-long v4, v4

    sub-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final zScore(D)D
    .locals 7
    .param p1, "value"    # D

    .prologue
    const-wide/16 v4, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/navdy/util/RunningStats;->standardDeviation()D

    move-result-wide v2

    .line 39
    .local v2, "standardDeviation":D
    invoke-virtual {p0}, Lcom/navdy/util/RunningStats;->mean()D

    move-result-wide v0

    .line 40
    .local v0, "mean":D
    cmpg-double v6, v2, v4

    if-nez v6, :cond_1

    .line 42
    cmpg-double v6, p1, v0

    if-nez v6, :cond_0

    .line 44
    :goto_0
    return-wide v4

    .line 42
    :cond_0
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    goto :goto_0

    .line 44
    :cond_1
    sub-double v4, p1, v0

    div-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    goto :goto_0
.end method
