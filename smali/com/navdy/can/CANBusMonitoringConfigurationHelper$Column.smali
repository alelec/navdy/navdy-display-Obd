.class Lcom/navdy/can/CANBusMonitoringConfigurationHelper$Column;
.super Ljava/lang/Object;
.source "CANBusMonitoringConfigurationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/can/CANBusMonitoringConfigurationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Column"
.end annotation


# static fields
.field public static final BIT_LENGTH:Ljava/lang/String; = "BitLength"

.field public static final BIT_OFFSET:Ljava/lang/String; = "BitOffset"

.field public static final BYTE_OFFSET:Ljava/lang/String; = "ByteOffset"

.field public static final ENDIAN:Ljava/lang/String; = "Endian"

.field public static final FREQ:Ljava/lang/String; = "Freq"

.field public static final HEADER:Ljava/lang/String; = "Header"

.field public static final MAX_VALUE:Ljava/lang/String; = "MaxValue"

.field public static final MIN_VALUE:Ljava/lang/String; = "MinValue"

.field public static final NAME:Ljava/lang/String; = "Name"

.field public static final OBD_PID:Ljava/lang/String; = "OBD_Pid"

.field public static final RESOLUTION:Ljava/lang/String; = "Resolution"

.field public static final VALUE_OFFSET:Ljava/lang/String; = "ValueOffset"


# instance fields
.field final synthetic this$0:Lcom/navdy/can/CANBusMonitoringConfigurationHelper;


# direct methods
.method constructor <init>(Lcom/navdy/can/CANBusMonitoringConfigurationHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/can/CANBusMonitoringConfigurationHelper;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/navdy/can/CANBusMonitoringConfigurationHelper$Column;->this$0:Lcom/navdy/can/CANBusMonitoringConfigurationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
