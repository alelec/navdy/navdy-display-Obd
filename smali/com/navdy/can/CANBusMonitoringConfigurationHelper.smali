.class public Lcom/navdy/can/CANBusMonitoringConfigurationHelper;
.super Ljava/lang/Object;
.source "CANBusMonitoringConfigurationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/can/CANBusMonitoringConfigurationHelper$Column;
    }
.end annotation


# static fields
.field private static CAR_DETAILS_CONFIGURATION_MAPPING:[Lutil/Configuration; = null

.field public static final CAR_DETAILS_CONFIGURATION_MAPPING_FILE:Ljava/lang/String; = "configuration_mapping.csv"

.field private static VIN_CONFIGURATION_MAPPING:[Lutil/Configuration; = null

.field public static final VIN_CONFIGURATION_MAPPING_FILE:Ljava/lang/String; = "vin_configuration_mapping.csv"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadCANBusMonitoringSetupCommand(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "vin"    # Ljava/lang/String;

    .prologue
    .line 50
    sget-object v2, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->VIN_CONFIGURATION_MAPPING:[Lutil/Configuration;

    if-nez v2, :cond_0

    .line 51
    const-string v2, "vin_configuration_mapping.csv"

    invoke-static {p0, v2}, Lutil/Util;->loadConfigurationMappingList(Landroid/content/Context;Ljava/lang/String;)[Lutil/Configuration;

    move-result-object v2

    sput-object v2, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->VIN_CONFIGURATION_MAPPING:[Lutil/Configuration;

    .line 53
    :cond_0
    sget-object v2, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->VIN_CONFIGURATION_MAPPING:[Lutil/Configuration;

    invoke-static {v2, p1}, Lutil/Util;->pickConfiguration([Lutil/Configuration;Ljava/lang/String;)Lutil/Configuration;

    move-result-object v1

    .line 54
    .local v1, "matchingConfiguration":Lutil/Configuration;
    if-eqz v1, :cond_1

    .line 55
    iget-object v0, v1, Lutil/Configuration;->configurationName:Ljava/lang/String;

    .line 56
    .local v0, "configurationName":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->loadCommandFromConfigurationFile(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/command/CANBusMonitoringCommand;

    move-result-object v2

    .line 58
    .end local v0    # "configurationName":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static loadCANBusMonitoringSetupCommand(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;
    .param p3, "year"    # Ljava/lang/String;

    .prologue
    .line 62
    sget-object v3, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lutil/Configuration;

    if-nez v3, :cond_0

    .line 63
    const-string v3, "vin_configuration_mapping.csv"

    invoke-static {p0, v3}, Lutil/Util;->loadConfigurationMappingList(Landroid/content/Context;Ljava/lang/String;)[Lutil/Configuration;

    move-result-object v3

    sput-object v3, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lutil/Configuration;

    .line 65
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string p1, "*"

    .end local p1    # "make":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string p2, "*"

    .end local p2    # "model":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string p3, "*"

    .end local p3    # "year":Ljava/lang/String;
    :cond_3
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, "name":Ljava/lang/String;
    sget-object v3, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lutil/Configuration;

    invoke-static {v3, v2}, Lutil/Util;->pickConfiguration([Lutil/Configuration;Ljava/lang/String;)Lutil/Configuration;

    move-result-object v1

    .line 67
    .local v1, "matchingConfiguration":Lutil/Configuration;
    if-eqz v1, :cond_4

    .line 68
    iget-object v0, v1, Lutil/Configuration;->configurationName:Ljava/lang/String;

    .line 69
    .local v0, "configurationName":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/navdy/can/CANBusMonitoringConfigurationHelper;->loadCommandFromConfigurationFile(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/command/CANBusMonitoringCommand;

    move-result-object v3

    .line 71
    .end local v0    # "configurationName":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static loadCommandFromConfigurationFile(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configurationName"    # Ljava/lang/String;

    .prologue
    .line 75
    const/16 v19, 0x0

    .line 77
    .local v19, "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v22

    .line 78
    .local v22, "is":Ljava/io/InputStream;
    const/16 v23, 0x0

    .line 79
    .local v23, "reader":Ljava/io/BufferedReader;
    new-instance v23, Ljava/io/BufferedReader;

    .end local v23    # "reader":Ljava/io/BufferedReader;
    new-instance v2, Ljava/io/InputStreamReader;

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 80
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    sget-object v2, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v2, v10}, Lorg/apache/commons/csv/CSVFormat;->withHeader([Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Lorg/apache/commons/csv/CSVFormat;->parse(Ljava/io/Reader;)Lorg/apache/commons/csv/CSVParser;

    move-result-object v25

    .line 81
    .local v25, "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v21, "descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    invoke-interface/range {v25 .. v25}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_0
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/commons/csv/CSVRecord;

    .line 83
    .local v24, "record":Lorg/apache/commons/csv/CSVRecord;
    const-string v2, "Name"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "name":Ljava/lang/String;
    const-string v2, "Header"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "header":Ljava/lang/String;
    const-string v2, "ByteOffset"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 86
    .local v5, "byteOffset":I
    const-string v2, "BitOffset"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 87
    .local v6, "bitOffset":I
    const-string v2, "BitLength"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 88
    .local v7, "bitLength":I
    const-string v2, "ValueOffset"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    .line 89
    .local v26, "valueOffset":I
    const-string v2, "Resolution"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 90
    .local v8, "resolution":D
    const-string v2, "MinValue"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 91
    .local v12, "minValue":D
    const-string v2, "MaxValue"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 92
    .local v14, "maxValue":D
    const-string v2, "L"

    const-string v10, "Endian"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    .line 93
    .local v18, "littleEndian":Z
    const-string v2, "OBD_Pid"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 94
    .local v16, "obd2Pid":I
    const-string v2, "Freq"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 95
    .local v17, "frequency":I
    new-instance v2, Lcom/navdy/obd/can/CANBusDataDescriptor;

    move/from16 v0, v26

    int-to-long v10, v0

    invoke-direct/range {v2 .. v18}, Lcom/navdy/obd/can/CANBusDataDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IIIDJDDIIZ)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 99
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "header":Ljava/lang/String;
    .end local v5    # "byteOffset":I
    .end local v6    # "bitOffset":I
    .end local v7    # "bitLength":I
    .end local v8    # "resolution":D
    .end local v12    # "minValue":D
    .end local v14    # "maxValue":D
    .end local v16    # "obd2Pid":I
    .end local v17    # "frequency":I
    .end local v18    # "littleEndian":Z
    .end local v21    # "descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    .end local v22    # "is":Ljava/io/InputStream;
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v24    # "record":Lorg/apache/commons/csv/CSVRecord;
    .end local v25    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    .end local v26    # "valueOffset":I
    :catch_0
    move-exception v2

    .line 102
    const/16 v20, 0x0

    :goto_1
    return-object v20

    .line 97
    .restart local v21    # "descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    .restart local v22    # "is":Ljava/io/InputStream;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    .restart local v25    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :cond_0
    new-instance v20, Lcom/navdy/obd/command/CANBusMonitoringCommand;

    invoke-direct/range {v20 .. v21}, Lcom/navdy/obd/command/CANBusMonitoringCommand;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v19    # "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .local v20, "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    move-object/from16 v19, v20

    .line 98
    .end local v20    # "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .restart local v19    # "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    goto :goto_1
.end method
