.class public Lcom/navdy/obd/can/CANBusDataDescriptor;
.super Ljava/lang/Object;
.source "CANBusDataDescriptor.java"


# instance fields
.field public bitOffset:I

.field public byteOffset:I

.field public dataFrequency:I

.field public header:Ljava/lang/String;

.field public lengthInBits:I

.field public littleEndian:Z

.field public maxValue:D

.field public minValue:D

.field public name:Ljava/lang/String;

.field public obdPid:I

.field public resolution:D

.field public valueOffset:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIDJDDIIZ)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "byteOffset"    # I
    .param p4, "bitOffset"    # I
    .param p5, "lengthInBits"    # I
    .param p6, "resolution"    # D
    .param p8, "valueOffset"    # J
    .param p10, "minValue"    # D
    .param p12, "maxValue"    # D
    .param p14, "obd2Pid"    # I
    .param p15, "dataFrequency"    # I
    .param p16, "littleEndian"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->name:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    .line 24
    iput p3, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->byteOffset:I

    .line 25
    iput p4, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->bitOffset:I

    .line 26
    iput p5, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->lengthInBits:I

    .line 27
    iput-wide p6, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->resolution:D

    .line 28
    iput-wide p8, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->valueOffset:J

    .line 29
    iput-wide p10, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->minValue:D

    .line 30
    iput-wide p12, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->maxValue:D

    .line 31
    move/from16 v0, p14

    iput v0, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->obdPid:I

    .line 32
    move/from16 v0, p15

    iput v0, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->dataFrequency:I

    .line 33
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/navdy/obd/can/CANBusDataDescriptor;->littleEndian:Z

    .line 34
    return-void
.end method
