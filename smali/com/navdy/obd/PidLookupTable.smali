.class public Lcom/navdy/obd/PidLookupTable;
.super Ljava/lang/Object;
.source "PidLookupTable.java"


# instance fields
.field private mSnapshot:[Lcom/navdy/obd/Pid;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-array v0, p1, [Lcom/navdy/obd/Pid;

    iput-object v0, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    .line 15
    return-void
.end method

.method private validId(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 34
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public build(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v1, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    array-length v1, v1

    new-array v1, v1, [Lcom/navdy/obd/Pid;

    iput-object v1, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    .line 19
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 20
    .local v0, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 21
    iget-object v2, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v3

    aput-object v0, v2, v3

    goto :goto_0

    .line 24
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_1
    return-void
.end method

.method public getPid(I)Lcom/navdy/obd/Pid;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/obd/PidLookupTable;->validId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/navdy/obd/PidLookupTable;->mSnapshot:[Lcom/navdy/obd/Pid;

    aget-object v0, v0, p1

    .line 30
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPidValue(I)D
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/navdy/obd/PidLookupTable;->getPid(I)Lcom/navdy/obd/Pid;

    move-result-object v0

    .line 39
    .local v0, "pid":Lcom/navdy/obd/Pid;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public updatePid(ID)Z
    .locals 4
    .param p1, "id"    # I
    .param p2, "value"    # D

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/navdy/obd/PidLookupTable;->getPid(I)Lcom/navdy/obd/Pid;

    move-result-object v1

    .line 44
    .local v1, "pid":Lcom/navdy/obd/Pid;
    const/4 v0, 0x0

    .line 45
    .local v0, "changed":Z
    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    cmpl-double v2, v2, p2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 47
    :goto_0
    invoke-virtual {v1, p2, p3}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 49
    :cond_0
    return v0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
