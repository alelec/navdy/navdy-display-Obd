.class public Lcom/navdy/obd/DefaultPidProcessorFactory;
.super Ljava/lang/Object;
.source "DefaultPidProcessorFactory.java"

# interfaces
.implements Lcom/navdy/obd/PidProcessorFactory;


# static fields
.field private static PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/obd/DefaultPidProcessorFactory;->PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;

    .line 15
    sget-object v0, Lcom/navdy/obd/DefaultPidProcessorFactory;->PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;

    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    sget-object v0, Lcom/navdy/obd/DefaultPidProcessorFactory;->PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    sget-object v0, Lcom/navdy/obd/DefaultPidProcessorFactory;->PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public buildPidProcessorForPid(I)Lcom/navdy/obd/PidProcessor;
    .locals 1
    .param p1, "pid"    # I

    .prologue
    .line 21
    sparse-switch p1, :sswitch_data_0

    .line 30
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 23
    :sswitch_0
    new-instance v0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;

    invoke-direct {v0}, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;-><init>()V

    goto :goto_0

    .line 25
    :sswitch_1
    new-instance v0, Lcom/navdy/obd/FuelLevelPidProcessor;

    invoke-direct {v0}, Lcom/navdy/obd/FuelLevelPidProcessor;-><init>()V

    goto :goto_0

    .line 27
    :sswitch_2
    new-instance v0, Lcom/navdy/obd/SpeedPidProcessor;

    invoke-direct {v0}, Lcom/navdy/obd/SpeedPidProcessor;-><init>()V

    goto :goto_0

    .line 21
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_2
        0x2f -> :sswitch_1
        0x100 -> :sswitch_0
    .end sparse-switch
.end method

.method public getPidsHavingProcessors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/obd/DefaultPidProcessorFactory;->PIDS_WITH_PRE_PROCESSORS:Ljava/util/List;

    return-object v0
.end method
