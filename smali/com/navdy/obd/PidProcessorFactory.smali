.class public interface abstract Lcom/navdy/obd/PidProcessorFactory;
.super Ljava/lang/Object;
.source "PidProcessorFactory.java"


# virtual methods
.method public abstract buildPidProcessorForPid(I)Lcom/navdy/obd/PidProcessor;
.end method

.method public abstract getPidsHavingProcessors()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
