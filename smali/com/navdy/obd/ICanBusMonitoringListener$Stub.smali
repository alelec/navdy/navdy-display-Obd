.class public abstract Lcom/navdy/obd/ICanBusMonitoringListener$Stub;
.super Landroid/os/Binder;
.source "ICanBusMonitoringListener.java"

# interfaces
.implements Lcom/navdy/obd/ICanBusMonitoringListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ICanBusMonitoringListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ICanBusMonitoringListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.navdy.obd.ICanBusMonitoringListener"

.field static final TRANSACTION_getGpsSpeed:I = 0x4

.field static final TRANSACTION_getLatitude:I = 0x5

.field static final TRANSACTION_getLongitude:I = 0x6

.field static final TRANSACTION_getMake:I = 0x8

.field static final TRANSACTION_getModel:I = 0x9

.field static final TRANSACTION_getYear:I = 0xa

.field static final TRANSACTION_isMonitoringLimitReached:I = 0x7

.field static final TRANSACTION_onCanBusMonitorSuccess:I = 0x3

.field static final TRANSACTION_onCanBusMonitoringError:I = 0x2

.field static final TRANSACTION_onNewDataAvailable:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p0, p0, v0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/ICanBusMonitoringListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/navdy/obd/ICanBusMonitoringListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/navdy/obd/ICanBusMonitoringListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/navdy/obd/ICanBusMonitoringListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 131
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 44
    :sswitch_0
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->onNewDataAvailable(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 58
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_2
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 61
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->onCanBusMonitoringError(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_3
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 70
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->onCanBusMonitorSuccess(I)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":I
    :sswitch_4
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getGpsSpeed()I

    move-result v2

    .line 78
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 79
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 84
    .end local v2    # "_result":I
    :sswitch_5
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getLatitude()D

    move-result-wide v2

    .line 86
    .local v2, "_result":D
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 87
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    goto :goto_0

    .line 92
    .end local v2    # "_result":D
    :sswitch_6
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getLongitude()D

    move-result-wide v2

    .line 94
    .restart local v2    # "_result":D
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 95
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    goto :goto_0

    .line 100
    .end local v2    # "_result":D
    :sswitch_7
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->isMonitoringLimitReached()Z

    move-result v2

    .line 102
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 103
    if-eqz v2, :cond_0

    move v1, v4

    :goto_1
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 108
    .end local v2    # "_result":Z
    :sswitch_8
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getMake()Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    .end local v2    # "_result":Ljava/lang/String;
    :sswitch_9
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getModel()Ljava/lang/String;

    move-result-object v2

    .line 118
    .restart local v2    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 119
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 124
    .end local v2    # "_result":Ljava/lang/String;
    :sswitch_a
    const-string v1, "com.navdy.obd.ICanBusMonitoringListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->getYear()Ljava/lang/String;

    move-result-object v2

    .line 126
    .restart local v2    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 127
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
