.class public Lcom/navdy/obd/util/HexUtil;
.super Ljava/lang/Object;
.source "HexUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseHexDigit(I)I
    .locals 1
    .param p0, "nybble"    # I

    .prologue
    .line 16
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 17
    add-int/lit8 v0, p0, -0x30

    .line 21
    :goto_0
    return v0

    .line 18
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 19
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 20
    :cond_1
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    .line 21
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 23
    :cond_2
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0
.end method

.method public static parseHexString(Ljava/lang/String;[BII)V
    .locals 5
    .param p0, "hex"    # Ljava/lang/String;
    .param p1, "dest"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 8
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 9
    .local v0, "charBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 10
    add-int v2, v1, p2

    mul-int/lit8 v3, v1, 0x2

    aget-byte v3, v0, v3

    invoke-static {v3}, Lcom/navdy/obd/util/HexUtil;->parseHexDigit(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-byte v4, v0, v4

    invoke-static {v4}, Lcom/navdy/obd/util/HexUtil;->parseHexDigit(I)I

    move-result v4

    add-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    .line 9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 12
    :cond_0
    return-void
.end method
