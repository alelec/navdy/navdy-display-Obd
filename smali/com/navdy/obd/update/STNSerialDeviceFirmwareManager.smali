.class public Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;
.super Ljava/lang/Object;
.source "STNSerialDeviceFirmwareManager.java"

# interfaces
.implements Lcom/navdy/obd/update/ObdDeviceFirmwareManager;


# static fields
.field public static final BOOTLOADER_MAJOR_VERSION:B = 0x2t

.field private static final CARRIAGE_RETURN:I = 0xd

.field public static final CURRENT_UPDATE_FILE_VERSION:Ljava/lang/String; = "05"

.field private static final DELAY_AFTER_RESET:J = 0x96L

.field public static final FILE_SIGNATURE:Ljava/lang/String; = "STNFWv"

.field public static final FIRMWARE_VERSION_PATTERN:Ljava/util/regex/Pattern;

.field public static final INFO_COMMAND:Ljava/lang/String; = "sti"

.field public static final MAX_RETRIES_TO_CONNECT:I = 0xa

.field public static final RESET_COMMAND:Ljava/lang/String; = "atz"

.field public static final TAG:Ljava/lang/String;

.field public static final UPDATE_HEADER_SIGNATURE_SIZE:I = 0x6


# instance fields
.field private mChannel:Lcom/navdy/obd/io/IChannel;

.field private mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mObdService:Lcom/navdy/obd/ObdService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "^STN(.+)\\s{1}v(.+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->FIRMWARE_VERSION_PATTERN:Ljava/util/regex/Pattern;

    .line 34
    const-class v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V
    .locals 2
    .param p1, "obdService"    # Lcom/navdy/obd/ObdService;
    .param p2, "channel"    # Lcom/navdy/obd/io/IChannel;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->init(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V

    .line 56
    return-void
.end method

.method private ensureFile(Ljava/lang/String;)Lcom/navdy/obd/update/UpdateFileHeader;
    .locals 28
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 284
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 285
    .local v10, "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->canRead()Z

    move-result v23

    if-nez v23, :cond_1

    .line 286
    :cond_0
    const/16 v20, 0x0

    .line 353
    :goto_0
    return-object v20

    .line 288
    :cond_1
    const/4 v11, 0x0

    .line 290
    .local v11, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .local v12, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/DataInputStream;

    invoke-direct {v8, v12}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 292
    .local v8, "dis":Ljava/io/DataInputStream;
    new-instance v20, Lcom/navdy/obd/update/UpdateFileHeader;

    invoke-direct/range {v20 .. v20}, Lcom/navdy/obd/update/UpdateFileHeader;-><init>()V

    .line 293
    .local v20, "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 294
    .local v18, "signatureData":[B
    const/16 v23, 0x0

    const/16 v24, 0x6

    move-object/from16 v0, v18

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v8, v0, v1, v2}, Ljava/io/DataInputStream;->read([BII)I

    .line 295
    new-instance v17, Ljava/lang/String;

    invoke-direct/range {v17 .. v18}, Ljava/lang/String;-><init>([B)V

    .line 296
    .local v17, "signature":Ljava/lang/String;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Signature : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const-string v23, "STNFWv"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v23

    if-nez v23, :cond_2

    .line 298
    const/16 v20, 0x0

    .line 348
    .end local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 349
    :catch_0
    move-exception v9

    .line 350
    .local v9, "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :cond_2
    const/16 v23, 0x2

    :try_start_3
    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v22, v0

    .line 301
    .local v22, "versionData":[B
    const/16 v23, 0x0

    const/16 v24, 0x2

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v8, v0, v1, v2}, Ljava/io/DataInputStream;->read([BII)I

    .line 302
    new-instance v21, Ljava/lang/String;

    invoke-direct/range {v21 .. v22}, Ljava/lang/String;-><init>([B)V

    .line 303
    .local v21, "version":Ljava/lang/String;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "File version : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    const-string v23, "05"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v23

    if-nez v23, :cond_3

    .line 305
    const/16 v20, 0x0

    .line 348
    .end local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :try_start_4
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 349
    :catch_1
    move-exception v9

    .line 350
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 307
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :cond_3
    :try_start_5
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    int-to-short v7, v0

    .line 308
    .local v7, "deviceIdCount":S
    const/16 v19, 0x0

    .line 309
    .local v19, "stnDeviceFound":Z
    const/4 v13, 0x0

    .local v13, "i":S
    :goto_1
    if-ge v13, v7, :cond_5

    .line 310
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readShort()S

    move-result v23

    const v24, 0xffff

    and-int v6, v23, v24

    .line 311
    .local v6, "deviceId":I
    const/16 v23, 0x1110

    move/from16 v0, v23

    if-ne v6, v0, :cond_4

    .line 312
    const/16 v19, 0x1

    .line 309
    :cond_4
    add-int/lit8 v23, v13, 0x1

    move/from16 v0, v23

    int-to-short v13, v0

    goto :goto_1

    .line 315
    .end local v6    # "deviceId":I
    :cond_5
    if-nez v19, :cond_6

    .line 317
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Update not compatible with our Obd Chip"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 318
    const/16 v20, 0x0

    .line 348
    .end local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 349
    :catch_2
    move-exception v9

    .line 350
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 320
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    :cond_6
    :try_start_7
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    int-to-short v15, v0

    .line 321
    .local v15, "imageDescriptorCount":S
    move-object/from16 v0, v20

    iput-short v15, v0, Lcom/navdy/obd/update/UpdateFileHeader;->fwImageDescriptorsCount:S

    .line 322
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Number of image descriptors : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v20

    iget-short v0, v0, Lcom/navdy/obd/update/UpdateFileHeader;->fwImageDescriptorsCount:S

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    new-array v5, v15, [Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;

    .line 324
    .local v5, "descriptors":[Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    const/4 v13, 0x0

    :goto_2
    if-ge v13, v15, :cond_7

    .line 325
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "---------------------------------------------\n\n"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    new-instance v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;

    invoke-direct {v4}, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;-><init>()V

    .line 327
    .local v4, "descriptor":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageType:S

    .line 328
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "ImageType : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageType:S

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v16, v0

    .line 330
    .local v16, "reserved":S
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->nextFwIndex:S

    .line 331
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Next FW index : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->nextFwIndex:S

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v23

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->errorFwIndex:S

    .line 333
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Error FW index : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-short v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->errorFwIndex:S

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageOffset:J

    .line 335
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Image offset : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-wide v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageOffset:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageSize:J

    .line 337
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Image size : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-wide v0, v4, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageSize:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    aput-object v4, v5, v13

    .line 324
    add-int/lit8 v23, v13, 0x1

    move/from16 v0, v23

    int-to-short v13, v0

    goto/16 :goto_2

    .line 340
    .end local v4    # "descriptor":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    .end local v16    # "reserved":S
    :cond_7
    move-object/from16 v0, v20

    iput-object v5, v0, Lcom/navdy/obd/update/UpdateFileHeader;->fwImageDescriptors:[Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 348
    :try_start_8
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 349
    :catch_3
    move-exception v9

    .line 350
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 342
    .end local v5    # "descriptors":[Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    .end local v7    # "deviceIdCount":S
    .end local v8    # "dis":Ljava/io/DataInputStream;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "i":S
    .end local v15    # "imageDescriptorCount":S
    .end local v17    # "signature":Ljava/lang/String;
    .end local v18    # "signatureData":[B
    .end local v19    # "stnDeviceFound":Z
    .end local v20    # "updateFileHeader":Lcom/navdy/obd/update/UpdateFileHeader;
    .end local v21    # "version":Ljava/lang/String;
    .end local v22    # "versionData":[B
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v14

    .line 343
    .local v14, "ie":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_9
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "IOException while reading from update file "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 348
    :try_start_a
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 353
    .end local v14    # "ie":Ljava/io/FileNotFoundException;
    :goto_4
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 349
    .restart local v14    # "ie":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v9

    .line 350
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 344
    .end local v9    # "e":Ljava/io/IOException;
    .end local v14    # "ie":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v9

    .line 345
    .local v9, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_b
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Error while reading from update file "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 348
    :try_start_c
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_4

    .line 349
    :catch_7
    move-exception v9

    .line 350
    .local v9, "e":Ljava/io/IOException;
    sget-object v23, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v24, "Error closing the file input stream"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 347
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v23

    .line 348
    :goto_6
    :try_start_d
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 351
    :goto_7
    throw v23

    .line 349
    :catch_8
    move-exception v9

    .line 350
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v24, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v25, "Error closing the file input stream"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 347
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v23

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .line 344
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_9
    move-exception v9

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 342
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v14

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    .locals 3
    .param p1, "isInBootMode"    # Z
    .param p2, "bootloaderChannel"    # Lcom/navdy/obd/update/STNBootloaderChannel;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 236
    :try_start_0
    invoke-virtual {p2}, Lcom/navdy/obd/update/STNBootloaderChannel;->reset()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :cond_0
    :goto_0
    const/4 p2, 0x0

    .line 243
    iget-object v1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 245
    :cond_1
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v2, "Exception while resetting the device"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getFirmwareVersion()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 256
    :try_start_0
    iget-object v6, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mObdService:Lcom/navdy/obd/ObdService;

    const-string v7, "sti"

    invoke-virtual {v6, v7}, Lcom/navdy/obd/ObdService;->sendCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 257
    .local v5, "versionResponse":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 258
    sget-object v6, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->FIRMWARE_VERSION_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 259
    .local v4, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 260
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "compactVersion":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 262
    .local v3, "expression":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "device":Ljava/lang/String;
    sget-object v6, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "STN device :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", firmware :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v0    # "compactVersion":Ljava/lang/String;
    .end local v1    # "device":Ljava/lang/String;
    .end local v3    # "expression":Ljava/lang/String;
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "versionResponse":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v4    # "matcher":Ljava/util/regex/Matcher;
    .restart local v5    # "versionResponse":Ljava/lang/String;
    :cond_0
    move-object v0, v5

    .line 267
    goto :goto_0

    .line 270
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "versionResponse":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 271
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V
    .locals 0
    .param p1, "obdService"    # Lcom/navdy/obd/ObdService;
    .param p2, "channel"    # Lcom/navdy/obd/io/IChannel;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mObdService:Lcom/navdy/obd/ObdService;

    .line 60
    iput-object p2, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    .line 61
    return-void
.end method

.method public isUpdatingFirmware()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public update(Lcom/navdy/obd/update/Update;Lcom/navdy/hardware/SerialPort;Z)Z
    .locals 42
    .param p1, "update"    # Lcom/navdy/obd/update/Update;
    .param p2, "serialPort"    # Lcom/navdy/hardware/SerialPort;
    .param p3, "isInValidState"    # Z

    .prologue
    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    const/16 v39, 0x1

    invoke-virtual/range {v37 .. v39}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v37

    if-eqz v37, :cond_2

    .line 87
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/obd/update/Update;->mUpdateFilePath:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->ensureFile(Ljava/lang/String;)Lcom/navdy/obd/update/UpdateFileHeader;

    move-result-object v18

    .line 88
    .local v18, "headerFile":Lcom/navdy/obd/update/UpdateFileHeader;
    if-nez v18, :cond_0

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mIsUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 90
    const/16 v35, 0x0

    .line 228
    .end local v18    # "headerFile":Lcom/navdy/obd/update/UpdateFileHeader;
    :goto_0
    return v35

    .line 92
    .restart local v18    # "headerFile":Lcom/navdy/obd/update/UpdateFileHeader;
    :cond_0
    new-instance v5, Lcom/navdy/obd/update/STNBootloaderChannel;

    move-object/from16 v0, p2

    invoke-direct {v5, v0}, Lcom/navdy/obd/update/STNBootloaderChannel;-><init>(Lcom/navdy/hardware/SerialPort;)V

    .line 93
    .local v5, "bootloaderChannel":Lcom/navdy/obd/update/STNBootloaderChannel;
    const/4 v15, 0x0

    .line 94
    .local v15, "enteredBootMode":Z
    const/16 v28, 0x0

    .line 99
    .local v28, "reader":Ljava/io/FileInputStream;
    if-eqz p3, :cond_1

    .line 100
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lcom/navdy/obd/io/IChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v25

    .line 101
    .local v25, "out":Ljava/io/OutputStream;
    const-string v12, "atz"

    .line 102
    .local v12, "command":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v21

    .line 103
    .local v21, "length":I
    add-int/lit8 v37, v21, 0x1

    move/from16 v0, v37

    new-array v7, v0, [B

    .line 104
    .local v7, "bytes":[B
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v37

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v21

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    const/16 v37, 0xd

    aput-byte v37, v7, v21

    .line 106
    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/io/OutputStream;->write([B)V

    .line 107
    invoke-virtual/range {v25 .. v25}, Ljava/io/OutputStream;->flush()V

    .line 108
    const-wide/16 v38, 0x96

    invoke-static/range {v38 .. v39}, Ljava/lang/Thread;->sleep(J)V

    .line 112
    .end local v7    # "bytes":[B
    .end local v12    # "command":Ljava/lang/String;
    .end local v21    # "length":I
    .end local v25    # "out":Ljava/io/OutputStream;
    :cond_1
    const/16 v34, 0x0

    .line 113
    .local v34, "tries":I
    :goto_1
    if-nez v15, :cond_3

    const/16 v37, 0xa

    move/from16 v0, v34

    move/from16 v1, v37

    if-ge v0, v1, :cond_3

    .line 114
    add-int/lit8 v34, v34, 0x1

    .line 115
    invoke-virtual {v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->enterBootloaderMode()Z

    move-result v15

    .line 116
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Enter boot mode , Success : "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, ", Tries : "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 221
    .end local v34    # "tries":I
    :catch_0
    move-exception v33

    .line 222
    .local v33, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_1
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Unhandled error during update process "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    .line 228
    .end local v5    # "bootloaderChannel":Lcom/navdy/obd/update/STNBootloaderChannel;
    .end local v15    # "enteredBootMode":Z
    .end local v18    # "headerFile":Lcom/navdy/obd/update/UpdateFileHeader;
    .end local v28    # "reader":Ljava/io/FileInputStream;
    .end local v33    # "t":Ljava/lang/Throwable;
    :cond_2
    const/16 v35, 0x0

    goto/16 :goto_0

    .line 118
    .restart local v5    # "bootloaderChannel":Lcom/navdy/obd/update/STNBootloaderChannel;
    .restart local v15    # "enteredBootMode":Z
    .restart local v18    # "headerFile":Lcom/navdy/obd/update/UpdateFileHeader;
    .restart local v28    # "reader":Ljava/io/FileInputStream;
    .restart local v34    # "tries":I
    :cond_3
    if-nez v15, :cond_4

    .line 120
    :try_start_2
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v38, "Failed to enter boot mode, aborting update"

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/16 v37, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    const/16 v35, 0x0

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 124
    :cond_4
    :try_start_3
    invoke-virtual {v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->getVersion()[B

    move-result-object v36

    .line 125
    .local v36, "version":[B
    invoke-virtual {v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->getFirmwareStatus()Z

    move-result v32

    .line 126
    .local v32, "success":Z
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Firmware status "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/16 v37, 0x2

    aget-byte v22, v36, v37

    .line 128
    .local v22, "majorVersion":B
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Major version "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    const/16 v39, 0x2

    aget-byte v39, v36, v39

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, ", Minor version "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    const/16 v39, 0x3

    aget-byte v39, v36, v39

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/16 v37, 0x2

    move/from16 v0, v22

    move/from16 v1, v37

    if-eq v0, v1, :cond_5

    .line 131
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "The bootloader version is not supported"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    const/16 v35, 0x0

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 136
    :cond_5
    :try_start_4
    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/navdy/obd/update/UpdateFileHeader;->fwImageDescriptorsCount:S

    move/from16 v20, v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 137
    .local v20, "imagesCount":I
    const/16 v24, 0x0

    .local v24, "nextFwImageIndex":S
    move-object/from16 v29, v28

    .line 139
    .end local v28    # "reader":Ljava/io/FileInputStream;
    .local v29, "reader":Ljava/io/FileInputStream;
    :goto_3
    const/16 v37, 0xff

    move/from16 v0, v24

    move/from16 v1, v37

    if-eq v0, v1, :cond_11

    if-ltz v24, :cond_11

    move/from16 v0, v24

    move/from16 v1, v20

    if-ge v0, v1, :cond_11

    .line 140
    :try_start_5
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Uploading fw image"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/obd/update/UpdateFileHeader;->fwImageDescriptors:[Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;

    move-object/from16 v37, v0

    aget-object v13, v37, v24

    .line 142
    .local v13, "desc":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    iget-short v0, v13, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->nextFwIndex:S

    move/from16 v24, v0

    .line 143
    iget-wide v0, v13, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageSize:J

    move-wide/from16 v38, v0

    const-wide/32 v40, 0xffffff

    and-long v38, v38, v40

    move-wide/from16 v0, v38

    long-to-int v0, v0

    move/from16 v19, v0

    .line 144
    .local v19, "imageSize":I
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Image size "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    move/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/navdy/obd/update/STNBootloaderChannel;->startUpload(I)I

    move-result v23

    .line 146
    .local v23, "maxChunkSize":I
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Max chunk size "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    if-lez v23, :cond_10

    .line 150
    const/16 v37, 0x1400

    move/from16 v0, v37

    move/from16 v1, v23

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 151
    .local v11, "chunkSize":I
    div-int v9, v19, v11

    .line 152
    .local v9, "chunkCount":I
    move/from16 v17, v9

    .line 153
    .local v17, "fullChunks":I
    rem-int v30, v19, v11

    .line 155
    .local v30, "remainingData":I
    div-int/lit8 v37, v30, 0x10

    add-int v9, v9, v37

    .line 156
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Total chunk count : "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/obd/update/Update;->mUpdateFilePath:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v16, "file":Ljava/io/File;
    iget-wide v0, v13, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageOffset:J

    move-wide/from16 v26, v0

    .line 159
    .local v26, "offset":J
    new-instance v28, Ljava/io/FileInputStream;

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 160
    .end local v29    # "reader":Ljava/io/FileInputStream;
    .restart local v28    # "reader":Ljava/io/FileInputStream;
    :try_start_6
    move-object/from16 v0, v28

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/io/FileInputStream;->skip(J)J
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 161
    const/4 v8, 0x0

    .line 163
    .local v8, "chunk":I
    if-lez v17, :cond_8

    move/from16 v37, v11

    :goto_4
    :try_start_7
    move/from16 v0, v37

    new-array v6, v0, [B

    .line 164
    .local v6, "buffer":[B
    const/4 v8, 0x0

    :goto_5
    if-ge v8, v9, :cond_d

    .line 165
    move/from16 v31, v11

    .line 166
    .local v31, "size":I
    move/from16 v0, v17

    if-lt v8, v0, :cond_6

    .line 167
    const/16 v31, 0x10

    .line 169
    :cond_6
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Writing chunk "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, ", Size : "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    array-length v0, v6

    move/from16 v37, v0

    move/from16 v0, v37

    move/from16 v1, v31

    if-eq v0, v1, :cond_7

    .line 171
    move/from16 v0, v31

    new-array v6, v0, [B

    .line 173
    :cond_7
    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/io/FileInputStream;->read([B)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 175
    :try_start_8
    invoke-virtual {v5, v8, v6}, Lcom/navdy/obd/update/STNBootloaderChannel;->sendChunk(I[B)I

    move-result v10

    .line 176
    .local v10, "chunkId":I
    if-ne v8, v10, :cond_9

    .line 177
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Successfully sent chunk "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    .end local v10    # "chunkId":I
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 163
    .end local v6    # "buffer":[B
    .end local v31    # "size":I
    :cond_8
    const/16 v37, 0x10

    goto :goto_4

    .line 179
    .restart local v6    # "buffer":[B
    .restart local v10    # "chunkId":I
    .restart local v31    # "size":I
    :cond_9
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Sending chunk "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, " failed, id mismatch :"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_8
    .catch Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 181
    const/16 v35, 0x0

    .line 196
    if-eqz v28, :cond_a

    .line 198
    :try_start_9
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 225
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 199
    :catch_1
    move-exception v14

    .line 200
    .local v14, "e":Ljava/io/IOException;
    :try_start_a
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Error closing the file reader"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_7

    .line 225
    .end local v6    # "buffer":[B
    .end local v8    # "chunk":I
    .end local v9    # "chunkCount":I
    .end local v10    # "chunkId":I
    .end local v11    # "chunkSize":I
    .end local v13    # "desc":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v16    # "file":Ljava/io/File;
    .end local v17    # "fullChunks":I
    .end local v19    # "imageSize":I
    .end local v20    # "imagesCount":I
    .end local v22    # "majorVersion":B
    .end local v23    # "maxChunkSize":I
    .end local v24    # "nextFwImageIndex":S
    .end local v26    # "offset":J
    .end local v30    # "remainingData":I
    .end local v31    # "size":I
    .end local v32    # "success":Z
    .end local v34    # "tries":I
    .end local v36    # "version":[B
    :catchall_0
    move-exception v37

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    throw v37

    .line 183
    .restart local v6    # "buffer":[B
    .restart local v8    # "chunk":I
    .restart local v9    # "chunkCount":I
    .restart local v11    # "chunkSize":I
    .restart local v13    # "desc":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    .restart local v16    # "file":Ljava/io/File;
    .restart local v17    # "fullChunks":I
    .restart local v19    # "imageSize":I
    .restart local v20    # "imagesCount":I
    .restart local v22    # "majorVersion":B
    .restart local v23    # "maxChunkSize":I
    .restart local v24    # "nextFwImageIndex":S
    .restart local v26    # "offset":J
    .restart local v30    # "remainingData":I
    .restart local v31    # "size":I
    .restart local v32    # "success":Z
    .restart local v34    # "tries":I
    .restart local v36    # "version":[B
    :catch_2
    move-exception v4

    .line 184
    .local v4, "be":Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
    :try_start_b
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Error response from bootloader while sending chunk "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-short v0, v13, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->imageType:S

    move/from16 v37, v0

    const/16 v38, 0x10

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_b

    iget-short v0, v4, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;->errorCode:S

    move/from16 v37, v0

    const/16 v38, -0x70

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_b

    .line 187
    iget-short v0, v13, Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;->errorFwIndex:S

    move/from16 v24, v0

    .line 188
    goto/16 :goto_6

    .line 190
    :cond_b
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 191
    const/16 v35, 0x0

    .line 196
    if-eqz v28, :cond_c

    .line 198
    :try_start_c
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 225
    :cond_c
    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 199
    :catch_3
    move-exception v14

    .line 200
    .restart local v14    # "e":Ljava/io/IOException;
    :try_start_d
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Error closing the file reader"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_9

    .line 196
    .end local v4    # "be":Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v31    # "size":I
    :cond_d
    if-eqz v28, :cond_e

    .line 198
    :try_start_e
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_e
    :goto_a
    move-object/from16 v29, v28

    .line 210
    .end local v28    # "reader":Ljava/io/FileInputStream;
    .restart local v29    # "reader":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .line 199
    .end local v29    # "reader":Ljava/io/FileInputStream;
    .restart local v28    # "reader":Ljava/io/FileInputStream;
    :catch_4
    move-exception v14

    .line 200
    .restart local v14    # "e":Ljava/io/IOException;
    :try_start_f
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Error closing the file reader"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_a

    .line 196
    .end local v6    # "buffer":[B
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v37

    if-eqz v28, :cond_f

    .line 198
    :try_start_10
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 202
    :cond_f
    :goto_b
    :try_start_11
    throw v37

    .line 199
    :catch_5
    move-exception v14

    .line 200
    .restart local v14    # "e":Ljava/io/IOException;
    sget-object v38, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "Error closing the file reader"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_b

    .line 206
    .end local v8    # "chunk":I
    .end local v9    # "chunkCount":I
    .end local v11    # "chunkSize":I
    .end local v14    # "e":Ljava/io/IOException;
    .end local v16    # "file":Ljava/io/File;
    .end local v17    # "fullChunks":I
    .end local v26    # "offset":J
    .end local v28    # "reader":Ljava/io/FileInputStream;
    .end local v30    # "remainingData":I
    .restart local v29    # "reader":Ljava/io/FileInputStream;
    :cond_10
    :try_start_12
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v38, "Start upload command failed, bootloader returned invalid max chunk size"

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 208
    const/16 v35, 0x0

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 211
    .end local v13    # "desc":Lcom/navdy/obd/update/UpdateFileHeader$FWImageDescriptor;
    .end local v19    # "imageSize":I
    .end local v23    # "maxChunkSize":I
    :cond_11
    :try_start_13
    invoke-virtual {v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->getFirmwareStatus()Z

    move-result v35

    .line 212
    .local v35, "validFirmware":Z
    if-eqz v35, :cond_12

    .line 213
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v38, "After uploading the firmware is valid"

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :goto_c
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_6
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->updateStopped(ZLcom/navdy/obd/update/STNBootloaderChannel;)V

    goto/16 :goto_0

    .line 215
    :cond_12
    :try_start_14
    sget-object v37, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    const-string v38, "Invalid firmware, needs re installation"

    invoke-static/range {v37 .. v38}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto :goto_c

    .line 221
    .end local v35    # "validFirmware":Z
    :catch_6
    move-exception v33

    move-object/from16 v28, v29

    .end local v29    # "reader":Ljava/io/FileInputStream;
    .restart local v28    # "reader":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 225
    .end local v28    # "reader":Ljava/io/FileInputStream;
    .restart local v29    # "reader":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v37

    move-object/from16 v28, v29

    .end local v29    # "reader":Ljava/io/FileInputStream;
    .restart local v28    # "reader":Ljava/io/FileInputStream;
    goto/16 :goto_8
.end method

.method public updateFirmware(Lcom/navdy/obd/update/Update;)Z
    .locals 4
    .param p1, "update"    # Lcom/navdy/obd/update/Update;

    .prologue
    .line 65
    sget-object v1, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFirmware  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    instance-of v1, v1, Lcom/navdy/obd/io/STNSerialChannel;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->mChannel:Lcom/navdy/obd/io/IChannel;

    check-cast v1, Lcom/navdy/obd/io/STNSerialChannel;

    invoke-virtual {v1}, Lcom/navdy/obd/io/STNSerialChannel;->getSerialPort()Lcom/navdy/hardware/SerialPort;

    move-result-object v0

    .line 68
    .local v0, "serialPort":Lcom/navdy/hardware/SerialPort;
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->update(Lcom/navdy/obd/update/Update;Lcom/navdy/hardware/SerialPort;Z)Z

    move-result v1

    .line 70
    .end local v0    # "serialPort":Lcom/navdy/hardware/SerialPort;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
