.class public Lcom/navdy/obd/update/STNBootloaderChannel;
.super Ljava/lang/Object;
.source "STNBootloaderChannel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
    }
.end annotation


# static fields
.field public static final ACK:I = 0x40

.field public static final CONNECT_BOOTLOADER_COMMAND:[B

.field public static final DEFAULT_CHUNK_SIZE:I = 0x1400

.field public static final DLE:B = 0x5t

.field public static final ERROR_CODE_CRC_FAILED:B = -0x2t

.field public static final ERROR_CODE_INVALID_RESPONSE:B = -0x1t

.field public static final ERROR_CODE_IO_TIMED_OUT:B = -0x3t

.field public static final ERROR_CODE_VALIDATION_ERROR:B = -0x70t

.field public static final ETX:B = 0x4t

.field public static final GET_BOOTLOADER_VERSION_COMMAND:[B

.field public static final GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND:[B

.field public static final INDEX_COMMAND:I = 0x0

.field public static final INDEX_DATA:I = 0x2

.field public static final INDEX_DATA_LENGTH:I = 0x1

.field public static final MAX_RETRIES:I = 0xa

.field public static final MINIMUM_CHUNK_SIZE:I = 0x10

.field public static final RESET_BOOTLOADER_COMMAND:[B

.field public static final SEND_CHUNK_COMMAND:B = 0x31t

.field public static final START_UPLOAD_COMMAND:[B

.field public static final STX:B = 0x55t

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mChunkBuffer:[B

.field private mPacketBuffer:[B

.field private mSerialPort:Lcom/navdy/hardware/SerialPort;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 28
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->CONNECT_BOOTLOADER_COMMAND:[B

    .line 29
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->RESET_BOOTLOADER_COMMAND:[B

    .line 30
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->GET_BOOTLOADER_VERSION_COMMAND:[B

    .line 31
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND:[B

    .line 32
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->START_UPLOAD_COMMAND:[B

    .line 46
    const-class v0, Lcom/navdy/obd/update/STNBootloaderChannel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    return-void

    .line 28
    :array_0
    .array-data 1
        0x3t
        0x0t
        0x0t
    .end array-data

    .line 29
    :array_1
    .array-data 1
        0x2t
        0x0t
        0x0t
    .end array-data

    .line 30
    :array_2
    .array-data 1
        0x6t
        0x0t
        0x0t
    .end array-data

    .line 31
    :array_3
    .array-data 1
        0xft
        0x0t
        0x0t
    .end array-data

    .line 32
    :array_4
    .array-data 1
        0x30t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Lcom/navdy/hardware/SerialPort;)V
    .locals 0
    .param p1, "port"    # Lcom/navdy/hardware/SerialPort;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mSerialPort:Lcom/navdy/hardware/SerialPort;

    .line 56
    return-void
.end method

.method private clearBuffers()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 346
    iput-object v0, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    .line 347
    iput-object v0, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mPacketBuffer:[B

    .line 348
    return-void
.end method

.method private crc([B)[B
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 313
    invoke-static {p1}, Lcom/navdy/obd/Utility;->calculateCcittCrc([B)I

    move-result v0

    .line 314
    .local v0, "crc":I
    const/4 v2, 0x2

    new-array v1, v2, [B

    .line 316
    .local v1, "crcBytes":[B
    const/4 v2, 0x1

    and-int/lit16 v3, v0, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 318
    const/4 v2, 0x0

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 319
    return-object v1
.end method

.method private parseInt([BI)I
    .locals 4
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 275
    aget-byte v3, p1, p2

    and-int/lit16 v3, v3, 0xff

    int-to-short v0, v3

    .line 276
    .local v0, "h":S
    add-int/lit8 v3, p2, 0x1

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    int-to-short v1, v3

    .line 277
    .local v1, "l":S
    const/4 v2, 0x0

    .line 278
    .local v2, "val":I
    or-int/2addr v2, v0

    .line 279
    shl-int/lit8 v2, v2, 0x8

    .line 280
    or-int/2addr v2, v1

    .line 281
    return v2
.end method

.method private readByte()B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 285
    const/4 v3, 0x0

    .line 286
    .local v3, "tries":I
    const/4 v1, 0x0

    .line 288
    .local v1, "interrupted":Z
    :cond_0
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 289
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mSerialPort:Lcom/navdy/hardware/SerialPort;

    invoke-virtual {v4, v0}, Lcom/navdy/hardware/SerialPort;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 290
    .local v2, "result":I
    if-gez v2, :cond_1

    .line 291
    new-instance v4, Ljava/io/IOException;

    const-string v5, "EOF"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 292
    :cond_1
    if-nez v2, :cond_3

    .line 293
    sget-object v4, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    const-string v5, "Read timed out, retrying"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    add-int/lit8 v3, v3, 0x1

    const/16 v4, 0xa

    if-ge v3, v4, :cond_2

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    :cond_2
    if-eqz v1, :cond_4

    .line 300
    new-instance v4, Ljava/io/InterruptedIOException;

    invoke-direct {v4}, Ljava/io/InterruptedIOException;-><init>()V

    throw v4

    .line 295
    :cond_3
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    return v4

    .line 302
    :cond_4
    new-instance v4, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v4}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v4
.end method


# virtual methods
.method public enterBootloaderMode()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->clearBuffers()V

    .line 66
    :try_start_0
    sget-object v2, Lcom/navdy/obd/update/STNBootloaderChannel;->CONNECT_BOOTLOADER_COMMAND:[B

    invoke-virtual {p0, v2}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 67
    invoke-virtual {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->read()[B

    move-result-object v0

    .line 68
    .local v0, "data":[B
    sget-object v2, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    const-string v3, "Entered boot mode"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    const/4 v2, 0x1

    .line 73
    .end local v0    # "data":[B
    :goto_0
    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "er":Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
    sget-object v2, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFirmwareStatus()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    sget-object v1, Lcom/navdy/obd/update/STNBootloaderChannel;->GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND:[B

    invoke-virtual {p0, v1}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 94
    invoke-virtual {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->read()[B

    move-result-object v0

    .line 95
    .local v0, "data":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x2

    aget-byte v1, v0, v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVersion()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    sget-object v1, Lcom/navdy/obd/update/STNBootloaderChannel;->GET_BOOTLOADER_VERSION_COMMAND:[B

    invoke-virtual {p0, v1}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 83
    invoke-virtual {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->read()[B

    move-result-object v0

    .line 84
    .local v0, "response":[B
    return-object v0
.end method

.method public read()[B
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->readByte()B

    move-result v9

    .line 207
    .local v9, "firstByte":B
    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->readByte()B

    move-result v13

    .line 208
    .local v13, "secondByte":B
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "First byte :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", Second byte :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/16 v15, 0x55

    if-ne v9, v15, :cond_6

    const/16 v15, 0x55

    if-ne v13, v15, :cond_6

    .line 212
    const/16 v15, 0x105

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 213
    .local v11, "output":Ljava/nio/ByteBuffer;
    const/4 v8, 0x0

    .line 215
    .local v8, "escaped":Z
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->readByte()B

    move-result v1

    .local v1, "b":B
    const/4 v15, 0x4

    if-ne v1, v15, :cond_0

    if-eqz v8, :cond_2

    .line 216
    :cond_0
    const/4 v15, 0x5

    if-ne v1, v15, :cond_1

    if-nez v8, :cond_1

    .line 217
    const/4 v8, 0x1

    goto :goto_0

    .line 219
    :cond_1
    const/4 v8, 0x0

    .line 220
    invoke-virtual {v11, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    .end local v1    # "b":B
    .end local v8    # "escaped":Z
    .end local v9    # "firstByte":B
    .end local v11    # "output":Ljava/nio/ByteBuffer;
    .end local v13    # "secondByte":B
    :catch_0
    move-exception v14

    .line 261
    .local v14, "te":Ljava/util/concurrent/TimeoutException;
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    const-string v16, "IO Timed out"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    new-instance v15, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;

    const/16 v16, -0x3

    invoke-direct/range {v15 .. v16}, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;-><init>(S)V

    throw v15

    .line 223
    .end local v14    # "te":Ljava/util/concurrent/TimeoutException;
    .restart local v1    # "b":B
    .restart local v8    # "escaped":Z
    .restart local v9    # "firstByte":B
    .restart local v11    # "output":Ljava/nio/ByteBuffer;
    .restart local v13    # "secondByte":B
    :cond_2
    :try_start_1
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 224
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v15

    and-int/lit16 v15, v15, 0xff

    int-to-short v2, v15

    .line 225
    .local v2, "command":S
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Command "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    if-lez v2, :cond_5

    and-int/lit8 v15, v2, 0x40

    if-eqz v15, :cond_5

    .line 227
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    const-string v16, "Command ACK"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v15

    and-int/lit16 v15, v15, 0xff

    int-to-short v6, v15

    .line 230
    .local v6, "dataLength":S
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Data length "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    if-nez v6, :cond_4

    .line 232
    const/4 v12, 0x0

    .line 244
    :cond_3
    return-object v12

    .line 235
    :cond_4
    add-int/lit8 v15, v6, 0x2

    new-array v12, v15, [B

    .line 236
    .local v12, "outputData":[B
    const/4 v15, 0x0

    and-int/lit16 v0, v2, 0xff

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v12, v15

    .line 237
    const/4 v15, 0x1

    and-int/lit16 v0, v6, 0xff

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v12, v15

    .line 238
    const/4 v15, 0x2

    invoke-virtual {v11, v12, v15, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 239
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 240
    .local v3, "crcH":B
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    .line 241
    .local v4, "crcL":B
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/navdy/obd/update/STNBootloaderChannel;->crc([B)[B

    move-result-object v5

    .line 242
    .local v5, "dataCrc":[B
    const/4 v15, 0x0

    aget-byte v15, v5, v15

    if-eq v15, v3, :cond_3

    const/4 v15, 0x1

    aget-byte v15, v5, v15

    if-eq v15, v4, :cond_3

    .line 247
    new-instance v15, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;

    const/16 v16, -0x2

    invoke-direct/range {v15 .. v16}, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;-><init>(S)V

    throw v15

    .line 251
    .end local v3    # "crcH":B
    .end local v4    # "crcL":B
    .end local v5    # "dataCrc":[B
    .end local v6    # "dataLength":S
    .end local v12    # "outputData":[B
    :cond_5
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    const-string v16, "Command NACK"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v15

    int-to-short v10, v15

    .line 253
    .local v10, "length":S
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v15

    and-int/lit16 v15, v15, 0xff

    int-to-short v7, v15

    .line 254
    .local v7, "errorCode":S
    sget-object v15, Lcom/navdy/obd/update/STNBootloaderChannel;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Error code "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v15, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;

    invoke-direct {v15, v7}, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;-><init>(S)V

    throw v15

    .line 258
    .end local v1    # "b":B
    .end local v2    # "command":S
    .end local v7    # "errorCode":S
    .end local v8    # "escaped":Z
    .end local v10    # "length":S
    .end local v11    # "output":Ljava/nio/ByteBuffer;
    :cond_6
    new-instance v15, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;

    const/16 v16, -0x1

    invoke-direct/range {v15 .. v16}, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;-><init>(S)V

    throw v15
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public reset()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->clearBuffers()V

    .line 152
    sget-object v0, Lcom/navdy/obd/update/STNBootloaderChannel;->RESET_BOOTLOADER_COMMAND:[B

    invoke-virtual {p0, v0}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public sendChunk(I[B)I
    .locals 7
    .param p1, "chunkNum"    # I
    .param p2, "chunk"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v6, 0xffff

    .line 127
    array-length v2, p2

    .line 128
    .local v2, "chunkLen":I
    add-int/lit8 v4, v2, 0x5

    .line 130
    .local v4, "dataLength":I
    iget-object v5, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    array-length v5, v5

    if-eq v5, v4, :cond_1

    .line 131
    :cond_0
    new-array v5, v4, [B

    iput-object v5, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    .line 133
    :cond_1
    iget-object v5, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 134
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/16 v5, 0x31

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 135
    add-int/lit8 v5, v2, 0x2

    and-int/2addr v5, v6

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 136
    and-int v5, p1, v6

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 137
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 138
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 139
    iget-object v5, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mChunkBuffer:[B

    invoke-virtual {p0, v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 140
    invoke-virtual {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->read()[B

    move-result-object v3

    .line 141
    .local v3, "data":[B
    const/4 v5, 0x2

    invoke-direct {p0, v3, v5}, Lcom/navdy/obd/update/STNBootloaderChannel;->parseInt([BI)I

    move-result v1

    .line 142
    .local v1, "chunkId":I
    return v1
.end method

.method public startUpload(I)I
    .locals 5
    .param p1, "imageSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 106
    sget-object v2, Lcom/navdy/obd/update/STNBootloaderChannel;->START_UPLOAD_COMMAND:[B

    array-length v2, v2

    new-array v0, v2, [B

    .line 107
    .local v0, "command":[B
    sget-object v2, Lcom/navdy/obd/update/STNBootloaderChannel;->START_UPLOAD_COMMAND:[B

    sget-object v3, Lcom/navdy/obd/update/STNBootloaderChannel;->START_UPLOAD_COMMAND:[B

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    const v2, 0xffffff

    and-int/2addr p1, v2

    .line 109
    const/4 v2, 0x3

    shr-int/lit8 v3, p1, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 110
    const/4 v2, 0x4

    shr-int/lit8 v3, p1, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 111
    const/4 v2, 0x5

    and-int/lit16 v3, p1, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 112
    invoke-virtual {p0, v0}, Lcom/navdy/obd/update/STNBootloaderChannel;->write([B)V

    .line 113
    invoke-virtual {p0}, Lcom/navdy/obd/update/STNBootloaderChannel;->read()[B

    move-result-object v1

    .line 114
    .local v1, "data":[B
    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/navdy/obd/update/STNBootloaderChannel;->parseInt([BI)I

    move-result v2

    return v2
.end method

.method public write([B)V
    .locals 11
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x1

    const/16 v8, 0x55

    const/4 v5, 0x0

    const/4 v7, 0x5

    .line 163
    if-eqz p1, :cond_9

    array-length v4, p1

    if-lez v4, :cond_9

    .line 164
    array-length v4, p1

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v3, v4, 0x5

    .line 165
    .local v3, "packetLength":I
    iget-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mPacketBuffer:[B

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mPacketBuffer:[B

    array-length v4, v4

    if-eq v4, v3, :cond_1

    .line 166
    :cond_0
    new-array v4, v3, [B

    iput-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mPacketBuffer:[B

    .line 168
    :cond_1
    iget-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mPacketBuffer:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 169
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 170
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 172
    array-length v6, p1

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_4

    aget-byte v2, p1, v4

    .line 173
    .local v2, "input":B
    if-eq v2, v8, :cond_2

    if-eq v2, v7, :cond_2

    if-ne v2, v10, :cond_3

    .line 174
    :cond_2
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 175
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 172
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 177
    :cond_3
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 181
    .end local v2    # "input":B
    :cond_4
    invoke-direct {p0, p1}, Lcom/navdy/obd/update/STNBootloaderChannel;->crc([B)[B

    move-result-object v1

    .line 182
    .local v1, "crcBytes":[B
    aget-byte v4, v1, v5

    if-eq v4, v8, :cond_5

    aget-byte v4, v1, v5

    if-eq v4, v7, :cond_5

    aget-byte v4, v1, v5

    if-ne v4, v10, :cond_6

    .line 183
    :cond_5
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 185
    :cond_6
    aget-byte v4, v1, v5

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 186
    aget-byte v4, v1, v9

    if-eq v4, v8, :cond_7

    aget-byte v4, v1, v9

    if-eq v4, v7, :cond_7

    aget-byte v4, v1, v9

    if-ne v4, v10, :cond_8

    .line 187
    :cond_7
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 189
    :cond_8
    aget-byte v4, v1, v9

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 191
    invoke-virtual {v0, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 192
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 193
    iget-object v4, p0, Lcom/navdy/obd/update/STNBootloaderChannel;->mSerialPort:Lcom/navdy/hardware/SerialPort;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Lcom/navdy/hardware/SerialPort;->write(Ljava/nio/ByteBuffer;I)V

    .line 195
    .end local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v1    # "crcBytes":[B
    .end local v3    # "packetLength":I
    :cond_9
    return-void
.end method
