.class Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;
.super Ljava/lang/Error;
.source "STNBootloaderChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/update/STNBootloaderChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BootLoaderMessageError"
.end annotation


# instance fields
.field errorCode:S


# direct methods
.method public constructor <init>(S)V
    .locals 1
    .param p1, "errorCode"    # S

    .prologue
    .line 326
    const-string v0, "NACK"

    invoke-direct {p0, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    .line 327
    iput-short p1, p0, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;->errorCode:S

    .line 328
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 332
    const-string v0, "Bootloader error"

    .line 333
    .local v0, "errorMessage":Ljava/lang/String;
    iget-short v1, p0, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;->errorCode:S

    packed-switch v1, :pswitch_data_0

    .line 341
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bootloader E: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/navdy/obd/update/STNBootloaderChannel$BootLoaderMessageError;->errorCode:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 335
    :pswitch_0
    const-string v0, "Invalid response from bootloader"

    .line 336
    goto :goto_0

    .line 338
    :pswitch_1
    const-string v0, "Invalid CRC in the response"

    goto :goto_0

    .line 333
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
