.class Lcom/navdy/obd/ObdService$5;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->monitorCheckEngineLight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;

.field final synthetic val$command:Lcom/navdy/obd/command/CheckDTCCommand;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/CheckDTCCommand;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 958
    iput-object p1, p0, Lcom/navdy/obd/ObdService$5;->this$0:Lcom/navdy/obd/ObdService;

    iput-object p2, p0, Lcom/navdy/obd/ObdService$5;->val$command:Lcom/navdy/obd/command/CheckDTCCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 3
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 960
    if-eqz p2, :cond_36

    .line 961
    iget-object v0, p0, Lcom/navdy/obd/ObdService$5;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/ObdService$5;->val$command:Lcom/navdy/obd/command/CheckDTCCommand;

    invoke-virtual {v1}, Lcom/navdy/obd/command/CheckDTCCommand;->isCheckEngineLightOn()Z

    move-result v1

    iput-boolean v1, v0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    .line 962
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CEL: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/obd/ObdService$5;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    if-eqz v0, :cond_37

    const-string v0, "On"

    :goto_2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    :cond_36
    return-void

    .line 962
    :cond_37
    const-string v0, "Off"

    goto :goto_2b
.end method
