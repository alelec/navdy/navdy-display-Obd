.class Lcom/navdy/obd/ObdService$6;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->monitorTroubleCodes()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;

.field final synthetic val$command:Lcom/navdy/obd/command/ReadTroubleCodesCommand;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ReadTroubleCodesCommand;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 976
    iput-object p1, p0, Lcom/navdy/obd/ObdService$6;->this$0:Lcom/navdy/obd/ObdService;

    iput-object p2, p0, Lcom/navdy/obd/ObdService$6;->val$command:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 6
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 978
    if-eqz p2, :cond_3e

    .line 979
    const/4 v1, 0x0

    .line 980
    .local v1, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/navdy/obd/ObdService$6;->val$command:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-virtual {v2}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getTroubleCodes()Ljava/util/List;

    move-result-object v1

    .line 981
    if-eqz v1, :cond_3e

    .line 982
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_36

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 983
    .local v0, "troubleCode":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current Trouble Code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 985
    .end local v0    # "troubleCode":Ljava/lang/String;
    :cond_36
    iget-object v2, p0, Lcom/navdy/obd/ObdService$6;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v2

    iput-object v1, v2, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 988
    .end local v1    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3e
    return-void
.end method
