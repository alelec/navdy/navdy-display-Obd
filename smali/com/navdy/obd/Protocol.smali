.class public Lcom/navdy/obd/Protocol;
.super Ljava/lang/Object;
.source "Protocol.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/Protocol$FrameFormat;
    }
.end annotation


# static fields
.field public static final CAN_11_250:I = 0x8

.field public static final CAN_11_250_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final CAN_11_500:I = 0x6

.field public static final CAN_11_500_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final CAN_29_250:I = 0x9

.field public static final CAN_29_250_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final CAN_29_500:I = 0x7

.field public static final CAN_29_500_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final ISO_14230_4_KWP:I = 0x4

.field public static final ISO_14230_4_KWP_FAST:I = 0x5

.field public static final ISO_14230_4_KWP_FAST_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final ISO_14230_4_KWP_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final ISO_9141_2:I = 0x3

.field public static final ISO_9141_2_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static PROTOCOLS:[Lcom/navdy/obd/Protocol; = null

.field public static final SAE_J1850_PWM:I = 0x2

.field public static final SAE_J1850_PWM_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final SAE_J1850_VPW:I = 0x1

.field public static final SAE_J1850_VPW_PROTOCOL:Lcom/navdy/obd/Protocol;

.field public static final SAE_J1939_CAN:I = 0xa

.field public static final SAE_J1939_CAN_PROTOCOL:Lcom/navdy/obd/Protocol;


# instance fields
.field public final format:Lcom/navdy/obd/Protocol$FrameFormat;

.field public final id:I

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x6

    .line 39
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/4 v1, 0x1

    const-string v2, "SAE J1850 PWM"

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->SAE_J1850_VPW_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 42
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/4 v1, 0x2

    const-string v2, "SAE J1850 VPW"

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->SAE_J1850_PWM_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 45
    new-instance v0, Lcom/navdy/obd/Protocol;

    const-string v1, "ISO 9141-2"

    invoke-direct {v0, v6, v1, v4, v3}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->ISO_9141_2_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 49
    new-instance v0, Lcom/navdy/obd/Protocol;

    const-string v1, "ISO 14230-4 KWP"

    invoke-direct {v0, v4, v1, v4, v3}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->ISO_14230_4_KWP_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 51
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/4 v1, 0x5

    const-string v2, "ISO 14230-4 KWP (fast)"

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->ISO_14230_4_KWP_FAST_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 54
    new-instance v0, Lcom/navdy/obd/Protocol;

    const-string v1, "ISO 15765-4 (CAN 11/500)"

    invoke-direct {v0, v3, v1, v7, v6}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->CAN_11_500_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 57
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/4 v1, 0x7

    const-string v2, "ISO 15765-4 (CAN 29/500)"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->CAN_29_500_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 60
    new-instance v0, Lcom/navdy/obd/Protocol;

    const-string v1, "ISO 15765-4 (CAN 11/250)"

    invoke-direct {v0, v5, v1, v7, v6}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->CAN_11_250_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 63
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/16 v1, 0x9

    const-string v2, "ISO 15765-4 (CAN 29/250)"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->CAN_29_250_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 66
    new-instance v0, Lcom/navdy/obd/Protocol;

    const/16 v1, 0xa

    const-string v2, "SAE J1939 (CAN 29/250)"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/obd/Protocol;-><init>(ILjava/lang/String;II)V

    sput-object v0, Lcom/navdy/obd/Protocol;->SAE_J1939_CAN_PROTOCOL:Lcom/navdy/obd/Protocol;

    .line 68
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/navdy/obd/Protocol;

    sget-object v1, Lcom/navdy/obd/Protocol;->SAE_J1850_PWM_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v1, v0, v7

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/obd/Protocol;->SAE_J1850_VPW_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/obd/Protocol;->ISO_9141_2_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/obd/Protocol;->ISO_14230_4_KWP_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/obd/Protocol;->ISO_14230_4_KWP_FAST_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v1, v0, v4

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/obd/Protocol;->CAN_11_500_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/obd/Protocol;->CAN_29_500_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v1, v0, v3

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/obd/Protocol;->CAN_11_250_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/obd/Protocol;->CAN_29_250_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v1, v0, v5

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/obd/Protocol;->SAE_J1939_CAN_PROTOCOL:Lcom/navdy/obd/Protocol;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/obd/Protocol;->PROTOCOLS:[Lcom/navdy/obd/Protocol;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;II)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "ecuStart"    # I
    .param p4, "ecuEnd"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/navdy/obd/Protocol;->id:I

    .line 23
    iput-object p2, p0, Lcom/navdy/obd/Protocol;->name:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/navdy/obd/Protocol$FrameFormat;

    invoke-direct {v0, p3, p4}, Lcom/navdy/obd/Protocol$FrameFormat;-><init>(II)V

    iput-object v0, p0, Lcom/navdy/obd/Protocol;->format:Lcom/navdy/obd/Protocol$FrameFormat;

    .line 25
    return-void
.end method

.method static parse(Ljava/lang/String;)Lcom/navdy/obd/Protocol;
    .locals 5
    .param p0, "protocolNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 83
    const-string v3, "A"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 86
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 87
    const/4 v1, -0x1

    .line 89
    .local v1, "protocolIndex":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 93
    if-lt v1, v4, :cond_1

    sget-object v3, Lcom/navdy/obd/Protocol;->PROTOCOLS:[Lcom/navdy/obd/Protocol;

    array-length v3, v3

    if-le v1, v3, :cond_2

    .line 96
    :cond_1
    :goto_0
    return-object v2

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    sget-object v2, Lcom/navdy/obd/Protocol;->PROTOCOLS:[Lcom/navdy/obd/Protocol;

    add-int/lit8 v3, v1, -0x1

    aget-object v2, v2, v3

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Protocol{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/Protocol;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
