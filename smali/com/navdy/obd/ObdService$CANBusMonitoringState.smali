.class final enum Lcom/navdy/obd/ObdService$CANBusMonitoringState;
.super Ljava/lang/Enum;
.source "ObdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ObdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CANBusMonitoringState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/obd/ObdService$CANBusMonitoringState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/obd/ObdService$CANBusMonitoringState;

.field public static final enum MONITORING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

.field public static final enum SAMPLING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

.field public static final enum UNAVAILABLE:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

.field public static final enum UNKNOWN:Lcom/navdy/obd/ObdService$CANBusMonitoringState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 434
    new-instance v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/ObdService$CANBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->UNKNOWN:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    .line 435
    new-instance v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    const-string v1, "UNAVAILABLE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/obd/ObdService$CANBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->UNAVAILABLE:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    .line 436
    new-instance v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    const-string v1, "SAMPLING"

    invoke-direct {v0, v1, v4}, Lcom/navdy/obd/ObdService$CANBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->SAMPLING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    .line 437
    new-instance v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    const-string v1, "MONITORING"

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/ObdService$CANBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->MONITORING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    .line 433
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    sget-object v1, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->UNKNOWN:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->UNAVAILABLE:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->SAMPLING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->MONITORING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->$VALUES:[Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/obd/ObdService$CANBusMonitoringState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 433
    const-class v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/obd/ObdService$CANBusMonitoringState;
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->$VALUES:[Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    invoke-virtual {v0}, [Lcom/navdy/obd/ObdService$CANBusMonitoringState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    return-object v0
.end method
