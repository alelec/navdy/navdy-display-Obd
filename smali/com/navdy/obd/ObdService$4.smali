.class Lcom/navdy/obd/ObdService$4;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->monitorBatteryVoltage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;

.field final synthetic val$command:Lcom/navdy/obd/command/ReadVoltageCommand;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ReadVoltageCommand;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 923
    iput-object p1, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    iput-object p2, p0, Lcom/navdy/obd/ObdService$4;->val$command:Lcom/navdy/obd/command/ReadVoltageCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 11
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    const-wide/16 v9, 0x0

    const/4 v8, 0x4

    .line 925
    if-eqz p2, :cond_5d

    .line 926
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->val$command:Lcom/navdy/obd/command/ReadVoltageCommand;

    invoke-virtual {v4}, Lcom/navdy/obd/command/ReadVoltageCommand;->getVoltage()F

    move-result v4

    float-to-double v2, v4

    .line 927
    .local v2, "voltage":D
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4, v2, v3}, Lcom/navdy/obd/ObdService;->access$2502(Lcom/navdy/obd/ObdService;D)D

    .line 928
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 929
    .local v0, "now":J
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$3500(Lcom/navdy/obd/ObdService;)J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_2e

    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$3500(Lcom/navdy/obd/ObdService;)J

    move-result-wide v4

    sub-long v4, v0, v4

    sget v6, Lcom/navdy/obd/ObdService;->VOLTAGE_REPORT_INTERVAL:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_4d

    .line 930
    :cond_2e
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Battery voltage: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4, v0, v1}, Lcom/navdy/obd/ObdService;->access$3502(Lcom/navdy/obd/ObdService;J)J

    .line 933
    :cond_4d
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v4

    if-ne v4, v8, :cond_5d

    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$2600(Lcom/navdy/obd/ObdService;)Z

    move-result v4

    if-nez v4, :cond_5e

    .line 946
    .end local v0    # "now":J
    .end local v2    # "voltage":D
    :cond_5d
    :goto_5d
    return-void

    .line 936
    .restart local v0    # "now":J
    .restart local v2    # "voltage":D
    :cond_5e
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$3600(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VoltageSettings;

    move-result-object v4

    iget v4, v4, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    float-to-double v4, v4

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_73

    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$3700(Lcom/navdy/obd/ObdService;)Z

    move-result v4

    if-eqz v4, :cond_5d

    .line 939
    :cond_73
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$3800(Lcom/navdy/obd/ObdService;)J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_90

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v6}, Lcom/navdy/obd/ObdService;->access$3800(Lcom/navdy/obd/ObdService;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {}, Lcom/navdy/obd/ObdService;->access$3900()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_b4

    .line 940
    :cond_90
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Battery now charging: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    iget-object v4, p0, Lcom/navdy/obd/ObdService$4;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v4}, Lcom/navdy/obd/ObdService;->access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_5d

    .line 944
    :cond_b4
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Waiting for voltage to settle: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5d
.end method
