.class Lcom/navdy/obd/ObdService$2;
.super Lcom/navdy/obd/ICarService$Stub;
.source "ObdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ObdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Lcom/navdy/obd/ICarService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addListener(Ljava/util/List;Lcom/navdy/obd/IPidListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/navdy/obd/IPidListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Lcom/navdy/obd/IPidListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/obd/ObdService;->addListener(Ljava/util/List;Lcom/navdy/obd/IPidListener;)V

    .line 265
    return-void
.end method

.method public applyConfiguration(Ljava/lang/String;)Z
    .locals 3
    .param p1, "configuration"    # Ljava/lang/String;

    .prologue
    .line 277
    :try_start_0
    iget-object v2, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2, p1}, Lcom/navdy/obd/ObdService;->access$2200(Lcom/navdy/obd/ObdService;Ljava/lang/String;)V

    .line 278
    iget-object v2, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_a} :catch_c
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_a} :catch_13

    .line 279
    const/4 v2, 0x1

    .line 287
    :goto_b
    return v2

    .line 280
    :catch_c
    move-exception v0

    .line 281
    .local v0, "t":Ljava/lang/IllegalArgumentException;
    iget-object v2, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V

    .line 282
    throw v0

    .line 283
    .end local v0    # "t":Ljava/lang/IllegalArgumentException;
    :catch_13
    move-exception v1

    .line 285
    .local v1, "th":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$2400(Lcom/navdy/obd/ObdService;)V

    .line 287
    const/4 v2, 0x0

    goto :goto_b
.end method

.method public getBatteryVoltage()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2500(Lcom/navdy/obd/ObdService;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getConnectionState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v0

    return v0
.end method

.method public getCurrentConfigurationName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2100(Lcom/navdy/obd/ObdService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEcus()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 240
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/obd/VehicleInfo;->getEcus()Ljava/util/List;

    move-result-object v0

    .line 242
    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public getMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1800(Lcom/navdy/obd/ObdService;)I

    move-result v0

    return v0
.end method

.method public getObdChipFirmwareVersion()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$400(Lcom/navdy/obd/ObdService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/obd/VehicleInfo;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getReadings(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 257
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/jobs/AutoConnect;

    move-result-object v0

    if-nez v0, :cond_a

    .line 258
    const/4 v0, 0x0

    .line 260
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/jobs/AutoConnect;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/obd/jobs/AutoConnect;->getReadings(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_9
.end method

.method public getSupportedPids()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1800(Lcom/navdy/obd/ObdService;)I

    move-result v0

    if-nez v0, :cond_21

    .line 229
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 230
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/obd/VehicleInfo;->getPrimaryEcu()Lcom/navdy/obd/ECU;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v0}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v0

    .line 235
    :goto_20
    return-object v0

    .line 232
    :cond_21
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1800(Lcom/navdy/obd/ObdService;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_38

    invoke-virtual {p0}, Lcom/navdy/obd/ObdService$2;->getConnectionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_38

    .line 233
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v0}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v0

    goto :goto_20

    .line 235
    :cond_38
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public getTroubleCodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 381
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 382
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 384
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public getVIN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 251
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    .line 253
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public isCheckEngineLightOn()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 375
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    .line 377
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public isObdPidsScanningEnabled()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2600(Lcom/navdy/obd/ObdService;)Z

    move-result v0

    return v0
.end method

.method public removeListener(Lcom/navdy/obd/IPidListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/obd/IPidListener;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/ObdService;->removeListener(Lcom/navdy/obd/IPidListener;)V

    .line 269
    return-void
.end method

.method public rescan()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 297
    return-void
.end method

.method public setCANBusMonitoringListener(Lcom/navdy/obd/ICanBusMonitoringListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/obd/ICanBusMonitoringListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0, p1}, Lcom/navdy/obd/ObdService;->access$3002(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/ICanBusMonitoringListener;)Lcom/navdy/obd/ICanBusMonitoringListener;

    .line 360
    return-void
.end method

.method public setMode(IZ)V
    .locals 3
    .param p1, "mode"    # I
    .param p2, "persistent"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 350
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Persistent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    if-eqz p2, :cond_40

    .line 352
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Saving the mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ObdScanMode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 355
    :cond_40
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 356
    return-void
.end method

.method public setObdPidsScanningEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2600(Lcom/navdy/obd/ObdService;)Z

    move-result v0

    if-ne p1, v0, :cond_9

    .line 312
    :goto_8
    return-void

    .line 307
    :cond_9
    if-eqz p1, :cond_16

    .line 308
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_8

    .line 310
    :cond_16
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_8
.end method

.method public setVoltageSettings(Lcom/navdy/obd/VoltageSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/navdy/obd/VoltageSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0, p1}, Lcom/navdy/obd/ObdService;->access$2900(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/VoltageSettings;)V

    .line 332
    return-void
.end method

.method public sleep(Z)V
    .locals 2
    .param p1, "deep"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sleep API is invoked"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0, p1}, Lcom/navdy/obd/ObdService;->access$2702(Lcom/navdy/obd/ObdService;Z)Z

    .line 321
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$2800(Lcom/navdy/obd/ObdService;I)V

    .line 322
    return-void
.end method

.method public startCanBusMonitoring()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$3102(Lcom/navdy/obd/ObdService;Z)Z

    .line 364
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V

    .line 365
    return-void
.end method

.method public stopCanBusMonitoring()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$3102(Lcom/navdy/obd/ObdService;Z)Z

    .line 369
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$3002(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/ICanBusMonitoringListener;)Lcom/navdy/obd/ICanBusMonitoringListener;

    .line 370
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V

    .line 371
    return-void
.end method

.method public updateFirmware(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "updateFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateFirmware , version :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , Path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0, p1}, Lcom/navdy/obd/ObdService;->access$702(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0, p2}, Lcom/navdy/obd/ObdService;->access$802(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 343
    return-void
.end method

.method public updateScan(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V
    .locals 1
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;
    .param p2, "listener"    # Lcom/navdy/obd/IPidListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/obd/ObdService;->addListener(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V

    .line 293
    return-void
.end method

.method public wakeup()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_f

    .line 326
    iget-object v0, p0, Lcom/navdy/obd/ObdService$2;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$2800(Lcom/navdy/obd/ObdService;I)V

    .line 328
    :cond_f
    return-void
.end method
