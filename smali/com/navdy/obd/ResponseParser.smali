.class public Lcom/navdy/obd/ResponseParser;
.super Ljava/lang/Object;
.source "ResponseParser.java"


# static fields
.field private static final MULTI_LINE_CONTINUED:C = '2'

.field private static final MULTI_LINE_START:C = '1'

.field private static final SINGLE_LINE:C = '0'

.field private static groupPattern:Ljava/util/regex/Pattern;

.field private static multiLineResponse:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "^([0-9A-F]{3})\\s?0:"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ResponseParser;->multiLineResponse:Ljava/util/regex/Pattern;

    .line 15
    const-string v0, "([0-9A-F]:)((?:[0-9A-F]{2})+)\\s"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ResponseParser;->groupPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findEcuResponse(ILjava/util/List;)Lcom/navdy/obd/EcuResponse;
    .locals 3
    .param p0, "ecu"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/EcuResponse;",
            ">;)",
            "Lcom/navdy/obd/EcuResponse;"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/EcuResponse;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 135
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/EcuResponse;

    .line 136
    .local v1, "response":Lcom/navdy/obd/EcuResponse;
    iget v2, v1, Lcom/navdy/obd/EcuResponse;->ecu:I

    if-ne v2, p0, :cond_0

    .line 140
    .end local v1    # "response":Lcom/navdy/obd/EcuResponse;
    :goto_1
    return-object v1

    .line 134
    .restart local v1    # "response":Lcom/navdy/obd/EcuResponse;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    .end local v1    # "response":Lcom/navdy/obd/EcuResponse;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static join([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "strings"    # [Ljava/lang/String;

    .prologue
    .line 144
    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    .line 145
    :cond_0
    const-string v2, ""

    .line 152
    :goto_0
    return-object v2

    .line 148
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 150
    aget-object v2, p0, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 152
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static parse(ZLjava/lang/String;Lcom/navdy/obd/Protocol;)[Lcom/navdy/obd/EcuResponse;
    .locals 22
    .param p0, "textResponse"    # Z
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "protocol"    # Lcom/navdy/obd/Protocol;

    .prologue
    .line 31
    sget-object v17, Lcom/navdy/obd/ResponseParser;->multiLineResponse:Ljava/util/regex/Pattern;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 32
    .local v13, "matcher":Ljava/util/regex/Matcher;
    const/16 v16, 0x0

    .line 36
    .local v16, "result":[Lcom/navdy/obd/EcuResponse;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 37
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 38
    .local v2, "byteLength":I
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Lcom/navdy/obd/EcuResponse;

    move-object/from16 v16, v0

    .line 40
    new-array v4, v2, [B

    .line 41
    .local v4, "data":[B
    const/4 v14, 0x0

    .line 42
    .local v14, "offset":I
    sget-object v17, Lcom/navdy/obd/ResponseParser;->groupPattern:Ljava/util/regex/Pattern;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 43
    .local v9, "groupMatcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 44
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 45
    .local v8, "group":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v17

    div-int/lit8 v3, v17, 0x2

    .line 46
    .local v3, "bytes":I
    invoke-static {v8, v4, v14, v3}, Lcom/navdy/obd/util/HexUtil;->parseHexString(Ljava/lang/String;[BII)V

    .line 47
    add-int/2addr v14, v3

    .line 48
    goto :goto_0

    .line 49
    .end local v3    # "bytes":I
    .end local v8    # "group":Ljava/lang/String;
    :cond_0
    const/16 v17, 0x0

    new-instance v18, Lcom/navdy/obd/EcuResponse;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v4}, Lcom/navdy/obd/EcuResponse;-><init>(I[B)V

    aput-object v18, v16, v17

    .line 94
    .end local v2    # "byteLength":I
    .end local v4    # "data":[B
    .end local v9    # "groupMatcher":Ljava/util/regex/Matcher;
    .end local v14    # "offset":I
    :cond_1
    :goto_1
    return-object v16

    .line 50
    :cond_2
    if-nez p0, :cond_1

    .line 51
    const-string v17, "\r"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 52
    .local v12, "lines":[Ljava/lang/String;
    if-nez p2, :cond_3

    .line 54
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Lcom/navdy/obd/EcuResponse;

    move-object/from16 v16, v0

    .line 55
    invoke-static {v12}, Lcom/navdy/obd/ResponseParser;->join([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "data":Ljava/lang/String;
    const/16 v17, 0x0

    new-instance v18, Lcom/navdy/obd/EcuResponse;

    const/16 v19, 0x0

    const/16 v20, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v21

    div-int/lit8 v21, v21, 0x2

    invoke-direct/range {v18 .. v21}, Lcom/navdy/obd/EcuResponse;-><init>(IZI)V

    aput-object v18, v16, v17

    .line 57
    const/16 v17, 0x0

    aget-object v17, v16, v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/navdy/obd/EcuResponse;->append(Ljava/lang/String;)V

    goto :goto_1

    .line 59
    .end local v4    # "data":Ljava/lang/String;
    :cond_3
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v15, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/EcuResponse;>;"
    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/navdy/obd/Protocol;->format:Lcom/navdy/obd/Protocol$FrameFormat;

    .line 62
    .local v7, "format":Lcom/navdy/obd/Protocol$FrameFormat;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    array-length v0, v12

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v10, v0, :cond_4

    .line 63
    aget-object v11, v12, v10

    .line 66
    .local v11, "line":Ljava/lang/String;
    iget v0, v7, Lcom/navdy/obd/Protocol$FrameFormat;->ecuStart:I

    move/from16 v17, v0

    iget v0, v7, Lcom/navdy/obd/Protocol$FrameFormat;->ecuEnd:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 69
    .local v5, "ecu":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/navdy/obd/Protocol;->id:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    .line 79
    iget v0, v7, Lcom/navdy/obd/Protocol$FrameFormat;->ecuEnd:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v5, v11, v0, v15}, Lcom/navdy/obd/ResponseParser;->parseCanFrame(ILjava/lang/String;ILjava/util/List;)V

    .line 62
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 75
    :pswitch_0
    iget v0, v7, Lcom/navdy/obd/Protocol$FrameFormat;->ecuEnd:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v5, v11, v0, v15}, Lcom/navdy/obd/ResponseParser;->parseIsoFrame(ILjava/lang/String;ILjava/util/List;)V

    goto :goto_3

    .line 83
    .end local v5    # "ecu":I
    .end local v11    # "line":Ljava/lang/String;
    :cond_4
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_1

    .line 84
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    new-array v0, v0, [Lcom/navdy/obd/EcuResponse;

    move-object/from16 v16, v0

    .line 86
    const/4 v10, 0x0

    :goto_4
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    .line 87
    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/EcuResponse;

    .line 88
    .local v6, "ecuResponse":Lcom/navdy/obd/EcuResponse;
    invoke-virtual {v6}, Lcom/navdy/obd/EcuResponse;->flatten()V

    .line 89
    aput-object v6, v16, v10

    .line 86
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static parseCanFrame(ILjava/lang/String;ILjava/util/List;)V
    .locals 9
    .param p0, "ecu"    # I
    .param p1, "line"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/EcuResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/EcuResponse;>;"
    const/4 v4, 0x1

    .line 107
    add-int/lit8 v6, p2, 0x1

    .end local p2    # "offset":I
    .local v6, "offset":I
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 109
    .local v2, "frameFlag":C
    packed-switch v2, :pswitch_data_0

    .line 129
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "invalid frame flag"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 112
    :pswitch_0
    const/16 v7, 0x31

    if-ne v2, v7, :cond_1

    move v5, v4

    .line 113
    .local v5, "multiline":Z
    :goto_0
    if-eqz v5, :cond_0

    const/4 v4, 0x3

    .line 114
    .local v4, "lenFieldLen":I
    :cond_0
    add-int v7, v6, v4

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 115
    .local v3, "len":I
    add-int p2, v6, v4

    .line 116
    .end local v6    # "offset":I
    .restart local p2    # "offset":I
    new-instance v0, Lcom/navdy/obd/EcuResponse;

    invoke-direct {v0, p0, v5, v3}, Lcom/navdy/obd/EcuResponse;-><init>(IZI)V

    .line 117
    .local v0, "ecuResponse":Lcom/navdy/obd/EcuResponse;
    mul-int/lit8 v7, v3, 0x2

    add-int/2addr v7, p2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {p1, p2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/navdy/obd/EcuResponse;->append(Ljava/lang/String;)V

    .line 118
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    .end local v0    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .end local v3    # "len":I
    .end local v4    # "lenFieldLen":I
    .end local v5    # "multiline":Z
    :goto_1
    return-void

    .line 112
    .end local p2    # "offset":I
    .restart local v6    # "offset":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 121
    :pswitch_1
    add-int/lit8 p2, v6, 0x1

    .line 122
    .end local v6    # "offset":I
    .restart local p2    # "offset":I
    invoke-static {p0, p3}, Lcom/navdy/obd/ResponseParser;->findEcuResponse(ILjava/util/List;)Lcom/navdy/obd/EcuResponse;

    move-result-object v1

    .line 123
    .local v1, "existingResponse":Lcom/navdy/obd/EcuResponse;
    if-nez v1, :cond_2

    .line 124
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "missing starting ecu response"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 126
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/navdy/obd/EcuResponse;->append(Ljava/lang/String;)V

    goto :goto_1

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static parseIsoFrame(ILjava/lang/String;ILjava/util/List;)V
    .locals 2
    .param p0, "ecu"    # I
    .param p1, "line"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/EcuResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p3, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/EcuResponse;>;"
    invoke-static {p0, p3}, Lcom/navdy/obd/ResponseParser;->findEcuResponse(ILjava/util/List;)Lcom/navdy/obd/EcuResponse;

    move-result-object v0

    .line 99
    .local v0, "response":Lcom/navdy/obd/EcuResponse;
    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/navdy/obd/EcuResponse;

    .end local v0    # "response":Lcom/navdy/obd/EcuResponse;
    invoke-direct {v0, p0}, Lcom/navdy/obd/EcuResponse;-><init>(I)V

    .line 101
    .restart local v0    # "response":Lcom/navdy/obd/EcuResponse;
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/obd/EcuResponse;->addFrame(Ljava/lang/String;)V

    .line 104
    return-void
.end method
