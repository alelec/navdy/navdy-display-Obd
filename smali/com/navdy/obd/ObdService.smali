.class public Lcom/navdy/obd/ObdService;
.super Landroid/app/Service;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/io/IChannelSink;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ObdService$ServiceHandler;,
        Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;,
        Lcom/navdy/obd/ObdService$CANBusMonitoringState;
    }
.end annotation


# static fields
.field private static final BATTERY_CHECK_INTERVAL:I = 0x7d0

.field public static final BAUD_RATE:I

.field private static final BAUD_RATE_PROP:Ljava/lang/String; = "persist.sys.obd.baudrate"

.field public static final CAN_BUS_LOGGER:Ljava/lang/String; = "com.navdy.obd.CanBusRaw"

.field public static final CAN_BUS_MONITORING_ENABLED:Z = false

.field public static final CAN_BUS_SAMPLING_INTERVAL:I = 0x3e8

.field private static final DEFAULT_MODE:I

.field private static final DEFAULT_MODE_PROP:Ljava/lang/String; = "persist.sys.obd.mode"

.field public static final DEFAULT_SCAN_INTERVAL:I = 0x1f4

.field public static final GATHER_CAN_BUS_DATA_INTERVAL:I = 0x3e8

.field public static final MESSAGE_CHANGE_MODE:I = 0x9

.field public static final MESSAGE_CHARGING:I = 0x4

.field public static final MESSAGE_DISABLE_OBD_PID_SCAN:I = 0x5

.field public static final MESSAGE_ENABLE_OBD_PID_SCAN:I = 0x6

.field public static final MESSAGE_RESCAN:I = 0x3

.field public static final MESSAGE_RESET_CHANNEL:I = 0x7

.field public static final MESSAGE_STATE_CHANGE:I = 0x1

.field public static final MESSAGE_TOAST:I = 0x2

.field public static final MESSAGE_UPDATE_FIRMWARE:I = 0x8

.field public static final MODE_ONE_READ_RESPONSE:I = 0x41

.field public static final MONITOR_CAN_BUS_BATCH_INTERVAL:I = 0x7530

.field public static final MONITOR_CAN_BUS_BATCH_SIZE:I = 0x5

.field public static final OBD_DEBUG_MODE:Ljava/lang/String; = "persist.sys.obd.debug"

.field public static final OVERRIDE_PROTOCOL:Ljava/lang/String; = "persist.sys.obd.protocol"

.field public static final PREFS_FILE_OBD_DEVICE:Ljava/lang/String; = "ObdDevice"

.field public static final PREFS_KEY_DEFAULT_CHANNEL_INFO:Ljava/lang/String; = "DefaultChannelInfo"

.field public static final PREFS_KEY_LAST_PROTOCOL:Ljava/lang/String; = "Protocol"

.field public static final PREFS_KEY_OBD_SCAN_ENABLED:Ljava/lang/String; = "ObdCommunicationEnabled"

.field public static final PREFS_KEY_OBD_SCAN_MODE:Ljava/lang/String; = "ObdScanMode"

.field public static final PREFS_LAST_CONNECTED_BAUD_RATE:Ljava/lang/String; = "last_connected_baud_rate"

.field private static RECONNECT_DELAY:J = 0x0L

.field private static final SCAN_INTERVAL:I = 0xa

.field private static final TAG:Ljava/lang/String;

.field public static final TOAST:Ljava/lang/String; = "toast"

.field public static final VOLTAGE_FREQUENCY_PROPERTY:Ljava/lang/String; = "persist.sys.obd.volt.freq"

.field public static final VOLTAGE_REPORT_INTERVAL:I

.field public static final VOLTAGE_THRESHOLD_MINIMUM_TIME_IN_SECONDS:I = 0xa

.field private static obdService:Lcom/navdy/obd/ObdService;


# instance fields
.field private apiEndpoint:Lcom/navdy/obd/IObdService$Stub;

.field private autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

.field private canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

.field private canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

.field private carApiEndpoint:Lcom/navdy/obd/ICarService$Stub;

.field private celMonitoring:Ljava/util/concurrent/ScheduledFuture;

.field private channel:Lcom/navdy/obd/io/IChannel;

.field private channelInfo:Lcom/navdy/obd/io/ChannelInfo;

.field private clear0401:Z

.field private configurationName:Ljava/lang/String;

.field private connectionState:I

.field private currentBatteryVoltage:D

.field private currentFirmwareVersion:Ljava/lang/String;

.field private currentMode:I

.field private currentScan:Lcom/navdy/obd/ObdScanJob;

.field private currentSchedule:Ljava/util/concurrent/ScheduledFuture;

.field private debugMode:Z

.field private deepSleep:Z

.field private dtcClearCommand:Ljava/lang/String;

.field private firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

.field private firmwareUpdatePath:Ljava/lang/String;

.field private firmwareUpdateVersion:Ljava/lang/String;

.field private gatherCanBusDataToLogs:Z

.field private volatile isScanningEnabled:Z

.field private j1939Profile:Lcom/navdy/obd/j1939/J1939Profile;

.field private volatile lastConnectedBaudRate:I

.field private lastDisconnectTime:J

.field private lastMonitorBatchCompletedTime:J

.field private lastVoltageReport:J

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/IObdServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mSharedPrefs:Landroid/content/SharedPreferences;

.field private mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

.field private monitorCounter:I

.field public monitoringCanBusLimitReached:Z

.field private obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

.field private onMonitoringStatusReported:Z

.field private pendingMonitoring:Ljava/util/concurrent/ScheduledFuture;

.field private final pidListeners:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/navdy/obd/PidListener;",
            ">;"
        }
    .end annotation
.end field

.field private profile:Lcom/navdy/obd/Profile;

.field private protocol:Lcom/navdy/obd/Protocol;

.field private reconnectImmediately:Z

.field private final scheduler:Ljava/util/concurrent/ScheduledExecutorService;

.field private serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

.field private serviceLooper:Landroid/os/Looper;

.field private sleepWithWakeUpTriggersCommand:Lcom/navdy/obd/command/BatchCommand;

.field private troubleMonitoring:Ljava/util/concurrent/ScheduledFuture;

.field private vehicleInfo:Lcom/navdy/obd/VehicleInfo;

.field private voltageMonitoring:Ljava/util/concurrent/ScheduledFuture;

.field private voltageSettings:Lcom/navdy/obd/VoltageSettings;

.field private final voltageSettingsLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const-string v0, "persist.sys.obd.baudrate"

    const v1, 0x70800

    invoke-static {v0, v1}, Lcom/navdy/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/navdy/obd/ObdService;->BAUD_RATE:I

    .line 74
    const-string v0, "persist.sys.obd.mode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/navdy/obd/ObdService;->DEFAULT_MODE:I

    .line 97
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/obd/ObdService;->RECONNECT_DELAY:J

    .line 99
    const-class v0, Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    .line 102
    const-string v0, "persist.sys.obd.volt.freq"

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Lcom/navdy/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/navdy/obd/ObdService;->VOLTAGE_REPORT_INTERVAL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 510
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 105
    new-instance v0, Lcom/navdy/obd/ObdService$1;

    invoke-direct {v0, p0}, Lcom/navdy/obd/ObdService$1;-><init>(Lcom/navdy/obd/ObdService;)V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->apiEndpoint:Lcom/navdy/obd/IObdService$Stub;

    .line 221
    new-instance v0, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    invoke-direct {v0}, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    .line 222
    new-instance v0, Lcom/navdy/obd/ObdService$2;

    invoke-direct {v0, p0}, Lcom/navdy/obd/ObdService$2;-><init>(Lcom/navdy/obd/ObdService;)V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->carApiEndpoint:Lcom/navdy/obd/ICarService$Stub;

    .line 390
    iput v2, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    .line 391
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/navdy/obd/ObdService;->currentBatteryVoltage:D

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->currentFirmwareVersion:Ljava/lang/String;

    .line 393
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    .line 397
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->deepSleep:Z

    .line 398
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->clear0401:Z

    .line 399
    const-string v0, "04"

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->dtcClearCommand:Ljava/lang/String;

    .line 403
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    .line 406
    iput v2, p0, Lcom/navdy/obd/ObdService;->lastConnectedBaudRate:I

    .line 407
    iput-wide v3, p0, Lcom/navdy/obd/ObdService;->lastDisconnectTime:J

    .line 408
    iput-wide v3, p0, Lcom/navdy/obd/ObdService;->lastMonitorBatchCompletedTime:J

    .line 410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    .line 413
    iput v2, p0, Lcom/navdy/obd/ObdService;->monitorCounter:I

    .line 414
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    .line 416
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->onMonitoringStatusReported:Z

    .line 417
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    .line 420
    iput-boolean v2, p0, Lcom/navdy/obd/ObdService;->reconnectImmediately:Z

    .line 421
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 431
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->voltageSettingsLock:Ljava/lang/Object;

    .line 511
    new-instance v0, Lcom/navdy/obd/VoltageSettings;

    invoke-direct {v0}, Lcom/navdy/obd/VoltageSettings;-><init>()V

    invoke-direct {p0, v0}, Lcom/navdy/obd/ObdService;->setVoltageSettings(Lcom/navdy/obd/VoltageSettings;)V

    .line 512
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/obd/ObdService;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Lcom/navdy/obd/command/ICommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdScanJob;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->currentScan:Lcom/navdy/obd/ObdScanJob;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/ObdScanJob;)Lcom/navdy/obd/ObdScanJob;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Lcom/navdy/obd/ObdScanJob;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->currentScan:Lcom/navdy/obd/ObdScanJob;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/Protocol;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/obd/ObdService;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    return v0
.end method

.method static synthetic access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/io/IChannel;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/jobs/AutoConnect;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/navdy/obd/ObdService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->configurationName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/obd/ObdService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->applyConfigurationInternal(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->updateScan()V

    return-void
.end method

.method static synthetic access$2400(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->applyFallbackConfiguration()V

    return-void
.end method

.method static synthetic access$2500(Lcom/navdy/obd/ObdService;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/obd/ObdService;->currentBatteryVoltage:D

    return-wide v0
.end method

.method static synthetic access$2502(Lcom/navdy/obd/ObdService;D)D
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/obd/ObdService;->currentBatteryVoltage:D

    return-wide p1
.end method

.method static synthetic access$2600(Lcom/navdy/obd/ObdService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/navdy/obd/ObdService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    return p1
.end method

.method static synthetic access$2702(Lcom/navdy/obd/ObdService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/obd/ObdService;->deepSleep:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/navdy/obd/ObdService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->postStateChange(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/VoltageSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Lcom/navdy/obd/VoltageSettings;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->setVoltageSettings(Lcom/navdy/obd/VoltageSettings;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/obd/ObdService;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    return v0
.end method

.method static synthetic access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/ICanBusMonitoringListener;)Lcom/navdy/obd/ICanBusMonitoringListener;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Lcom/navdy/obd/ICanBusMonitoringListener;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/navdy/obd/ObdService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/navdy/obd/ObdService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/navdy/obd/ObdService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->handleStateChange(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/navdy/obd/ObdService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->setMode(I)V

    return-void
.end method

.method static synthetic access$3500(Lcom/navdy/obd/ObdService;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/obd/ObdService;->lastVoltageReport:J

    return-wide v0
.end method

.method static synthetic access$3502(Lcom/navdy/obd/ObdService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/obd/ObdService;->lastVoltageReport:J

    return-wide p1
.end method

.method static synthetic access$3600(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VoltageSettings;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->voltageSettings:Lcom/navdy/obd/VoltageSettings;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/navdy/obd/ObdService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->debugMode:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/navdy/obd/ObdService;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/obd/ObdService;->lastDisconnectTime:J

    return-wide v0
.end method

.method static synthetic access$3900()J
    .locals 2

    .prologue
    .line 66
    sget-wide v0, Lcom/navdy/obd/ObdService;->RECONNECT_DELAY:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/navdy/obd/ObdService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->currentFirmwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/navdy/obd/ObdService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->clear0401:Z

    return v0
.end method

.method static synthetic access$4100(Lcom/navdy/obd/ObdService;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->updateListenersWithNewData(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/navdy/obd/ObdService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->onMonitoringStatusReported:Z

    return v0
.end method

.method static synthetic access$4302(Lcom/navdy/obd/ObdService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/obd/ObdService;->onMonitoringStatusReported:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/navdy/obd/ObdService;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/obd/ObdService;->monitorCounter:I

    return v0
.end method

.method static synthetic access$4402(Lcom/navdy/obd/ObdService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/navdy/obd/ObdService;->monitorCounter:I

    return p1
.end method

.method static synthetic access$4502(Lcom/navdy/obd/ObdService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/obd/ObdService;->lastMonitorBatchCompletedTime:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/update/ObdDeviceFirmwareManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/obd/ObdService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/obd/ObdService;->reconnectImmediately:Z

    return p1
.end method

.method static synthetic access$702(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->firmwareUpdateVersion:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->firmwareUpdatePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    return-void
.end method

.method private applyConfigurationInternal(Ljava/lang/String;)V
    .locals 2
    .param p1, "configuration"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 702
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Applying device configuration"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Executing the write configuration command"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    new-instance v0, Lcom/navdy/obd/command/WriteConfigurationCommand;

    invoke-direct {v0, p1}, Lcom/navdy/obd/command/WriteConfigurationCommand;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 705
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->resetChannel()V

    .line 706
    return-void
.end method

.method private applyFallbackConfiguration()V
    .locals 6

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 688
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/navdy/obd/Utility;->convertInputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 690
    .local v0, "configuration":Ljava/lang/String;
    :try_start_f
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_12} :catch_16

    .line 695
    :goto_12
    :try_start_12
    invoke-direct {p0, v0}, Lcom/navdy/obd/ObdService;->applyConfigurationInternal(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_15} :catch_1f

    .line 699
    :goto_15
    return-void

    .line 691
    :catch_16
    move-exception v1

    .line 692
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v5, "Error closing the input stream while reading from asset file "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_12

    .line 696
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1f
    move-exception v3

    .line 697
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v5, "Critical error, failed to apply fallback configuration"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_15
.end method

.method static build(Lcom/navdy/obd/Protocol;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;)Lcom/navdy/obd/VehicleInfo;
    .locals 6
    .param p0, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p2, "vin"    # Ljava/lang/String;
    .param p3, "isScanningEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/obd/Protocol;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/obd/VehicleInfo;"
        }
    .end annotation

    .prologue
    .line 1477
    .local p1, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .local p4, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/navdy/obd/VehicleInfo;

    if-eqz p0, :cond_e

    iget-object v1, p0, Lcom/navdy/obd/Protocol;->name:Ljava/lang/String;

    :goto_6
    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/VehicleInfo;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;)V

    return-object v0

    :cond_e
    const-string v1, ""

    goto :goto_6
.end method

.method public static buildEcus(Lcom/navdy/obd/command/ValidPidsCommand;)Ljava/util/List;
    .locals 15
    .param p0, "myCommand"    # Lcom/navdy/obd/command/ValidPidsCommand;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/obd/command/ValidPidsCommand;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v14, 0x0

    .line 1447
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 1448
    .local v5, "ecusByAddress":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/navdy/obd/ECU;>;"
    const/4 v7, 0x0

    .line 1450
    .local v7, "offset":I
    invoke-virtual {p0}, Lcom/navdy/obd/command/ValidPidsCommand;->supportedPids()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_f
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_cf

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/command/ObdCommand;

    .line 1451
    .local v2, "command":Lcom/navdy/obd/command/ObdCommand;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1c
    invoke-virtual {v2}, Lcom/navdy/obd/command/ObdCommand;->getResponseCount()I

    move-result v10

    if-ge v6, v10, :cond_cb

    .line 1452
    invoke-virtual {v2, v6}, Lcom/navdy/obd/command/ObdCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v8

    .line 1453
    .local v8, "response":Lcom/navdy/obd/EcuResponse;
    iget v0, v8, Lcom/navdy/obd/EcuResponse;->ecu:I

    .line 1454
    .local v0, "address":I
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/obd/ECU;

    .line 1455
    .local v3, "ecu":Lcom/navdy/obd/ECU;
    if-nez v3, :cond_3d

    .line 1456
    new-instance v3, Lcom/navdy/obd/ECU;

    .end local v3    # "ecu":Lcom/navdy/obd/ECU;
    new-instance v10, Lcom/navdy/obd/PidSet;

    invoke-direct {v10}, Lcom/navdy/obd/PidSet;-><init>()V

    invoke-direct {v3, v0, v10}, Lcom/navdy/obd/ECU;-><init>(ILcom/navdy/obd/PidSet;)V

    .line 1457
    .restart local v3    # "ecu":Lcom/navdy/obd/ECU;
    invoke-virtual {v5, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1459
    :cond_3d
    iget-object v10, v8, Lcom/navdy/obd/EcuResponse;->data:[B

    aget-byte v10, v10, v14

    const/16 v11, 0x41

    if-ne v10, v11, :cond_4a

    iget v10, v8, Lcom/navdy/obd/EcuResponse;->length:I

    const/4 v11, 0x6

    if-ge v10, v11, :cond_7b

    .line 1460
    :cond_4a
    sget-object v10, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Skipping "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/navdy/obd/EcuResponse;->data:[B

    aget-byte v12, v12, v14

    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " response for ecu "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1451
    :goto_78
    add-int/lit8 v6, v6, 0x1

    goto :goto_1c

    .line 1462
    :cond_7b
    iget-object v10, v8, Lcom/navdy/obd/EcuResponse;->data:[B

    const/4 v11, 0x2

    const/4 v12, 0x4

    invoke-static {v10, v11, v12}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1463
    .local v1, "bitField":I
    sget-object v10, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Merging "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " for ecu "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " at offset "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1464
    iget-object v10, v3, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    new-instance v11, Lcom/navdy/obd/PidSet;

    int-to-long v12, v1

    invoke-direct {v11, v12, v13, v7}, Lcom/navdy/obd/PidSet;-><init>(JI)V

    invoke-virtual {v10, v11}, Lcom/navdy/obd/PidSet;->merge(Lcom/navdy/obd/PidSet;)V

    goto :goto_78

    .line 1467
    .end local v0    # "address":I
    .end local v1    # "bitField":I
    .end local v3    # "ecu":Lcom/navdy/obd/ECU;
    .end local v8    # "response":Lcom/navdy/obd/EcuResponse;
    :cond_cb
    add-int/lit8 v7, v7, 0x20

    .line 1468
    goto/16 :goto_f

    .line 1469
    .end local v2    # "command":Lcom/navdy/obd/command/ObdCommand;
    .end local v6    # "i":I
    :cond_cf
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1470
    .local v4, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_d5
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v6, v9, :cond_e5

    .line 1471
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1470
    add-int/lit8 v6, v6, 0x1

    goto :goto_d5

    .line 1473
    :cond_e5
    return-object v4
.end method

.method private cancelCurrentSchedule()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 763
    const/4 v1, 0x1

    .line 764
    .local v1, "z":Z
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_13

    .line 765
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    .line 766
    .local v0, "scheduledFuture":Ljava/util/concurrent/ScheduledFuture;
    iget v2, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_e

    .line 767
    const/4 v1, 0x0

    .line 769
    :cond_e
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 770
    iput-object v4, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    .line 772
    .end local v0    # "scheduledFuture":Ljava/util/concurrent/ScheduledFuture;
    :cond_13
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->currentScan:Lcom/navdy/obd/ObdScanJob;

    if-eqz v2, :cond_1e

    .line 773
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->currentScan:Lcom/navdy/obd/ObdScanJob;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdScanJob;->cancel()V

    .line 774
    iput-object v4, p0, Lcom/navdy/obd/ObdService;->currentScan:Lcom/navdy/obd/ObdScanJob;

    .line 776
    :cond_1e
    return-void
.end method

.method private cancelDtcMonitoring()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 748
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->celMonitoring:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_d

    .line 749
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->celMonitoring:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 750
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->celMonitoring:Ljava/util/concurrent/ScheduledFuture;

    .line 752
    :cond_d
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->troubleMonitoring:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_18

    .line 753
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->troubleMonitoring:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 754
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->troubleMonitoring:Ljava/util/concurrent/ScheduledFuture;

    .line 756
    :cond_18
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->pendingMonitoring:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_23

    .line 757
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->pendingMonitoring:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 758
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->pendingMonitoring:Ljava/util/concurrent/ScheduledFuture;

    .line 760
    :cond_23
    return-void
.end method

.method private cancelVoltageMonitoring()V
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->voltageMonitoring:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_d

    .line 741
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->voltageMonitoring:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->voltageMonitoring:Ljava/util/concurrent/ScheduledFuture;

    .line 744
    :cond_d
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/navdy/obd/ObdService;->currentBatteryVoltage:D

    .line 745
    return-void
.end method

.method private dump(Lcom/navdy/obd/VehicleInfo;)V
    .locals 7
    .param p1, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;

    .prologue
    .line 1426
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v4, "Vehicle Info:"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Protocol:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/obd/VehicleInfo;->protocol:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1428
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "VIN:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1429
    iget-object v3, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    if-eqz v3, :cond_c3

    .line 1430
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ecus:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    invoke-virtual {p1}, Lcom/navdy/obd/VehicleInfo;->getPrimaryEcu()Lcom/navdy/obd/ECU;

    move-result-object v1

    .line 1432
    .local v1, "primaryEcu":Lcom/navdy/obd/ECU;
    iget-object v3, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_67
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/ECU;

    .line 1433
    .local v0, "ecu":Lcom/navdy/obd/ECU;
    sget-object v5, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  ecu: 0x"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v6, v0, Lcom/navdy/obd/ECU;->address:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-ne v1, v0, :cond_c0

    const-string v3, " PRIMARY"

    :goto_8e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1434
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "     : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v6}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_67

    .line 1433
    :cond_c0
    const-string v3, ""

    goto :goto_8e

    .line 1437
    .end local v0    # "ecu":Lcom/navdy/obd/ECU;
    .end local v1    # "primaryEcu":Lcom/navdy/obd/ECU;
    :cond_c3
    sget-object v4, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Check Engine light : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v3, p1, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    if-eqz v3, :cond_104

    const-string v3, "ON"

    :goto_d6
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1438
    iget-object v3, p1, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    if-eqz v3, :cond_107

    .line 1439
    sget-object v3, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v4, "Trouble Codes"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1440
    iget-object v3, p1, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_107

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1441
    .local v2, "troubleCode":Ljava/lang/String;
    sget-object v4, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f2

    .line 1437
    .end local v2    # "troubleCode":Ljava/lang/String;
    :cond_104
    const-string v3, "OFF"

    goto :goto_d6

    .line 1444
    :cond_107
    return-void
.end method

.method public static getObdService()Lcom/navdy/obd/ObdService;
    .locals 1

    .prologue
    .line 1550
    sget-object v0, Lcom/navdy/obd/ObdService;->obdService:Lcom/navdy/obd/ObdService;

    return-object v0
.end method

.method private getSavedProtocol()Lcom/navdy/obd/Protocol;
    .locals 5

    .prologue
    .line 1304
    const-string v1, "persist.sys.obd.protocol"

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "Protocol"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/navdy/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1305
    .local v0, "index":I
    if-lez v0, :cond_18

    add-int/lit8 v1, v0, -0x1

    sget-object v2, Lcom/navdy/obd/Protocol;->PROTOCOLS:[Lcom/navdy/obd/Protocol;

    array-length v2, v2

    if-lt v1, v2, :cond_1a

    .line 1306
    :cond_18
    const/4 v1, 0x0

    .line 1308
    :goto_19
    return-object v1

    :cond_1a
    sget-object v1, Lcom/navdy/obd/Protocol;->PROTOCOLS:[Lcom/navdy/obd/Protocol;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    goto :goto_19
.end method

.method private handleStateChange(I)V
    .locals 9
    .param p1, "state"    # I

    .prologue
    .line 551
    iget v6, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    if-eq p1, v6, :cond_e7

    .line 553
    iput p1, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    .line 554
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Switching to state:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    iget v6, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    packed-switch v6, :pswitch_data_e8

    .line 594
    :goto_23
    :pswitch_23
    iget-object v7, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    monitor-enter v7

    .line 595
    :try_start_26
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_a2

    move-result v6

    add-int/lit8 v5, v6, -0x1

    .local v5, "i":I
    :goto_2e
    if-ltz v5, :cond_ae

    .line 597
    :try_start_30
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/IObdServiceListener;

    invoke-interface {v6, p1}, Lcom/navdy/obd/IObdServiceListener;->onConnectionStateChange(I)V
    :try_end_3b
    .catch Landroid/os/DeadObjectException; {:try_start_30 .. :try_end_3b} :catch_94
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_3b} :catch_a5
    .catchall {:try_start_30 .. :try_end_3b} :catchall_a2

    .line 595
    :goto_3b
    add-int/lit8 v5, v5, -0x1

    goto :goto_2e

    .line 557
    .end local v5    # "i":I
    :pswitch_3e
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 558
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    .line 559
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    .line 560
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->closeChannel()V

    .line 561
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/navdy/obd/ObdService;->lastDisconnectTime:J

    goto :goto_23

    .line 564
    :pswitch_51
    iget-boolean v6, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    if-nez v6, :cond_61

    .line 565
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "handleStateChange : New State is \'connected\' but scanning is not enabled , moving back to \'idle\' state"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    const/4 v6, 0x4

    invoke-direct {p0, v6}, Lcom/navdy/obd/ObdService;->postStateChange(I)V

    goto :goto_23

    .line 569
    :cond_61
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->updateScan()V

    .line 570
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->monitorCheckEngineLight()V

    .line 572
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->monitorPendingCodes()V

    goto :goto_23

    .line 575
    :pswitch_6b
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->resetChannel()V

    goto :goto_23

    .line 578
    :pswitch_6f
    iget-boolean v6, p0, Lcom/navdy/obd/ObdService;->reconnectImmediately:Z

    if-nez v6, :cond_7a

    .line 579
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->resetObdChip()V

    .line 580
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->monitorBatteryVoltage()V

    goto :goto_23

    .line 584
    :cond_7a
    :try_start_7a
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->apiEndpoint:Lcom/navdy/obd/IObdService$Stub;

    iget-object v7, p0, Lcom/navdy/obd/ObdService;->firmwareUpdateVersion:Ljava/lang/String;

    iget-object v8, p0, Lcom/navdy/obd/ObdService;->firmwareUpdatePath:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/navdy/obd/IObdService$Stub;->updateDeviceFirmware(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_83
    .catch Landroid/os/RemoteException; {:try_start_7a .. :try_end_83} :catch_87

    .line 588
    :goto_83
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/navdy/obd/ObdService;->reconnectImmediately:Z

    goto :goto_23

    .line 585
    :catch_87
    move-exception v0

    .line 586
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "Error updating firmware "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_83

    .line 591
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_90
    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->sleepWithVoltageWakeUpTriggers()V

    goto :goto_23

    .line 598
    .restart local v5    # "i":I
    :catch_94
    move-exception v1

    .line 599
    .local v1, "e2":Landroid/os/DeadObjectException;
    :try_start_95
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v8, "Removing dead obd service listener"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3b

    .line 605
    .end local v1    # "e2":Landroid/os/DeadObjectException;
    .end local v5    # "i":I
    :catchall_a2
    move-exception v6

    monitor-exit v7
    :try_end_a4
    .catchall {:try_start_95 .. :try_end_a4} :catchall_a2

    throw v6

    .line 601
    .restart local v5    # "i":I
    :catch_a5
    move-exception v2

    .line 602
    .local v2, "e3":Landroid/os/RemoteException;
    :try_start_a6
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v8, "Failed to notify listener of connection state change"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3b

    .line 605
    .end local v2    # "e3":Landroid/os/RemoteException;
    :cond_ae
    monitor-exit v7
    :try_end_af
    .catchall {:try_start_a6 .. :try_end_af} :catchall_a2

    .line 606
    iget-object v7, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    monitor-enter v7

    .line 607
    :try_start_b2
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v6}, Landroid/util/ArrayMap;->size()I
    :try_end_b7
    .catchall {:try_start_b2 .. :try_end_b7} :catchall_da

    move-result v6

    add-int/lit8 v5, v6, -0x1

    :goto_ba
    if-ltz v5, :cond_e6

    .line 609
    :try_start_bc
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v6, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/PidListener;

    iget-object v6, v6, Lcom/navdy/obd/PidListener;->listener:Lcom/navdy/obd/IPidListener;

    invoke-interface {v6, p1}, Lcom/navdy/obd/IPidListener;->onConnectionStateChange(I)V
    :try_end_c9
    .catch Landroid/os/DeadObjectException; {:try_start_bc .. :try_end_c9} :catch_cc
    .catch Landroid/os/RemoteException; {:try_start_bc .. :try_end_c9} :catch_dd
    .catchall {:try_start_bc .. :try_end_c9} :catchall_da

    .line 607
    :goto_c9
    add-int/lit8 v5, v5, -0x1

    goto :goto_ba

    .line 610
    :catch_cc
    move-exception v3

    .line 611
    .local v3, "e4":Landroid/os/DeadObjectException;
    :try_start_cd
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v8, "Removing dead pid listener"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v6, v5}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    goto :goto_c9

    .line 617
    .end local v3    # "e4":Landroid/os/DeadObjectException;
    :catchall_da
    move-exception v6

    monitor-exit v7
    :try_end_dc
    .catchall {:try_start_cd .. :try_end_dc} :catchall_da

    throw v6

    .line 613
    :catch_dd
    move-exception v4

    .line 614
    .local v4, "e5":Landroid/os/RemoteException;
    :try_start_de
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v8, "Failed to notify listener of connection state change"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c9

    .line 617
    .end local v4    # "e5":Landroid/os/RemoteException;
    :cond_e6
    monitor-exit v7
    :try_end_e7
    .catchall {:try_start_de .. :try_end_e7} :catchall_da

    .line 619
    .end local v5    # "i":I
    :cond_e7
    return-void

    .line 555
    :pswitch_data_e8
    .packed-switch 0x0
        :pswitch_3e
        :pswitch_23
        :pswitch_51
        :pswitch_6b
        :pswitch_6f
        :pswitch_90
    .end packed-switch
.end method

.method private postStateChange(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    .line 1422
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/navdy/obd/ObdService$ServiceHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1423
    return-void
.end method

.method private setMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 526
    iget v0, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    if-eq p1, v0, :cond_44

    .line 527
    iput p1, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    .line 528
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currentMode "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    if-nez v0, :cond_45

    const-string v0, "OBD2"

    :goto_19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    iget v0, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_44

    iget-object v0, p0, Lcom/navdy/obd/ObdService;->j1939Profile:Lcom/navdy/obd/j1939/J1939Profile;

    if-nez v0, :cond_44

    .line 530
    new-instance v0, Lcom/navdy/obd/j1939/J1939Profile;

    invoke-direct {v0}, Lcom/navdy/obd/j1939/J1939Profile;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->j1939Profile:Lcom/navdy/obd/j1939/J1939Profile;

    .line 531
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->j1939Profile:Lcom/navdy/obd/j1939/J1939Profile;

    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/obd/j1939/J1939Profile;->load(Ljava/io/InputStream;)V

    .line 534
    :cond_44
    return-void

    .line 528
    :cond_45
    const-string v0, "J1939"

    goto :goto_19
.end method

.method private setVoltageSettings(Lcom/navdy/obd/VoltageSettings;)V
    .locals 3
    .param p1, "settings"    # Lcom/navdy/obd/VoltageSettings;

    .prologue
    .line 515
    if-nez p1, :cond_1b

    .line 516
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid voltage settings "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :goto_1a
    return-void

    .line 519
    :cond_1b
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting voltage settings to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->voltageSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 521
    :try_start_36
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->voltageSettings:Lcom/navdy/obd/VoltageSettings;

    .line 522
    monitor-exit v1

    goto :goto_1a

    :catchall_3a
    move-exception v0

    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_36 .. :try_end_3c} :catchall_3a

    throw v0
.end method

.method private synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    .locals 9
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 779
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 780
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    .line 781
    .local v2, "obdChannel":Lcom/navdy/obd/io/IChannel;
    if-nez v2, :cond_10

    .line 782
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Invalid channel - unable to execute command"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    const/4 v0, 0x0

    .line 798
    :goto_f
    return-object v0

    .line 786
    :cond_10
    :try_start_10
    iget-object v8, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/ObdService$3;

    invoke-direct {v4, p0}, Lcom/navdy/obd/ObdService$3;-><init>(Lcom/navdy/obd/ObdService;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 794
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 795
    invoke-interface {p1}, Lcom/navdy/obd/command/ICommand;->getResponse()Ljava/lang/String;
    :try_end_2b
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_2b} :catch_2d
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_10 .. :try_end_2b} :catch_3a

    move-result-object v0

    goto :goto_f

    .line 796
    :catch_2d
    move-exception v6

    .line 797
    .local v6, "e":Ljava/lang/InterruptedException;
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Interrupted while executing command"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 798
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    .line 799
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catch_3a
    move-exception v7

    .line 800
    .local v7, "e2":Ljava/util/concurrent/ExecutionException;
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Error executing command"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 801
    throw v7
.end method

.method private updateListenersWithNewData(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1242
    .local p1, "pidsData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    monitor-enter v6

    .line 1243
    :try_start_3
    iget-object v5, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_25

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/obd/IObdServiceListener;
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_22

    .line 1245
    .local v4, "listener":Lcom/navdy/obd/IObdServiceListener;
    :try_start_15
    invoke-interface {v4, p1}, Lcom/navdy/obd/IObdServiceListener;->scannedPids(Ljava/util/List;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_18} :catch_19
    .catchall {:try_start_15 .. :try_end_18} :catchall_22

    goto :goto_9

    .line 1246
    :catch_19
    move-exception v0

    .line 1247
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1a
    sget-object v7, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v8, "Error notifying listener"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 1250
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v4    # "listener":Lcom/navdy/obd/IObdServiceListener;
    :catchall_22
    move-exception v5

    monitor-exit v6
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_22

    throw v5

    :cond_25
    :try_start_25
    monitor-exit v6
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_22

    .line 1251
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    monitor-enter v6

    .line 1252
    :try_start_29
    iget-object v5, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v5}, Landroid/util/ArrayMap;->size()I
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_4f

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .local v3, "i":I
    :goto_31
    if-ltz v3, :cond_5b

    .line 1254
    :try_start_33
    iget-object v5, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v5, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/obd/PidListener;

    invoke-virtual {v5, p1}, Lcom/navdy/obd/PidListener;->recordReadings(Ljava/util/List;)V
    :try_end_3e
    .catch Landroid/os/DeadObjectException; {:try_start_33 .. :try_end_3e} :catch_41
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_3e} :catch_52
    .catchall {:try_start_33 .. :try_end_3e} :catchall_4f

    .line 1252
    :goto_3e
    add-int/lit8 v3, v3, -0x1

    goto :goto_31

    .line 1255
    :catch_41
    move-exception v1

    .line 1256
    .local v1, "e2":Landroid/os/DeadObjectException;
    :try_start_42
    sget-object v5, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "Removing dead pid listener"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    iget-object v5, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v5, v3}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    goto :goto_3e

    .line 1262
    .end local v1    # "e2":Landroid/os/DeadObjectException;
    .end local v3    # "i":I
    :catchall_4f
    move-exception v5

    monitor-exit v6
    :try_end_51
    .catchall {:try_start_42 .. :try_end_51} :catchall_4f

    throw v5

    .line 1258
    .restart local v3    # "i":I
    :catch_52
    move-exception v2

    .line 1259
    .local v2, "e3":Landroid/os/RemoteException;
    :try_start_53
    sget-object v5, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "Error notifying pid listener"

    invoke-static {v5, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3e

    .line 1262
    .end local v2    # "e3":Landroid/os/RemoteException;
    :cond_5b
    monitor-exit v6
    :try_end_5c
    .catchall {:try_start_53 .. :try_end_5c} :catchall_4f

    .line 1263
    return-void
.end method

.method private updateScan()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/16 v11, 0x3e8

    const/4 v10, 0x0

    .line 828
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "updateScan"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    iput-boolean v10, p0, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    .line 830
    iget v6, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_1b

    .line 831
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "ignoring scan update - not connected"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :goto_1a
    return-void

    .line 832
    :cond_1b
    iget-boolean v6, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    if-eqz v6, :cond_da

    .line 833
    new-instance v1, Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v1}, Lcom/navdy/obd/ScanSchedule;-><init>()V

    .line 834
    .local v1, "mergedSchedule":Lcom/navdy/obd/ScanSchedule;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 835
    .local v2, "now":J
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    iget-object v6, v6, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->state:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    sget-object v7, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->SAMPLING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    if-eq v6, v7, :cond_38

    iget-object v6, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    iget-object v6, v6, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->state:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    sget-object v7, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->MONITORING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    if-ne v6, v7, :cond_3f

    .line 836
    :cond_38
    iput-boolean v10, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    .line 837
    const/16 v6, 0x3e9

    invoke-virtual {v1, v6, v11}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 839
    :cond_3f
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    iget-object v6, v6, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->state:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    sget-object v7, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->MONITORING:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    if-ne v6, v7, :cond_69

    .line 840
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    iget-object v6, v6, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->canBusMonitoringCommand:Lcom/navdy/obd/command/CANBusMonitoringCommand;

    invoke-virtual {v6}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->getMonitoredPidsList()Ljava/util/List;

    move-result-object v5

    .line 841
    .local v5, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v5, :cond_69

    .line 842
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_55
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_69

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 843
    .local v4, "pid":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v1, v7}, Lcom/navdy/obd/ScanSchedule;->remove(I)Lcom/navdy/obd/ScanSchedule$Scan;

    goto :goto_55

    .line 847
    .end local v4    # "pid":Ljava/lang/Integer;
    .end local v5    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_69
    iget-boolean v6, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    if-eqz v6, :cond_a5

    iget-wide v6, p0, Lcom/navdy/obd/ObdService;->lastMonitorBatchCompletedTime:J

    cmp-long v6, v6, v12

    if-eqz v6, :cond_7d

    iget-wide v6, p0, Lcom/navdy/obd/ObdService;->lastMonitorBatchCompletedTime:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v6, v6, v8

    if-lez v6, :cond_a5

    .line 848
    :cond_7d
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "Starting new batch of monitoring"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    iput v10, p0, Lcom/navdy/obd/ObdService;->monitorCounter:I

    .line 850
    invoke-virtual {v1, v11, v11}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 855
    :cond_89
    :goto_89
    iget-object v7, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    monitor-enter v7

    .line 856
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_8d
    :try_start_8d
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v6}, Landroid/util/ArrayMap;->size()I

    move-result v6

    if-ge v0, v6, :cond_ae

    .line 857
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-virtual {v6, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/PidListener;

    iget-object v6, v6, Lcom/navdy/obd/PidListener;->schedule:Lcom/navdy/obd/ScanSchedule;

    invoke-virtual {v1, v6}, Lcom/navdy/obd/ScanSchedule;->merge(Lcom/navdy/obd/ScanSchedule;)V
    :try_end_a2
    .catchall {:try_start_8d .. :try_end_a2} :catchall_c1

    .line 856
    add-int/lit8 v0, v0, 0x1

    goto :goto_8d

    .line 851
    .end local v0    # "i":I
    :cond_a5
    iget-boolean v6, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    if-nez v6, :cond_89

    .line 852
    iput v10, p0, Lcom/navdy/obd/ObdService;->monitorCounter:I

    .line 853
    iput-wide v12, p0, Lcom/navdy/obd/ObdService;->lastMonitorBatchCompletedTime:J

    goto :goto_89

    .line 859
    .restart local v0    # "i":I
    :cond_ae
    :try_start_ae
    monitor-exit v7
    :try_end_af
    .catchall {:try_start_ae .. :try_end_af} :catchall_c1

    .line 860
    invoke-virtual {v1}, Lcom/navdy/obd/ScanSchedule;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_c4

    .line 861
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "cancelling scan schedule - no pids to scan"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    goto/16 :goto_1a

    .line 859
    :catchall_c1
    move-exception v6

    :try_start_c2
    monitor-exit v7
    :try_end_c3
    .catchall {:try_start_c2 .. :try_end_c3} :catchall_c1

    throw v6

    .line 865
    :cond_c4
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    invoke-virtual {v6, v1}, Lcom/navdy/obd/VehicleStateManager;->resolveDependenciesForCustomPids(Lcom/navdy/obd/ScanSchedule;)V

    .line 866
    iget v6, p0, Lcom/navdy/obd/ObdService;->currentMode:I

    packed-switch v6, :pswitch_data_e4

    goto/16 :goto_1a

    .line 868
    :pswitch_d0
    invoke-virtual {p0, v1}, Lcom/navdy/obd/ObdService;->scanPids(Lcom/navdy/obd/ScanSchedule;)V

    goto/16 :goto_1a

    .line 871
    :pswitch_d5
    invoke-virtual {p0, v1}, Lcom/navdy/obd/ObdService;->monitorCANJ1939(Lcom/navdy/obd/ScanSchedule;)V

    goto/16 :goto_1a

    .line 878
    .end local v0    # "i":I
    .end local v1    # "mergedSchedule":Lcom/navdy/obd/ScanSchedule;
    .end local v2    # "now":J
    :cond_da
    sget-object v6, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v7, "ignoring scan update - not enabled"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1a

    .line 866
    nop

    :pswitch_data_e4
    .packed-switch 0x0
        :pswitch_d0
        :pswitch_d5
    .end packed-switch
.end method


# virtual methods
.method public addListener(Lcom/navdy/obd/IObdServiceListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/obd/IObdServiceListener;

    .prologue
    .line 883
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 884
    :try_start_3
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 885
    monitor-exit v1

    .line 886
    return-void

    .line 885
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public addListener(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V
    .locals 4
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;
    .param p2, "listener"    # Lcom/navdy/obd/IPidListener;

    .prologue
    .line 812
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 813
    :try_start_3
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "adding listener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ibinder:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/navdy/obd/IPidListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-interface {p2}, Lcom/navdy/obd/IPidListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    new-instance v3, Lcom/navdy/obd/PidListener;

    invoke-direct {v3, p2, p1}, Lcom/navdy/obd/PidListener;-><init>(Lcom/navdy/obd/IPidListener;Lcom/navdy/obd/ScanSchedule;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_3 .. :try_end_38} :catchall_3c

    .line 816
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->updateScan()V

    .line 817
    return-void

    .line 815
    :catchall_3c
    move-exception v0

    :try_start_3d
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    throw v0
.end method

.method public addListener(Ljava/util/List;Lcom/navdy/obd/IPidListener;)V
    .locals 2
    .param p2, "listener"    # Lcom/navdy/obd/IPidListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Lcom/navdy/obd/IPidListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 806
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v0, Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v0}, Lcom/navdy/obd/ScanSchedule;-><init>()V

    .line 807
    .local v0, "schedule":Lcom/navdy/obd/ScanSchedule;
    const/16 v1, 0x1f4

    invoke-virtual {v0, p1, v1}, Lcom/navdy/obd/ScanSchedule;->addPids(Ljava/util/List;I)V

    .line 808
    invoke-virtual {p0, v0, p2}, Lcom/navdy/obd/ObdService;->addListener(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V

    .line 809
    return-void
.end method

.method public closeChannel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1519
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    .line 1520
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    .line 1521
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/navdy/obd/ObdService;->currentBatteryVoltage:D

    .line 1522
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channelInfo:Lcom/navdy/obd/io/ChannelInfo;

    if-eqz v0, :cond_f

    .line 1523
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->channelInfo:Lcom/navdy/obd/io/ChannelInfo;

    .line 1525
    :cond_f
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-eqz v0, :cond_15

    .line 1526
    iput-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    .line 1528
    :cond_15
    return-void
.end method

.method public forceReconnect()V
    .locals 3

    .prologue
    .line 541
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-eqz v0, :cond_11

    .line 542
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Disconnecting channel as its connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v0}, Lcom/navdy/obd/io/IChannel;->disconnect()V

    .line 548
    :goto_10
    return-void

    .line 546
    :cond_11
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Triggering reconnect, current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/ObdService;->connectionState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-virtual {v0}, Lcom/navdy/obd/jobs/AutoConnect;->triggerReconnect()V

    goto :goto_10
.end method

.method public getDefaultChannelInfo()Lcom/navdy/obd/io/ChannelInfo;
    .locals 5

    .prologue
    .line 713
    const/4 v1, 0x0

    .line 715
    .local v1, "info":Lcom/navdy/obd/io/ChannelInfo;
    :try_start_1
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "DefaultChannelInfo"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/obd/io/ChannelInfo;->parse(Ljava/lang/String;)Lcom/navdy/obd/io/ChannelInfo;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_d} :catch_f

    move-result-object v1

    .line 719
    :goto_e
    return-object v1

    .line 716
    :catch_f
    move-exception v0

    .line 717
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    return-object v0
.end method

.method public getProtocol()Lcom/navdy/obd/Protocol;
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    return-object v0
.end method

.method public monitorBatteryVoltage()V
    .locals 10

    .prologue
    .line 916
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Starting to monitor voltage"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-nez v0, :cond_13

    .line 918
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to start monitoring voltage"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    :goto_12
    return-void

    .line 921
    :cond_13
    new-instance v1, Lcom/navdy/obd/command/ReadVoltageCommand;

    invoke-direct {v1}, Lcom/navdy/obd/command/ReadVoltageCommand;-><init>()V

    .line 922
    .local v1, "command":Lcom/navdy/obd/command/ReadVoltageCommand;
    iget-object v9, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 923
    .local v9, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    const/4 v3, 0x0

    new-instance v4, Lcom/navdy/obd/ObdService$4;

    invoke-direct {v4, p0, v1}, Lcom/navdy/obd/ObdService$4;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ReadVoltageCommand;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x7d0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v2, v9

    move-object v3, v0

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->voltageMonitoring:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_12
.end method

.method public monitorCANJ1939(Lcom/navdy/obd/ScanSchedule;)V
    .locals 8
    .param p1, "scanSchedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 1114
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 1115
    :cond_c
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    .line 1116
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    .line 1117
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Starting to monitor the CAN bus for data, using SAE J1939"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    invoke-virtual {p1}, Lcom/navdy/obd/ScanSchedule;->getScanList()Ljava/util/List;

    move-result-object v7

    .line 1119
    .local v7, "scanList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ScanSchedule$Scan;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1120
    .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 1121
    .local v6, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    new-instance v2, Lcom/navdy/obd/Pid;

    iget v4, v6, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    invoke-direct {v2, v4}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_26

    .line 1123
    .end local v6    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_3d
    new-instance v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;

    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->j1939Profile:Lcom/navdy/obd/j1939/J1939Profile;

    iget-object v4, p0, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;-><init>(Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/j1939/J1939Profile;Ljava/util/List;Lcom/navdy/obd/VehicleStateManager;Lcom/navdy/obd/ObdDataObserver;)V

    .line 1124
    .local v0, "jobdAdpater":Lcom/navdy/obd/j1939/J1939ObdJobAdapter;
    new-instance v1, Lcom/navdy/obd/ObdService$9;

    invoke-direct {v1, p0, v0}, Lcom/navdy/obd/ObdService$9;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/j1939/J1939ObdJobAdapter;)V

    invoke-virtual {v0, v1}, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->setListener(Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;)V

    .line 1129
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v4, 0x0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v4, v5, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    .line 1133
    .end local v0    # "jobdAdpater":Lcom/navdy/obd/j1939/J1939ObdJobAdapter;
    .end local v3    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v7    # "scanList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ScanSchedule$Scan;>;"
    :goto_5e
    return-void

    .line 1132
    :cond_5f
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Monitoring is already running"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5e
.end method

.method public monitorCheckEngineLight()V
    .locals 10

    .prologue
    .line 951
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Starting to monitor CEL"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-nez v0, :cond_13

    .line 953
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to start monitoring CEL"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    :goto_12
    return-void

    .line 956
    :cond_13
    new-instance v1, Lcom/navdy/obd/command/CheckDTCCommand;

    iget-object v0, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    iget-object v0, v0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/navdy/obd/command/CheckDTCCommand;-><init>(Ljava/util/List;)V

    .line 957
    .local v1, "command":Lcom/navdy/obd/command/CheckDTCCommand;
    iget-object v9, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 958
    .local v9, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/ObdService$5;

    invoke-direct {v4, p0, v1}, Lcom/navdy/obd/ObdService$5;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/CheckDTCCommand;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x7d0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v2, v9

    move-object v3, v0

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->celMonitoring:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_12
.end method

.method public monitorPendingCodes()V
    .locals 7

    .prologue
    .line 993
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Starting to monitor Trouble and Pending Codes"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-nez v1, :cond_13

    .line 995
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to start monitoring PendingCodes"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    :goto_12
    return-void

    .line 999
    :cond_13
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1000
    .local v0, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/ObdService$7;

    invoke-direct {v4, p0}, Lcom/navdy/obd/ObdService$7;-><init>(Lcom/navdy/obd/ObdService;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    .line 1001
    invoke-static {v1, v2, v3, v4, v5}, Lcom/navdy/obd/jobs/ScanDtcJob;->newScanDtcJob(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)Lcom/navdy/obd/jobs/ScanDtcJob;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x7d0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1000
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->pendingMonitoring:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_12
.end method

.method public monitorTroubleCodes()V
    .locals 10

    .prologue
    .line 969
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Starting to monitor TroubleCodes"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-nez v0, :cond_13

    .line 971
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to start monitoring TroubleCodes"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :goto_12
    return-void

    .line 974
    :cond_13
    new-instance v1, Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    iget-object v0, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    iget-object v0, v0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;-><init>(Ljava/util/List;)V

    .line 975
    .local v1, "command":Lcom/navdy/obd/command/ReadTroubleCodesCommand;
    iget-object v9, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 976
    .local v9, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/ObdService$6;

    invoke-direct {v4, p0, v1}, Lcom/navdy/obd/ObdService$6;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ReadTroubleCodesCommand;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x7d0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v2, v9

    move-object v3, v0

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->troubleMonitoring:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_12
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 642
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    const-class v0, Lcom/navdy/obd/IObdService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 644
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "returning IObdService API endpoint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->apiEndpoint:Lcom/navdy/obd/IObdService$Stub;

    .line 650
    :goto_35
    return-object v0

    .line 646
    :cond_36
    const-class v0, Lcom/navdy/obd/ICarService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_48

    .line 647
    const/4 v0, 0x0

    goto :goto_35

    .line 649
    :cond_48
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "returning ICarService API endpoint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->carApiEndpoint:Lcom/navdy/obd/ICarService$Stub;

    goto :goto_35
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 622
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 623
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Creating service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    sput-object p0, Lcom/navdy/obd/ObdService;->obdService:Lcom/navdy/obd/ObdService;

    .line 625
    new-instance v1, Lcom/navdy/obd/Profile;

    invoke-direct {v1}, Lcom/navdy/obd/Profile;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->profile:Lcom/navdy/obd/Profile;

    .line 626
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->profile:Lcom/navdy/obd/Profile;

    invoke-virtual {p0}, Lcom/navdy/obd/ObdService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/obd/Profile;->load(Ljava/io/InputStream;)V

    .line 627
    const-string v1, "ObdDevice"

    invoke-virtual {p0, v1, v4}, Lcom/navdy/obd/ObdService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 628
    const-string v1, "persist.sys.obd.debug"

    invoke-static {v1, v4}, Lcom/navdy/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/obd/ObdService;->debugMode:Z

    .line 629
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "last_connected_baud_rate"

    sget v3, Lcom/navdy/obd/ObdService;->BAUD_RATE:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/navdy/obd/ObdService;->lastConnectedBaudRate:I

    .line 630
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "ObdCommunicationEnabled"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    .line 631
    const-string v1, "persist.sys.obd.clear0401"

    invoke-static {v1, v4}, Lcom/navdy/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/obd/ObdService;->clear0401:Z

    .line 632
    const-string v1, "persist.sys.obd.clearCommand"

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->dtcClearCommand:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->dtcClearCommand:Ljava/lang/String;

    .line 633
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "ObdScanMode"

    sget v3, Lcom/navdy/obd/ObdService;->DEFAULT_MODE:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/navdy/obd/ObdService;->setMode(I)V

    .line 634
    new-instance v1, Lcom/navdy/obd/ObdDataObserver;

    invoke-direct {v1}, Lcom/navdy/obd/ObdDataObserver;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    .line 635
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ObdServiceHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 636
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 637
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->serviceLooper:Landroid/os/Looper;

    .line 638
    new-instance v1, Lcom/navdy/obd/ObdService$ServiceHandler;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->serviceLooper:Landroid/os/Looper;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/navdy/obd/ObdService$ServiceHandler;-><init>(Lcom/navdy/obd/ObdService;Landroid/os/Looper;Lcom/navdy/obd/ObdService$1;)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    .line 639
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 675
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 676
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    if-eqz v0, :cond_c

    .line 677
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-virtual {v0}, Lcom/navdy/obd/jobs/AutoConnect;->stop()V

    .line 679
    :cond_c
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 680
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    .line 681
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    .line 682
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->serviceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 683
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v1, "Destroying service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    return-void
.end method

.method public onLogFileRollOver(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "loggerName"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1407
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLogFileRollover Logger :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", File name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    const-string v1, "com.navdy.obd.CanBusRaw"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_52

    iget-boolean v1, p0, Lcom/navdy/obd/ObdService;->gatherCanBusDataToLogs:Z

    if-eqz v1, :cond_52

    .line 1409
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    .line 1410
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLogFileRollOver , new data file is available "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1411
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    if-eqz v1, :cond_52

    .line 1413
    :try_start_4d
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    invoke-interface {v1, p2}, Lcom/navdy/obd/ICanBusMonitoringListener;->onNewDataAvailable(Ljava/lang/String;)V
    :try_end_52
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_52} :catch_53

    .line 1419
    :cond_52
    :goto_52
    return-void

    .line 1414
    :catch_53
    move-exception v0

    .line 1415
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_52
.end method

.method public onMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1542
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/obd/ObdService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1543
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1544
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "toast"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1545
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1546
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1547
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 655
    if-eqz p1, :cond_1e

    .line 656
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 657
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.navdy.obd.action.START_AUTO_CONNECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 658
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    if-nez v1, :cond_1e

    .line 659
    new-instance v1, Lcom/navdy/obd/jobs/AutoConnect;

    invoke-direct {v1, p0}, Lcom/navdy/obd/jobs/AutoConnect;-><init>(Lcom/navdy/obd/ObdService;)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    .line 660
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-virtual {v1}, Lcom/navdy/obd/jobs/AutoConnect;->start()V

    .line 671
    .end local v0    # "action":Ljava/lang/String;
    :cond_1e
    :goto_1e
    const/4 v1, 0x1

    return v1

    .line 662
    .restart local v0    # "action":Ljava/lang/String;
    :cond_20
    const-string v1, "com.navdy.obd.action.STOP_AUTO_CONNECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 663
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    if-eqz v1, :cond_31

    .line 664
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-virtual {v1}, Lcom/navdy/obd/jobs/AutoConnect;->stop()V

    .line 666
    :cond_31
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->autoConnect:Lcom/navdy/obd/jobs/AutoConnect;

    goto :goto_1e

    .line 667
    :cond_35
    const-string v1, "com.navdy.obd.action.RESCAN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 668
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->serviceHandler:Lcom/navdy/obd/ObdService$ServiceHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_1e
.end method

.method public onStateChange(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    .line 1531
    const/4 v0, 0x2

    if-ne p1, v0, :cond_25

    .line 1532
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    instance-of v0, v0, Lcom/navdy/obd/io/STNSerialChannel;

    if-eqz v0, :cond_24

    .line 1533
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    check-cast v0, Lcom/navdy/obd/io/STNSerialChannel;

    invoke-virtual {v0}, Lcom/navdy/obd/io/STNSerialChannel;->getBaudRate()I

    move-result v0

    iput v0, p0, Lcom/navdy/obd/ObdService;->lastConnectedBaudRate:I

    .line 1534
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_connected_baud_rate"

    iget v2, p0, Lcom/navdy/obd/ObdService;->lastConnectedBaudRate:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1536
    :cond_24
    const/4 p1, 0x4

    .line 1538
    :cond_25
    invoke-direct {p0, p1}, Lcom/navdy/obd/ObdService;->postStateChange(I)V

    .line 1539
    return-void
.end method

.method public openChannel(Lcom/navdy/obd/io/ChannelInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/navdy/obd/io/ChannelInfo;

    .prologue
    .line 1485
    if-eqz p1, :cond_5b

    .line 1486
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channelInfo:Lcom/navdy/obd/io/ChannelInfo;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channelInfo:Lcom/navdy/obd/io/ChannelInfo;

    invoke-virtual {v1, p1}, Lcom/navdy/obd/io/ChannelInfo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5b

    .line 1487
    :cond_e
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-eqz v1, :cond_1a

    .line 1488
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v1}, Lcom/navdy/obd/io/IChannel;->disconnect()V

    .line 1489
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    .line 1491
    :cond_1a
    iput-object p1, p0, Lcom/navdy/obd/ObdService;->channelInfo:Lcom/navdy/obd/io/ChannelInfo;

    .line 1492
    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->getConnectionType()Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    move-result-object v0

    .line 1493
    .local v0, "channelType":Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Opening channel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    sget-object v1, Lcom/navdy/obd/ObdService$13;->$SwitchMap$com$navdy$obd$io$ChannelInfo$ConnectionType:[I

    invoke-virtual {v0}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_96

    .line 1510
    :goto_47
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    if-nez v1, :cond_52

    .line 1511
    new-instance v1, Lcom/navdy/obd/update/DefaultFirmwareManager;

    invoke-direct {v1}, Lcom/navdy/obd/update/DefaultFirmwareManager;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    .line 1513
    :cond_52
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/navdy/obd/io/IChannel;->connect(Ljava/lang/String;)V

    .line 1516
    .end local v0    # "channelType":Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    :cond_5b
    return-void

    .line 1496
    .restart local v0    # "channelType":Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    :pswitch_5c
    new-instance v1, Lcom/navdy/obd/io/STNSerialChannel;

    iget v2, p0, Lcom/navdy/obd/ObdService;->lastConnectedBaudRate:I

    sget v3, Lcom/navdy/obd/ObdService;->BAUD_RATE:I

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/navdy/obd/io/STNSerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;II)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    .line 1497
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    if-eqz v1, :cond_7b

    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    instance-of v1, v1, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;

    if-eqz v1, :cond_7b

    .line 1498
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    check-cast v1, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-virtual {v1, p0, v2}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->init(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V

    goto :goto_47

    .line 1500
    :cond_7b
    new-instance v1, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-direct {v1, p0, v2}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    goto :goto_47

    .line 1504
    :pswitch_85
    new-instance v1, Lcom/navdy/obd/io/BluetoothChannel;

    invoke-direct {v1, p0}, Lcom/navdy/obd/io/BluetoothChannel;-><init>(Lcom/navdy/obd/io/IChannelSink;)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    goto :goto_47

    .line 1507
    :pswitch_8d
    new-instance v1, Lcom/navdy/obd/simulator/MockObdChannel;

    invoke-direct {v1, p0}, Lcom/navdy/obd/simulator/MockObdChannel;-><init>(Lcom/navdy/obd/io/IChannelSink;)V

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    goto :goto_47

    .line 1494
    nop

    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_5c
        :pswitch_85
        :pswitch_8d
    .end packed-switch
.end method

.method public reconnectImmediately()Z
    .locals 1

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->reconnectImmediately:Z

    return v0
.end method

.method public removeListener(Lcom/navdy/obd/IObdServiceListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/obd/IObdServiceListener;

    .prologue
    .line 889
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 890
    :try_start_3
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 891
    monitor-exit v1

    .line 892
    return-void

    .line 891
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public removeListener(Lcom/navdy/obd/IPidListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/navdy/obd/IPidListener;

    .prologue
    .line 820
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 821
    :try_start_3
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removing listener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " binder:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/navdy/obd/IPidListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->pidListeners:Landroid/util/ArrayMap;

    invoke-interface {p1}, Lcom/navdy/obd/IPidListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    monitor-exit v1
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_37

    .line 824
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->updateScan()V

    .line 825
    return-void

    .line 823
    :catchall_37
    move-exception v0

    :try_start_38
    monitor-exit v1
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    throw v0
.end method

.method public resetChannel()V
    .locals 25

    .prologue
    .line 1312
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Resetting channel"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 1314
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/obd/ObdService;->isScanningEnabled:Z

    move/from16 v21, v0

    if-eqz v21, :cond_28d

    .line 1315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->canBusMonitoringStatus:Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    move-object/from16 v21, v0

    sget-object v22, Lcom/navdy/obd/ObdService$CANBusMonitoringState;->UNKNOWN:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->state:Lcom/navdy/obd/ObdService$CANBusMonitoringState;

    .line 1316
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/obd/ObdService;->onMonitoringStatusReported:Z

    .line 1317
    const/4 v10, 0x4

    .line 1318
    .local v10, "newState":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    move-object/from16 v21, v0

    if-eqz v21, :cond_15f

    .line 1320
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/obd/ObdService;->currentMode:I

    move/from16 v21, v0

    if-nez v21, :cond_1f7

    .line 1323
    const/16 v21, 0x0

    :try_start_3b
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    .line 1324
    new-instance v7, Lcom/navdy/obd/command/InitializeCommand;

    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/ObdService;->getSavedProtocol()Lcom/navdy/obd/Protocol;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Lcom/navdy/obd/command/InitializeCommand;-><init>(Lcom/navdy/obd/Protocol;)V

    .line 1325
    .local v7, "initializeCommand":Lcom/navdy/obd/command/InitializeCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1326
    invoke-virtual {v7}, Lcom/navdy/obd/command/InitializeCommand;->getProtocol()Ljava/lang/String;

    move-result-object v14

    .line 1327
    .local v14, "protocolResponse":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_165

    .line 1328
    const/4 v13, 0x0

    .line 1332
    .local v13, "protocol":Lcom/navdy/obd/Protocol;
    :goto_5c
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    .line 1333
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Protocol response:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    move-object/from16 v21, v0

    if-eqz v21, :cond_15f

    .line 1335
    invoke-virtual {v7}, Lcom/navdy/obd/command/InitializeCommand;->getConfigurationName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->configurationName:Ljava/lang/String;

    .line 1336
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Configuration name :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->configurationName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Initialized - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Lcom/navdy/obd/command/InitializeCommand;->getResponse()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", Protocol:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1338
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Scanning for VIN"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    new-instance v20, Lcom/navdy/obd/command/VinCommand;

    invoke-direct/range {v20 .. v20}, Lcom/navdy/obd/command/VinCommand;-><init>()V

    .line 1340
    .local v20, "vinCommand":Lcom/navdy/obd/command/VinCommand;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1341
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Scanning for ECUs"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    new-instance v4, Lcom/navdy/obd/command/ValidPidsCommand;

    invoke-direct {v4}, Lcom/navdy/obd/command/ValidPidsCommand;-><init>()V

    .line 1343
    .local v4, "command":Lcom/navdy/obd/command/ValidPidsCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1344
    invoke-static {v4}, Lcom/navdy/obd/ObdService;->buildEcus(Lcom/navdy/obd/command/ValidPidsCommand;)Ljava/util/List;

    move-result-object v6

    .line 1345
    .local v6, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    new-instance v3, Lcom/navdy/obd/command/CheckDTCCommand;

    invoke-direct {v3, v6}, Lcom/navdy/obd/command/CheckDTCCommand;-><init>(Ljava/util/List;)V

    .line 1346
    .local v3, "checkDTCCommand":Lcom/navdy/obd/command/CheckDTCCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1347
    invoke-virtual {v3}, Lcom/navdy/obd/command/CheckDTCCommand;->isCheckEngineLightOn()Z

    move-result v9

    .line 1348
    .local v9, "isCheckEngineLightIsOn":Z
    const/16 v19, 0x0

    .line 1349
    .local v19, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/navdy/obd/command/CheckDTCCommand;->getNumberOfTroubleCodes()I

    move-result v21

    if-lez v21, :cond_16b

    .line 1350
    new-instance v15, Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-direct {v15, v6}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;-><init>(Ljava/util/List;)V

    .line 1351
    .local v15, "readTroubleCodesCommand":Lcom/navdy/obd/command/ReadTroubleCodesCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1352
    invoke-virtual {v15}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getTroubleCodes()Ljava/util/List;

    move-result-object v19

    .line 1353
    if-eqz v19, :cond_16b

    .line 1354
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_128
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_16b

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 1355
    .local v18, "troubleCode":Ljava/lang/String;
    sget-object v22, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Initial DTC: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_150
    .catch Ljava/lang/Throwable; {:try_start_3b .. :try_end_150} :catch_151

    goto :goto_128

    .line 1374
    .end local v3    # "checkDTCCommand":Lcom/navdy/obd/command/CheckDTCCommand;
    .end local v4    # "command":Lcom/navdy/obd/command/ValidPidsCommand;
    .end local v6    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .end local v7    # "initializeCommand":Lcom/navdy/obd/command/InitializeCommand;
    .end local v9    # "isCheckEngineLightIsOn":Z
    .end local v13    # "protocol":Lcom/navdy/obd/Protocol;
    .end local v14    # "protocolResponse":Ljava/lang/String;
    .end local v15    # "readTroubleCodesCommand":Lcom/navdy/obd/command/ReadTroubleCodesCommand;
    .end local v18    # "troubleCode":Ljava/lang/String;
    .end local v19    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "vinCommand":Lcom/navdy/obd/command/VinCommand;
    :catch_151
    move-exception v16

    .line 1375
    .local v16, "t":Ljava/lang/Throwable;
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Error while resetting the channel "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1400
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_15f
    :goto_15f
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/navdy/obd/ObdService;->postStateChange(I)V

    .line 1404
    .end local v10    # "newState":I
    :goto_164
    return-void

    .line 1330
    .restart local v7    # "initializeCommand":Lcom/navdy/obd/command/InitializeCommand;
    .restart local v10    # "newState":I
    .restart local v14    # "protocolResponse":Ljava/lang/String;
    :cond_165
    :try_start_165
    invoke-static {v14}, Lcom/navdy/obd/Protocol;->parse(Ljava/lang/String;)Lcom/navdy/obd/Protocol;

    move-result-object v13

    .restart local v13    # "protocol":Lcom/navdy/obd/Protocol;
    goto/16 :goto_5c

    .line 1359
    .restart local v3    # "checkDTCCommand":Lcom/navdy/obd/command/CheckDTCCommand;
    .restart local v4    # "command":Lcom/navdy/obd/command/ValidPidsCommand;
    .restart local v6    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .restart local v9    # "isCheckEngineLightIsOn":Z
    .restart local v19    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v20    # "vinCommand":Lcom/navdy/obd/command/VinCommand;
    :cond_16b
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Building vehicle info"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/obd/command/VinCommand;->getVIN()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-static {v0, v6, v1, v9, v2}, Lcom/navdy/obd/ObdService;->build(Lcom/navdy/obd/Protocol;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    .line 1361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/navdy/obd/ObdService;->dump(Lcom/navdy/obd/VehicleInfo;)V

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/navdy/obd/VehicleInfo;->getPrimaryEcu()Lcom/navdy/obd/ECU;

    move-result-object v12

    .line 1363
    .local v12, "primaryEcu":Lcom/navdy/obd/ECU;
    if-eqz v12, :cond_1e4

    .line 1364
    iget-object v11, v12, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    .line 1365
    .local v11, "pids":Lcom/navdy/obd/PidSet;
    new-instance v21, Lcom/navdy/obd/VehicleStateManager;

    new-instance v22, Lcom/navdy/obd/DefaultPidProcessorFactory;

    invoke-direct/range {v22 .. v22}, Lcom/navdy/obd/DefaultPidProcessorFactory;-><init>()V

    invoke-direct/range {v21 .. v22}, Lcom/navdy/obd/VehicleStateManager;-><init>(Lcom/navdy/obd/PidProcessorFactory;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    .line 1366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/navdy/obd/VehicleStateManager;->updateSupportedPids(Lcom/navdy/obd/PidSet;)V

    .line 1367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v21

    const-string v22, "Protocol"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/navdy/obd/Protocol;->id:I

    move/from16 v23, v0

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1368
    const/4 v10, 0x2

    .line 1369
    goto/16 :goto_15f

    .line 1370
    .end local v11    # "pids":Lcom/navdy/obd/PidSet;
    :cond_1e4
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "No ECUs found - falling back to IDLE state"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v21

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/obd/ObdService;->lastDisconnectTime:J
    :try_end_1f5
    .catch Ljava/lang/Throwable; {:try_start_165 .. :try_end_1f5} :catch_151

    goto/16 :goto_15f

    .line 1378
    .end local v3    # "checkDTCCommand":Lcom/navdy/obd/command/CheckDTCCommand;
    .end local v4    # "command":Lcom/navdy/obd/command/ValidPidsCommand;
    .end local v6    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .end local v7    # "initializeCommand":Lcom/navdy/obd/command/InitializeCommand;
    .end local v9    # "isCheckEngineLightIsOn":Z
    .end local v12    # "primaryEcu":Lcom/navdy/obd/ECU;
    .end local v13    # "protocol":Lcom/navdy/obd/Protocol;
    .end local v14    # "protocolResponse":Ljava/lang/String;
    .end local v19    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "vinCommand":Lcom/navdy/obd/command/VinCommand;
    :cond_1f7
    new-instance v8, Lcom/navdy/obd/command/InitializeJ1939Command;

    invoke-direct {v8}, Lcom/navdy/obd/command/InitializeJ1939Command;-><init>()V

    .line 1380
    .local v8, "initializeCommand2":Lcom/navdy/obd/command/InitializeJ1939Command;
    :try_start_1fc
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1381
    invoke-virtual {v8}, Lcom/navdy/obd/command/InitializeJ1939Command;->getConfigurationName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->configurationName:Ljava/lang/String;

    .line 1382
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Configuration name :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->configurationName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Scanning for VIN"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    new-instance v20, Lcom/navdy/obd/command/VinCommand;

    invoke-direct/range {v20 .. v20}, Lcom/navdy/obd/command/VinCommand;-><init>()V
    :try_end_235
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1fc .. :try_end_235} :catch_27f

    .line 1386
    .restart local v20    # "vinCommand":Lcom/navdy/obd/command/VinCommand;
    :try_start_235
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    :try_end_23c
    .catch Ljava/lang/Throwable; {:try_start_235 .. :try_end_23c} :catch_276
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_235 .. :try_end_23c} :catch_27f

    .line 1390
    :goto_23c
    :try_start_23c
    new-instance v21, Lcom/navdy/obd/VehicleInfo;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/obd/command/VinCommand;->getVIN()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lcom/navdy/obd/VehicleInfo;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    .line 1391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/navdy/obd/ObdService;->dump(Lcom/navdy/obd/VehicleInfo;)V

    .line 1392
    new-instance v21, Lcom/navdy/obd/VehicleStateManager;

    new-instance v22, Lcom/navdy/obd/DefaultPidProcessorFactory;

    invoke-direct/range {v22 .. v22}, Lcom/navdy/obd/DefaultPidProcessorFactory;-><init>()V

    invoke-direct/range {v21 .. v22}, Lcom/navdy/obd/VehicleStateManager;-><init>(Lcom/navdy/obd/PidProcessorFactory;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    .line 1393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    move-object/from16 v21, v0

    sget-object v22, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/obd/VehicleStateManager;->updateSupportedPids(Lcom/navdy/obd/PidSet;)V

    .line 1394
    const/4 v10, 0x2

    goto/16 :goto_15f

    .line 1387
    :catch_276
    move-exception v17

    .line 1388
    .local v17, "th":Ljava/lang/Throwable;
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Failed to read VIN - trying to connect anyway"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_27e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_23c .. :try_end_27e} :catch_27f

    goto :goto_23c

    .line 1395
    .end local v17    # "th":Ljava/lang/Throwable;
    .end local v20    # "vinCommand":Lcom/navdy/obd/command/VinCommand;
    :catch_27f
    move-exception v5

    .line 1396
    .local v5, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v21, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v22, "Error while resetting the channel "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_15f

    .line 1403
    .end local v5    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v8    # "initializeCommand2":Lcom/navdy/obd/command/InitializeJ1939Command;
    .end local v10    # "newState":I
    :cond_28d
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/navdy/obd/ObdService;->postStateChange(I)V

    goto/16 :goto_164
.end method

.method public resetObdChip()V
    .locals 7

    .prologue
    .line 895
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Resetting the obd chip"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    .line 897
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    .line 899
    :try_start_d
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Turning off echo"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    new-instance v1, Lcom/navdy/obd/command/BatchCommand;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/navdy/obd/command/ICommand;

    const/4 v3, 0x0

    new-instance v4, Lcom/navdy/obd/command/ObdCommand;

    const-string v5, "DUMMY"

    const-string v6, "\r"

    invoke-direct {v4, v5, v6}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    invoke-direct {p0, v1}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    :try_end_35
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_d .. :try_end_35} :catch_41

    .line 904
    :goto_35
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    if-nez v1, :cond_4a

    .line 905
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to reset the obd chip, Channel is null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    :goto_40
    return-void

    .line 901
    :catch_41
    move-exception v0

    .line 902
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to turn off echo"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_35

    .line 908
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_4a
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->currentFirmwareVersion:Ljava/lang/String;

    .line 909
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    if-eqz v1, :cond_59

    .line 910
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->firmwareManager:Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    invoke-interface {v1}, Lcom/navdy/obd/update/ObdDeviceFirmwareManager;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/obd/ObdService;->currentFirmwareVersion:Ljava/lang/String;

    .line 912
    :cond_59
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current firmware version of the Obd chip : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->currentFirmwareVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_40
.end method

.method public scanPids(Lcom/navdy/obd/ScanSchedule;)V
    .locals 10
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 1136
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 1137
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting scan for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    iget-object v9, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1139
    .local v9, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v0, Lcom/navdy/obd/jobs/ScanPidsJob;

    iget-object v1, p0, Lcom/navdy/obd/ObdService;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->profile:Lcom/navdy/obd/Profile;

    iget-object v4, p0, Lcom/navdy/obd/ObdService;->mVehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v6, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v7, Lcom/navdy/obd/ObdService$10;

    invoke-direct {v7, p0}, Lcom/navdy/obd/ObdService$10;-><init>(Lcom/navdy/obd/ObdService;)V

    iget-object v8, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/navdy/obd/jobs/ScanPidsJob;-><init>(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/Profile;Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/VehicleStateManager;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/jobs/ScanPidsJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0xa

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, v9

    move-object v2, v0

    invoke-interface/range {v1 .. v7}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/ObdService;->currentSchedule:Ljava/util/concurrent/ScheduledFuture;

    .line 1239
    return-void
.end method

.method public scanPids(Ljava/util/List;I)V
    .locals 1
    .param p2, "intervalMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1108
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v0, Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v0}, Lcom/navdy/obd/ScanSchedule;-><init>()V

    .line 1109
    .local v0, "schedule":Lcom/navdy/obd/ScanSchedule;
    invoke-virtual {v0, p1, p2}, Lcom/navdy/obd/ScanSchedule;->addPids(Ljava/util/List;I)V

    .line 1110
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ObdService;->scanPids(Lcom/navdy/obd/ScanSchedule;)V

    .line 1111
    return-void
.end method

.method public scanSupportedPids()V
    .locals 2

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/navdy/obd/ObdService$12;

    invoke-direct {v1, p0}, Lcom/navdy/obd/ObdService$12;-><init>(Lcom/navdy/obd/ObdService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1301
    return-void
.end method

.method public scanVIN()V
    .locals 2

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/navdy/obd/ObdService$11;

    invoke-direct {v1, p0}, Lcom/navdy/obd/ObdService$11;-><init>(Lcom/navdy/obd/ObdService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1280
    return-void
.end method

.method public sendCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 724
    :try_start_0
    new-instance v1, Lcom/navdy/obd/command/ObdCommand;

    invoke-direct {v1, p1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    :try_end_8
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_8} :catch_a

    move-result-object v1

    .line 727
    :goto_9
    return-object v1

    .line 725
    :catch_a
    move-exception v0

    .line 726
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const/4 v1, 0x0

    goto :goto_9
.end method

.method public setDefaultChannelInfo(Lcom/navdy/obd/io/ChannelInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/navdy/obd/io/ChannelInfo;

    .prologue
    .line 733
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DefaultChannelInfo"

    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_13} :catch_14

    .line 737
    :goto_13
    return-void

    .line 734
    :catch_14
    move-exception v0

    .line 735
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13
.end method

.method public sleepWithVoltageWakeUpTriggers()V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 1067
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelVoltageMonitoring()V

    .line 1068
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelDtcMonitoring()V

    .line 1069
    invoke-direct {p0}, Lcom/navdy/obd/ObdService;->cancelCurrentSchedule()V

    .line 1070
    new-instance v9, Lcom/navdy/obd/command/BatchCommand;

    new-array v0, v0, [Lcom/navdy/obd/command/ICommand;

    sget-object v3, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v3, v0, v4

    invoke-direct {v9, v0}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 1071
    .local v9, "sleepCommand":Lcom/navdy/obd/command/BatchCommand;
    iget-object v3, p0, Lcom/navdy/obd/ObdService;->voltageSettingsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1072
    :try_start_19
    iget-boolean v0, p0, Lcom/navdy/obd/ObdService;->deepSleep:Z

    if-eqz v0, :cond_6c

    .line 1073
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/navdy/obd/ObdService;->voltageSettings:Lcom/navdy/obd/VoltageSettings;

    iget v4, v4, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/navdy/obd/command/ObdCommand;->createSetVoltageLevelWakeupTriggerCommand(ZFI)Lcom/navdy/obd/command/ObdCommand;

    move-result-object v6

    .line 1074
    .local v6, "chargingVoltageTrigger":Lcom/navdy/obd/command/ObdCommand;
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1075
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1076
    invoke-virtual {v9, v6}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1083
    .end local v6    # "chargingVoltageTrigger":Lcom/navdy/obd/command/ObdCommand;
    :goto_34
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1084
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1085
    monitor-exit v3
    :try_end_3f
    .catchall {:try_start_19 .. :try_end_3f} :catchall_85

    .line 1086
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Configuring sleep triggers:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Lcom/navdy/obd/command/BatchCommand;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    :try_start_5b
    invoke-direct {p0, v9}, Lcom/navdy/obd/ObdService;->synchronousCommand(Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;

    .line 1089
    sget-object v1, Lcom/navdy/obd/command/ObdCommand;->SLEEP_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 1090
    .local v1, "waitCommand":Lcom/navdy/obd/command/ObdCommand;
    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    .line 1091
    .local v2, "obdChannel":Lcom/navdy/obd/io/IChannel;
    if-nez v2, :cond_88

    .line 1092
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v3, "Invalid channel - unable to execute command"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5b .. :try_end_6b} :catch_9c

    .line 1105
    .end local v1    # "waitCommand":Lcom/navdy/obd/command/ObdCommand;
    .end local v2    # "obdChannel":Lcom/navdy/obd/io/IChannel;
    :goto_6b
    return-void

    .line 1078
    :cond_6c
    const/4 v0, 0x1

    :try_start_6d
    iget-object v4, p0, Lcom/navdy/obd/ObdService;->voltageSettings:Lcom/navdy/obd/VoltageSettings;

    iget v4, v4, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    const/16 v5, 0xa

    invoke-static {v0, v4, v5}, Lcom/navdy/obd/command/ObdCommand;->createSetVoltageLevelWakeupTriggerCommand(ZFI)Lcom/navdy/obd/command/ObdCommand;

    move-result-object v8

    .line 1079
    .local v8, "lowVoltageTrigger":Lcom/navdy/obd/command/ObdCommand;
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1080
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v9, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 1081
    invoke-virtual {v9, v8}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    goto :goto_34

    .line 1085
    .end local v8    # "lowVoltageTrigger":Lcom/navdy/obd/command/ObdCommand;
    :catchall_85
    move-exception v0

    monitor-exit v3
    :try_end_87
    .catchall {:try_start_6d .. :try_end_87} :catchall_85

    throw v0

    .line 1094
    .restart local v1    # "waitCommand":Lcom/navdy/obd/command/ObdCommand;
    .restart local v2    # "obdChannel":Lcom/navdy/obd/io/IChannel;
    :cond_88
    :try_start_88
    iget-object v10, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/ObdService$8;

    invoke-direct {v4, p0}, Lcom/navdy/obd/ObdService$8;-><init>(Lcom/navdy/obd/ObdService;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    invoke-interface {v10, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_9b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_88 .. :try_end_9b} :catch_9c

    goto :goto_6b

    .line 1102
    .end local v1    # "waitCommand":Lcom/navdy/obd/command/ObdCommand;
    .end local v2    # "obdChannel":Lcom/navdy/obd/io/IChannel;
    :catch_9c
    move-exception v7

    .line 1103
    .local v7, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v0, Lcom/navdy/obd/ObdService;->TAG:Ljava/lang/String;

    const-string v3, "Failed to enable monitoring during sleep"

    invoke-static {v0, v3, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6b
.end method

.method public submitCommand(Lcom/navdy/obd/command/ObdCommand;Lcom/navdy/obd/ObdJob$IListener;)V
    .locals 7
    .param p1, "command"    # Lcom/navdy/obd/command/ObdCommand;
    .param p2, "listener"    # Lcom/navdy/obd/ObdJob$IListener;

    .prologue
    .line 1283
    iget-object v6, p0, Lcom/navdy/obd/ObdService;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService;->channel:Lcom/navdy/obd/io/IChannel;

    iget-object v3, p0, Lcom/navdy/obd/ObdService;->protocol:Lcom/navdy/obd/Protocol;

    iget-object v5, p0, Lcom/navdy/obd/ObdService;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1284
    return-void
.end method
