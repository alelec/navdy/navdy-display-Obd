.class Lcom/navdy/obd/discovery/BluetoothObdScanner$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothObdScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/discovery/BluetoothObdScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;


# direct methods
.method constructor <init>(Lcom/navdy/obd/discovery/BluetoothObdScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/discovery/BluetoothObdScanner;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 113
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.bluetooth.device.action.NAME_CHANGED"

    .line 117
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    :cond_0
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 121
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v3, "BluetoothObdScanner"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Friendly name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "android.bluetooth.device.extra.NAME"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v3

    const/16 v4, 0xc

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    invoke-static {v3, v1}, Lcom/navdy/obd/discovery/BluetoothObdScanner;->access$100(Lcom/navdy/obd/discovery/BluetoothObdScanner;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    new-instance v2, Lcom/navdy/obd/io/ChannelInfo;

    sget-object v3, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->BLUETOOTH:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .local v2, "info":Lcom/navdy/obd/io/ChannelInfo;
    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    iget-object v3, v3, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    iget-object v4, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    invoke-interface {v3, v4, v2}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V

    .line 131
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "info":Lcom/navdy/obd/io/ChannelInfo;
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    const-string v3, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 129
    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    iget-object v3, v3, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    iget-object v4, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;->this$0:Lcom/navdy/obd/discovery/BluetoothObdScanner;

    invoke-interface {v3, v4}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    goto :goto_0
.end method
