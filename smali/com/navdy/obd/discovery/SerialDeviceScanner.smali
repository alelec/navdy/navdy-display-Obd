.class public Lcom/navdy/obd/discovery/SerialDeviceScanner;
.super Lcom/navdy/obd/discovery/ObdChannelScanner;
.source "SerialDeviceScanner.java"


# static fields
.field private static final SERIAL_DEVICE:Ljava/lang/String; = "/dev/ttymxc3"

.field private static final SERIAL_PORTS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SerialDeviceScanner"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/dev/ttymxc3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/obd/discovery/SerialDeviceScanner;->SERIAL_PORTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/discovery/ObdChannelScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/discovery/SerialDeviceScanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/discovery/SerialDeviceScanner;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/navdy/obd/discovery/SerialDeviceScanner;->findDevices()V

    return-void
.end method

.method private findDevices()V
    .locals 6

    .prologue
    .line 41
    sget-object v2, Lcom/navdy/obd/discovery/SerialDeviceScanner;->SERIAL_PORTS:[Ljava/lang/String;

    .line 42
    .local v2, "ports":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 43
    new-instance v1, Lcom/navdy/obd/io/ChannelInfo;

    sget-object v3, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->SERIAL:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    const-string v4, "SerialPort"

    aget-object v5, v2, v0

    invoke-direct {v1, v3, v4, v5}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .local v1, "info":Lcom/navdy/obd/io/ChannelInfo;
    iget-object v3, p0, Lcom/navdy/obd/discovery/SerialDeviceScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v3, p0, v1}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    :cond_0
    return-void
.end method


# virtual methods
.method public startScan()Z
    .locals 2

    .prologue
    .line 23
    iget-object v1, p0, Lcom/navdy/obd/discovery/SerialDeviceScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v1, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 25
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/navdy/obd/discovery/SerialDeviceScanner$1;

    invoke-direct {v1, p0}, Lcom/navdy/obd/discovery/SerialDeviceScanner$1;-><init>(Lcom/navdy/obd/discovery/SerialDeviceScanner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 31
    const/4 v1, 0x1

    return v1
.end method

.method public stopScan()Z
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/obd/discovery/SerialDeviceScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v0, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 37
    const/4 v0, 0x1

    return v0
.end method
