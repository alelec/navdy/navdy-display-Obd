.class public Lcom/navdy/obd/discovery/BluetoothObdScanner;
.super Lcom/navdy/obd/discovery/ObdChannelScanner;
.source "BluetoothObdScanner.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothObdScanner"


# instance fields
.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private scanning:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/discovery/ObdChannelScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    .line 110
    new-instance v0, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;

    invoke-direct {v0, p0}, Lcom/navdy/obd/discovery/BluetoothObdScanner$2;-><init>(Lcom/navdy/obd/discovery/BluetoothObdScanner;)V

    iput-object v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 36
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/discovery/BluetoothObdScanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/discovery/BluetoothObdScanner;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/obd/discovery/BluetoothObdScanner;->enumerateBondedDevices()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/obd/discovery/BluetoothObdScanner;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/discovery/BluetoothObdScanner;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/navdy/obd/discovery/BluetoothObdScanner;->isObdDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method private enumerateBondedDevices()V
    .locals 8

    .prologue
    .line 90
    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 93
    .local v2, "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 94
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 95
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/navdy/obd/discovery/BluetoothObdScanner;->isObdDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 96
    new-instance v1, Lcom/navdy/obd/io/ChannelInfo;

    sget-object v4, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->BLUETOOTH:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 97
    .local v1, "info":Lcom/navdy/obd/io/ChannelInfo;
    iget-object v4, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v4, p0, v1}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V

    goto :goto_0

    .line 101
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    :cond_1
    return-void
.end method

.method private isObdDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 105
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "obd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public startScan()Z
    .locals 4

    .prologue
    .line 41
    iget-boolean v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    if-nez v2, :cond_1

    .line 42
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    .line 44
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 49
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 51
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 60
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 61
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/navdy/obd/discovery/BluetoothObdScanner$1;

    invoke-direct {v2, p0}, Lcom/navdy/obd/discovery/BluetoothObdScanner$1;-><init>(Lcom/navdy/obd/discovery/BluetoothObdScanner;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 70
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "handler":Landroid/os/Handler;
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    return v2
.end method

.method public stopScan()Z
    .locals 2

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    .line 78
    iget-object v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 86
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/obd/discovery/BluetoothObdScanner;->scanning:Z

    return v0
.end method
