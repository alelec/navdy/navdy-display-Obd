.class public Lcom/navdy/obd/discovery/MockObdScanner;
.super Lcom/navdy/obd/discovery/ObdChannelScanner;
.source "MockObdScanner.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/discovery/ObdChannelScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 15
    return-void
.end method


# virtual methods
.method public startScan()Z
    .locals 2

    .prologue
    .line 19
    iget-object v1, p0, Lcom/navdy/obd/discovery/MockObdScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v1, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 22
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/navdy/obd/discovery/MockObdScanner$1;

    invoke-direct {v1, p0}, Lcom/navdy/obd/discovery/MockObdScanner$1;-><init>(Lcom/navdy/obd/discovery/MockObdScanner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 28
    const/4 v1, 0x1

    return v1
.end method

.method public stopScan()Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/obd/discovery/MockObdScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v0, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 34
    const/4 v0, 0x1

    return v0
.end method
