.class public interface abstract Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;
.super Ljava/lang/Object;
.source "ObdChannelScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/discovery/ObdChannelScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V
.end method

.method public abstract onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
.end method

.method public abstract onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
.end method
