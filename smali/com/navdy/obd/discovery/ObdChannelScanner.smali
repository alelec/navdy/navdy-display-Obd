.class public abstract Lcom/navdy/obd/discovery/ObdChannelScanner;
.super Ljava/lang/Object;
.source "ObdChannelScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/navdy/obd/discovery/ObdChannelScanner;->mContext:Landroid/content/Context;

    .line 18
    iput-object p2, p0, Lcom/navdy/obd/discovery/ObdChannelScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .line 19
    return-void
.end method


# virtual methods
.method public abstract startScan()Z
.end method

.method public abstract stopScan()Z
.end method
