.class public Lcom/navdy/obd/discovery/GroupScanner;
.super Lcom/navdy/obd/discovery/ObdChannelScanner;
.source "GroupScanner.java"

# interfaces
.implements Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;


# static fields
.field public static final DEFAULT_CHANNELS:Ljava/lang/String; = "bluetooth,serial"

.field public static final DEFAULT_CHANNELS_PROPERTY_NAME:Ljava/lang/String; = "obd.channels"

.field public static final DEFAULT_HUD_CHANNELS:Ljava/lang/String; = "serial"

.field private static final TAG:Ljava/lang/String; = "GroupScanner"


# instance fields
.field discoveredChannels:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private scanners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/discovery/ObdChannelScanner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/discovery/ObdChannelScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 34
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/navdy/obd/discovery/GroupScanner;->scanners:Ljava/util/List;

    .line 36
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/navdy/obd/discovery/GroupScanner;->discoveredChannels:Ljava/util/HashSet;

    .line 41
    const-string v4, "obd.channels"

    invoke-direct {p0}, Lcom/navdy/obd/discovery/GroupScanner;->defaultChannels()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 42
    .local v3, "setting":Ljava/lang/String;
    const-string v4, ","

    invoke-static {v3, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "channelTypes":[Ljava/lang/String;
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 44
    .local v0, "channelType":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/navdy/obd/discovery/GroupScanner;->instantiateScanner(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/discovery/ObdChannelScanner;

    move-result-object v2

    .line 45
    .local v2, "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    if-eqz v2, :cond_0

    .line 46
    iget-object v6, p0, Lcom/navdy/obd/discovery/GroupScanner;->scanners:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 49
    .end local v0    # "channelType":Ljava/lang/String;
    .end local v2    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    :cond_1
    return-void
.end method

.method private defaultChannels()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "NAVDY_HUD-MX6DL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Display"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    const-string v0, "serial"

    .line 81
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "bluetooth,serial"

    goto :goto_0
.end method

.method private instantiateScanner(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/obd/discovery/ObdChannelScanner;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "channelType"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v2, 0x0

    .line 53
    .local v2, "type":Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 55
    :try_start_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->valueOf(Ljava/lang/String;)Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 60
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 61
    .local v1, "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    if-eqz v2, :cond_1

    .line 62
    sget-object v3, Lcom/navdy/obd/discovery/GroupScanner$1;->$SwitchMap$com$navdy$obd$io$ChannelInfo$ConnectionType:[I

    invoke-virtual {v2}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 74
    :cond_1
    :goto_1
    return-object v1

    .line 56
    .end local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "GroupScanner"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to parse channelType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    :pswitch_0
    new-instance v1, Lcom/navdy/obd/discovery/BluetoothObdScanner;

    .end local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    invoke-direct {v1, p1, p0}, Lcom/navdy/obd/discovery/BluetoothObdScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 65
    .restart local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    goto :goto_1

    .line 67
    :pswitch_1
    new-instance v1, Lcom/navdy/obd/discovery/MockObdScanner;

    .end local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    invoke-direct {v1, p1, p0}, Lcom/navdy/obd/discovery/MockObdScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .line 68
    .restart local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    goto :goto_1

    .line 70
    :pswitch_2
    new-instance v1, Lcom/navdy/obd/discovery/SerialDeviceScanner;

    .end local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    invoke-direct {v1, p1, p0}, Lcom/navdy/obd/discovery/SerialDeviceScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    .restart local v1    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    goto :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getDiscoveredChannels()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->discoveredChannels:Ljava/util/HashSet;

    return-object v0
.end method

.method public onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V
    .locals 1
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;
    .param p2, "obdDevice"    # Lcom/navdy/obd/io/ChannelInfo;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->discoveredChannels:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v0, p1, p2}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V

    .line 125
    :cond_0
    return-void
.end method

.method public onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
    .locals 1
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v0, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 111
    :cond_0
    return-void
.end method

.method public onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
    .locals 1
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/navdy/obd/discovery/GroupScanner;->mListener:Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;

    invoke-interface {v0, p0}, Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;->onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V

    .line 117
    :cond_0
    return-void
.end method

.method public startScan()Z
    .locals 3

    .prologue
    .line 91
    iget-object v1, p0, Lcom/navdy/obd/discovery/GroupScanner;->discoveredChannels:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 93
    iget-object v1, p0, Lcom/navdy/obd/discovery/GroupScanner;->scanners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/discovery/ObdChannelScanner;

    .line 94
    .local v0, "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    invoke-virtual {v0}, Lcom/navdy/obd/discovery/ObdChannelScanner;->startScan()Z

    goto :goto_0

    .line 96
    .end local v0    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public stopScan()Z
    .locals 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/navdy/obd/discovery/GroupScanner;->scanners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/discovery/ObdChannelScanner;

    .line 102
    .local v0, "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    invoke-virtual {v0}, Lcom/navdy/obd/discovery/ObdChannelScanner;->stopScan()Z

    goto :goto_0

    .line 104
    .end local v0    # "scanner":Lcom/navdy/obd/discovery/ObdChannelScanner;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method
