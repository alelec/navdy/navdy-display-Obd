.class public Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;
.super Lcom/navdy/obd/PidProcessor;
.source "InstantFuelConsumptionPidProcessor.java"


# static fields
.field public static final FUEL_STATUS_OPEN_LOOP:I = 0x4

.field private static final MINIMUM_FUEL_CONSUMPTION_WHEN_DECELERATING:D = 2.35214

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mHasFuelSystemStatus:Z

.field private mHasThrottlePosition:Z

.field private mMinThrottlePosition:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/navdy/obd/VehicleStateManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/navdy/obd/PidProcessor;-><init>()V

    .line 11
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    iput-wide v0, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mMinThrottlePosition:D

    return-void
.end method


# virtual methods
.method public isSupported(Lcom/navdy/obd/PidSet;)Z
    .locals 1
    .param p1, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 18
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasFuelSystemStatus:Z

    .line 19
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasThrottlePosition:Z

    .line 20
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processPidValue(Lcom/navdy/obd/PidLookupTable;)Z
    .locals 12
    .param p1, "vehicleState"    # Lcom/navdy/obd/PidLookupTable;

    .prologue
    .line 25
    const/16 v3, 0x10

    invoke-virtual {p1, v3}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v4

    .line 26
    .local v4, "maf":D
    const/16 v3, 0xd

    invoke-virtual {p1, v3}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v8

    .line 27
    .local v8, "vehicleSpeed":D
    const-wide/16 v0, 0x0

    .line 28
    .local v0, "cons":D
    iget-boolean v3, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasFuelSystemStatus:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasThrottlePosition:Z

    if-eqz v3, :cond_2

    .line 29
    const/16 v3, 0x11

    invoke-virtual {p1, v3}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v6

    .line 30
    .local v6, "throttlePosition":D
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v10

    double-to-int v3, v10

    int-to-short v2, v3

    .line 31
    .local v2, "fuelSystemStatus":S
    iget-wide v10, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mMinThrottlePosition:D

    cmpg-double v3, v6, v10

    if-gez v3, :cond_0

    const-wide/16 v10, 0x0

    cmpl-double v3, v6, v10

    if-lez v3, :cond_0

    .line 32
    iput-wide v6, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mMinThrottlePosition:D

    .line 34
    :cond_0
    iget-wide v10, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mMinThrottlePosition:D

    cmpg-double v3, v6, v10

    if-gez v3, :cond_1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 35
    sget-object v3, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->TAG:Ljava/lang/String;

    const-string v10, "Vehicle is decelerating"

    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const-wide v0, 0x4002d12ec6bce853L    # 2.35214

    .line 44
    .end local v2    # "fuelSystemStatus":S
    .end local v6    # "throttlePosition":D
    :goto_0
    const/16 v3, 0x100

    invoke-virtual {p1, v3, v0, v1}, Lcom/navdy/obd/PidLookupTable;->updatePid(ID)Z

    move-result v3

    return v3

    .line 39
    .restart local v2    # "fuelSystemStatus":S
    .restart local v6    # "throttlePosition":D
    :cond_1
    invoke-static {v4, v5, v8, v9}, Lcom/navdy/obd/Utility;->calculateInstantaneousFuelConsumption(DD)D

    move-result-wide v0

    goto :goto_0

    .line 42
    .end local v2    # "fuelSystemStatus":S
    .end local v6    # "throttlePosition":D
    :cond_2
    invoke-static {v4, v5, v8, v9}, Lcom/navdy/obd/Utility;->calculateInstantaneousFuelConsumption(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method public resolveDependencies()Lcom/navdy/obd/PidSet;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/navdy/obd/PidSet;

    invoke-direct {v0}, Lcom/navdy/obd/PidSet;-><init>()V

    .line 50
    .local v0, "requiredPids":Lcom/navdy/obd/PidSet;
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 51
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 52
    iget-boolean v1, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasFuelSystemStatus:Z

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 55
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/obd/InstantFuelConsumptionPidProcessor;->mHasThrottlePosition:Z

    if-eqz v1, :cond_1

    .line 56
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 59
    :cond_1
    return-object v0
.end method
