.class Lcom/navdy/obd/ObdService$1;
.super Lcom/navdy/obd/IObdService$Stub;
.source "ObdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ObdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Lcom/navdy/obd/IObdService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addListener(Lcom/navdy/obd/IObdServiceListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/obd/IObdServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/ObdService;->addListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 213
    return-void
.end method

.method public connect(Ljava/lang/String;Lcom/navdy/obd/IObdServiceListener;)V
    .locals 4
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/navdy/obd/IObdServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v2

    monitor-enter v2

    .line 108
    :try_start_7
    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_37

    .line 110
    invoke-static {p1}, Lcom/navdy/obd/io/ChannelInfo;->parse(Ljava/lang/String;)Lcom/navdy/obd/io/ChannelInfo;

    move-result-object v0

    .line 111
    .local v0, "info":Lcom/navdy/obd/io/ChannelInfo;
    if-eqz v0, :cond_36

    .line 112
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to connect to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v1, v0}, Lcom/navdy/obd/ObdService;->openChannel(Lcom/navdy/obd/io/ChannelInfo;)V

    .line 115
    :cond_36
    return-void

    .line 109
    .end local v0    # "info":Lcom/navdy/obd/io/ChannelInfo;
    :catchall_37
    move-exception v1

    :try_start_38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    throw v1
.end method

.method public disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/io/IChannel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 119
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/io/IChannel;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/obd/io/IChannel;->disconnect()V

    .line 121
    :cond_11
    return-void
.end method

.method public getFirmwareVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$400(Lcom/navdy/obd/ObdService;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v1

    .line 131
    :goto_6
    return-object v1

    .line 130
    :catch_7
    move-exception v0

    .line 131
    .local v0, "th":Ljava/lang/Throwable;
    const/4 v1, 0x0

    goto :goto_6
.end method

.method public getState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v0

    return v0
.end method

.method public readPid(I)Ljava/lang/String;
    .locals 8
    .param p1, "pid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 180
    :try_start_1
    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    new-instance v2, Lcom/navdy/obd/command/ObdCommand;

    const-string v3, "01%02x1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/navdy/obd/ObdService;->access$1400(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/command/ICommand;)Ljava/lang/String;
    :try_end_1b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1b} :catch_1c

    .line 183
    :goto_1b
    return-object v7

    .line 182
    :catch_1c
    move-exception v0

    .line 183
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    goto :goto_1b
.end method

.method public removeListener(Lcom/navdy/obd/IObdServiceListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/obd/IObdServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/ObdService;->removeListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 217
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1300(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$ServiceHandler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 168
    return-void
.end method

.method public scanPids(Ljava/util/List;I)V
    .locals 1
    .param p2, "intervalMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/obd/ObdService;->scanPids(Ljava/util/List;I)V

    .line 189
    return-void
.end method

.method public scanSupportedPids()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->scanSupportedPids()V

    .line 172
    return-void
.end method

.method public scanVIN()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->scanVIN()V

    .line 176
    return-void
.end method

.method public startRawScan()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$900(Lcom/navdy/obd/ObdService;)V

    .line 193
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1000(Lcom/navdy/obd/ObdService;)V

    .line 194
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1100(Lcom/navdy/obd/ObdService;)V

    .line 195
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    new-instance v1, Lcom/navdy/obd/ObdScanJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/io/IChannel;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1600(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/Protocol;

    move-result-object v3

    new-instance v4, Lcom/navdy/obd/ObdService$1$2;

    invoke-direct {v4, p0}, Lcom/navdy/obd/ObdService$1$2;-><init>(Lcom/navdy/obd/ObdService$1;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    .line 207
    invoke-static {v5}, Lcom/navdy/obd/ObdService;->access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/navdy/obd/ObdScanJob;-><init>(Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdScanJob$IRawData;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 195
    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$1502(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/ObdScanJob;)Lcom/navdy/obd/ObdScanJob;

    .line 208
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1200(Lcom/navdy/obd/ObdService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$1500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdScanJob;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 209
    return-void
.end method

.method public updateDeviceFirmware(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "updateFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 137
    :try_start_1
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    move-result-object v3

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/navdy/obd/update/ObdDeviceFirmwareManager;->isUpdatingFirmware()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 138
    :cond_15
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    move-result-object v3

    if-eqz v3, :cond_26

    .line 139
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "FirmwareManager is busy installing update already"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_26
    :goto_26
    return v6

    .line 143
    :cond_27
    new-instance v1, Lcom/navdy/obd/update/Update;

    invoke-direct {v1}, Lcom/navdy/obd/update/Update;-><init>()V

    .line 144
    .local v1, "update":Lcom/navdy/obd/update/Update;
    iput-object p1, v1, Lcom/navdy/obd/update/Update;->mVersion:Ljava/lang/String;

    .line 145
    iput-object p2, v1, Lcom/navdy/obd/update/Update;->mUpdateFilePath:Ljava/lang/String;

    .line 146
    new-instance v2, Lcom/navdy/obd/ObdService$1$1;

    invoke-direct {v2, p0, v1}, Lcom/navdy/obd/ObdService$1$1;-><init>(Lcom/navdy/obd/ObdService$1;Lcom/navdy/obd/update/Update;)V

    .line 155
    .local v2, "updateJob":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$900(Lcom/navdy/obd/ObdService;)V

    .line 156
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1000(Lcom/navdy/obd/ObdService;)V

    .line 157
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1100(Lcom/navdy/obd/ObdService;)V

    .line 158
    iget-object v3, p0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1200(Lcom/navdy/obd/ObdService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_4d
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_4d} :catch_4e

    goto :goto_26

    .line 160
    .end local v1    # "update":Lcom/navdy/obd/update/Update;
    .end local v2    # "updateJob":Ljava/lang/Runnable;
    :catch_4e
    move-exception v0

    .line 161
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception while updating the firmware :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_26
.end method
