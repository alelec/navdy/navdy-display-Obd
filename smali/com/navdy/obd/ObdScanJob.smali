.class public Lcom/navdy/obd/ObdScanJob;
.super Lcom/navdy/obd/ObdJob;
.source "ObdScanJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ObdScanJob$IRawData;
    }
.end annotation


# instance fields
.field private listener:Lcom/navdy/obd/ObdScanJob$IRawData;

.field private shuttingDown:Z


# direct methods
.method public constructor <init>(Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdScanJob$IRawData;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 1
    .param p1, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p2, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p3, "listener"    # Lcom/navdy/obd/ObdScanJob$IRawData;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->SCAN_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-direct {p0, v0, p1, p2, p4}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/ObdScanJob;->shuttingDown:Z

    .line 25
    iput-object p3, p0, Lcom/navdy/obd/ObdScanJob;->listener:Lcom/navdy/obd/ObdScanJob$IRawData;

    .line 26
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/obd/ObdScanJob;->shuttingDown:Z

    .line 30
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    .line 34
    sget-object v6, Lcom/navdy/obd/ObdScanJob;->Log:Lorg/slf4j/Logger;

    const-string v7, "Staring scan"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 35
    const/4 v4, 0x0

    .line 36
    .local v4, "input":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 39
    .local v5, "output":Ljava/io/OutputStream;
    :try_start_0
    iget-object v6, p0, Lcom/navdy/obd/ObdScanJob;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v6}, Lcom/navdy/obd/io/IChannel;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 40
    iget-object v6, p0, Lcom/navdy/obd/ObdScanJob;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v6}, Lcom/navdy/obd/io/IChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 41
    iget-object v6, p0, Lcom/navdy/obd/ObdScanJob;->command:Lcom/navdy/obd/command/ICommand;

    iget-object v7, p0, Lcom/navdy/obd/ObdScanJob;->protocol:Lcom/navdy/obd/Protocol;

    iget-object v8, p0, Lcom/navdy/obd/ObdScanJob;->commandObserver:Lcom/navdy/obd/command/IObdDataObserver;

    invoke-interface {v6, v4, v5, v7, v8}, Lcom/navdy/obd/command/ICommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 45
    :goto_0
    if-eqz v4, :cond_2

    .line 46
    const/16 v6, 0x50

    new-array v0, v6, [B

    .line 47
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 48
    .local v1, "bytesRead":I
    :cond_0
    :goto_1
    iget-boolean v6, p0, Lcom/navdy/obd/ObdScanJob;->shuttingDown:Z

    if-nez v6, :cond_2

    if-ltz v1, :cond_2

    .line 50
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->available()I

    move-result v6

    if-lez v6, :cond_1

    .line 51
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 52
    if-lez v1, :cond_0

    .line 53
    new-instance v2, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v2, v0, v6, v1}, Ljava/lang/String;-><init>([BII)V

    .line 54
    .local v2, "data":Ljava/lang/String;
    sget-object v6, Lcom/navdy/obd/ObdScanJob;->Log:Lorg/slf4j/Logger;

    const-string v7, "Scanned - {}"

    invoke-interface {v6, v7, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    iget-object v6, p0, Lcom/navdy/obd/ObdScanJob;->listener:Lcom/navdy/obd/ObdScanJob$IRawData;

    invoke-interface {v6, v2}, Lcom/navdy/obd/ObdScanJob$IRawData;->onRawData(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 60
    .end local v2    # "data":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 61
    .local v3, "e":Ljava/io/IOException;
    sget-object v6, Lcom/navdy/obd/ObdScanJob;->Log:Lorg/slf4j/Logger;

    const-string v7, "Exception while scanning"

    invoke-interface {v6, v7, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 42
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 43
    .restart local v3    # "e":Ljava/io/IOException;
    sget-object v6, Lcom/navdy/obd/ObdScanJob;->Log:Lorg/slf4j/Logger;

    const-string v7, "Failed executing command {}"

    iget-object v8, p0, Lcom/navdy/obd/ObdScanJob;->command:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v8}, Lcom/navdy/obd/command/ICommand;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    :cond_1
    const-wide/16 v6, 0xa

    :try_start_2
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 62
    :catch_2
    move-exception v3

    .line 63
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v6, Lcom/navdy/obd/ObdScanJob;->Log:Lorg/slf4j/Logger;

    const-string v7, "Interrupted"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 67
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_2
    return-void
.end method
