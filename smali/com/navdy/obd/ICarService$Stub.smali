.class public abstract Lcom/navdy/obd/ICarService$Stub;
.super Landroid/os/Binder;
.source "ICarService.java"

# interfaces
.implements Lcom/navdy/obd/ICarService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ICarService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ICarService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.navdy.obd.ICarService"

.field static final TRANSACTION_addListener:I = 0x6

.field static final TRANSACTION_applyConfiguration:I = 0xa

.field static final TRANSACTION_getBatteryVoltage:I = 0xd

.field static final TRANSACTION_getConnectionState:I = 0x1

.field static final TRANSACTION_getCurrentConfigurationName:I = 0x9

.field static final TRANSACTION_getEcus:I = 0x3

.field static final TRANSACTION_getMode:I = 0x15

.field static final TRANSACTION_getObdChipFirmwareVersion:I = 0x13

.field static final TRANSACTION_getProtocol:I = 0x4

.field static final TRANSACTION_getReadings:I = 0x5

.field static final TRANSACTION_getSupportedPids:I = 0x2

.field static final TRANSACTION_getTroubleCodes:I = 0x1b

.field static final TRANSACTION_getVIN:I = 0x8

.field static final TRANSACTION_isCheckEngineLightOn:I = 0x1a

.field static final TRANSACTION_isObdPidsScanningEnabled:I = 0xf

.field static final TRANSACTION_removeListener:I = 0x7

.field static final TRANSACTION_rescan:I = 0xc

.field static final TRANSACTION_setCANBusMonitoringListener:I = 0x17

.field static final TRANSACTION_setMode:I = 0x16

.field static final TRANSACTION_setObdPidsScanningEnabled:I = 0xe

.field static final TRANSACTION_setVoltageSettings:I = 0x12

.field static final TRANSACTION_sleep:I = 0x10

.field static final TRANSACTION_startCanBusMonitoring:I = 0x18

.field static final TRANSACTION_stopCanBusMonitoring:I = 0x19

.field static final TRANSACTION_updateFirmware:I = 0x14

.field static final TRANSACTION_updateScan:I = 0xb

.field static final TRANSACTION_wakeup:I = 0x11


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 27
    const-string v0, "com.navdy.obd.ICarService"

    invoke-virtual {p0, p0, v0}, Lcom/navdy/obd/ICarService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/ICarService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 35
    if-nez p0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 42
    :goto_0
    return-object v0

    .line 38
    :cond_0
    const-string v1, "com.navdy.obd.ICarService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 39
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/navdy/obd/ICarService;

    if-eqz v1, :cond_1

    .line 40
    check-cast v0, Lcom/navdy/obd/ICarService;

    goto :goto_0

    .line 42
    :cond_1
    new-instance v0, Lcom/navdy/obd/ICarService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/navdy/obd/ICarService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 46
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 50
    sparse-switch p1, :sswitch_data_0

    .line 301
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 54
    :sswitch_0
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :sswitch_1
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getConnectionState()I

    move-result v4

    .line 61
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 67
    .end local v4    # "_result":I
    :sswitch_2
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getSupportedPids()Ljava/util/List;

    move-result-object v6

    .line 69
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 75
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :sswitch_3
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getEcus()Ljava/util/List;

    move-result-object v3

    .line 77
    .local v3, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 83
    .end local v3    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    :sswitch_4
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getProtocol()Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_5
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    sget-object v8, Lcom/navdy/obd/Pid;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 94
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-virtual {p0, v1}, Lcom/navdy/obd/ICarService$Stub;->getReadings(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 95
    .restart local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 101
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :sswitch_6
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    sget-object v8, Lcom/navdy/obd/Pid;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 105
    .restart local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/obd/IPidListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IPidListener;

    move-result-object v2

    .line 106
    .local v2, "_arg1":Lcom/navdy/obd/IPidListener;
    invoke-virtual {p0, v1, v2}, Lcom/navdy/obd/ICarService$Stub;->addListener(Ljava/util/List;Lcom/navdy/obd/IPidListener;)V

    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 112
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v2    # "_arg1":Lcom/navdy/obd/IPidListener;
    :sswitch_7
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/obd/IPidListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IPidListener;

    move-result-object v0

    .line 115
    .local v0, "_arg0":Lcom/navdy/obd/IPidListener;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->removeListener(Lcom/navdy/obd/IPidListener;)V

    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 121
    .end local v0    # "_arg0":Lcom/navdy/obd/IPidListener;
    :sswitch_8
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getVIN()Ljava/lang/String;

    move-result-object v4

    .line 123
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 129
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_9
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getCurrentConfigurationName()Ljava/lang/String;

    move-result-object v4

    .line 131
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 137
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_a
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->applyConfiguration(Ljava/lang/String;)Z

    move-result v4

    .line 141
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 142
    if-eqz v4, :cond_0

    move v8, v9

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":Z
    :sswitch_b
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    .line 150
    sget-object v8, Lcom/navdy/obd/ScanSchedule;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/ScanSchedule;

    .line 156
    .local v0, "_arg0":Lcom/navdy/obd/ScanSchedule;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/obd/IPidListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IPidListener;

    move-result-object v2

    .line 157
    .restart local v2    # "_arg1":Lcom/navdy/obd/IPidListener;
    invoke-virtual {p0, v0, v2}, Lcom/navdy/obd/ICarService$Stub;->updateScan(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V

    .line 158
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 153
    .end local v0    # "_arg0":Lcom/navdy/obd/ScanSchedule;
    .end local v2    # "_arg1":Lcom/navdy/obd/IPidListener;
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/navdy/obd/ScanSchedule;
    goto :goto_1

    .line 163
    .end local v0    # "_arg0":Lcom/navdy/obd/ScanSchedule;
    :sswitch_c
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->rescan()V

    .line 165
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 170
    :sswitch_d
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getBatteryVoltage()D

    move-result-wide v4

    .line 172
    .local v4, "_result":D
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 173
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    goto/16 :goto_0

    .line 178
    .end local v4    # "_result":D
    :sswitch_e
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2

    move v0, v9

    .line 181
    .local v0, "_arg0":Z
    :goto_2
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->setObdPidsScanningEnabled(Z)V

    .line 182
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_2
    move v0, v8

    .line 180
    goto :goto_2

    .line 187
    :sswitch_f
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->isObdPidsScanningEnabled()Z

    move-result v4

    .line 189
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 190
    if-eqz v4, :cond_3

    move v8, v9

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 195
    .end local v4    # "_result":Z
    :sswitch_10
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_4

    move v0, v9

    .line 198
    .restart local v0    # "_arg0":Z
    :goto_3
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->sleep(Z)V

    .line 199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_4
    move v0, v8

    .line 197
    goto :goto_3

    .line 204
    :sswitch_11
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->wakeup()V

    .line 206
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 211
    :sswitch_12
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5

    .line 214
    sget-object v8, Lcom/navdy/obd/VoltageSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/VoltageSettings;

    .line 219
    .local v0, "_arg0":Lcom/navdy/obd/VoltageSettings;
    :goto_4
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->setVoltageSettings(Lcom/navdy/obd/VoltageSettings;)V

    .line 220
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 217
    .end local v0    # "_arg0":Lcom/navdy/obd/VoltageSettings;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/navdy/obd/VoltageSettings;
    goto :goto_4

    .line 225
    .end local v0    # "_arg0":Lcom/navdy/obd/VoltageSettings;
    :sswitch_13
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getObdChipFirmwareVersion()Ljava/lang/String;

    move-result-object v4

    .line 227
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 228
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_14
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/navdy/obd/ICarService$Stub;->updateFirmware(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 244
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_15
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getMode()I

    move-result v4

    .line 246
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 247
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 252
    .end local v4    # "_result":I
    :sswitch_16
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 256
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_6

    move v2, v9

    .line 257
    .local v2, "_arg1":Z
    :goto_5
    invoke-virtual {p0, v0, v2}, Lcom/navdy/obd/ICarService$Stub;->setMode(IZ)V

    .line 258
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_6
    move v2, v8

    .line 256
    goto :goto_5

    .line 263
    .end local v0    # "_arg0":I
    :sswitch_17
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v0

    .line 266
    .local v0, "_arg0":Lcom/navdy/obd/ICanBusMonitoringListener;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/ICarService$Stub;->setCANBusMonitoringListener(Lcom/navdy/obd/ICanBusMonitoringListener;)V

    .line 267
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 272
    .end local v0    # "_arg0":Lcom/navdy/obd/ICanBusMonitoringListener;
    :sswitch_18
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->startCanBusMonitoring()V

    .line 274
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 279
    :sswitch_19
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->stopCanBusMonitoring()V

    .line 281
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 286
    :sswitch_1a
    const-string v10, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 287
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->isCheckEngineLightOn()Z

    move-result v4

    .line 288
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 289
    if-eqz v4, :cond_7

    move v8, v9

    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 294
    .end local v4    # "_result":Z
    :sswitch_1b
    const-string v8, "com.navdy.obd.ICarService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Lcom/navdy/obd/ICarService$Stub;->getTroubleCodes()Ljava/util/List;

    move-result-object v7

    .line 296
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 297
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
