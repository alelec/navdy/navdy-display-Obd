.class public Lcom/navdy/obd/io/BluetoothChannel;
.super Ljava/lang/Object;
.source "BluetoothChannel.java"

# interfaces
.implements Lcom/navdy/obd/io/IChannel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field private static final SERIAL_UUID:Ljava/util/UUID;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

.field private final mSink:Lcom/navdy/obd/io/IChannelSink;

.field private mSocket:Landroid/bluetooth/BluetoothSocket;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/navdy/obd/io/BluetoothChannel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    .line 40
    const-string v0, "00001101-0000-1000-8000-00805F9B34FB"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/io/BluetoothChannel;->SERIAL_UUID:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/io/IChannelSink;)V
    .locals 1
    .param p1, "mSink"    # Lcom/navdy/obd/io/IChannelSink;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSink:Lcom/navdy/obd/io/IChannelSink;

    .line 56
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mState:I

    .line 58
    return-void
.end method

.method static synthetic access$000()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/obd/io/BluetoothChannel;->SERIAL_UUID:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/obd/io/BluetoothChannel;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/io/BluetoothChannel;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/navdy/obd/io/BluetoothChannel;->connectionFailed()V

    return-void
.end method

.method static synthetic access$302(Lcom/navdy/obd/io/BluetoothChannel;Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;)Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/io/BluetoothChannel;
    .param p1, "x1"    # Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    return-object p1
.end method

.method private connectionFailed()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSink:Lcom/navdy/obd/io/IChannelSink;

    const-string v1, "Unable to connect device"

    invoke-interface {v0, v1}, Lcom/navdy/obd/io/IChannelSink;->onMessage(Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/obd/io/BluetoothChannel;->setState(I)V

    .line 146
    return-void
.end method

.method private declared-synchronized setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/io/BluetoothChannel;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iput p1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mState:I

    .line 68
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSink:Lcom/navdy/obd/io/IChannelSink;

    invoke-interface {v0, p1}, Lcom/navdy/obd/io/IChannelSink;->onStateChange(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized connect(Ljava/lang/String;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 84
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v1, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connect to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mState:I

    if-ne v1, v4, :cond_0

    .line 88
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    invoke-virtual {v1}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .line 92
    :cond_0
    new-instance v1, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    invoke-direct {v1, p0, v0}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;-><init>(Lcom/navdy/obd/io/BluetoothChannel;Landroid/bluetooth/BluetoothDevice;)V

    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .line 93
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    invoke-virtual {v1}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->start()V

    .line 94
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/navdy/obd/io/BluetoothChannel;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 82
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "socket"    # Landroid/bluetooth/BluetoothSocket;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    const-string v1, "connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .line 112
    :cond_0
    iput-object p1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    .line 114
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSink:Lcom/navdy/obd/io/IChannelSink;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connected to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/obd/io/IChannelSink;->onMessage(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/obd/io/BluetoothChannel;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized disconnect()V
    .locals 1

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/obd/io/BluetoothChannel;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getState()I
    .locals 1

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/obd/io/BluetoothChannel;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 3

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    const-string v2, "stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    invoke-virtual {v1}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mConnectThread:Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 128
    :try_start_1
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 132
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    .line 136
    :cond_1
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/obd/io/BluetoothChannel;->setState(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    monitor-exit p0

    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v1, Lcom/navdy/obd/io/BluetoothChannel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to close bluetooth socket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 132
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 132
    :catchall_1
    move-exception v1

    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lcom/navdy/obd/io/BluetoothChannel;->mSocket:Landroid/bluetooth/BluetoothSocket;

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
