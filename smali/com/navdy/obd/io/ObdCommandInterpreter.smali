.class public Lcom/navdy/obd/io/ObdCommandInterpreter;
.super Ljava/lang/Object;
.source "ObdCommandInterpreter.java"


# static fields
.field private static final CARRIAGE_RETURN:I = 0xd

.field static final Log:Lorg/slf4j/Logger;

.field public static final NANOS_PER_MS:J = 0xf4240L

.field public static final TERMINAL_INPUT_INDICATOR:C = '>'


# instance fields
.field public debug:Z

.field private didGetTextResponse:Z

.field private inputStream:Ljava/io/InputStream;

.field private isInterrupted:Z

.field private lastCommandBytes:[B

.field private lastReadCharacter:I

.field private outputStream:Ljava/io/OutputStream;

.field private readTimeOutMillis:J

.field private readTimeOutNanos:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/navdy/obd/io/ObdCommandInterpreter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "outputStream"    # Ljava/io/OutputStream;

    .prologue
    const-wide/16 v2, 0x3e8

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutMillis:J

    .line 24
    iget-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutMillis:J

    mul-long/2addr v0, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->debug:Z

    .line 32
    iput-object p1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->inputStream:Ljava/io/InputStream;

    .line 33
    iput-object p2, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->outputStream:Ljava/io/OutputStream;

    .line 34
    return-void
.end method


# virtual methods
.method public clearTerminal()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 122
    invoke-virtual {p0, v0, v0}, Lcom/navdy/obd/io/ObdCommandInterpreter;->clearTerminal(ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clearTerminal(ZZ)Ljava/lang/String;
    .locals 8
    .param p1, "mayInterrupt"    # Z
    .param p2, "timed"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 136
    sget-object v3, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v6, "ObdCommandInterpreter : clearTerminal"

    invoke-interface {v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    .local v2, "responseBuilder":Ljava/lang/StringBuilder;
    const-wide/16 v0, 0x0

    .line 139
    .local v0, "elapsedTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 140
    .local v4, "start":J
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    iput v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/4 v6, -0x1

    if-eq v3, v6, :cond_2

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz p2, :cond_1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long v0, v6, v4

    iget-wide v6, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v3, v0, v6

    if-gtz v3, :cond_2

    .line 141
    :cond_1
    iget v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/4 v6, -0x2

    if-ne v3, v6, :cond_3

    .line 142
    sget-object v3, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v6, " ObdCommandInterpreter : terminalClear, response {}"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 148
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 149
    if-eqz p2, :cond_4

    iget-wide v6, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v3, v0, v6

    if-lez v3, :cond_4

    .line 150
    new-instance v3, Ljava/util/concurrent/TimeoutException;

    const-string v6, "Clearing terminal timed out"

    invoke-direct {v3, v6}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 144
    :cond_3
    iget v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    if-ltz v3, :cond_0

    .line 145
    iget v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 152
    :cond_4
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 153
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->isInterrupted:Z

    .line 154
    sget-object v3, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v6, "readResponse, Thread is interrupted"

    invoke-interface {v3, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    new-instance v3, Ljava/lang/InterruptedException;

    invoke-direct {v3}, Ljava/lang/InterruptedException;-><init>()V

    throw v3

    .line 157
    :cond_5
    sget-object v3, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v6, "Clearing terminal took {} ms"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public clearTerminalBulk()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-virtual {p0, v0, v0}, Lcom/navdy/obd/io/ObdCommandInterpreter;->clearTerminalBulk(ZZ)V

    .line 127
    return-void
.end method

.method public clearTerminalBulk(ZZ)V
    .locals 10
    .param p1, "mayInterrupt"    # Z
    .param p2, "timed"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 162
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "ObdCommandInterpreter : clearTerminal, timed {}"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    const-wide/16 v2, 0x0

    .line 164
    .local v2, "elapsedTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 165
    .local v6, "start":J
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_1
    if-eqz p2, :cond_2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v2, v8, v6

    iget-wide v8, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v5, v2, v8

    if-gtz v5, :cond_3

    .line 167
    :cond_2
    const/16 v5, 0x4e20

    :try_start_0
    new-array v0, v5, [B

    .line 168
    .local v0, "buffer":[B
    invoke-virtual {p0, v0}, Lcom/navdy/obd/io/ObdCommandInterpreter;->read([B)I

    move-result v4

    .line 169
    .local v4, "length":I
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "clearTerminal length {} "

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 170
    if-gtz v4, :cond_0

    .line 171
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "Terminal clear"

    invoke-interface {v5, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 183
    .end local v0    # "buffer":[B
    .end local v4    # "length":I
    :cond_3
    :goto_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v2, v8, v6

    .line 184
    if-eqz p2, :cond_4

    iget-wide v8, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v5, v2, v8

    if-lez v5, :cond_4

    .line 185
    new-instance v5, Ljava/util/concurrent/TimeoutException;

    const-string v8, "Clearing terminal timed out"

    invoke-direct {v5, v8}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 174
    :catch_0
    move-exception v1

    .line 175
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "Interrupted exception {}"

    invoke-interface {v5, v8, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 178
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 179
    .local v1, "e":Ljava/io/IOException;
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "IOException exception {}"

    invoke-interface {v5, v8, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 187
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    if-eqz p1, :cond_5

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 196
    :goto_1
    return-void

    .line 190
    :cond_5
    if-eqz p1, :cond_6

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 191
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->isInterrupted:Z

    .line 192
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "readResponse, Thread is interrupted"

    invoke-interface {v5, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 193
    new-instance v5, Ljava/lang/InterruptedException;

    invoke-direct {v5}, Ljava/lang/InterruptedException;-><init>()V

    throw v5

    .line 195
    :cond_6
    sget-object v5, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "Clearing terminal took {} ms"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public didGetTextResponse()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse:Z

    return v0
.end method

.method public getCommandBytes()[B
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    return-object v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 1
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    return v0
.end method

.method public readLine()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/obd/io/ObdCommandInterpreter;->readResponse(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readResponse()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/obd/io/ObdCommandInterpreter;->readResponse(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readResponse(Z)Ljava/lang/String;
    .locals 10
    .param p1, "readCompleteResponse"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v3, "responseBuilder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse:Z

    .line 78
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 79
    .local v4, "start":J
    const-wide/16 v0, 0x0

    .line 80
    .local v0, "elapsedTime":J
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7

    if-nez v7, :cond_a

    iget-object v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    iput v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_a

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x3e

    if-eq v7, v8, :cond_a

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v0, v8, v4

    iget-wide v8, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v7, v0, v8

    if-gez v7, :cond_a

    .line 81
    iget-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->debug:Z

    if-eqz v7, :cond_1

    .line 82
    sget-object v7, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "Character {}"

    iget v9, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    int-to-char v9, v9

    invoke-static {v9}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    :cond_1
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/4 v8, -0x2

    if-ne v7, v8, :cond_2

    .line 85
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 86
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->isInterrupted:Z

    .line 87
    new-instance v7, Ljava/lang/InterruptedException;

    invoke-direct {v7}, Ljava/lang/InterruptedException;-><init>()V

    throw v7

    .line 92
    :cond_2
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x30

    if-lt v7, v8, :cond_3

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x39

    if-le v7, v8, :cond_5

    :cond_3
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x41

    if-lt v7, v8, :cond_4

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x46

    if-le v7, v8, :cond_5

    :cond_4
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x61

    if-lt v7, v8, :cond_b

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x66

    if-gt v7, v8, :cond_b

    :cond_5
    const/4 v6, 0x1

    .line 93
    .local v6, "validHexDigit":Z
    :goto_0
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x20

    if-eq v7, v8, :cond_6

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0xd

    if-ne v7, v8, :cond_c

    :cond_6
    const/4 v2, 0x1

    .line 94
    .local v2, "isWhiteSpace":Z
    :goto_1
    if-nez v6, :cond_7

    if-nez v2, :cond_7

    .line 95
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse:Z

    .line 97
    :cond_7
    iget-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse:Z

    if-nez v7, :cond_8

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x20

    if-eq v7, v8, :cond_9

    .line 98
    :cond_8
    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    int-to-char v7, v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 100
    :cond_9
    if-nez p1, :cond_0

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0xd

    if-ne v7, v8, :cond_0

    .line 104
    .end local v2    # "isWhiteSpace":Z
    .end local v6    # "validHexDigit":Z
    :cond_a
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 105
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->isInterrupted:Z

    .line 106
    sget-object v7, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "readResponse, Thread is interrupted"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 107
    new-instance v7, Ljava/lang/InterruptedException;

    invoke-direct {v7}, Ljava/lang/InterruptedException;-><init>()V

    throw v7

    .line 92
    :cond_b
    const/4 v6, 0x0

    goto :goto_0

    .line 93
    .restart local v6    # "validHexDigit":Z
    :cond_c
    const/4 v2, 0x0

    goto :goto_1

    .line 109
    .end local v6    # "validHexDigit":Z
    :cond_d
    iget-wide v8, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    cmp-long v7, v0, v8

    if-lez v7, :cond_e

    .line 110
    const-wide/32 v8, 0xf4240

    div-long/2addr v0, v8

    .line 111
    sget-object v7, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v8, "ObdCommandInterpreter : readResponse, timed out"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 112
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Timeout after "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 115
    :cond_e
    if-eqz p1, :cond_f

    iget v7, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastReadCharacter:I

    const/16 v8, 0x3e

    if-eq v7, v8, :cond_f

    .line 116
    new-instance v7, Ljava/io/IOException;

    const-string v8, "ObdCommandInterpreter : Unexpected end of stream"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 118
    :cond_f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public setReadTimeOut(I)V
    .locals 4
    .param p1, "readTimeOutMillis"    # I

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v1, "ObdCommandInterpreter : setReadTimeOut {} "

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 38
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutMillis:J

    .line 39
    iget-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutMillis:J

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->readTimeOutNanos:J

    .line 40
    return-void
.end method

.method public write(Ljava/lang/String;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 48
    sget-object v1, Lcom/navdy/obd/io/ObdCommandInterpreter;->Log:Lorg/slf4j/Logger;

    const-string v2, "Command {} "

    invoke-interface {v1, v2, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 50
    .local v0, "length":I
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    .line 51
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    invoke-static {v1, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    iget-object v1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    const/16 v2, 0xd

    aput-byte v2, v1, v0

    .line 53
    iget-object v1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->outputStream:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 54
    iget-object v1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 55
    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1, "commandBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 59
    iget-object v0, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 60
    iput-object p1, p0, Lcom/navdy/obd/io/ObdCommandInterpreter;->lastCommandBytes:[B

    .line 61
    return-void
.end method
