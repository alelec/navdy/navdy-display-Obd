.class public Lcom/navdy/obd/io/ChannelInfo;
.super Ljava/lang/Object;
.source "ChannelInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    }
.end annotation


# static fields
.field private static final SEPARATOR:Ljava/lang/String; = "|"

.field private static final SEPARATOR_REGEX:Ljava/lang/String; = "\\|"


# instance fields
.field private address:Ljava/lang/String;

.field private bonded:Z

.field private connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "connectionType"    # Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "connectionType"    # Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "bonded"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/io/ChannelInfo;->bonded:Z

    .line 24
    iput-object p1, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    .line 25
    iput-object p2, p0, Lcom/navdy/obd/io/ChannelInfo;->name:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    .line 27
    iput-boolean p4, p0, Lcom/navdy/obd/io/ChannelInfo;->bonded:Z

    .line 28
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/navdy/obd/io/ChannelInfo;
    .locals 5
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 31
    if-nez p0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-object v1

    .line 34
    :cond_1
    const-string v2, "\\|"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "parts":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 36
    new-instance v1, Lcom/navdy/obd/io/ChannelInfo;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->valueOf(Ljava/lang/String;)Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 73
    check-cast v0, Lcom/navdy/obd/io/ChannelInfo;

    .line 75
    .local v0, "that":Lcom/navdy/obd/io/ChannelInfo;
    iget-object v3, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    iget-object v4, v0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getConnectionType()Lcom/navdy/obd/io/ChannelInfo$ConnectionType;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/obd/io/ChannelInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 80
    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    invoke-virtual {v1}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->hashCode()I

    move-result v0

    .line 81
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 82
    return v0
.end method

.method public isBonded()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/navdy/obd/io/ChannelInfo;->bonded:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->connectionType:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/obd/io/ChannelInfo;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
