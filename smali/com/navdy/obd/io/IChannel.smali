.class public interface abstract Lcom/navdy/obd/io/IChannel;
.super Ljava/lang/Object;
.source "IChannel.java"


# static fields
.field public static final READ_TIMED_OUT:I = -0x2

.field public static final STATE_CONNECTED:I = 0x2

.field public static final STATE_CONNECTING:I = 0x1

.field public static final STATE_DISCONNECTED:I = 0x0

.field public static final STATE_IDLE:I = 0x4

.field public static final STATE_INITIALIZING:I = 0x3

.field public static final STATE_SLEEPING:I = 0x5


# virtual methods
.method public abstract connect(Ljava/lang/String;)V
.end method

.method public abstract disconnect()V
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getOutputStream()Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getState()I
.end method
