.class Lcom/navdy/obd/io/STNSerialChannel$1;
.super Ljava/lang/Object;
.source "STNSerialChannel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/io/STNSerialChannel;->connect(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/io/STNSerialChannel;

.field final synthetic val$address:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/io/STNSerialChannel;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    iput-object p2, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->val$address:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 83
    const/4 v2, 0x0

    .line 84
    .local v2, "success":Z
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Lcom/navdy/obd/io/STNSerialChannel;->access$000(Lcom/navdy/obd/io/STNSerialChannel;I)V

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v1, "possibleBaudRatesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4}, Lcom/navdy/obd/io/STNSerialChannel;->access$100(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4}, Lcom/navdy/obd/io/STNSerialChannel;->access$200(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v4

    iget-object v6, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v6}, Lcom/navdy/obd/io/STNSerialChannel;->access$100(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v6

    if-eq v4, v6, :cond_0

    .line 90
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4}, Lcom/navdy/obd/io/STNSerialChannel;->access$200(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    invoke-static {}, Lcom/navdy/obd/io/STNSerialChannel;->access$300()[I

    move-result-object v6

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_2

    aget v0, v6, v4

    .line 93
    .local v0, "possibleBaudRate":I
    iget-object v8, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v8}, Lcom/navdy/obd/io/STNSerialChannel;->access$100(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v8

    if-eq v0, v8, :cond_1

    iget-object v8, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v8}, Lcom/navdy/obd/io/STNSerialChannel;->access$200(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v8

    if-eq v0, v8, :cond_1

    .line 94
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "possibleBaudRate":I
    :cond_2
    const/4 v3, -0x1

    .line 97
    .local v3, "successBaudrate":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 98
    .restart local v0    # "possibleBaudRate":I
    sget-object v4, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Trying to connect at baud rate : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    iget-object v7, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->val$address:Ljava/lang/String;

    invoke-static {v4, v7, v0}, Lcom/navdy/obd/io/STNSerialChannel;->access$400(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;I)Z

    move-result v2

    .line 100
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 101
    sget-object v4, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Connection attempt has been interrupted, Attempt , Baud rate : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Success : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 102
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->access$000(Lcom/navdy/obd/io/STNSerialChannel;I)V

    .line 127
    .end local v0    # "possibleBaudRate":I
    :goto_1
    return-void

    .line 105
    .restart local v0    # "possibleBaudRate":I
    :cond_4
    if-eqz v2, :cond_3

    .line 106
    move v3, v0

    .line 110
    .end local v0    # "possibleBaudRate":I
    :cond_5
    const/4 v4, -0x1

    if-ne v3, v4, :cond_6

    .line 111
    sget-object v4, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v6, "Not able to communicate with chip at any baud rate"

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 112
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->access$000(Lcom/navdy/obd/io/STNSerialChannel;I)V

    goto :goto_1

    .line 115
    :cond_6
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4}, Lcom/navdy/obd/io/STNSerialChannel;->access$200(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 116
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    iget-object v5, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->val$address:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->access$500(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_7
    sget-object v4, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Successfully connected at Baud rate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , Trying to change to required baud rate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 119
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    iget-object v6, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->val$address:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v7}, Lcom/navdy/obd/io/STNSerialChannel;->access$200(Lcom/navdy/obd/io/STNSerialChannel;)I

    move-result v7

    invoke-static {v4, v6, v3, v7}, Lcom/navdy/obd/io/STNSerialChannel;->access$600(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;II)Z

    move-result v2

    .line 120
    if-eqz v2, :cond_8

    .line 121
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    iget-object v5, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->val$address:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->access$500(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_8
    iget-object v4, p0, Lcom/navdy/obd/io/STNSerialChannel$1;->this$0:Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->access$000(Lcom/navdy/obd/io/STNSerialChannel;I)V

    goto :goto_1
.end method
