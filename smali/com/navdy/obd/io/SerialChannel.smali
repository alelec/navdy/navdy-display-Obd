.class public Lcom/navdy/obd/io/SerialChannel;
.super Ljava/lang/Object;
.source "SerialChannel.java"

# interfaces
.implements Lcom/navdy/obd/io/IChannel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/io/SerialChannel$SerialInputStream;,
        Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;
    }
.end annotation


# static fields
.field public static final DEFAULT_BAUD_RATE:I = 0x2580

.field private static final DEFAULT_BAUD_RATE_PROP:Ljava/lang/String; = "persist.sys.obd.baudrate"

.field public static final SERIAL_SERIVCE:Ljava/lang/String; = "serial"

.field private static final TAG:Ljava/lang/String; = "SerialChannel"


# instance fields
.field private baudRate:I

.field private final context:Landroid/content/Context;

.field private serialPort:Lcom/navdy/hardware/SerialPort;

.field private final sink:Lcom/navdy/obd/io/IChannelSink;

.field private state:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sink"    # Lcom/navdy/obd/io/IChannelSink;

    .prologue
    .line 31
    const/16 v0, 0x2580

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sink"    # Lcom/navdy/obd/io/IChannelSink;
    .param p3, "baudRate"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/io/SerialChannel;->state:I

    .line 28
    const/16 v0, 0x2580

    iput v0, p0, Lcom/navdy/obd/io/SerialChannel;->baudRate:I

    .line 34
    iput-object p1, p0, Lcom/navdy/obd/io/SerialChannel;->context:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/navdy/obd/io/SerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    .line 36
    iput p3, p0, Lcom/navdy/obd/io/SerialChannel;->baudRate:I

    .line 37
    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/navdy/obd/io/SerialChannel;->state:I

    if-eq p1, v0, :cond_0

    .line 46
    iput p1, p0, Lcom/navdy/obd/io/SerialChannel;->state:I

    .line 47
    iget-object v0, p0, Lcom/navdy/obd/io/SerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/navdy/obd/io/SerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    invoke-interface {v0, p1}, Lcom/navdy/obd/io/IChannelSink;->onStateChange(I)V

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public connect(Ljava/lang/String;)V
    .locals 4
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 57
    :try_start_0
    new-instance v1, Lcom/navdy/hardware/SerialPort;

    invoke-direct {v1, p1}, Lcom/navdy/hardware/SerialPort;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    .line 58
    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    iget v2, p0, Lcom/navdy/obd/io/SerialChannel;->baudRate:I

    invoke-virtual {v1, v2}, Lcom/navdy/hardware/SerialPort;->open(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/navdy/obd/io/SerialChannel;->setState(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "SerialChannel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get fd for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-virtual {v1}, Lcom/navdy/hardware/SerialPort;->close()V

    .line 72
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/obd/io/SerialChannel;->setState(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "SerialChannel"

    const-string v2, "Failed to close underlying file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Lcom/navdy/obd/io/SerialChannel$SerialInputStream;

    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-direct {v0, v1}, Lcom/navdy/obd/io/SerialChannel$SerialInputStream;-><init>(Lcom/navdy/hardware/SerialPort;)V

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    new-instance v0, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;

    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-direct {v0, v1}, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;-><init>(Lcom/navdy/hardware/SerialPort;)V

    return-object v0
.end method

.method public getSerialPort()Lcom/navdy/hardware/SerialPort;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/obd/io/SerialChannel;->serialPort:Lcom/navdy/hardware/SerialPort;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/obd/io/SerialChannel;->state:I

    return v0
.end method
