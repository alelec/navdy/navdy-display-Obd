.class Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;
.super Ljava/lang/Thread;
.source "BluetoothChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/io/BluetoothChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectThread"
.end annotation


# instance fields
.field private final mmDevice:Landroid/bluetooth/BluetoothDevice;

.field private mmSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/navdy/obd/io/BluetoothChannel;


# direct methods
.method public constructor <init>(Lcom/navdy/obd/io/BluetoothChannel;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->this$0:Lcom/navdy/obd/io/BluetoothChannel;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 168
    iput-object p2, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    .line 169
    const/4 v1, 0x0

    .line 174
    .local v1, "tmp":Landroid/bluetooth/BluetoothSocket;
    :try_start_0
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$000()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 178
    :goto_0
    iput-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    .line 179
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "create() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 231
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "close() of connect socket failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    .line 182
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BEGIN mConnectThread"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const-string v4, "ConnectThread"

    invoke-virtual {p0, v4}, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->setName(Ljava/lang/String;)V

    .line 192
    :try_start_0
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :cond_0
    iget-object v5, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->this$0:Lcom/navdy/obd/io/BluetoothChannel;

    monitor-enter v5

    .line 222
    :try_start_1
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->this$0:Lcom/navdy/obd/io/BluetoothChannel;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/navdy/obd/io/BluetoothChannel;->access$302(Lcom/navdy/obd/io/BluetoothChannel;Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;)Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;

    .line 223
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->this$0:Lcom/navdy/obd/io/BluetoothChannel;

    iget-object v5, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    iget-object v6, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4, v5, v6}, Lcom/navdy/obd/io/BluetoothChannel;->connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V

    .line 227
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Exception connecting"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 195
    const/4 v3, 0x1

    .line 198
    .local v3, "fallbackFailed":Z
    :try_start_2
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "trying fallback..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "createRfcommSocket"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothSocket;

    iput-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    .line 201
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothSocket;->connect()V

    .line 203
    const/4 v3, 0x0

    .line 204
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Connected"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 208
    :goto_1
    if-eqz v3, :cond_0

    .line 209
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->this$0:Lcom/navdy/obd/io/BluetoothChannel;

    invoke-static {v4}, Lcom/navdy/obd/io/BluetoothChannel;->access$200(Lcom/navdy/obd/io/BluetoothChannel;)V

    .line 212
    :try_start_3
    iget-object v4, p0, Lcom/navdy/obd/io/BluetoothChannel$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 213
    :catch_1
    move-exception v2

    .line 214
    .local v2, "e2":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "unable to close() socket during connection failure"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 205
    .end local v2    # "e2":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 206
    .local v1, "e1":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/obd/io/BluetoothChannel;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Failed to connect using fallback"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 223
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e1":Ljava/lang/Exception;
    .end local v3    # "fallbackFailed":Z
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4
.end method
