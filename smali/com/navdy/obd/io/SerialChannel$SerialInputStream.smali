.class Lcom/navdy/obd/io/SerialChannel$SerialInputStream;
.super Ljava/io/InputStream;
.source "SerialChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/io/SerialChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SerialInputStream"
.end annotation


# instance fields
.field serialPort:Lcom/navdy/hardware/SerialPort;


# direct methods
.method public constructor <init>(Lcom/navdy/hardware/SerialPort;)V
    .locals 0
    .param p1, "serialPort"    # Lcom/navdy/hardware/SerialPort;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/navdy/obd/io/SerialChannel$SerialInputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    .line 107
    return-void
.end method


# virtual methods
.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 112
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/navdy/obd/io/SerialChannel$SerialInputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-virtual {v2, v0}, Lcom/navdy/hardware/SerialPort;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 113
    .local v1, "result":I
    if-gez v1, :cond_0

    .line 118
    .end local v1    # "result":I
    :goto_0
    return v1

    .line 115
    .restart local v1    # "result":I
    :cond_0
    if-nez v1, :cond_1

    .line 116
    const/4 v1, -0x2

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    and-int/lit16 v1, v2, 0xff

    goto :goto_0
.end method

.method public read([B)I
    .locals 3
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    array-length v2, p1

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 126
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/navdy/obd/io/SerialChannel$SerialInputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-virtual {v2, v0}, Lcom/navdy/hardware/SerialPort;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 127
    .local v1, "length":I
    if-lez v1, :cond_0

    .line 128
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 130
    :cond_0
    return v1
.end method
