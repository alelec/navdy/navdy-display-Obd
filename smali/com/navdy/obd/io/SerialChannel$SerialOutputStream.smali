.class Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;
.super Ljava/io/OutputStream;
.source "SerialChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/io/SerialChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SerialOutputStream"
.end annotation


# instance fields
.field serialPort:Lcom/navdy/hardware/SerialPort;


# direct methods
.method public constructor <init>(Lcom/navdy/hardware/SerialPort;)V
    .locals 0
    .param p1, "serialPort"    # Lcom/navdy/hardware/SerialPort;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    .line 83
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 4
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 87
    new-array v0, v3, [B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 88
    .local v0, "bytes":[B
    iget-object v1, p0, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hardware/SerialPort;->write(Ljava/nio/ByteBuffer;I)V

    .line 89
    return-void
.end method

.method public write([B)V
    .locals 3
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hardware/SerialPort;->write(Ljava/nio/ByteBuffer;I)V

    .line 99
    return-void
.end method

.method public write([BII)V
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/obd/io/SerialChannel$SerialOutputStream;->serialPort:Lcom/navdy/hardware/SerialPort;

    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/navdy/hardware/SerialPort;->write(Ljava/nio/ByteBuffer;I)V

    .line 94
    return-void
.end method
