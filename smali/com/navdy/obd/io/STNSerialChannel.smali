.class public Lcom/navdy/obd/io/STNSerialChannel;
.super Ljava/lang/Object;
.source "STNSerialChannel.java"

# interfaces
.implements Lcom/navdy/obd/io/IChannel;
.implements Lcom/navdy/obd/io/IChannelSink;


# static fields
.field private static final CARRIAGE_RETURN:I = 0xd

.field public static final CONNECT_TIMEOUT_MILLIS:I = 0xea60

.field private static final DEFAULT_BAUD_RATE:I = 0x2580

.field static final Log:Lorg/slf4j/Logger;

.field public static final NANOS_PER_MS:J = 0xf4240L

.field public static final OK:Ljava/lang/String; = "ok"

.field private static final POSSIBLE_BAUD_RATES_TABLE:[I

.field public static final SET_BAUD_RATE_COMMAND:Ljava/lang/String; = "STSBR"

.field public static final TIMEOUT_MILLIS:I = 0xbb8

.field private static singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private baudRate:I

.field private final context:Landroid/content/Context;

.field private future:Ljava/util/concurrent/Future;

.field private lastConnectedBaudRate:I

.field private serialChannel:Lcom/navdy/obd/io/SerialChannel;

.field private final sink:Lcom/navdy/obd/io/IChannelSink;

.field private volatile state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/navdy/obd/io/STNSerialChannel;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    .line 35
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/obd/io/STNSerialChannel;->POSSIBLE_BAUD_RATES_TABLE:[I

    .line 47
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/io/STNSerialChannel;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0x2580
        0x4b00
        0x9600
        0xe100
        0x1c200
        0x38400
        0x70800
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sink"    # Lcom/navdy/obd/io/IChannelSink;
    .param p3, "lastConnectedBaudRate"    # I
    .param p4, "baudRate"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    .line 46
    const/16 v0, 0x2580

    iput v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    .line 53
    iput-object p1, p0, Lcom/navdy/obd/io/STNSerialChannel;->context:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/navdy/obd/io/STNSerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    .line 55
    iput p4, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    .line 56
    iput p3, p0, Lcom/navdy/obd/io/STNSerialChannel;->lastConnectedBaudRate:I

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/io/STNSerialChannel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;
    .param p1, "x1"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/navdy/obd/io/STNSerialChannel;->setState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/obd/io/STNSerialChannel;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->lastConnectedBaudRate:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/obd/io/STNSerialChannel;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    return v0
.end method

.method static synthetic access$300()[I
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/obd/io/STNSerialChannel;->POSSIBLE_BAUD_RATES_TABLE:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/io/STNSerialChannel;->tryConnectingAtBaudRate(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/navdy/obd/io/STNSerialChannel;->onSuccess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/io/STNSerialChannel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/obd/io/STNSerialChannel;->setBaudRateAndConnect(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method private onSuccess(Ljava/lang/String;)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 169
    sget-object v0, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v1, "Success"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 170
    new-instance v0, Lcom/navdy/obd/io/SerialChannel;

    iget-object v1, p0, Lcom/navdy/obd/io/STNSerialChannel;->context:Landroid/content/Context;

    iget v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    invoke-direct {v0, v1, p0, v2}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V

    iput-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    .line 171
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/io/SerialChannel;->connect(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method private runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;
    .locals 25
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "serialChannel"    # Lcom/navdy/obd/io/SerialChannel;
    .param p3, "timeoutMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 227
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v22

    if-nez v22, :cond_1

    .line 228
    :cond_0
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Empty command "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 229
    const/16 v22, 0x0

    .line 297
    :goto_0
    return-object v22

    .line 232
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v11

    .line 233
    .local v11, "length":I
    add-int/lit8 v22, v11, 0x1

    move/from16 v0, v22

    new-array v5, v0, [B

    .line 234
    .local v5, "commandBytes":[B
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v5, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 235
    array-length v0, v5

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    const/16 v23, 0xd

    aput-byte v23, v5, v22

    .line 236
    invoke-virtual/range {p2 .. p2}, Lcom/navdy/obd/io/SerialChannel;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 237
    .local v8, "inputStream":Ljava/io/InputStream;
    invoke-virtual/range {p2 .. p2}, Lcom/navdy/obd/io/SerialChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v12

    .line 238
    .local v12, "outputStream":Ljava/io/OutputStream;
    invoke-virtual {v12, v5}, Ljava/io/OutputStream;->write([B)V

    .line 239
    invoke-virtual {v12}, Ljava/io/OutputStream;->flush()V

    .line 242
    const/4 v4, -0x1

    .line 243
    .local v4, "b":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .local v14, "responseBuilder":Ljava/lang/StringBuilder;
    const/16 v18, 0x0

    .line 245
    .local v18, "textResponse":Z
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    .line 246
    .local v16, "start":J
    const-wide/16 v6, 0x0

    .line 247
    .local v6, "elapsedTime":J
    const-wide/32 v22, 0xf4240

    mul-long v20, p3, v22

    .line 248
    .local v20, "timeoutNanos":J
    const-string v22, "STSBR"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    .line 249
    .local v15, "setBaudRateCommand":Z
    :cond_2
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v22

    if-nez v22, :cond_d

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v4

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v4, v0, :cond_d

    const/16 v22, 0x3e

    move/from16 v0, v22

    if-eq v4, v0, :cond_d

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v22

    sub-long v6, v22, v16

    cmp-long v22, v6, v20

    if-gez v22, :cond_d

    .line 250
    const/16 v22, -0x2

    move/from16 v0, v22

    if-eq v4, v0, :cond_2

    .line 253
    const/16 v22, 0x30

    move/from16 v0, v22

    if-lt v4, v0, :cond_3

    const/16 v22, 0x39

    move/from16 v0, v22

    if-le v4, v0, :cond_5

    :cond_3
    const/16 v22, 0x41

    move/from16 v0, v22

    if-lt v4, v0, :cond_4

    const/16 v22, 0x46

    move/from16 v0, v22

    if-le v4, v0, :cond_5

    :cond_4
    const/16 v22, 0x61

    move/from16 v0, v22

    if-lt v4, v0, :cond_a

    const/16 v22, 0x66

    move/from16 v0, v22

    if-gt v4, v0, :cond_a

    :cond_5
    const/16 v19, 0x1

    .line 254
    .local v19, "validHexDigit":Z
    :goto_2
    const/16 v22, 0x20

    move/from16 v0, v22

    if-eq v4, v0, :cond_6

    const/16 v22, 0xd

    move/from16 v0, v22

    if-ne v4, v0, :cond_b

    :cond_6
    const/4 v9, 0x1

    .line 255
    .local v9, "isWhiteSpace":Z
    :goto_3
    if-nez v19, :cond_7

    if-nez v9, :cond_7

    .line 256
    const/16 v18, 0x1

    .line 258
    :cond_7
    if-nez v18, :cond_8

    const/16 v22, 0x20

    move/from16 v0, v22

    if-eq v4, v0, :cond_9

    .line 259
    :cond_8
    int-to-char v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 261
    :cond_9
    if-eqz v15, :cond_2

    .line 262
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    .line 263
    .local v13, "response":Ljava/lang/String;
    const-string v22, "ok"

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 264
    const-string v22, "ok"

    goto/16 :goto_0

    .line 253
    .end local v9    # "isWhiteSpace":Z
    .end local v13    # "response":Ljava/lang/String;
    .end local v19    # "validHexDigit":Z
    :cond_a
    const/16 v19, 0x0

    goto :goto_2

    .line 254
    .restart local v19    # "validHexDigit":Z
    :cond_b
    const/4 v9, 0x0

    goto :goto_3

    .line 266
    .restart local v9    # "isWhiteSpace":Z
    .restart local v13    # "response":Ljava/lang/String;
    :cond_c
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Unexpected response for the set baud rate command : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 270
    .end local v9    # "isWhiteSpace":Z
    .end local v13    # "response":Ljava/lang/String;
    .end local v19    # "validHexDigit":Z
    :cond_d
    cmp-long v22, v6, v20

    if-lez v22, :cond_e

    .line 271
    const-wide/32 v22, 0xf4240

    div-long v6, v6, v22

    .line 272
    new-instance v22, Ljava/io/IOException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Timeout after "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " ms"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 274
    :cond_e
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v23, "Flushing out remaining data"

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 276
    move v10, v4

    .line 277
    .local v10, "lastChar":I
    :cond_f
    :goto_4
    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v4

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v4, v0, :cond_10

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v22

    if-nez v22, :cond_10

    .line 278
    const/16 v22, -0x2

    move/from16 v0, v22

    if-ne v4, v0, :cond_11

    .line 279
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v23, "Read timed out during flushing"

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 286
    :cond_10
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v22

    if-eqz v22, :cond_12

    .line 287
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v23, "Thread was interrupted "

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 288
    new-instance v22, Ljava/lang/InterruptedException;

    invoke-direct/range {v22 .. v22}, Ljava/lang/InterruptedException;-><init>()V

    throw v22

    .line 281
    :cond_11
    if-ltz v4, :cond_f

    .line 282
    move v10, v4

    .line 283
    int-to-char v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 290
    :cond_12
    move v4, v10

    .line 291
    const/16 v22, 0x3e

    move/from16 v0, v22

    if-eq v4, v0, :cond_13

    .line 292
    new-instance v22, Ljava/io/IOException;

    const-string v23, "Unexpected end of stream"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 294
    :cond_13
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 295
    .restart local v13    # "response":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 296
    sget-object v22, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Response for command : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " is : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0
.end method

.method private setBaudRateAndConnect(Ljava/lang/String;II)Z
    .locals 8
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "currentBaudRate"    # I
    .param p3, "requiredBaud"    # I

    .prologue
    const/4 v4, 0x0

    .line 175
    new-instance v3, Lcom/navdy/obd/io/SerialChannel;

    iget-object v5, p0, Lcom/navdy/obd/io/STNSerialChannel;->context:Landroid/content/Context;

    invoke-direct {v3, v5, p0, p2}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V

    .line 176
    .local v3, "serialChannel":Lcom/navdy/obd/io/SerialChannel;
    invoke-virtual {v3, p1}, Lcom/navdy/obd/io/SerialChannel;->connect(Ljava/lang/String;)V

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    .local v0, "commandBuilder":Ljava/lang/StringBuilder;
    const-string v5, "STSBR"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 181
    :try_start_0
    const-string v5, "ate0"

    const-wide/16 v6, 0xbb8

    invoke-direct {p0, v5, v3, v6, v7}, Lcom/navdy/obd/io/STNSerialChannel;->runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;

    .line 182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0xbb8

    invoke-direct {p0, v5, v3, v6, v7}, Lcom/navdy/obd/io/STNSerialChannel;->runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, "response":Ljava/lang/String;
    const-string v5, "ok"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 184
    sget-object v5, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Setting baud rate , failed with response "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    .line 208
    .end local v2    # "response":Ljava/lang/String;
    :goto_0
    return v4

    .line 191
    .restart local v2    # "response":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    .line 193
    new-instance v3, Lcom/navdy/obd/io/SerialChannel;

    .end local v3    # "serialChannel":Lcom/navdy/obd/io/SerialChannel;
    iget-object v5, p0, Lcom/navdy/obd/io/STNSerialChannel;->context:Landroid/content/Context;

    invoke-direct {v3, v5, p0, p3}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V

    .line 194
    .restart local v3    # "serialChannel":Lcom/navdy/obd/io/SerialChannel;
    invoke-virtual {v3, p1}, Lcom/navdy/obd/io/SerialChannel;->connect(Ljava/lang/String;)V

    .line 196
    :try_start_1
    const-string v5, "\n"

    const-wide/16 v6, 0xbb8

    invoke-direct {p0, v5, v3, v6, v7}, Lcom/navdy/obd/io/STNSerialChannel;->runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;

    .line 197
    const-string v5, "stwbr"

    const-wide/16 v6, 0xbb8

    invoke-direct {p0, v5, v3, v6, v7}, Lcom/navdy/obd/io/STNSerialChannel;->runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ok"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    if-eqz v5, :cond_1

    .line 199
    const/4 v4, 0x1

    .line 206
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    goto :goto_0

    .line 187
    .end local v2    # "response":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v5, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while runCommand "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    throw v4

    .line 201
    .restart local v2    # "response":Ljava/lang/String;
    :cond_1
    :try_start_3
    sget-object v6, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected response for STWBR command "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "null"

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 206
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    goto/16 :goto_0

    .line 201
    :cond_2
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    goto :goto_1

    .line 203
    :catch_1
    move-exception v1

    .line 204
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v5, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while runCommand "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 206
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    goto/16 :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    throw v4
.end method

.method private setState(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 65
    iget v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    if-eq p1, v0, :cond_0

    .line 66
    iput p1, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    .line 67
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    invoke-interface {v0, p1}, Lcom/navdy/obd/io/IChannelSink;->onStateChange(I)V

    .line 71
    :cond_0
    return-void
.end method

.method private tryConnectingAtBaudRate(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "baudRate"    # I

    .prologue
    .line 212
    new-instance v2, Lcom/navdy/obd/io/SerialChannel;

    iget-object v3, p0, Lcom/navdy/obd/io/STNSerialChannel;->context:Landroid/content/Context;

    invoke-direct {v2, v3, p0, p2}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;I)V

    .line 213
    .local v2, "serialChannel":Lcom/navdy/obd/io/SerialChannel;
    invoke-virtual {v2, p1}, Lcom/navdy/obd/io/SerialChannel;->connect(Ljava/lang/String;)V

    .line 215
    :try_start_0
    const-string v3, "ati"

    const-wide/16 v4, 0xbb8

    invoke-direct {p0, v3, v2, v4, v5}, Lcom/navdy/obd/io/STNSerialChannel;->runCommand(Ljava/lang/String;Lcom/navdy/obd/io/SerialChannel;J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 216
    .local v1, "response":Ljava/lang/String;
    const/4 v3, 0x1

    .line 220
    invoke-virtual {v2}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    .line 222
    .end local v1    # "response":Ljava/lang/String;
    :goto_0
    return v3

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException while runCommand "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    invoke-virtual {v2}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    .line 222
    const/4 v3, 0x0

    goto :goto_0

    .line 220
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    throw v3
.end method


# virtual methods
.method public connect(Ljava/lang/String;)V
    .locals 7
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 75
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Last connected Baud :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/obd/io/STNSerialChannel;->lastConnectedBaudRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Required :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 76
    iget v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    if-ne v2, v6, :cond_0

    .line 77
    invoke-direct {p0, v6}, Lcom/navdy/obd/io/STNSerialChannel;->setState(I)V

    .line 166
    :goto_0
    return-void

    .line 80
    :cond_0
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/navdy/obd/io/STNSerialChannel$1;

    invoke-direct {v3, p0, p1}, Lcom/navdy/obd/io/STNSerialChannel$1;-><init>(Lcom/navdy/obd/io/STNSerialChannel;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->future:Ljava/util/concurrent/Future;

    .line 134
    :try_start_0
    iget-object v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->future:Ljava/util/concurrent/Future;

    const-wide/32 v4, 0xea60

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 138
    :catch_0
    move-exception v1

    .line 143
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v3, "Execution InterruptedException"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    iget-object v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->future:Ljava/util/concurrent/Future;

    invoke-interface {v2, v6}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v0

    .line 145
    .local v0, "cancelled":Z
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cancel task "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 164
    .end local v0    # "cancelled":Z
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :goto_1
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/navdy/obd/io/STNSerialChannel;->setState(I)V

    goto :goto_0

    .line 148
    :catch_1
    move-exception v1

    .line 153
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v3, "ExecutionException during execution"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 156
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v1

    .line 161
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    sget-object v2, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v3, "TimeoutException during execution"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    invoke-virtual {v0}, Lcom/navdy/obd/io/SerialChannel;->disconnect()V

    .line 308
    :goto_0
    return-void

    .line 305
    :cond_0
    sget-object v0, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    const-string v1, "disconnect when trying to connect, aborting"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->future:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0
.end method

.method public getBaudRate()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->baudRate:I

    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    invoke-virtual {v0}, Lcom/navdy/obd/io/SerialChannel;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 315
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    invoke-virtual {v0}, Lcom/navdy/obd/io/SerialChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 323
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSerialPort()Lcom/navdy/hardware/SerialPort;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    invoke-virtual {v0}, Lcom/navdy/obd/io/SerialChannel;->getSerialPort()Lcom/navdy/hardware/SerialPort;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    return v0
.end method

.method public onMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_1

    .line 347
    sget-object v0, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forwarding message to main sink , Message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    invoke-interface {v0, p1}, Lcom/navdy/obd/io/IChannelSink;->onMessage(Ljava/lang/String;)V

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    sget-object v0, Lcom/navdy/obd/io/STNSerialChannel;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Internal message , message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/io/STNSerialChannel;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStateChange(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 335
    iget-object v0, p0, Lcom/navdy/obd/io/STNSerialChannel;->serialChannel:Lcom/navdy/obd/io/SerialChannel;

    if-eqz v0, :cond_0

    .line 336
    invoke-direct {p0, p1}, Lcom/navdy/obd/io/STNSerialChannel;->setState(I)V

    .line 338
    :cond_0
    return-void
.end method
