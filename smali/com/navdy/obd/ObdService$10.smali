.class Lcom/navdy/obd/ObdService$10;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/jobs/ScanPidsJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->scanPids(Lcom/navdy/obd/ScanSchedule;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 1139
    iput-object p1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCanBusMonitoringCommand()Lcom/navdy/obd/command/CANBusMonitoringCommand;
    .locals 1

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$4200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/obd/ObdService$CANBusMonitoringStatus;->canBusMonitoringCommand:Lcom/navdy/obd/command/CANBusMonitoringCommand;

    return-object v0
.end method

.method public onCanBusDataRead(Lcom/navdy/obd/jobs/ScanPidsJob;)V
    .locals 2
    .param p1, "job"    # Lcom/navdy/obd/jobs/ScanPidsJob;

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {p1}, Lcom/navdy/obd/jobs/ScanPidsJob;->getFullPidsList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$4100(Lcom/navdy/obd/ObdService;Ljava/util/List;)V

    .line 1142
    return-void
.end method

.method public onCanBusMonitoringErrorDetected()V
    .locals 3

    .prologue
    .line 1149
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$3100(Lcom/navdy/obd/ObdService;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 1150
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/navdy/obd/ObdService;->access$3102(Lcom/navdy/obd/ObdService;Z)Z

    .line 1151
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v1

    if-eqz v1, :cond_31

    .line 1153
    :try_start_16
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v1

    const-string v2, "BAD_STATE"

    invoke-interface {v1, v2}, Lcom/navdy/obd/ICanBusMonitoringListener;->onCanBusMonitoringError(Ljava/lang/String;)V

    .line 1154
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    .line 1155
    iget-object v1, p0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v1}, Lcom/navdy/obd/ObdService;->access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Lcom/navdy/obd/ObdDataObserver;->onRawCanBusMessage(Ljava/lang/String;)V
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_31} :catch_32

    .line 1161
    :cond_31
    :goto_31
    return-void

    .line 1156
    :catch_32
    move-exception v0

    .line 1157
    .local v0, "e":Landroid/os/RemoteException;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RemoteException "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_31
.end method

.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 17
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 1164
    check-cast p1, Lcom/navdy/obd/jobs/ScanPidsJob;

    .end local p1    # "job":Lcom/navdy/obd/ObdJob;
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/obd/jobs/ScanPidsJob;->getFullPidsList()Ljava/util/List;

    move-result-object v7

    .line 1165
    .local v7, "fullPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3100(Lcom/navdy/obd/ObdService;)Z

    move-result v13

    if-eqz v13, :cond_84

    .line 1166
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->isMonitoringFailureDetected()Z

    move-result v13

    if-eqz v13, :cond_97

    .line 1167
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    const-string v14, "Can bus monitoring failed, disabling the monitoring"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1168
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->reset()V

    .line 1169
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/navdy/obd/ObdService;->access$3102(Lcom/navdy/obd/ObdService;Z)Z

    .line 1170
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    if-eqz v13, :cond_51

    .line 1172
    :try_start_38
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->getErrorData()Ljava/lang/String;

    move-result-object v6

    .line 1173
    .local v6, "errorData":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v1

    .line 1174
    .local v1, "access$2400":Lcom/navdy/obd/ICanBusMonitoringListener;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4e

    .line 1175
    const-string v6, "NO_DATA"

    .line 1177
    :cond_4e
    invoke-interface {v1, v6}, Lcom/navdy/obd/ICanBusMonitoringListener;->onCanBusMonitoringError(Ljava/lang/String;)V
    :try_end_51
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_51} :catch_8c

    .line 1182
    .end local v1    # "access$2400":Lcom/navdy/obd/ICanBusMonitoringListener;
    .end local v6    # "errorData":Ljava/lang/String;
    :cond_51
    :goto_51
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V

    .line 1225
    :cond_58
    :goto_58
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    if-eqz v13, :cond_84

    .line 1227
    :try_start_62
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    invoke-interface {v13}, Lcom/navdy/obd/ICanBusMonitoringListener;->isMonitoringLimitReached()Z

    move-result v13

    if-eqz v13, :cond_84

    .line 1228
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    .line 1229
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Lcom/navdy/obd/ObdDataObserver;->onRawCanBusMessage(Ljava/lang/String;)V
    :try_end_84
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_84} :catch_248

    .line 1236
    :cond_84
    :goto_84
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13, v7}, Lcom/navdy/obd/ObdService;->access$4100(Lcom/navdy/obd/ObdService;Ljava/util/List;)V

    .line 1237
    return-void

    .line 1178
    :catch_8c
    move-exception v2

    .line 1179
    .local v2, "e":Landroid/os/RemoteException;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    const-string v14, "RemoteException "

    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_51

    .line 1183
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_97
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->getLastSampleSucceeded()Z

    move-result v13

    if-eqz v13, :cond_58

    .line 1184
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 1185
    .local v12, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v13, "OBD_DATA,"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1186
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_ad
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_e2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/obd/Pid;

    .line 1187
    .local v10, "pid":Lcom/navdy/obd/Pid;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Lcom/navdy/obd/Pid;->getId()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_ad

    .line 1190
    .end local v10    # "pid":Lcom/navdy/obd/Pid;
    :cond_e2
    :try_start_e2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    if-eqz v13, :cond_160

    .line 1191
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Lat:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v14}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v14

    invoke-interface {v14}, Lcom/navdy/obd/ICanBusMonitoringListener;->getLatitude()D

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Long:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v15}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/navdy/obd/ICanBusMonitoringListener;->getLongitude()D

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Speed:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v15}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/navdy/obd/ICanBusMonitoringListener;->getGpsSpeed()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_160
    .catch Ljava/lang/Exception; {:try_start_e2 .. :try_end_160} :catch_22e

    .line 1196
    :cond_160
    :goto_160
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->getLastSampleDataSize()J

    move-result-wide v13

    long-to-double v8, v13

    .line 1197
    .local v8, "lastSampleDataSize":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$4300(Lcom/navdy/obd/ObdService;)Z

    move-result v13

    if-nez v13, :cond_1d9

    const-wide/16 v13, 0x0

    cmpl-double v13, v8, v13

    if-lez v13, :cond_1d9

    .line 1199
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/navdy/obd/ObdService;->access$4302(Lcom/navdy/obd/ObdService;Z)Z

    .line 1200
    const-wide v13, 0x408f400000000000L    # 1000.0

    cmpl-double v13, v8, v13

    if-lez v13, :cond_23a

    .line 1201
    const-wide v13, 0x408f400000000000L    # 1000.0

    div-double v13, v8, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->round(D)J

    move-result-wide v13

    long-to-double v8, v13

    .line 1202
    const-wide/high16 v13, 0x4049000000000000L    # 50.0

    div-double v13, v8, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->round(D)J

    move-result-wide v13

    const-wide/16 v15, 0x32

    mul-long/2addr v13, v15

    long-to-int v11, v13

    .line 1206
    .local v11, "quantized":I
    :goto_1a0
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Monitor succeeded , Data size, Actual : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", Quantized : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    if-eqz v13, :cond_1d9

    .line 1209
    :try_start_1ce
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$3000(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ICanBusMonitoringListener;

    move-result-object v13

    invoke-interface {v13, v11}, Lcom/navdy/obd/ICanBusMonitoringListener;->onCanBusMonitorSuccess(I)V
    :try_end_1d9
    .catch Landroid/os/RemoteException; {:try_start_1ce .. :try_end_1d9} :catch_23d

    .line 1215
    .end local v11    # "quantized":I
    :cond_1d9
    :goto_1d9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v13

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/obd/ObdDataObserver;->onRawCanBusMessage(Ljava/lang/String;)V

    .line 1216
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v14}, Lcom/navdy/obd/ObdService;->access$4400(Lcom/navdy/obd/ObdService;)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-static {v13, v14}, Lcom/navdy/obd/ObdService;->access$4402(Lcom/navdy/obd/ObdService;I)I

    .line 1217
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$4400(Lcom/navdy/obd/ObdService;)I

    move-result v13

    const/4 v14, 0x5

    if-lt v13, v14, :cond_227

    .line 1218
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    const-string v14, "Monitor Batch completed"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    invoke-static {v13, v14, v15}, Lcom/navdy/obd/ObdService;->access$4502(Lcom/navdy/obd/ObdService;J)J

    .line 1220
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/navdy/obd/ObdService;->access$4402(Lcom/navdy/obd/ObdService;I)I

    .line 1221
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/ObdService$10;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v13}, Lcom/navdy/obd/ObdService;->access$2300(Lcom/navdy/obd/ObdService;)V

    .line 1223
    :cond_227
    sget-object v13, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v13}, Lcom/navdy/obd/command/GatherCANBusDataCommand;->resetLastSampleStats()V

    goto/16 :goto_58

    .line 1193
    .end local v8    # "lastSampleDataSize":D
    :catch_22e
    move-exception v3

    .line 1194
    .local v3, "e2":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_160

    .line 1204
    .end local v3    # "e2":Ljava/lang/Exception;
    .restart local v8    # "lastSampleDataSize":D
    :cond_23a
    double-to-int v11, v8

    .restart local v11    # "quantized":I
    goto/16 :goto_1a0

    .line 1210
    :catch_23d
    move-exception v4

    .line 1211
    .local v4, "e3":Landroid/os/RemoteException;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v13

    const-string v14, "Error reporting the can bus monitor success"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1d9

    .line 1231
    .end local v4    # "e3":Landroid/os/RemoteException;
    .end local v8    # "lastSampleDataSize":D
    .end local v11    # "quantized":I
    .end local v12    # "stringBuilder":Ljava/lang/StringBuilder;
    :catch_248
    move-exception v5

    .line 1232
    .local v5, "e4":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_84
.end method
