.class public Lcom/navdy/obd/FuelLevelPidProcessor;
.super Lcom/navdy/obd/PidProcessor;
.source "FuelLevelPidProcessor.java"


# instance fields
.field private SAMPLE_SIZE:I

.field private STEP_SIZE:I

.field private mFilteredValue:D

.field private mReadIndex:I

.field private mReadings:[D

.field private mReadingsCount:I

.field private mRunningTotal:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lcom/navdy/obd/PidProcessor;-><init>()V

    .line 10
    iput v0, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    .line 11
    iput v0, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadingsCount:I

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    .line 13
    iput v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->SAMPLE_SIZE:I

    .line 14
    iput v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->STEP_SIZE:I

    .line 17
    iget v0, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->SAMPLE_SIZE:I

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadings:[D

    .line 18
    return-void
.end method

.method private quantize(ID)D
    .locals 4
    .param p1, "stepSize"    # I
    .param p2, "value"    # D

    .prologue
    .line 47
    int-to-double v0, p1

    div-double v0, p2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    long-to-double v0, v0

    return-wide v0
.end method


# virtual methods
.method public isSupported(Lcom/navdy/obd/PidSet;)Z
    .locals 1
    .param p1, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 22
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    return v0
.end method

.method public processPidValue(Lcom/navdy/obd/PidLookupTable;)Z
    .locals 7
    .param p1, "vehicleState"    # Lcom/navdy/obd/PidLookupTable;

    .prologue
    const/16 v6, 0x2f

    .line 28
    invoke-virtual {p1, v6}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v0

    .line 29
    .local v0, "fuelLevel":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 30
    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v6, v2, v3}, Lcom/navdy/obd/PidLookupTable;->updatePid(ID)Z

    move-result v2

    .line 43
    :goto_0
    return v2

    .line 32
    :cond_1
    iget-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    iget-object v4, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadings:[D

    iget v5, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    aget-wide v4, v4, v5

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    .line 33
    iget-object v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadings:[D

    iget v3, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    aput-wide v0, v2, v3

    .line 34
    iget-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    add-double/2addr v2, v0

    iput-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    .line 35
    iget v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadingsCount:I

    iget v3, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->SAMPLE_SIZE:I

    if-ge v2, v3, :cond_2

    .line 36
    iget v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadingsCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadingsCount:I

    .line 38
    :cond_2
    iget v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    iget v3, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->SAMPLE_SIZE:I

    if-ne v2, v3, :cond_3

    .line 39
    const/4 v2, 0x0

    iput v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadIndex:I

    .line 41
    :cond_3
    iget-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mRunningTotal:D

    iget v4, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mReadingsCount:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mFilteredValue:D

    .line 42
    iget v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->STEP_SIZE:I

    iget-wide v4, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mFilteredValue:D

    invoke-direct {p0, v2, v4, v5}, Lcom/navdy/obd/FuelLevelPidProcessor;->quantize(ID)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mFilteredValue:D

    .line 43
    iget-wide v2, p0, Lcom/navdy/obd/FuelLevelPidProcessor;->mFilteredValue:D

    invoke-virtual {p1, v6, v2, v3}, Lcom/navdy/obd/PidLookupTable;->updatePid(ID)Z

    move-result v2

    goto :goto_0
.end method

.method public resolveDependencies()Lcom/navdy/obd/PidSet;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method
