.class public Lcom/navdy/obd/Profile;
.super Ljava/lang/Object;
.source "Profile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/Profile$Column;
    }
.end annotation


# static fields
.field static final Log:Lorg/slf4j/Logger;


# instance fields
.field public mapByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/obd/command/ObdCommand;",
            ">;"
        }
    .end annotation
.end field

.field public mapByPid:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/obd/command/ObdCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/navdy/obd/Profile;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/Profile;->mapByName:Ljava/util/HashMap;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/Profile;->mapByPid:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method add(Ljava/lang/String;Lcom/navdy/obd/command/ObdCommand;)V
    .locals 3
    .param p1, "modeAndPid"    # Ljava/lang/String;
    .param p2, "command"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 56
    const-string v1, "01"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const/4 v1, 0x2

    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 58
    .local v0, "pid":I
    iget-object v1, p0, Lcom/navdy/obd/Profile;->mapByPid:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .end local v0    # "pid":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/obd/Profile;->mapByName:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/navdy/obd/command/ObdCommand;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public load(Ljava/io/InputStream;)V
    .locals 13
    .param p1, "csvProfile"    # Ljava/io/InputStream;

    .prologue
    .line 64
    const/4 v5, 0x0

    .line 66
    .local v5, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .local v6, "reader":Ljava/io/BufferedReader;
    :try_start_1
    sget-object v9, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v9, v10}, Lorg/apache/commons/csv/CSVFormat;->withHeader([Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v9

    invoke-virtual {v9, v6}, Lorg/apache/commons/csv/CSVFormat;->parse(Ljava/io/Reader;)Lorg/apache/commons/csv/CSVParser;

    move-result-object v8

    .line 68
    .local v8, "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/commons/csv/CSVRecord;

    .line 69
    .local v7, "record":Lorg/apache/commons/csv/CSVRecord;
    const-string v10, "ShortName"

    invoke-virtual {v7, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 70
    .local v4, "name":Ljava/lang/String;
    const-string v10, "ModeAndPID"

    invoke-virtual {v7, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "modeAndPid":Ljava/lang/String;
    const-string v10, "Equation"

    invoke-virtual {v7, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 73
    .local v2, "equation":Ljava/lang/String;
    :try_start_2
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    new-instance v10, Lcom/navdy/obd/converters/CustomConversion;

    invoke-direct {v10, v2}, Lcom/navdy/obd/converters/CustomConversion;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v4, v3, v10}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;)V

    .line 77
    .local v0, "command":Lcom/navdy/obd/command/ObdCommand;
    invoke-virtual {p0, v3, v0}, Lcom/navdy/obd/Profile;->add(Ljava/lang/String;Lcom/navdy/obd/command/ObdCommand;)V
    :try_end_2
    .catch Lparsii/tokenizer/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 78
    .end local v0    # "command":Lcom/navdy/obd/command/ObdCommand;
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Lparsii/tokenizer/ParseException;
    :try_start_3
    sget-object v10, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to parse equation ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 82
    .end local v1    # "e":Lparsii/tokenizer/ParseException;
    .end local v2    # "equation":Ljava/lang/String;
    .end local v3    # "modeAndPid":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    .end local v7    # "record":Lorg/apache/commons/csv/CSVRecord;
    .end local v8    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :catch_1
    move-exception v1

    move-object v5, v6

    .line 83
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .local v1, "e":Ljava/io/IOException;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_4
    sget-object v9, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    const-string v10, "Error reading custom profile"

    invoke-interface {v9, v10, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 85
    if-eqz v5, :cond_0

    .line 87
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 94
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-void

    .line 85
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :cond_1
    if-eqz v6, :cond_3

    .line 87
    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    move-object v5, v6

    .line 90
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 88
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    .line 89
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    const-string v10, "Error closing reader"

    invoke-interface {v9, v10, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v5, v6

    .line 90
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 88
    .end local v8    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :catch_3
    move-exception v1

    .line 89
    sget-object v9, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    const-string v10, "Error closing reader"

    invoke-interface {v9, v10, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 85
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_3
    if-eqz v5, :cond_2

    .line 87
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 90
    :cond_2
    :goto_4
    throw v9

    .line 88
    :catch_4
    move-exception v1

    .line 89
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/obd/Profile;->Log:Lorg/slf4j/Logger;

    const-string v11, "Error closing reader"

    invoke-interface {v10, v11, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 85
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 82
    :catch_5
    move-exception v1

    goto :goto_1

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :cond_3
    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public lookup(I)Lcom/navdy/obd/command/ObdCommand;
    .locals 3
    .param p1, "pid"    # I

    .prologue
    .line 48
    iget-object v1, p0, Lcom/navdy/obd/Profile;->mapByPid:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ObdCommand;

    .line 49
    .local v0, "command":Lcom/navdy/obd/command/ObdCommand;
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v0}, Lcom/navdy/obd/command/ObdCommand;->clone()Lcom/navdy/obd/command/ObdCommand;

    move-result-object v1

    .line 52
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lookup(Ljava/lang/String;)Lcom/navdy/obd/command/ObdCommand;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/navdy/obd/Profile;->mapByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ObdCommand;

    .line 41
    .local v0, "command":Lcom/navdy/obd/command/ObdCommand;
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Lcom/navdy/obd/command/ObdCommand;->clone()Lcom/navdy/obd/command/ObdCommand;

    move-result-object v1

    .line 44
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
