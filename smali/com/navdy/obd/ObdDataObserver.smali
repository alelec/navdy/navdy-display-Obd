.class public Lcom/navdy/obd/ObdDataObserver;
.super Ljava/lang/Object;
.source "ObdDataObserver.java"

# interfaces
.implements Lcom/navdy/obd/command/IObdDataObserver;


# static fields
.field static final CAN_RAW_LOG:Lorg/slf4j/Logger;

.field static final OBD_RAW_LOG:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "com.navdy.obd.ObdRaw"

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ObdDataObserver;->OBD_RAW_LOG:Lorg/slf4j/Logger;

    .line 16
    const-string v0, "com.navdy.obd.CanBusRaw"

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ObdDataObserver;->CAN_RAW_LOG:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)V
    .locals 2
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/obd/ObdDataObserver;->OBD_RAW_LOG:Lorg/slf4j/Logger;

    const-string v1, "C,{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/obd/ObdDataObserver;->OBD_RAW_LOG:Lorg/slf4j/Logger;

    const-string v1, "E,{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public onRawCanBusMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/obd/ObdDataObserver;->CAN_RAW_LOG:Lorg/slf4j/Logger;

    invoke-interface {v0, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public onResponse(Ljava/lang/String;)V
    .locals 2
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/obd/ObdDataObserver;->OBD_RAW_LOG:Lorg/slf4j/Logger;

    const-string v1, "R,{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 26
    return-void
.end method
