.class public Lcom/navdy/obd/ObdJob;
.super Ljava/lang/Object;
.source "ObdJob.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ObdJob$IListener;
    }
.end annotation


# static fields
.field static final Log:Lorg/slf4j/Logger;


# instance fields
.field protected channel:Lcom/navdy/obd/io/IChannel;

.field protected command:Lcom/navdy/obd/command/ICommand;

.field protected commandObserver:Lcom/navdy/obd/command/IObdDataObserver;

.field protected disconnectUponFailure:Z

.field protected listener:Lcom/navdy/obd/ObdJob$IListener;

.field protected protocol:Lcom/navdy/obd/Protocol;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/navdy/obd/ObdJob;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 7
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;
    .param p2, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "listener"    # Lcom/navdy/obd/ObdJob$IListener;
    .param p5, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    .line 51
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;Z)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;Z)V
    .locals 1
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;
    .param p2, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "listener"    # Lcom/navdy/obd/ObdJob$IListener;
    .param p5, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .param p6, "disconnectUponFailure"    # Z

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/ObdJob;->disconnectUponFailure:Z

    .line 38
    iput-object p1, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    .line 39
    iput-object p2, p0, Lcom/navdy/obd/ObdJob;->channel:Lcom/navdy/obd/io/IChannel;

    .line 40
    iput-object p4, p0, Lcom/navdy/obd/ObdJob;->listener:Lcom/navdy/obd/ObdJob$IListener;

    .line 41
    iput-object p3, p0, Lcom/navdy/obd/ObdJob;->protocol:Lcom/navdy/obd/Protocol;

    .line 42
    iput-object p5, p0, Lcom/navdy/obd/ObdJob;->commandObserver:Lcom/navdy/obd/command/IObdDataObserver;

    .line 43
    iput-boolean p6, p0, Lcom/navdy/obd/ObdJob;->disconnectUponFailure:Z

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 7
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;
    .param p2, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    .line 47
    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;Z)V

    .line 48
    return-void
.end method


# virtual methods
.method protected postExecute()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 60
    sget-object v2, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    const-string v3, "executing command {}"

    iget-object v4, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v4}, Lcom/navdy/obd/command/ICommand;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    const/4 v1, 0x0

    .line 63
    .local v1, "succeeded":Z
    :try_start_0
    iget-object v2, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    iget-object v3, p0, Lcom/navdy/obd/ObdJob;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v3}, Lcom/navdy/obd/io/IChannel;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/obd/ObdJob;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v4}, Lcom/navdy/obd/io/IChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/obd/ObdJob;->protocol:Lcom/navdy/obd/Protocol;

    iget-object v6, p0, Lcom/navdy/obd/ObdJob;->commandObserver:Lcom/navdy/obd/command/IObdDataObserver;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/navdy/obd/command/ICommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 64
    sget-object v2, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    const-string v3, "response {}"

    iget-object v4, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v4}, Lcom/navdy/obd/command/ICommand;->getResponse()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    invoke-virtual {p0}, Lcom/navdy/obd/ObdJob;->postExecute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    const/4 v1, 0x1

    .line 80
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/navdy/obd/ObdJob;->listener:Lcom/navdy/obd/ObdJob$IListener;

    if-eqz v2, :cond_1

    .line 81
    iget-object v2, p0, Lcom/navdy/obd/ObdJob;->listener:Lcom/navdy/obd/ObdJob$IListener;

    invoke-interface {v2, p0, v1}, Lcom/navdy/obd/ObdJob$IListener;->onComplete(Lcom/navdy/obd/ObdJob;Z)V

    .line 82
    :cond_1
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    instance-of v2, v2, Lcom/navdy/obd/command/InitializeCommand;

    if-eqz v2, :cond_2

    .line 69
    sget-object v2, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InitializeCommand failed, with an IOException. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 76
    :goto_1
    iget-boolean v2, p0, Lcom/navdy/obd/ObdJob;->disconnectUponFailure:Z

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/navdy/obd/ObdJob;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v2}, Lcom/navdy/obd/io/IChannel;->disconnect()V

    goto :goto_0

    .line 71
    :cond_2
    sget-object v2, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    const-string v3, "I/O error {} executing command {} - closing channel"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 72
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    .line 73
    invoke-interface {v6}, Lcom/navdy/obd/command/ICommand;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    .line 71
    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    sget-object v2, Lcom/navdy/obd/ObdJob;->Log:Lorg/slf4j/Logger;

    const-string v3, "Response received {}"

    iget-object v4, p0, Lcom/navdy/obd/ObdJob;->command:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v4}, Lcom/navdy/obd/command/ICommand;->getResponse()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method
