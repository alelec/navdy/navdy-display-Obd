.class public Lcom/navdy/obd/VehicleStateManager;
.super Ljava/lang/Object;
.source "VehicleStateManager.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mObdSnapshot:Lcom/navdy/obd/PidLookupTable;

.field private mPidProcessorFactory:Lcom/navdy/obd/PidProcessorFactory;

.field private mPidProcessors:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/obd/PidProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/navdy/obd/VehicleStateManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/VehicleStateManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/PidProcessorFactory;)V
    .locals 2
    .param p1, "factory"    # Lcom/navdy/obd/PidProcessorFactory;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/navdy/obd/PidLookupTable;

    const/16 v1, 0x140

    invoke-direct {v0, v1}, Lcom/navdy/obd/PidLookupTable;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/obd/VehicleStateManager;->mObdSnapshot:Lcom/navdy/obd/PidLookupTable;

    .line 37
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessors:Landroid/util/SparseArray;

    .line 38
    iput-object p1, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessorFactory:Lcom/navdy/obd/PidProcessorFactory;

    .line 39
    return-void
.end method


# virtual methods
.method public getObdSnapshot()Lcom/navdy/obd/PidLookupTable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/obd/VehicleStateManager;->mObdSnapshot:Lcom/navdy/obd/PidLookupTable;

    return-object v0
.end method

.method public onScanComplete()V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessors:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 58
    iget-object v2, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessors:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/PidProcessor;

    .line 59
    .local v1, "processor":Lcom/navdy/obd/PidProcessor;
    iget-object v2, p0, Lcom/navdy/obd/VehicleStateManager;->mObdSnapshot:Lcom/navdy/obd/PidLookupTable;

    invoke-virtual {v1, v2}, Lcom/navdy/obd/PidProcessor;->processPidValue(Lcom/navdy/obd/PidLookupTable;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "processor":Lcom/navdy/obd/PidProcessor;
    :cond_0
    return-void
.end method

.method public resolveDependenciesForCustomPids(Lcom/navdy/obd/ScanSchedule;)V
    .locals 8
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 70
    if-eqz p1, :cond_2

    .line 71
    new-instance v0, Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v0}, Lcom/navdy/obd/ScanSchedule;-><init>()V

    .line 72
    .local v0, "customPids":Lcom/navdy/obd/ScanSchedule;
    iget-object v5, p1, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 73
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 74
    .local v4, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    iget v6, v4, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    const/16 v7, 0x100

    if-lt v6, v7, :cond_0

    .line 75
    iget-object v6, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessors:Landroid/util/SparseArray;

    iget v7, v4, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/obd/PidProcessor;

    .line 76
    .local v3, "processor":Lcom/navdy/obd/PidProcessor;
    if-eqz v3, :cond_0

    .line 77
    invoke-virtual {v3}, Lcom/navdy/obd/PidProcessor;->resolveDependencies()Lcom/navdy/obd/PidSet;

    move-result-object v1

    .line 78
    .local v1, "dependencies":Lcom/navdy/obd/PidSet;
    if-eqz v1, :cond_0

    .line 79
    invoke-virtual {v1}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v6

    iget v7, v4, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    invoke-virtual {v0, v6, v7}, Lcom/navdy/obd/ScanSchedule;->addPids(Ljava/util/List;I)V

    goto :goto_0

    .line 84
    .end local v1    # "dependencies":Lcom/navdy/obd/PidSet;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    .end local v3    # "processor":Lcom/navdy/obd/PidProcessor;
    .end local v4    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/obd/ScanSchedule;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 85
    invoke-virtual {p1, v0}, Lcom/navdy/obd/ScanSchedule;->merge(Lcom/navdy/obd/ScanSchedule;)V

    .line 88
    .end local v0    # "customPids":Lcom/navdy/obd/ScanSchedule;
    :cond_2
    return-void
.end method

.method public update(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "pidList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v0, p0, Lcom/navdy/obd/VehicleStateManager;->mObdSnapshot:Lcom/navdy/obd/PidLookupTable;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/PidLookupTable;->build(Ljava/util/List;)V

    .line 50
    return-void
.end method

.method public updateSupportedPids(Lcom/navdy/obd/PidSet;)V
    .locals 6
    .param p1, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 21
    iget-object v3, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessorFactory:Lcom/navdy/obd/PidProcessorFactory;

    if-eqz v3, :cond_1

    .line 22
    iget-object v3, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessorFactory:Lcom/navdy/obd/PidProcessorFactory;

    invoke-interface {v3}, Lcom/navdy/obd/PidProcessorFactory;->getPidsHavingProcessors()Ljava/util/List;

    move-result-object v1

    .line 23
    .local v1, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 24
    .local v0, "pid":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessorFactory:Lcom/navdy/obd/PidProcessorFactory;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/navdy/obd/PidProcessorFactory;->buildPidProcessorForPid(I)Lcom/navdy/obd/PidProcessor;

    move-result-object v2

    .line 25
    .local v2, "processor":Lcom/navdy/obd/PidProcessor;
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Lcom/navdy/obd/PidProcessor;->isSupported(Lcom/navdy/obd/PidSet;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    iget-object v4, p0, Lcom/navdy/obd/VehicleStateManager;->mPidProcessors:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 27
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 28
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/navdy/obd/PidSet;->add(I)V

    goto :goto_0

    .line 33
    .end local v0    # "pid":Ljava/lang/Integer;
    .end local v1    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "processor":Lcom/navdy/obd/PidProcessor;
    :cond_1
    return-void
.end method
