.class public Lcom/navdy/obd/Utility;
.super Ljava/lang/Object;
.source "Utility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bytesToInt([BIIZ)I
    .locals 6
    .param p0, "byteArray"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "bigEndian"    # Z

    .prologue
    .line 83
    if-eqz p0, :cond_2

    if-lez p2, :cond_2

    const/4 v4, 0x4

    if-gt p2, v4, :cond_2

    array-length v4, p0

    add-int v5, p1, p2

    if-lt v4, v5, :cond_2

    .line 84
    const/4 v1, 0x0

    .line 85
    .local v1, "data":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, p2, :cond_3

    .line 86
    if-eqz p3, :cond_1

    move v4, v3

    :goto_1
    add-int v2, p1, v4

    .line 87
    .local v2, "effectiveOffset":I
    aget-byte v4, p0, v2

    and-int/lit16 v0, v4, 0xff

    .line 88
    .local v0, "byteData":I
    add-int/2addr v1, v0

    .line 89
    add-int/lit8 v4, p2, -0x1

    if-ge v3, v4, :cond_0

    .line 90
    shl-int/lit8 v1, v1, 0x8

    .line 85
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "byteData":I
    .end local v2    # "effectiveOffset":I
    :cond_1
    sub-int v4, p2, v3

    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 95
    .end local v1    # "data":I
    .end local v3    # "i":I
    :cond_2
    new-instance v4, Ljava/lang/NumberFormatException;

    const-string v5, "Incorrect data"

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 93
    .restart local v1    # "data":I
    .restart local v3    # "i":I
    :cond_3
    return v1
.end method

.method public static calculateCcittCrc([B)I
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "crc":I
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-byte v1, p0, v2

    .line 39
    .local v1, "input":B
    invoke-static {v0, v1}, Lcom/navdy/obd/Utility;->updateCcittCrc(IB)I

    move-result v0

    .line 38
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    .end local v1    # "input":B
    :cond_0
    return v0
.end method

.method public static calculateInstantaneousFuelConsumption(DD)D
    .locals 4
    .param p0, "maf"    # D
    .param p2, "vss"    # D

    .prologue
    .line 61
    const-wide/16 v0, 0x0

    .line 62
    .local v0, "consumption":D
    const-wide/16 v2, 0x0

    cmpl-double v2, p2, v2

    if-lez v2, :cond_0

    .line 63
    const-wide v2, 0x4041038ef34d6a16L    # 34.0278

    mul-double/2addr v2, p0

    div-double v0, v2, p2

    .line 65
    :cond_0
    return-wide v0
.end method

.method public static convertInputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 70
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 71
    .local v1, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x4000

    new-array v0, v4, [B

    .line 73
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "n":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 74
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 77
    .end local v0    # "buffer":[B
    .end local v1    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "n":I
    :catch_0
    move-exception v3

    .line 78
    .local v3, "t":Ljava/lang/Throwable;
    const/4 v4, 0x0

    .end local v3    # "t":Ljava/lang/Throwable;
    :goto_1
    return-object v4

    .line 76
    .restart local v0    # "buffer":[B
    .restart local v1    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "n":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_1
.end method

.method public static isUserBuild()Z
    .locals 2

    .prologue
    .line 100
    const-string v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static updateCcittCrc(IB)I
    .locals 3
    .param p0, "crc"    # I
    .param p1, "data"    # B

    .prologue
    .line 22
    shr-int/lit8 v1, p0, 0x8

    xor-int/2addr v1, p1

    and-int/lit16 v0, v1, 0xff

    .line 23
    .local v0, "x":I
    shr-int/lit8 v1, v0, 0x4

    xor-int/2addr v0, v1

    .line 24
    shl-int/lit8 v1, p0, 0x8

    shl-int/lit8 v2, v0, 0xc

    xor-int/2addr v1, v2

    shl-int/lit8 v2, v0, 0x5

    xor-int/2addr v1, v2

    xor-int p0, v1, v0

    .line 25
    return p0
.end method
