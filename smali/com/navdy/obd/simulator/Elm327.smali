.class public Lcom/navdy/obd/simulator/Elm327;
.super Ljava/lang/Thread;
.source "Elm327.java"


# static fields
.field static final Log:Lorg/slf4j/Logger;


# instance fields
.field private ecu:Lcom/navdy/obd/simulator/Ecu;

.field private input:Ljava/io/InputStream;

.field private output:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/navdy/obd/simulator/Elm327;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/obd/simulator/Elm327;->input:Ljava/io/InputStream;

    .line 31
    iput-object p2, p0, Lcom/navdy/obd/simulator/Elm327;->output:Ljava/io/OutputStream;

    .line 33
    new-instance v0, Lcom/navdy/obd/simulator/Ecu;

    invoke-direct {v0}, Lcom/navdy/obd/simulator/Ecu;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/simulator/Elm327;->ecu:Lcom/navdy/obd/simulator/Ecu;

    .line 34
    return-void
.end method

.method private handleCommand(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "writer"    # Ljava/io/OutputStreamWriter;
    .param p2, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v1, "Handling command {}"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    const-string v0, "OK\r>"

    invoke-virtual {p1, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private handleRead(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V
    .locals 6
    .param p1, "writer"    # Ljava/io/OutputStreamWriter;
    .param p2, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 72
    sget-object v3, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v4, "Reading pid {}"

    invoke-interface {v3, v4, p2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "tail":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v1, "response":Ljava/lang/StringBuilder;
    const-string v3, "41"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_1

    .line 79
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "pid":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/obd/simulator/Elm327;->ecu:Lcom/navdy/obd/simulator/Ecu;

    invoke-virtual {v3, v0}, Lcom/navdy/obd/simulator/Ecu;->supports(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v3, p0, Lcom/navdy/obd/simulator/Elm327;->ecu:Lcom/navdy/obd/simulator/Ecu;

    invoke-virtual {v3, v0}, Lcom/navdy/obd/simulator/Ecu;->readPid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 85
    goto :goto_0

    .line 86
    .end local v0    # "pid":Ljava/lang/String;
    :cond_1
    const-string v3, "\r>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 88
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 38
    :try_start_0
    sget-object v4, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v5, "Starting Elm327 reader thread"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 39
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v5, p0, Lcom/navdy/obd/simulator/Elm327;->input:Ljava/io/InputStream;

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 40
    .local v2, "reader":Ljava/io/BufferedReader;
    new-instance v3, Ljava/io/OutputStreamWriter;

    iget-object v4, p0, Lcom/navdy/obd/simulator/Elm327;->output:Ljava/io/OutputStream;

    invoke-direct {v3, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 41
    .local v3, "writer":Ljava/io/OutputStreamWriter;
    const/4 v0, 0x0

    .line 42
    .local v0, "command":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v4, "at"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    invoke-direct {p0, v3, v0}, Lcom/navdy/obd/simulator/Elm327;->handleCommand(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V

    .line 57
    :goto_1
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    .end local v0    # "command":Ljava/lang/String;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "writer":Ljava/io/OutputStreamWriter;
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v5, "Exception - exiting Elm327 thread"

    invoke-interface {v4, v5, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-void

    .line 47
    .restart local v0    # "command":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "writer":Ljava/io/OutputStreamWriter;
    :cond_1
    :try_start_1
    const-string v4, "01"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    invoke-direct {p0, v3, v0}, Lcom/navdy/obd/simulator/Elm327;->handleRead(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V

    goto :goto_1

    .line 49
    :cond_2
    const-string v4, "quit"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 51
    sget-object v4, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v5, "Quitting Elm327 reader thread"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_2

    .line 54
    :cond_3
    sget-object v4, Lcom/navdy/obd/simulator/Elm327;->Log:Lorg/slf4j/Logger;

    const-string v5, "Unknown command {}"

    invoke-interface {v4, v5, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    const-string v4, "UNKNOWN COMMAND\r>"

    invoke-virtual {v3, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
