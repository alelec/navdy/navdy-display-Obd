.class Lcom/navdy/obd/simulator/Ecu$Pid;
.super Ljava/lang/Object;
.source "Ecu.java"

# interfaces
.implements Lcom/navdy/obd/simulator/IPid;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/simulator/Ecu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Pid"
.end annotation


# instance fields
.field bytes:I

.field code:Ljava/lang/String;

.field interpolator:Lcom/navdy/obd/simulator/LinearInterpolation;

.field name:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/obd/simulator/Ecu;


# direct methods
.method constructor <init>(Lcom/navdy/obd/simulator/Ecu;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/obd/simulator/LinearInterpolation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/simulator/Ecu;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "code"    # Ljava/lang/String;
    .param p4, "bytes"    # I
    .param p5, "interpolator"    # Lcom/navdy/obd/simulator/LinearInterpolation;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->this$0:Lcom/navdy/obd/simulator/Ecu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->name:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->code:Ljava/lang/String;

    .line 29
    iput p4, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->bytes:I

    .line 30
    iput-object p5, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->interpolator:Lcom/navdy/obd/simulator/LinearInterpolation;

    .line 31
    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getValue(J)Ljava/lang/String;
    .locals 7
    .param p1, "timestamp"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 34
    iget-object v2, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->interpolator:Lcom/navdy/obd/simulator/LinearInterpolation;

    invoke-virtual {v2, p1, p2}, Lcom/navdy/obd/simulator/LinearInterpolation;->getValue(J)J

    move-result-wide v0

    .line 35
    .local v0, "value":J
    iget v2, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->bytes:I

    if-ne v2, v4, :cond_0

    .line 36
    const-string v2, "%02x"

    new-array v3, v4, [Ljava/lang/Object;

    const-wide/16 v4, 0xff

    and-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 40
    :goto_0
    return-object v2

    .line 37
    :cond_0
    iget v2, p0, Lcom/navdy/obd/simulator/Ecu$Pid;->bytes:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 38
    const-string v2, "%04x"

    new-array v3, v4, [Ljava/lang/Object;

    const-wide/32 v4, 0xffff

    and-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 40
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method
