.class public Lcom/navdy/obd/simulator/LinearInterpolation;
.super Ljava/lang/Object;
.source "LinearInterpolation.java"


# static fields
.field static final MAX_SIZE:I = 0x64


# instance fields
.field private count:I

.field timestamps:[J

.field values:[J


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    .line 15
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    .line 17
    iput v1, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    .line 22
    iget-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    aput-wide v4, v0, v1

    .line 23
    iget-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    aput-wide v4, v0, v1

    .line 24
    return-void
.end method

.method static generate(IIIII)Lcom/navdy/obd/simulator/LinearInterpolation;
    .locals 8
    .param p0, "samples"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "avgDelay"    # I
    .param p4, "delayRange"    # I

    .prologue
    .line 29
    new-instance v4, Lcom/navdy/obd/simulator/LinearInterpolation;

    invoke-direct {v4}, Lcom/navdy/obd/simulator/LinearInterpolation;-><init>()V

    .line 30
    .local v4, "result":Lcom/navdy/obd/simulator/LinearInterpolation;
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 31
    .local v3, "random":Ljava/util/Random;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p0, :cond_0

    .line 32
    mul-int/lit8 v5, p4, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/2addr v5, p3

    sub-int/2addr v5, p4

    int-to-long v0, v5

    .line 33
    .local v0, "delay":J
    sub-int v5, p2, p1

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/2addr v5, p1

    int-to-long v6, v5

    .line 34
    .local v6, "value":J
    invoke-virtual {v4, v0, v1, v6, v7}, Lcom/navdy/obd/simulator/LinearInterpolation;->add(JJ)V

    .line 31
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 36
    .end local v0    # "delay":J
    .end local v6    # "value":J
    :cond_0
    return-object v4
.end method


# virtual methods
.method add(JJ)V
    .locals 5
    .param p1, "delay"    # J
    .param p3, "value"    # J

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    iget v1, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    iget-object v2, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    iget v3, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    add-int/lit8 v3, v3, -0x1

    aget-wide v2, v2, v3

    add-long/2addr v2, p1

    aput-wide v2, v0, v1

    .line 80
    iget-object v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    iget v1, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    aput-wide p3, v0, v1

    .line 81
    iget v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    .line 82
    return-void
.end method

.method getValue(J)J
    .locals 21
    .param p1, "timestamp"    # J

    .prologue
    .line 62
    invoke-virtual/range {p0 .. p2}, Lcom/navdy/obd/simulator/LinearInterpolation;->localTime(J)J

    move-result-wide v2

    .line 63
    .local v2, "localTimestamp":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v7, v0, v1, v2, v3}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v6

    .line 64
    .local v6, "position":I
    if-ltz v6, :cond_0

    .line 65
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    aget-wide v16, v7, v6

    .line 73
    :goto_0
    return-wide v16

    .line 67
    :cond_0
    neg-int v7, v6

    add-int/lit8 v4, v7, -0x2

    .line 68
    .local v4, "p1":I
    add-int/lit8 v5, v4, 0x1

    .line 69
    .local v5, "p2":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    aget-wide v8, v7, v4

    .line 70
    .local v8, "t1":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    aget-wide v10, v7, v5

    .line 71
    .local v10, "t2":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    aget-wide v12, v7, v4

    .line 72
    .local v12, "v1":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/obd/simulator/LinearInterpolation;->values:[J

    aget-wide v14, v7, v5

    .line 73
    .local v14, "v2":J
    sub-long v16, v14, v12

    sub-long v18, v2, v8

    mul-long v16, v16, v18

    sub-long v18, v10, v8

    div-long v16, v16, v18

    add-long v16, v16, v12

    goto :goto_0
.end method

.method localTime(J)J
    .locals 11
    .param p1, "timestamp"    # J

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/navdy/obd/simulator/LinearInterpolation;->timeRange()J

    move-result-wide v4

    .line 50
    .local v4, "range":J
    div-long v0, p1, v4

    .line 51
    .local v0, "multiple":J
    rem-long v2, p1, v4

    .line 52
    .local v2, "offset":J
    const-wide/16 v6, 0x2

    rem-long v6, v0, v6

    const-wide/16 v8, 0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 54
    sub-long v2, v4, v2

    .line 56
    .end local v2    # "offset":J
    :cond_0
    return-wide v2
.end method

.method timeRange()J
    .locals 4

    .prologue
    .line 41
    iget-object v2, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->timestamps:[J

    iget v3, p0, Lcom/navdy/obd/simulator/LinearInterpolation;->count:I

    add-int/lit8 v3, v3, -0x1

    aget-wide v0, v2, v3

    .line 43
    .local v0, "maxTimestamp":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .end local v0    # "maxTimestamp":J
    :goto_0
    return-wide v0

    .restart local v0    # "maxTimestamp":J
    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0
.end method
