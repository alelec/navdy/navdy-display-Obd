.class public Lcom/navdy/obd/simulator/MockObdChannel;
.super Ljava/lang/Object;
.source "MockObdChannel.java"

# interfaces
.implements Lcom/navdy/obd/io/IChannel;


# static fields
.field public static final CONNECTION_NAME:Ljava/lang/String; = "mock_obd_channel"

.field static final Log:Lorg/slf4j/Logger;

.field public static MOCK_CHANNEL_INFO:Lcom/navdy/obd/io/ChannelInfo;


# instance fields
.field private inputStream:Ljava/io/PipedInputStream;

.field private outputStream:Ljava/io/PipedOutputStream;

.field private simulator:Lcom/navdy/obd/simulator/Elm327;

.field private sink:Lcom/navdy/obd/io/IChannelSink;

.field private state:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/obd/io/ChannelInfo;

    sget-object v1, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->MOCK:Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    const-string v2, "simulated"

    const-string v3, "mock_obd_channel"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/obd/io/ChannelInfo;-><init>(Lcom/navdy/obd/io/ChannelInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/simulator/MockObdChannel;->MOCK_CHANNEL_INFO:Lcom/navdy/obd/io/ChannelInfo;

    .line 31
    const-class v0, Lcom/navdy/obd/simulator/MockObdChannel;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/io/IChannelSink;)V
    .locals 1
    .param p1, "sink"    # Lcom/navdy/obd/io/IChannelSink;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/simulator/MockObdChannel;->state:I

    .line 40
    iput-object p1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    .line 41
    return-void
.end method

.method private safelyClose(Ljava/io/Closeable;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/Closeable;

    .prologue
    .line 107
    if-eqz p1, :cond_0

    .line 109
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Exception closing stream"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private sendQuitCommand()V
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    if-eqz v1, :cond_0

    .line 99
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    const-string v2, "QUIT\r>"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PipedOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Exception sending quit command"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setState(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->state:I

    .line 50
    iget-object v0, p0, Lcom/navdy/obd/simulator/MockObdChannel;->sink:Lcom/navdy/obd/io/IChannelSink;

    invoke-interface {v0, p1}, Lcom/navdy/obd/io/IChannelSink;->onStateChange(I)V

    .line 51
    return-void
.end method


# virtual methods
.method public connect(Ljava/lang/String;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v1, "mock_obd_channel"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    iget v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->state:I

    if-nez v1, :cond_0

    .line 57
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Connecting to mock obd channel"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 58
    new-instance v1, Ljava/io/PipedOutputStream;

    invoke-direct {v1}, Ljava/io/PipedOutputStream;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    .line 59
    new-instance v1, Ljava/io/PipedInputStream;

    invoke-direct {v1}, Ljava/io/PipedInputStream;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->inputStream:Ljava/io/PipedInputStream;

    .line 61
    :try_start_0
    new-instance v1, Lcom/navdy/obd/simulator/Elm327;

    new-instance v2, Ljava/io/PipedInputStream;

    iget-object v3, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    invoke-direct {v2, v3}, Ljava/io/PipedInputStream;-><init>(Ljava/io/PipedOutputStream;)V

    new-instance v3, Ljava/io/PipedOutputStream;

    iget-object v4, p0, Lcom/navdy/obd/simulator/MockObdChannel;->inputStream:Ljava/io/PipedInputStream;

    invoke-direct {v3, v4}, Ljava/io/PipedOutputStream;-><init>(Ljava/io/PipedInputStream;)V

    invoke-direct {v1, v2, v3}, Lcom/navdy/obd/simulator/Elm327;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->simulator:Lcom/navdy/obd/simulator/Elm327;

    .line 62
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->simulator:Lcom/navdy/obd/simulator/Elm327;

    invoke-virtual {v1}, Lcom/navdy/obd/simulator/Elm327;->start()V

    .line 63
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/navdy/obd/simulator/MockObdChannel;->setState(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Exception starting elm327"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 71
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Invalid address passed to channel"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    iget v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->state:I

    if-eqz v1, :cond_1

    .line 78
    invoke-direct {p0}, Lcom/navdy/obd/simulator/MockObdChannel;->sendQuitCommand()V

    .line 79
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    invoke-direct {p0, v1}, Lcom/navdy/obd/simulator/MockObdChannel;->safelyClose(Ljava/io/Closeable;)V

    .line 80
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->inputStream:Ljava/io/PipedInputStream;

    invoke-direct {p0, v1}, Lcom/navdy/obd/simulator/MockObdChannel;->safelyClose(Ljava/io/Closeable;)V

    .line 81
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->simulator:Lcom/navdy/obd/simulator/Elm327;

    if-eqz v1, :cond_0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/simulator/MockObdChannel;->simulator:Lcom/navdy/obd/simulator/Elm327;

    invoke-virtual {v1}, Lcom/navdy/obd/simulator/Elm327;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    iput-object v3, p0, Lcom/navdy/obd/simulator/MockObdChannel;->simulator:Lcom/navdy/obd/simulator/Elm327;

    .line 89
    :cond_0
    iput-object v3, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    .line 90
    iput-object v3, p0, Lcom/navdy/obd/simulator/MockObdChannel;->inputStream:Ljava/io/PipedInputStream;

    .line 91
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/obd/simulator/MockObdChannel;->setState(I)V

    .line 94
    :cond_1
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/navdy/obd/simulator/MockObdChannel;->Log:Lorg/slf4j/Logger;

    const-string v2, "Interrupted joining simulator thread"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/navdy/obd/simulator/MockObdChannel;->inputStream:Ljava/io/PipedInputStream;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/obd/simulator/MockObdChannel;->outputStream:Ljava/io/PipedOutputStream;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/navdy/obd/simulator/MockObdChannel;->state:I

    return v0
.end method
