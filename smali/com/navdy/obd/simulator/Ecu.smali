.class public Lcom/navdy/obd/simulator/Ecu;
.super Ljava/lang/Object;
.source "Ecu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/simulator/Ecu$SupportedPids;,
        Lcom/navdy/obd/simulator/Ecu$Pid;
    }
.end annotation


# instance fields
.field private pids:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/obd/simulator/IPid;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/16 v13, 0x2710

    const/16 v12, 0x32

    const/4 v11, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/simulator/Ecu;->pids:Ljava/util/HashMap;

    .line 82
    new-instance v0, Lcom/navdy/obd/simulator/Ecu$Pid;

    const-string v2, "Speed"

    const-string v3, "0d"

    const/16 v1, 0x64

    const/16 v5, 0x1388

    invoke-static {v12, v11, v1, v13, v5}, Lcom/navdy/obd/simulator/LinearInterpolation;->generate(IIIII)Lcom/navdy/obd/simulator/LinearInterpolation;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/simulator/Ecu$Pid;-><init>(Lcom/navdy/obd/simulator/Ecu;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/obd/simulator/LinearInterpolation;)V

    invoke-virtual {p0, v0}, Lcom/navdy/obd/simulator/Ecu;->addPid(Lcom/navdy/obd/simulator/IPid;)V

    .line 83
    new-instance v5, Lcom/navdy/obd/simulator/Ecu$Pid;

    const-string v7, "RPM"

    const-string v8, "0c"

    const/4 v9, 0x2

    const/16 v0, 0x3e80

    const/16 v1, 0x1f4

    invoke-static {v12, v11, v0, v13, v1}, Lcom/navdy/obd/simulator/LinearInterpolation;->generate(IIIII)Lcom/navdy/obd/simulator/LinearInterpolation;

    move-result-object v10

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, Lcom/navdy/obd/simulator/Ecu$Pid;-><init>(Lcom/navdy/obd/simulator/Ecu;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/obd/simulator/LinearInterpolation;)V

    invoke-virtual {p0, v5}, Lcom/navdy/obd/simulator/Ecu;->addPid(Lcom/navdy/obd/simulator/IPid;)V

    .line 84
    new-instance v0, Lcom/navdy/obd/simulator/Ecu$Pid;

    const-string v2, "Temperature"

    const-string v3, "05"

    const/16 v1, 0xff

    const v5, 0x186a0

    invoke-static {v12, v11, v1, v5, v13}, Lcom/navdy/obd/simulator/LinearInterpolation;->generate(IIIII)Lcom/navdy/obd/simulator/LinearInterpolation;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/simulator/Ecu$Pid;-><init>(Lcom/navdy/obd/simulator/Ecu;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/obd/simulator/LinearInterpolation;)V

    invoke-virtual {p0, v0}, Lcom/navdy/obd/simulator/Ecu;->addPid(Lcom/navdy/obd/simulator/IPid;)V

    .line 87
    new-instance v0, Lcom/navdy/obd/simulator/Ecu$SupportedPids;

    iget-object v1, p0, Lcom/navdy/obd/simulator/Ecu;->pids:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/obd/simulator/Ecu$SupportedPids;-><init>(Lcom/navdy/obd/simulator/Ecu;Ljava/util/Set;)V

    invoke-virtual {p0, v0}, Lcom/navdy/obd/simulator/Ecu;->addPid(Lcom/navdy/obd/simulator/IPid;)V

    .line 88
    return-void
.end method


# virtual methods
.method public addPid(Lcom/navdy/obd/simulator/IPid;)V
    .locals 2
    .param p1, "pid"    # Lcom/navdy/obd/simulator/IPid;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/navdy/obd/simulator/Ecu;->pids:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/navdy/obd/simulator/IPid;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method public readPid(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 100
    .local v2, "timestamp":J
    iget-object v1, p0, Lcom/navdy/obd/simulator/Ecu;->pids:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/simulator/IPid;

    .line 101
    .local v0, "pid":Lcom/navdy/obd/simulator/IPid;
    if-eqz v0, :cond_0

    .line 102
    invoke-interface {v0, v2, v3}, Lcom/navdy/obd/simulator/IPid;->getValue(J)Ljava/lang/String;

    move-result-object v1

    .line 104
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public supports(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pid"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/obd/simulator/Ecu;->pids:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
