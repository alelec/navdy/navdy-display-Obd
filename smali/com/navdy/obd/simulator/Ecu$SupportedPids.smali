.class Lcom/navdy/obd/simulator/Ecu$SupportedPids;
.super Ljava/lang/Object;
.source "Ecu.java"

# interfaces
.implements Lcom/navdy/obd/simulator/IPid;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/simulator/Ecu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SupportedPids"
.end annotation


# instance fields
.field private bitMask:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/obd/simulator/Ecu;


# direct methods
.method constructor <init>(Lcom/navdy/obd/simulator/Ecu;Ljava/util/Set;)V
    .locals 12
    .param p1, "this$0"    # Lcom/navdy/obd/simulator/Ecu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "pidCodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/16 v11, 0x10

    .line 50
    iput-object p1, p0, Lcom/navdy/obd/simulator/Ecu$SupportedPids;->this$0:Lcom/navdy/obd/simulator/Ecu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v8, 0x4

    new-array v1, v8, [B

    .line 52
    .local v1, "bitField":[B
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 53
    .local v3, "code":Ljava/lang/String;
    invoke-static {v3, v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v5, v9, -0x1

    .line 54
    .local v5, "id":I
    div-int/lit8 v6, v5, 0x8

    .line 55
    .local v6, "pos":I
    rem-int/lit8 v0, v5, 0x8

    .line 56
    .local v0, "bit":I
    const/4 v9, 0x1

    rsub-int/lit8 v10, v0, 0x7

    shl-int v2, v9, v10

    .line 57
    .local v2, "bitMask":I
    aget-byte v9, v1, v6

    or-int/2addr v9, v2

    int-to-byte v9, v9

    aput-byte v9, v1, v6

    goto :goto_0

    .line 59
    .end local v0    # "bit":I
    .end local v2    # "bitMask":I
    .end local v3    # "code":Ljava/lang/String;
    .end local v5    # "id":I
    .end local v6    # "pos":I
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v4, "hexString":Ljava/lang/StringBuilder;
    array-length v9, v1

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v9, :cond_2

    aget-byte v7, v1, v8

    .line 61
    .local v7, "val":B
    if-ge v7, v11, :cond_1

    .line 62
    const/16 v10, 0x30

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    :cond_1
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 65
    .end local v7    # "val":B
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/obd/simulator/Ecu$SupportedPids;->bitMask:Ljava/lang/String;

    .line 66
    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "00"

    return-object v0
.end method

.method public getValue(J)Ljava/lang/String;
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/obd/simulator/Ecu$SupportedPids;->bitMask:Ljava/lang/String;

    return-object v0
.end method
