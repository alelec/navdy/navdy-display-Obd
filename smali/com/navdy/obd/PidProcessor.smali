.class public abstract Lcom/navdy/obd/PidProcessor;
.super Ljava/lang/Object;
.source "PidProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract isSupported(Lcom/navdy/obd/PidSet;)Z
.end method

.method public abstract processPidValue(Lcom/navdy/obd/PidLookupTable;)Z
.end method

.method public abstract resolveDependencies()Lcom/navdy/obd/PidSet;
.end method
