.class Lcom/navdy/obd/jobs/AutoConnect$1;
.super Lcom/navdy/obd/IPidListener$Stub;
.source "AutoConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/jobs/AutoConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/jobs/AutoConnect;


# direct methods
.method constructor <init>(Lcom/navdy/obd/jobs/AutoConnect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/jobs/AutoConnect;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/obd/jobs/AutoConnect$1;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-direct {p0}, Lcom/navdy/obd/IPidListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionStateChange(I)V
    .locals 0
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public pidsChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "pidsChanged":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method

.method public pidsRead(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "pidsRead":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .local p2, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method
