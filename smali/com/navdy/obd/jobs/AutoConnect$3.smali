.class Lcom/navdy/obd/jobs/AutoConnect$3;
.super Lcom/navdy/obd/IObdServiceListener$Stub;
.source "AutoConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/jobs/AutoConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/jobs/AutoConnect;


# direct methods
.method constructor <init>(Lcom/navdy/obd/jobs/AutoConnect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/jobs/AutoConnect;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-direct {p0}, Lcom/navdy/obd/IObdServiceListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionStateChange(I)V
    .locals 4
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 253
    if-nez p1, :cond_1

    .line 254
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->DISCONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-static {v1, v2}, Lcom/navdy/obd/jobs/AutoConnect;->access$000(Lcom/navdy/obd/jobs/AutoConnect;Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-static {}, Lcom/navdy/obd/jobs/AutoConnect;->access$200()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/navdy/obd/jobs/AutoConnect;->access$102(Lcom/navdy/obd/jobs/AutoConnect;J)J

    .line 260
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-static {v1, v2}, Lcom/navdy/obd/jobs/AutoConnect;->access$000(Lcom/navdy/obd/jobs/AutoConnect;Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    .line 262
    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v2, "Starting scanning"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v0, "initialSet":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v1, Lcom/navdy/obd/Pid;

    const/16 v2, 0xd

    invoke-direct {v1, v2}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-virtual {v1, v0}, Lcom/navdy/obd/jobs/AutoConnect;->getReadings(Ljava/util/List;)Ljava/util/List;

    goto :goto_0
.end method

.method public onRawData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 274
    return-void
.end method

.method public onStatusMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 271
    return-void
.end method

.method public scannedPids(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 278
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 279
    .local v0, "pidCount":I
    :goto_0
    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Recording readings for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pids"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-static {v1}, Lcom/navdy/obd/jobs/AutoConnect;->access$300(Lcom/navdy/obd/jobs/AutoConnect;)Lcom/navdy/obd/CarState;

    move-result-object v2

    monitor-enter v2

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect$3;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-static {v1}, Lcom/navdy/obd/jobs/AutoConnect;->access$300(Lcom/navdy/obd/jobs/AutoConnect;)Lcom/navdy/obd/CarState;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/navdy/obd/CarState;->recordReadings(Ljava/util/List;)V

    .line 282
    monitor-exit v2

    .line 283
    return-void

    .line 278
    .end local v0    # "pidCount":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 282
    .restart local v0    # "pidCount":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public scannedVIN(Ljava/lang/String;)V
    .locals 0
    .param p1, "vin"    # Ljava/lang/String;

    .prologue
    .line 289
    return-void
.end method

.method public supportedPids(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method
