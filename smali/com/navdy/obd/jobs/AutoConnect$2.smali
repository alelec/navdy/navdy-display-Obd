.class Lcom/navdy/obd/jobs/AutoConnect$2;
.super Ljava/lang/Object;
.source "AutoConnect.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/jobs/AutoConnect;->prioritizeChannels(Ljava/util/Set;)Ljava/util/LinkedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/navdy/obd/io/ChannelInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/jobs/AutoConnect;


# direct methods
.method constructor <init>(Lcom/navdy/obd/jobs/AutoConnect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/jobs/AutoConnect;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/obd/jobs/AutoConnect$2;->this$0:Lcom/navdy/obd/jobs/AutoConnect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/navdy/obd/io/ChannelInfo;Lcom/navdy/obd/io/ChannelInfo;)I
    .locals 4
    .param p1, "lhs"    # Lcom/navdy/obd/io/ChannelInfo;
    .param p2, "rhs"    # Lcom/navdy/obd/io/ChannelInfo;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->getConnectionType()Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->ordinal()I

    move-result v0

    .line 145
    .local v0, "lhsType":I
    invoke-virtual {p2}, Lcom/navdy/obd/io/ChannelInfo;->getConnectionType()Lcom/navdy/obd/io/ChannelInfo$ConnectionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/obd/io/ChannelInfo$ConnectionType;->ordinal()I

    move-result v1

    .line 146
    .local v1, "rhsType":I
    if-ge v0, v1, :cond_0

    .line 147
    const/4 v2, -0x1

    .line 150
    :goto_0
    return v2

    .line 148
    :cond_0
    if-le v0, v1, :cond_1

    .line 149
    const/4 v2, 0x1

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/navdy/obd/io/ChannelInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/navdy/obd/io/ChannelInfo;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 141
    check-cast p1, Lcom/navdy/obd/io/ChannelInfo;

    check-cast p2, Lcom/navdy/obd/io/ChannelInfo;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/obd/jobs/AutoConnect$2;->compare(Lcom/navdy/obd/io/ChannelInfo;Lcom/navdy/obd/io/ChannelInfo;)I

    move-result v0

    return v0
.end method
