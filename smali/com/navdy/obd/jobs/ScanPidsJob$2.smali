.class Lcom/navdy/obd/jobs/ScanPidsJob$2;
.super Ljava/lang/Object;
.source "ScanPidsJob.java"

# interfaces
.implements Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/jobs/ScanPidsJob;-><init>(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/Profile;Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/VehicleStateManager;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/jobs/ScanPidsJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

.field final synthetic val$listener:Lcom/navdy/obd/jobs/ScanPidsJob$IListener;


# direct methods
.method constructor <init>(Lcom/navdy/obd/jobs/ScanPidsJob;Lcom/navdy/obd/jobs/ScanPidsJob$IListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/jobs/ScanPidsJob;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    iput-object p2, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->val$listener:Lcom/navdy/obd/jobs/ScanPidsJob$IListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCanBusDataRead(ID)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "value"    # D

    .prologue
    .line 98
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-static {v1}, Lcom/navdy/obd/jobs/ScanPidsJob;->access$000(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-static {v1}, Lcom/navdy/obd/jobs/ScanPidsJob;->access$000(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 100
    .local v0, "pidObject":Lcom/navdy/obd/Pid;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 101
    invoke-virtual {v0, p2, p3}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 102
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->val$listener:Lcom/navdy/obd/jobs/ScanPidsJob$IListener;

    iget-object v2, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-interface {v1, v2}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->onCanBusDataRead(Lcom/navdy/obd/jobs/ScanPidsJob;)V

    .line 111
    :goto_0
    return-void

    .line 104
    .end local v0    # "pidObject":Lcom/navdy/obd/Pid;
    :cond_0
    new-instance v0, Lcom/navdy/obd/Pid;

    invoke-direct {v0, p1}, Lcom/navdy/obd/Pid;-><init>(I)V

    .line 105
    .restart local v0    # "pidObject":Lcom/navdy/obd/Pid;
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-static {v1}, Lcom/navdy/obd/jobs/ScanPidsJob;->access$100(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-static {v1}, Lcom/navdy/obd/jobs/ScanPidsJob;->access$000(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 108
    invoke-virtual {v0, p2, p3}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 109
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->val$listener:Lcom/navdy/obd/jobs/ScanPidsJob$IListener;

    iget-object v2, p0, Lcom/navdy/obd/jobs/ScanPidsJob$2;->this$0:Lcom/navdy/obd/jobs/ScanPidsJob;

    invoke-interface {v1, v2}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->onCanBusDataRead(Lcom/navdy/obd/jobs/ScanPidsJob;)V

    goto :goto_0
.end method
