.class public final enum Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;
.super Ljava/lang/Enum;
.source "AutoConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/jobs/AutoConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutoConnectState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum CONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum CONNECTING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum DISCONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum FULL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum INITIAL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum SLEEPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field public static final enum STOPPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->DISCONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "INITIAL_SCAN"

    invoke-direct {v0, v1, v4}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->INITIAL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "FULL_SCAN"

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->FULL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v6}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v7}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "SLEEPING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->SLEEPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    const-string v1, "STOPPING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->STOPPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    .line 39
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->DISCONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->INITIAL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->FULL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->SLEEPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->STOPPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->$VALUES:[Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->$VALUES:[Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-virtual {v0}, [Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    return-object v0
.end method
