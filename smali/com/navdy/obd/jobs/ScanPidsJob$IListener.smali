.class public interface abstract Lcom/navdy/obd/jobs/ScanPidsJob$IListener;
.super Ljava/lang/Object;
.source "ScanPidsJob.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/jobs/ScanPidsJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IListener"
.end annotation


# virtual methods
.method public abstract getCanBusMonitoringCommand()Lcom/navdy/obd/command/CANBusMonitoringCommand;
.end method

.method public abstract onCanBusDataRead(Lcom/navdy/obd/jobs/ScanPidsJob;)V
.end method

.method public abstract onCanBusMonitoringErrorDetected()V
.end method
