.class public Lcom/navdy/obd/jobs/ScanDtcJob;
.super Lcom/navdy/obd/ObdJob;
.source "ScanDtcJob.java"


# static fields
.field static final Log:Lorg/slf4j/Logger;

.field private static final MAX_SCAN_COUNT_WITH_NO_DATA:I = 0x5

.field public static final MINIMUM_SAMPLES_REQUIRED:I = 0xa

.field public static final SAMPLING_DATA_DELAY_THRESHOLD:D = 100.0


# instance fields
.field private activeTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

.field private checkDtcCommand:Lcom/navdy/obd/command/CheckDTCCommand;

.field private pendingTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

.field private started:Z

.field private vehicleInfo:Lcom/navdy/obd/VehicleInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/navdy/obd/ObdJob;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/jobs/ScanDtcJob;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/command/BatchCommand;Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 7
    .param p1, "cmds"    # Lcom/navdy/obd/command/BatchCommand;
    .param p2, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p3, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p4, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p5, "listener"    # Lcom/navdy/obd/ObdJob$IListener;
    .param p6, "obdDataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    const/4 v6, 0x0

    .line 49
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 40
    iput-boolean v6, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->started:Z

    .line 50
    iput-object p2, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    .line 51
    new-instance v0, Lcom/navdy/obd/command/CheckDTCCommand;

    iget-object v1, p2, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/CheckDTCCommand;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->checkDtcCommand:Lcom/navdy/obd/command/CheckDTCCommand;

    .line 52
    new-instance v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    iget-object v1, p2, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;-><init>(Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->activeTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    .line 53
    new-instance v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    iget-object v1, p2, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-direct {v0, v1, v6}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;-><init>(Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->pendingTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    .line 54
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->checkDtcCommand:Lcom/navdy/obd/command/CheckDTCCommand;

    invoke-virtual {p1, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 55
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->activeTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-virtual {p1, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 56
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->pendingTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-virtual {p1, v0}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 57
    return-void
.end method

.method private static buildCommand(Ljava/util/List;Lcom/navdy/obd/VehicleInfo;)Lcom/navdy/obd/command/BatchCommand;
    .locals 3
    .param p1, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/ObdCommand;",
            ">;",
            "Lcom/navdy/obd/VehicleInfo;",
            ")",
            "Lcom/navdy/obd/command/BatchCommand;"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/command/ObdCommand;>;"
    iget-object v1, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    if-eqz v1, :cond_25

    iget-object v1, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_25

    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_d
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_25

    .line 112
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/command/ObdCommand;

    iget-object v2, p1, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/ObdCommand;->setExpectedResponses(I)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 116
    .end local v0    # "i":I
    :cond_25
    new-instance v2, Lcom/navdy/obd/command/BatchCommand;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/navdy/obd/command/ICommand;

    invoke-interface {p0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/navdy/obd/command/ICommand;

    check-cast v1, [Lcom/navdy/obd/command/ICommand;

    invoke-direct {v2, v1}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    return-object v2
.end method

.method public static newScanDtcJob(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)Lcom/navdy/obd/jobs/ScanDtcJob;
    .locals 7
    .param p0, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p1, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p2, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p3, "listener"    # Lcom/navdy/obd/ObdJob$IListener;
    .param p4, "obdDataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    .line 121
    new-instance v1, Lcom/navdy/obd/command/BatchCommand;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2}, Lcom/navdy/obd/command/BatchCommand;-><init>(Ljava/util/List;)V

    .line 122
    .local v1, "cmds":Lcom/navdy/obd/command/BatchCommand;
    new-instance v0, Lcom/navdy/obd/jobs/ScanDtcJob;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/navdy/obd/jobs/ScanDtcJob;-><init>(Lcom/navdy/obd/command/BatchCommand;Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 123
    .local v0, "job":Lcom/navdy/obd/jobs/ScanDtcJob;
    return-object v0
.end method


# virtual methods
.method protected postExecute()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-boolean v3, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->started:Z

    if-nez v3, :cond_7

    .line 61
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->started:Z

    .line 64
    :cond_7
    iget-object v3, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->activeTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-virtual {v3}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getTroubleCodes()Ljava/util/List;

    move-result-object v2

    .line 66
    .local v2, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_38

    .line 67
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 68
    .local v1, "troubleCode":Ljava/lang/String;
    sget-object v4, Lcom/navdy/obd/jobs/ScanDtcJob;->Log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Active Code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_13

    .line 72
    .end local v1    # "troubleCode":Ljava/lang/String;
    :cond_38
    iget-object v3, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->pendingTroubleCodesCommand:Lcom/navdy/obd/command/ReadTroubleCodesCommand;

    invoke-virtual {v3}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getTroubleCodes()Ljava/util/List;

    move-result-object v0

    .line 74
    .local v0, "pendingCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_77

    .line 75
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_44
    :goto_44
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_74

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 76
    .restart local v1    # "troubleCode":Ljava/lang/String;
    sget-object v4, Lcom/navdy/obd/jobs/ScanDtcJob;->Log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pending Code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 77
    if-eqz v2, :cond_44

    .line 78
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_44

    .line 79
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_44

    .line 83
    .end local v1    # "troubleCode":Ljava/lang/String;
    :cond_74
    if-nez v2, :cond_77

    .line 84
    move-object v2, v0

    .line 88
    :cond_77
    iget-object v3, p0, Lcom/navdy/obd/jobs/ScanDtcJob;->vehicleInfo:Lcom/navdy/obd/VehicleInfo;

    iput-object v2, v3, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 90
    return-void
.end method
