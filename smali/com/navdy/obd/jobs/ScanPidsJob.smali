.class public Lcom/navdy/obd/jobs/ScanPidsJob;
.super Lcom/navdy/obd/ObdJob;
.source "ScanPidsJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/jobs/ScanPidsJob$IListener;
    }
.end annotation


# static fields
.field static final Log:Lorg/slf4j/Logger;

.field private static final MAX_SCAN_COUNT_WITH_NO_DATA:I = 0x5

.field public static final MINIMUM_SAMPLES_REQUIRED:I = 0xa

.field static final NO_OP_COMMAND:Lcom/navdy/obd/command/ICommand;

.field public static final SAMPLING_DATA_DELAY_THRESHOLD:D = 100.0


# instance fields
.field private fullPids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field private fullPidsLookup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field isGatheringCanBusData:Z

.field private sampledPids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field scanCountWithNoData:I

.field schedule:Lcom/navdy/obd/ScanSchedule;

.field private started:Z

.field private vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/navdy/obd/ObdJob;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/jobs/ScanPidsJob;->Log:Lorg/slf4j/Logger;

    .line 43
    new-instance v0, Lcom/navdy/obd/jobs/ScanPidsJob$1;

    invoke-direct {v0}, Lcom/navdy/obd/jobs/ScanPidsJob$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/jobs/ScanPidsJob;->NO_OP_COMMAND:Lcom/navdy/obd/command/ICommand;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/Profile;Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/VehicleStateManager;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/jobs/ScanPidsJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 8
    .param p1, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p2, "profile"    # Lcom/navdy/obd/Profile;
    .param p3, "schedule"    # Lcom/navdy/obd/ScanSchedule;
    .param p4, "vehicleStateManager"    # Lcom/navdy/obd/VehicleStateManager;
    .param p5, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p6, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p7, "listener"    # Lcom/navdy/obd/jobs/ScanPidsJob$IListener;
    .param p8, "obdDataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;

    .prologue
    .line 92
    invoke-interface {p7}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->getCanBusMonitoringCommand()Lcom/navdy/obd/command/CANBusMonitoringCommand;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/navdy/obd/jobs/ScanPidsJob;->buildCommand(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/Profile;Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/command/CANBusMonitoringCommand;)Lcom/navdy/obd/command/ScheduledBatchCommand;

    move-result-object v1

    move-object v0, p0

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object/from16 v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->isGatheringCanBusData:Z

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPidsLookup:Ljava/util/HashMap;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->sampledPids:Ljava/util/List;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->scanCountWithNoData:I

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->started:Z

    .line 93
    invoke-interface {p7}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->getCanBusMonitoringCommand()Lcom/navdy/obd/command/CANBusMonitoringCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    invoke-interface {p7}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->getCanBusMonitoringCommand()Lcom/navdy/obd/command/CANBusMonitoringCommand;

    move-result-object v6

    .line 95
    .local v6, "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    new-instance v0, Lcom/navdy/obd/jobs/ScanPidsJob$2;

    invoke-direct {v0, p0, p7}, Lcom/navdy/obd/jobs/ScanPidsJob$2;-><init>(Lcom/navdy/obd/jobs/ScanPidsJob;Lcom/navdy/obd/jobs/ScanPidsJob$IListener;)V

    invoke-virtual {v6, v0}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->setCANBusDataListener(Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;)V

    .line 114
    .end local v6    # "canBusMonitoringCommand":Lcom/navdy/obd/command/CANBusMonitoringCommand;
    :cond_0
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->command:Lcom/navdy/obd/command/ICommand;

    check-cast v0, Lcom/navdy/obd/command/ScheduledBatchCommand;

    invoke-virtual {v0}, Lcom/navdy/obd/command/ScheduledBatchCommand;->getPids()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    .line 115
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/obd/Pid;

    .line 116
    .local v7, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v7}, Lcom/navdy/obd/Pid;->getId()I

    move-result v1

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_1

    .line 117
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->isGatheringCanBusData:Z

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPidsLookup:Ljava/util/HashMap;

    invoke-virtual {v7}, Lcom/navdy/obd/Pid;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 121
    .end local v7    # "pid":Lcom/navdy/obd/Pid;
    :cond_2
    iput-object p3, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->schedule:Lcom/navdy/obd/ScanSchedule;

    .line 122
    iput-object p4, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/jobs/ScanPidsJob;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPidsLookup:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/obd/jobs/ScanPidsJob;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/jobs/ScanPidsJob;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    return-object v0
.end method

.method private static bestEcu(Lcom/navdy/obd/VehicleInfo;I)Lcom/navdy/obd/ECU;
    .locals 6
    .param p0, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p1, "pid"    # I

    .prologue
    .line 253
    const/4 v1, 0x0

    .line 254
    .local v1, "bestScore":I
    const/4 v0, 0x0

    .line 255
    .local v0, "bestEcu":Lcom/navdy/obd/ECU;
    iget-object v4, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/ECU;

    .line 256
    .local v2, "ecu":Lcom/navdy/obd/ECU;
    iget-object v5, v2, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v5}, Lcom/navdy/obd/PidSet;->size()I

    move-result v3

    .line 257
    .local v3, "score":I
    iget-object v5, v2, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v5, p1}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v5

    if-eqz v5, :cond_0

    if-le v3, v1, :cond_0

    .line 258
    move-object v0, v2

    .line 259
    move v1, v3

    goto :goto_0

    .line 262
    .end local v2    # "ecu":Lcom/navdy/obd/ECU;
    .end local v3    # "score":I
    :cond_1
    return-object v0
.end method

.method private static buildCommand(Lcom/navdy/obd/VehicleInfo;Lcom/navdy/obd/Profile;Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/command/CANBusMonitoringCommand;)Lcom/navdy/obd/command/ScheduledBatchCommand;
    .locals 12
    .param p0, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p1, "profile"    # Lcom/navdy/obd/Profile;
    .param p2, "schedule"    # Lcom/navdy/obd/ScanSchedule;
    .param p3, "canBusMonitoringCommand"    # Lcom/navdy/obd/command/CANBusMonitoringCommand;

    .prologue
    const/4 v11, 0x1

    .line 210
    invoke-virtual {p2}, Lcom/navdy/obd/ScanSchedule;->size()I

    move-result v4

    .line 212
    .local v4, "pidCount":I
    invoke-virtual {p2}, Lcom/navdy/obd/ScanSchedule;->getScanList()Ljava/util/List;

    move-result-object v7

    .line 214
    .local v7, "scanList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ScanSchedule$Scan;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v1, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/command/ICommand;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_5

    .line 216
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 217
    .local v6, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    iget v5, v6, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    .line 218
    .local v5, "pidId":I
    const/16 v8, 0x3e8

    if-ne v5, v8, :cond_0

    .line 219
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->Log:Lorg/slf4j/Logger;

    const-string v9, "Added CAN bus gathering data command to the schedule"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 220
    sget-object v8, Lcom/navdy/obd/command/GatherCANBusDataCommand;->MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 222
    :cond_0
    const/16 v8, 0x3e9

    if-ne v5, v8, :cond_1

    .line 223
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->Log:Lorg/slf4j/Logger;

    const-string v9, "Added CAN bus monitoring data command to the schedule"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p3}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->reset()V

    .line 225
    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 228
    :cond_1
    invoke-virtual {p1, v5}, Lcom/navdy/obd/Profile;->lookup(I)Lcom/navdy/obd/command/ObdCommand;

    move-result-object v0

    .line 229
    .local v0, "command":Lcom/navdy/obd/command/ObdCommand;
    if-eqz v0, :cond_4

    .line 230
    invoke-static {p0, v5}, Lcom/navdy/obd/jobs/ScanPidsJob;->supportedEcuCount(Lcom/navdy/obd/VehicleInfo;I)I

    move-result v2

    .line 231
    .local v2, "ecusSupportingPid":I
    if-le v2, v11, :cond_2

    .line 232
    invoke-static {p0, v5}, Lcom/navdy/obd/jobs/ScanPidsJob;->bestEcu(Lcom/navdy/obd/VehicleInfo;I)Lcom/navdy/obd/ECU;

    move-result-object v8

    iget v8, v8, Lcom/navdy/obd/ECU;->address:I

    invoke-virtual {v0, v8}, Lcom/navdy/obd/command/ObdCommand;->setTargetEcu(I)V

    .line 233
    invoke-virtual {v0, v2}, Lcom/navdy/obd/command/ObdCommand;->setExpectedResponses(I)V

    .line 234
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 235
    :cond_2
    if-ne v2, v11, :cond_3

    .line 236
    const/4 v8, -0x1

    invoke-virtual {v0, v8}, Lcom/navdy/obd/command/ObdCommand;->setTargetEcu(I)V

    .line 237
    invoke-virtual {v0, v11}, Lcom/navdy/obd/command/ObdCommand;->setExpectedResponses(I)V

    .line 238
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    :cond_3
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->Log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Scanning for unsupported pid! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 241
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->NO_OP_COMMAND:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 244
    .end local v2    # "ecusSupportingPid":I
    :cond_4
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->NO_OP_COMMAND:Lcom/navdy/obd/command/ICommand;

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v8, Lcom/navdy/obd/jobs/ScanPidsJob;->Log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to find obd command to read , adding a NO op command"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_1

    .line 248
    .end local v0    # "command":Lcom/navdy/obd/command/ObdCommand;
    .end local v5    # "pidId":I
    .end local v6    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_5
    new-instance v9, Lcom/navdy/obd/command/ScheduledBatchCommand;

    new-array v8, v4, [Lcom/navdy/obd/command/ICommand;

    invoke-interface {v1, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/navdy/obd/command/ICommand;

    invoke-direct {v9, v7, v8}, Lcom/navdy/obd/command/ScheduledBatchCommand;-><init>(Ljava/util/List;[Lcom/navdy/obd/command/ICommand;)V

    return-object v9
.end method

.method private getValue(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/Pid;)D
    .locals 10
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;
    .param p2, "pid"    # Lcom/navdy/obd/Pid;

    .prologue
    .line 187
    const-wide/high16 v6, -0x3e20000000000000L    # -2.147483648E9

    .line 188
    .local v6, "value":D
    instance-of v3, p1, Lcom/navdy/obd/command/ObdCommand;

    if-eqz v3, :cond_1

    move-object v3, p1

    check-cast v3, Lcom/navdy/obd/command/ObdCommand;

    move-object v1, v3

    .line 189
    .local v1, "obdCommand":Lcom/navdy/obd/command/ObdCommand;
    :goto_0
    invoke-virtual {p2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v3

    const/16 v8, 0x2f

    if-ne v3, v8, :cond_3

    if-eqz v1, :cond_3

    .line 191
    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getResponseCount()I

    move-result v3

    const/4 v8, 0x1

    if-le v3, v8, :cond_3

    .line 194
    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getResponseCount()I

    move-result v2

    .line 195
    .local v2, "responses":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 196
    invoke-virtual {v1, v0}, Lcom/navdy/obd/command/ObdCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v3

    iget v3, v3, Lcom/navdy/obd/EcuResponse;->ecu:I

    invoke-virtual {v1, v3}, Lcom/navdy/obd/command/ObdCommand;->setTargetEcu(I)V

    .line 197
    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getDoubleValue()D

    move-result-wide v4

    .line 198
    .local v4, "val":D
    const-wide/high16 v8, -0x3e20000000000000L    # -2.147483648E9

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_2

    const-wide/16 v8, 0x0

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_2

    .line 199
    move-wide v6, v4

    .line 206
    .end local v0    # "i":I
    .end local v2    # "responses":I
    .end local v4    # "val":D
    :cond_0
    :goto_2
    return-wide v6

    .line 188
    .end local v1    # "obdCommand":Lcom/navdy/obd/command/ObdCommand;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 195
    .restart local v0    # "i":I
    .restart local v1    # "obdCommand":Lcom/navdy/obd/command/ObdCommand;
    .restart local v2    # "responses":I
    .restart local v4    # "val":D
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 204
    .end local v0    # "i":I
    .end local v2    # "responses":I
    .end local v4    # "val":D
    :cond_3
    invoke-interface {p1}, Lcom/navdy/obd/command/ICommand;->getDoubleValue()D

    move-result-wide v6

    goto :goto_2
.end method

.method private static supportedEcuCount(Lcom/navdy/obd/VehicleInfo;I)I
    .locals 4
    .param p0, "vehicleInfo"    # Lcom/navdy/obd/VehicleInfo;
    .param p1, "pid"    # I

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "count":I
    iget-object v2, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ECU;

    .line 269
    .local v1, "ecu":Lcom/navdy/obd/ECU;
    iget-object v3, v1, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v3, p1}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    .end local v1    # "ecu":Lcom/navdy/obd/ECU;
    :cond_1
    return v0
.end method


# virtual methods
.method public getFullPidsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    return-object v0
.end method

.method public getSampledPids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/obd/jobs/ScanPidsJob;->sampledPids:Ljava/util/List;

    return-object v0
.end method

.method protected postExecute()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->started:Z

    if-nez v13, :cond_0

    .line 128
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->started:Z

    .line 129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/navdy/obd/VehicleStateManager;->update(Ljava/util/List;)V

    .line 131
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->command:Lcom/navdy/obd/command/ICommand;

    check-cast v4, Lcom/navdy/obd/command/ScheduledBatchCommand;

    .line 132
    .local v4, "batchCommand":Lcom/navdy/obd/command/ScheduledBatchCommand;
    invoke-virtual {v4}, Lcom/navdy/obd/command/ScheduledBatchCommand;->getCommands()Ljava/util/List;

    move-result-object v5

    .line 134
    .local v5, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/command/ICommand;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->sampledPids:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 135
    const/4 v6, 0x0

    .line 137
    .local v6, "hasData":Z
    const/4 v7, 0x0

    .local v7, "i":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v8

    .local v8, "length":I
    :goto_0
    if-ge v7, v8, :cond_4

    .line 138
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->fullPids:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/obd/Pid;

    .line 139
    .local v10, "pid":Lcom/navdy/obd/Pid;
    iget-object v13, v4, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/navdy/obd/command/Sample;

    .line 140
    .local v11, "sample":Lcom/navdy/obd/command/Sample;
    iget-boolean v13, v11, Lcom/navdy/obd/command/Sample;->updated:Z

    if-eqz v13, :cond_3

    .line 141
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->sampledPids:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/navdy/obd/command/ICommand;

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10}, Lcom/navdy/obd/jobs/ScanPidsJob;->getValue(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/Pid;)D

    move-result-wide v16

    .line 143
    .local v16, "value":D
    const-wide/high16 v18, -0x3e20000000000000L    # -2.147483648E9

    cmpl-double v13, v16, v18

    if-eqz v13, :cond_1

    .line 144
    const/4 v6, 0x1

    .line 146
    :cond_1
    invoke-virtual {v11}, Lcom/navdy/obd/command/Sample;->getLastSamplingTime()J

    move-result-wide v14

    .line 147
    .local v14, "samplingTime":J
    const/4 v9, 0x0

    .line 148
    .local v9, "outlierSample":Z
    const-wide/16 v18, 0x0

    cmp-long v13, v14, v18

    if-lez v13, :cond_2

    .line 149
    iget-object v12, v11, Lcom/navdy/obd/command/Sample;->samplingTimeStats:Lcom/navdy/util/RunningStats;

    .line 150
    .local v12, "sampleStats":Lcom/navdy/util/RunningStats;
    iget-object v13, v11, Lcom/navdy/obd/command/Sample;->samplingTimeStats:Lcom/navdy/util/RunningStats;

    long-to-double v0, v14

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lcom/navdy/util/RunningStats;->add(D)V

    .line 151
    invoke-virtual {v12}, Lcom/navdy/util/RunningStats;->count()J

    move-result-wide v18

    const-wide/16 v20, 0xa

    cmp-long v13, v18, v20

    if-lez v13, :cond_2

    long-to-double v0, v14

    move-wide/from16 v18, v0

    invoke-virtual {v12}, Lcom/navdy/util/RunningStats;->mean()D

    move-result-wide v20

    const-wide/high16 v22, 0x4059000000000000L    # 100.0

    add-double v20, v20, v22

    cmpl-double v13, v18, v20

    if-ltz v13, :cond_2

    .line 152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->commandObserver:Lcom/navdy/obd/command/IObdDataObserver;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "DELAYED,"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/navdy/util/RunningStats;->count()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/navdy/util/RunningStats;->mean()D

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/navdy/util/RunningStats;->standardDeviation()D

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Lcom/navdy/obd/command/IObdDataObserver;->onError(Ljava/lang/String;)V

    .line 153
    const/4 v9, 0x1

    .line 157
    .end local v12    # "sampleStats":Lcom/navdy/util/RunningStats;
    :cond_2
    iget-wide v0, v11, Lcom/navdy/obd/command/Sample;->lastSampleTimestamp:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v10, v0, v1}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 158
    if-nez v9, :cond_3

    .line 159
    move-wide/from16 v0, v16

    invoke-virtual {v10, v0, v1}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 137
    .end local v9    # "outlierSample":Z
    .end local v14    # "samplingTime":J
    .end local v16    # "value":D
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 163
    .end local v10    # "pid":Lcom/navdy/obd/Pid;
    .end local v11    # "sample":Lcom/navdy/obd/command/Sample;
    :cond_4
    if-eqz v6, :cond_6

    .line 164
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->scanCountWithNoData:I

    .line 168
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->scanCountWithNoData:I

    const/16 v18, 0x5

    move/from16 v0, v18

    if-lt v13, v0, :cond_7

    .line 169
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->isGatheringCanBusData:Z

    if-eqz v13, :cond_5

    .line 170
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->listener:Lcom/navdy/obd/ObdJob$IListener;

    check-cast v13, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;

    invoke-interface {v13}, Lcom/navdy/obd/jobs/ScanPidsJob$IListener;->onCanBusMonitoringErrorDetected()V

    .line 172
    :cond_5
    new-instance v13, Ljava/io/IOException;

    const-string v18, "Not getting any data from Obd for any PIDS"

    move-object/from16 v0, v18

    invoke-direct {v13, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 166
    :cond_6
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->scanCountWithNoData:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->scanCountWithNoData:I

    goto :goto_1

    .line 174
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/obd/jobs/ScanPidsJob;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    invoke-virtual {v13}, Lcom/navdy/obd/VehicleStateManager;->onScanComplete()V

    .line 175
    return-void
.end method
