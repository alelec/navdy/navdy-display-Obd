.class final Lcom/navdy/obd/jobs/ScanPidsJob$1;
.super Ljava/lang/Object;
.source "ScanPidsJob.java"

# interfaces
.implements Lcom/navdy/obd/command/ICommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/jobs/ScanPidsJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    return-void
.end method

.method public getDoubleValue()D
    .locals 2

    .prologue
    .line 56
    const-wide/high16 v0, -0x3e20000000000000L    # -2.147483648E9

    return-wide v0
.end method

.method public getIntValue()I
    .locals 1

    .prologue
    .line 61
    const/high16 v0, -0x80000000

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "NO_OP"

    return-object v0
.end method

.method public getResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, ""

    return-object v0
.end method
