.class public Lcom/navdy/obd/jobs/AutoConnect;
.super Ljava/lang/Object;
.source "AutoConnect.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;
    }
.end annotation


# static fields
.field private static final FULL_SCAN_DELAY:I = 0xfa0

.field private static final INITIAL_SCAN_DELAY:I = 0x64

.field static final Log:Lorg/slf4j/Logger;

.field public static final MINIMAL_DELAY_TO_RECONNECT:I = 0x1f4

.field private static final RECONNECT_DELAY:J

.field private static final SLEEP_DELAY:J


# instance fields
.field private autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

.field private final carState:Lcom/navdy/obd/CarState;

.field private obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

.field private pidListener:Lcom/navdy/obd/IPidListener;

.field private possibleChannels:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private scanner:Lcom/navdy/obd/discovery/GroupScanner;

.field private scanningPids:Lcom/navdy/obd/PidSet;

.field private service:Lcom/navdy/obd/ObdService;

.field private sleepDelay:J

.field private started:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    const-class v0, Lcom/navdy/obd/jobs/AutoConnect;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/obd/jobs/AutoConnect;->RECONNECT_DELAY:J

    .line 44
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/obd/jobs/AutoConnect;->SLEEP_DELAY:J

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 2
    .param p1, "service"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    sget-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->SLEEPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    iput-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    .line 56
    sget-wide v0, Lcom/navdy/obd/jobs/AutoConnect;->SLEEP_DELAY:J

    iput-wide v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->sleepDelay:J

    .line 60
    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$1;

    invoke-direct {v0, p0}, Lcom/navdy/obd/jobs/AutoConnect$1;-><init>(Lcom/navdy/obd/jobs/AutoConnect;)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->pidListener:Lcom/navdy/obd/IPidListener;

    .line 250
    new-instance v0, Lcom/navdy/obd/jobs/AutoConnect$3;

    invoke-direct {v0, p0}, Lcom/navdy/obd/jobs/AutoConnect$3;-><init>(Lcom/navdy/obd/jobs/AutoConnect;)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    .line 78
    iput-object p1, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    .line 79
    new-instance v0, Lcom/navdy/obd/CarState;

    invoke-direct {v0}, Lcom/navdy/obd/CarState;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->carState:Lcom/navdy/obd/CarState;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->started:Z

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/jobs/AutoConnect;Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/jobs/AutoConnect;
    .param p1, "x1"    # Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    return-void
.end method

.method static synthetic access$102(Lcom/navdy/obd/jobs/AutoConnect;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/jobs/AutoConnect;
    .param p1, "x1"    # J

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/navdy/obd/jobs/AutoConnect;->sleepDelay:J

    return-wide p1
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 35
    sget-wide v0, Lcom/navdy/obd/jobs/AutoConnect;->RECONNECT_DELAY:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/navdy/obd/jobs/AutoConnect;)Lcom/navdy/obd/CarState;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/jobs/AutoConnect;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->carState:Lcom/navdy/obd/CarState;

    return-object v0
.end method

.method private getDelay()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x1f4

    .line 231
    sget-wide v0, Lcom/navdy/obd/jobs/AutoConnect;->RECONNECT_DELAY:J

    .line 232
    .local v0, "delay":J
    sget-object v4, Lcom/navdy/obd/jobs/AutoConnect$4;->$SwitchMap$com$navdy$obd$jobs$AutoConnect$AutoConnectState:[I

    iget-object v5, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-virtual {v5}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 246
    :goto_0
    iget-object v4, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v4}, Lcom/navdy/obd/ObdService;->reconnectImmediately()Z

    move-result v4

    if-eqz v4, :cond_0

    move-wide v0, v2

    .line 247
    :cond_0
    return-wide v0

    .line 234
    :pswitch_0
    const-wide/16 v0, 0xfa0

    .line 235
    goto :goto_0

    .line 238
    :pswitch_1
    const-wide/16 v0, 0x64

    .line 239
    goto :goto_0

    .line 241
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v4}, Lcom/navdy/obd/ObdService;->reconnectImmediately()Z

    move-result v4

    if-eqz v4, :cond_1

    move-wide v0, v2

    .line 242
    :goto_1
    goto :goto_0

    .line 241
    :cond_1
    iget-wide v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->sleepDelay:J

    goto :goto_1

    .line 232
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private pidsChanged(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v1, 0x1

    .line 120
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanningPids:Lcom/navdy/obd/PidSet;

    if-nez v2, :cond_0

    .line 127
    :goto_0
    return v1

    .line 123
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 124
    .local v0, "pid":Lcom/navdy/obd/Pid;
    iget-object v3, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanningPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v3, v0}, Lcom/navdy/obd/PidSet;->contains(Lcom/navdy/obd/Pid;)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 127
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private prioritizeChannels(Ljava/util/Set;)Ljava/util/LinkedList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "channels":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/obd/io/ChannelInfo;>;"
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    new-array v0, v3, [Lcom/navdy/obd/io/ChannelInfo;

    .line 140
    .local v0, "channelArray":[Lcom/navdy/obd/io/ChannelInfo;
    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 141
    new-instance v3, Lcom/navdy/obd/jobs/AutoConnect$2;

    invoke-direct {v3, p0}, Lcom/navdy/obd/jobs/AutoConnect$2;-><init>(Lcom/navdy/obd/jobs/AutoConnect;)V

    invoke-static {v0, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 154
    const/4 v1, 0x0

    .line 155
    .local v1, "i":I
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 156
    .local v2, "result":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/navdy/obd/io/ChannelInfo;>;"
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 157
    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_0
    return-object v2
.end method

.method private setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V
    .locals 3
    .param p1, "newState"    # Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    if-eq p1, v0, :cond_0

    .line 89
    sget-object v0, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Switching to state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 90
    iput-object p1, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    .line 92
    :cond_0
    return-void
.end method

.method private updateScan(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v0, Lcom/navdy/obd/PidSet;

    invoke-direct {v0, p1}, Lcom/navdy/obd/PidSet;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanningPids:Lcom/navdy/obd/PidSet;

    .line 133
    sget-object v0, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting to scan for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pids"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect;->pidListener:Lcom/navdy/obd/IPidListener;

    invoke-virtual {v0, p1, v1}, Lcom/navdy/obd/ObdService;->addListener(Ljava/util/List;Lcom/navdy/obd/IPidListener;)V

    .line 135
    return-void
.end method


# virtual methods
.method public getReadings(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    sget-object v1, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/navdy/obd/jobs/AutoConnect;->pidsChanged(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-direct {p0, p1}, Lcom/navdy/obd/jobs/AutoConnect;->updateScan(Ljava/util/List;)V

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect;->carState:Lcom/navdy/obd/CarState;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->carState:Lcom/navdy/obd/CarState;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/CarState;->getReadings(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getState()Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    return-object v0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 165
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    sget-object v3, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->STOPPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    if-ne v2, v3, :cond_0

    .line 228
    :goto_0
    return-void

    .line 168
    :cond_0
    const/4 v1, 0x0

    .line 169
    .local v1, "info":Lcom/navdy/obd/io/ChannelInfo;
    const/4 v0, 0x0

    .line 171
    .local v0, "channels":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/obd/io/ChannelInfo;>;"
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$4;->$SwitchMap$com$navdy$obd$jobs$AutoConnect$AutoConnectState:[I

    iget-object v3, p0, Lcom/navdy/obd/jobs/AutoConnect;->autoConnectState:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-virtual {v3}, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 220
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 221
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AutoConnect: found channel "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/navdy/obd/io/ChannelInfo;->asString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 222
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->CONNECTING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v2}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    .line 223
    iput-object v5, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    .line 224
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2, v1}, Lcom/navdy/obd/ObdService;->openChannel(Lcom/navdy/obd/io/ChannelInfo;)V

    .line 227
    :cond_2
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {p0}, Lcom/navdy/obd/jobs/AutoConnect;->getDelay()J

    move-result-wide v4

    invoke-virtual {v2, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 173
    :pswitch_0
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v3, "AutoConnect: Checking for default channel"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 174
    iput-object v5, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanningPids:Lcom/navdy/obd/PidSet;

    .line 175
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdService;->getDefaultChannelInfo()Lcom/navdy/obd/io/ChannelInfo;

    move-result-object v1

    .line 176
    if-nez v1, :cond_3

    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->possibleChannels:Ljava/util/Queue;

    if-eqz v2, :cond_3

    .line 177
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->possibleChannels:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    check-cast v1, Lcom/navdy/obd/io/ChannelInfo;

    .line 178
    .restart local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    if-eqz v1, :cond_3

    .line 179
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v3, "Using next channel from scan"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 182
    :cond_3
    if-nez v1, :cond_1

    .line 183
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v3, "No default found and exhausted previous scan - sleeping"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 184
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->SLEEPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v2}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    goto :goto_1

    .line 189
    :pswitch_1
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->INITIAL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v2}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    .line 190
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    if-nez v2, :cond_4

    .line 191
    new-instance v2, Lcom/navdy/obd/discovery/GroupScanner;

    iget-object v3, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-direct {v2, v3, v5}, Lcom/navdy/obd/discovery/GroupScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    iput-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    .line 193
    :cond_4
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->startScan()Z

    goto/16 :goto_1

    .line 196
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->getDiscoveredChannels()Ljava/util/Set;

    move-result-object v0

    .line 197
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 198
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v3, "AutoConnect: No channels found after initial scan, waiting for FULL_SCAN"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 200
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->FULL_SCAN:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v2}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    goto/16 :goto_1

    .line 202
    :cond_5
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->stopScan()Z

    .line 203
    invoke-direct {p0, v0}, Lcom/navdy/obd/jobs/AutoConnect;->prioritizeChannels(Ljava/util/Set;)Ljava/util/LinkedList;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->possibleChannels:Ljava/util/Queue;

    .line 204
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->possibleChannels:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    check-cast v1, Lcom/navdy/obd/io/ChannelInfo;

    .line 206
    .restart local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    goto/16 :goto_1

    .line 208
    :pswitch_3
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->getDiscoveredChannels()Ljava/util/Set;

    move-result-object v0

    .line 209
    iget-object v2, p0, Lcom/navdy/obd/jobs/AutoConnect;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->stopScan()Z

    .line 210
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 211
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect;->Log:Lorg/slf4j/Logger;

    const-string v3, "AutoConnect: No channels found after full scan, restarting scan process"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 213
    sget-object v2, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->DISCONNECTED:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v2}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    goto/16 :goto_1

    .line 215
    :cond_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    check-cast v1, Lcom/navdy/obd/io/ChannelInfo;

    .line 217
    .restart local v1    # "info":Lcom/navdy/obd/io/ChannelInfo;
    goto/16 :goto_1

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public start()V
    .locals 2

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->started:Z

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->started:Z

    .line 106
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService;->addListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 107
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 109
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->started:Z

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->started:Z

    .line 114
    sget-object v0, Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;->STOPPING:Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;

    invoke-direct {p0, v0}, Lcom/navdy/obd/jobs/AutoConnect;->setState(Lcom/navdy/obd/jobs/AutoConnect$AutoConnectState;)V

    .line 115
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    iget-object v1, p0, Lcom/navdy/obd/jobs/AutoConnect;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    invoke-virtual {v0, v1}, Lcom/navdy/obd/ObdService;->removeListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 117
    :cond_0
    return-void
.end method

.method public triggerReconnect()V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 294
    iget-object v0, p0, Lcom/navdy/obd/jobs/AutoConnect;->service:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 295
    return-void
.end method
