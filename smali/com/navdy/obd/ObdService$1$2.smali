.class Lcom/navdy/obd/ObdService$1$2;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdScanJob$IRawData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService$1;->startRawScan()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/obd/ObdService$1;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/obd/ObdService$1;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/navdy/obd/ObdService$1$2;->this$1:Lcom/navdy/obd/ObdService$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRawData(Ljava/lang/String;)V
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 197
    iget-object v2, p0, Lcom/navdy/obd/ObdService$1$2;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v2, v2, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v3

    monitor-enter v3

    .line 198
    :try_start_9
    iget-object v2, p0, Lcom/navdy/obd/ObdService$1$2;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v2, v2, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_33

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/IObdServiceListener;
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_30

    .line 200
    .local v1, "listener":Lcom/navdy/obd/IObdServiceListener;
    :try_start_21
    invoke-interface {v1, p1}, Lcom/navdy/obd/IObdServiceListener;->onRawData(Ljava/lang/String;)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_24} :catch_25
    .catchall {:try_start_21 .. :try_end_24} :catchall_30

    goto :goto_15

    .line 201
    :catch_25
    move-exception v0

    .line 202
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_26
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error notifying listener"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_15

    .line 205
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "listener":Lcom/navdy/obd/IObdServiceListener;
    :catchall_30
    move-exception v2

    monitor-exit v3
    :try_end_32
    .catchall {:try_start_26 .. :try_end_32} :catchall_30

    throw v2

    :cond_33
    :try_start_33
    monitor-exit v3
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_30

    .line 206
    return-void
.end method
