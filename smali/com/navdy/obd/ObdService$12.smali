.class Lcom/navdy/obd/ObdService$12;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->scanSupportedPids()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 1287
    iput-object p1, p0, Lcom/navdy/obd/ObdService$12;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1289
    iget-object v3, p0, Lcom/navdy/obd/ObdService$12;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/obd/VehicleInfo;->getPrimaryEcu()Lcom/navdy/obd/ECU;

    move-result-object v3

    iget-object v3, v3, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v3}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v2

    .line 1290
    .local v2, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    iget-object v3, p0, Lcom/navdy/obd/ObdService$12;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v4

    monitor-enter v4

    .line 1291
    :try_start_17
    iget-object v3, p0, Lcom/navdy/obd/ObdService$12;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/IObdServiceListener;
    :try_end_2d
    .catchall {:try_start_17 .. :try_end_2d} :catchall_3c

    .line 1293
    .local v1, "listener":Lcom/navdy/obd/IObdServiceListener;
    :try_start_2d
    invoke-interface {v1, v2}, Lcom/navdy/obd/IObdServiceListener;->supportedPids(Ljava/util/List;)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_30} :catch_31
    .catchall {:try_start_2d .. :try_end_30} :catchall_3c

    goto :goto_21

    .line 1294
    :catch_31
    move-exception v0

    .line 1295
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_32
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error notifying listener"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21

    .line 1298
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "listener":Lcom/navdy/obd/IObdServiceListener;
    :catchall_3c
    move-exception v3

    monitor-exit v4
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_3c

    throw v3

    :cond_3f
    :try_start_3f
    monitor-exit v4
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3c

    .line 1299
    return-void
.end method
