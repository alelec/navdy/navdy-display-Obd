.class Lcom/navdy/obd/ObdService$8;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->sleepWithVoltageWakeUpTriggers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/navdy/obd/ObdService$8;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 2
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 1096
    if-eqz p2, :cond_b

    .line 1097
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Successfully able to put the Obd chip to sleep"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    :cond_b
    return-void
.end method
