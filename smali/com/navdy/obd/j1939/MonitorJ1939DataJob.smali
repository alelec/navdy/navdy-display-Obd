.class public Lcom/navdy/obd/j1939/MonitorJ1939DataJob;
.super Ljava/lang/Object;
.source "MonitorJ1939DataJob.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;
    }
.end annotation


# static fields
.field private static final ADD_PGN_FILTER_COMMAND:Ljava/lang/String; = "STFPGA %s"

.field private static final CARRIAGE_RETURN:I = 0xd

.field public static final CLEAR_ALL_PASS_FILTERS:Lcom/navdy/obd/command/ObdCommand;

.field private static CLEAR_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand; = null

.field public static final HEADER_SIZE:I = 0x4

.field private static final MONITORING_COMMAND:Ljava/lang/String; = "STM"

.field public static final TAG:Ljava/lang/String;

.field public static final TIMEOUT_MILLIS:I

.field private static TURN_OFF_SILENT_MONITORING_COMMAND:Lcom/navdy/obd/command/ObdCommand; = null

.field private static TURN_ON_HEADERS:Lcom/navdy/obd/command/ObdCommand; = null

.field private static final VALID_PGN_FILTER_REGEX:Ljava/lang/String; = "[0-9A-Fa-f]{1,6}"


# instance fields
.field private channel:Lcom/navdy/obd/io/IChannel;

.field private commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;

.field private dataObserver:Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;

.field private obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

.field private parameterGroupLookup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/j1939/Parameter;",
            ">;>;"
        }
    .end annotation
.end field

.field private parameterGroupsToMonitor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    const-class v0, Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TIMEOUT_MILLIS:I

    .line 36
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "TurnOffSilentMonitiroing"

    const-string v2, "ATCSM0"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TURN_OFF_SILENT_MONITORING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 37
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "Clear PGN filters"

    const-string v2, "STFPGC"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->CLEAR_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 38
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STFCA"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->CLEAR_ALL_PASS_FILTERS:Lcom/navdy/obd/command/ObdCommand;

    .line 39
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "Turn on Headers"

    const-string v2, "ATH1"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TURN_ON_HEADERS:Lcom/navdy/obd/command/ObdCommand;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/io/IChannel;Ljava/util/List;Lcom/navdy/obd/ObdDataObserver;Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;)V
    .locals 6
    .param p1, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p3, "logger"    # Lcom/navdy/obd/ObdDataObserver;
    .param p4, "dataObserver"    # Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/obd/io/IChannel;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/j1939/Parameter;",
            ">;",
            "Lcom/navdy/obd/ObdDataObserver;",
            "Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "parametersList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->channel:Lcom/navdy/obd/io/IChannel;

    .line 54
    iput-object p3, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    .line 55
    iput-object p4, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->dataObserver:Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;

    .line 56
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupLookup:Ljava/util/HashMap;

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupsToMonitor:Ljava/util/ArrayList;

    .line 59
    :try_start_0
    new-instance v3, Lcom/navdy/obd/io/ObdCommandInterpreter;

    invoke-interface {p1}, Lcom/navdy/obd/io/IChannel;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {p1}, Lcom/navdy/obd/io/IChannel;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/navdy/obd/io/ObdCommandInterpreter;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iput-object v3, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    iget-object v3, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;

    sget v4, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TIMEOUT_MILLIS:I

    invoke-virtual {v3, v4}, Lcom/navdy/obd/io/ObdCommandInterpreter;->setReadTimeOut(I)V

    .line 65
    if-eqz p2, :cond_1

    .line 66
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/j1939/Parameter;

    .line 67
    .local v1, "parameter":Lcom/navdy/obd/j1939/Parameter;
    iget-object v4, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupLookup:Ljava/util/HashMap;

    iget v5, v1, Lcom/navdy/obd/j1939/Parameter;->pgn:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 68
    .local v2, "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    if-nez v2, :cond_0

    .line 69
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .restart local v2    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    iget-object v4, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupLookup:Ljava/util/HashMap;

    iget v5, v1, Lcom/navdy/obd/j1939/Parameter;->pgn:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v4, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupsToMonitor:Ljava/util/ArrayList;

    iget v5, v1, Lcom/navdy/obd/j1939/Parameter;->pgn:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    .end local v1    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .end local v2    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v4, "Error accessing streams from channel "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->startMonitoring()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;)Lcom/navdy/obd/ObdDataObserver;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    return-object v0
.end method

.method private startMonitoring()V
    .locals 15

    .prologue
    const/4 v10, 0x1

    .line 129
    sget-object v11, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v12, "startMonitoring "

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :try_start_0
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;

    const-string v12, "STM"

    invoke-virtual {v11, v12}, Lcom/navdy/obd/io/ObdCommandInterpreter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :goto_0
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    const-string v12, "STM"

    invoke-virtual {v11, v12}, Lcom/navdy/obd/ObdDataObserver;->onCommand(Ljava/lang/String;)V

    .line 136
    sget-object v11, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v12, "STM"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    :goto_1
    :try_start_1
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;

    invoke-virtual {v11}, Lcom/navdy/obd/io/ObdCommandInterpreter;->readLine()Ljava/lang/String;

    move-result-object v8

    .line 140
    .local v8, "response":Ljava/lang/String;
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-virtual {v11, v8}, Lcom/navdy/obd/ObdDataObserver;->onResponse(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    new-array v0, v11, [B

    .line 142
    .local v0, "bytes":[B
    const/4 v11, 0x0

    array-length v12, v0

    invoke-static {v8, v0, v11, v12}, Lcom/navdy/obd/util/HexUtil;->parseHexString(Ljava/lang/String;[BII)V

    .line 143
    const/4 v11, 0x0

    aget-byte v3, v0, v11

    .line 144
    .local v3, "firstByte":B
    const/4 v11, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x1

    invoke-static {v0, v11, v12, v13}, Lcom/navdy/obd/Utility;->bytesToInt([BIIZ)I

    move-result v7

    .line 145
    .local v7, "pgn":I
    const/4 v11, 0x3

    aget-byte v9, v0, v11

    .line 146
    .local v9, "src":I
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupLookup:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 147
    .local v6, "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    if-eqz v6, :cond_0

    .line 148
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/obd/j1939/Parameter;

    .line 149
    .local v4, "parameter":Lcom/navdy/obd/j1939/Parameter;
    iget v12, v4, Lcom/navdy/obd/j1939/Parameter;->lengthInBits:I

    const/16 v13, 0x8

    if-lt v12, v13, :cond_2

    iget v12, v4, Lcom/navdy/obd/j1939/Parameter;->lengthInBits:I

    div-int/lit8 v1, v12, 0x8

    .line 150
    .local v1, "dataLength":I
    :goto_3
    invoke-virtual {v4}, Lcom/navdy/obd/j1939/Parameter;->getRecentData()[B

    move-result-object v12

    if-nez v12, :cond_1

    .line 151
    new-array v5, v1, [B

    .line 152
    .local v5, "parameterData":[B
    invoke-virtual {v4, v5}, Lcom/navdy/obd/j1939/Parameter;->setRecentData([B)V

    .line 154
    .end local v5    # "parameterData":[B
    :cond_1
    iget v12, v4, Lcom/navdy/obd/j1939/Parameter;->byteOffset:I

    add-int/lit8 v12, v12, 0x4

    invoke-virtual {v4}, Lcom/navdy/obd/j1939/Parameter;->getRecentData()[B

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v0, v12, v13, v14, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 160
    .end local v0    # "bytes":[B
    .end local v1    # "dataLength":I
    .end local v3    # "firstByte":B
    .end local v4    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .end local v6    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    .end local v7    # "pgn":I
    .end local v8    # "response":Ljava/lang/String;
    .end local v9    # "src":I
    :catch_0
    move-exception v2

    .line 161
    .local v2, "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v11, "Error while monitoring J1939 "

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 162
    iget-object v10, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException while monitoring "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/obd/ObdDataObserver;->onError(Ljava/lang/String;)V

    .line 167
    .end local v2    # "e":Ljava/io/IOException;
    :goto_4
    return-void

    .line 132
    :catch_1
    move-exception v2

    .line 133
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v11, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v12, "IOException when writing the command"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "bytes":[B
    .restart local v3    # "firstByte":B
    .restart local v4    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .restart local v6    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    .restart local v7    # "pgn":I
    .restart local v8    # "response":Ljava/lang/String;
    .restart local v9    # "src":I
    :cond_2
    move v1, v10

    .line 149
    goto :goto_3

    .line 156
    .end local v4    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    :cond_3
    :try_start_2
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->dataObserver:Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;

    if-eqz v11, :cond_0

    .line 157
    iget-object v11, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->dataObserver:Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;

    invoke-interface {v11, v7, v6}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;->onParameterGroupData(ILjava/util/List;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 164
    .end local v0    # "bytes":[B
    .end local v3    # "firstByte":B
    .end local v6    # "parameterGroup":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    .end local v7    # "pgn":I
    .end local v8    # "response":Ljava/lang/String;
    .end local v9    # "src":I
    :catch_2
    move-exception v2

    .line 165
    .local v2, "e":Ljava/lang/InterruptedException;
    sget-object v10, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v11, "Monitoring interrupted "

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 166
    iget-object v10, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    const-string v11, "Monitoring was interrupted, finishing execution"

    invoke-virtual {v10, v11}, Lcom/navdy/obd/ObdDataObserver;->onError(Ljava/lang/String;)V

    goto :goto_4
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 81
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->commandInterpreter:Lcom/navdy/obd/io/ObdCommandInterpreter;

    if-nez v2, :cond_0

    .line 82
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v3, "Command interpreter is not initialized properly"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_0
    return-void

    .line 86
    :cond_0
    :try_start_0
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v3, "Starting to monitor the J1939 data on the CAN bus"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupsToMonitor:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 88
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v3, "No parameters to be monitored, not monitoring"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v7

    .line 123
    .local v7, "exception":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception while adding filters "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 93
    .end local v7    # "exception":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    new-instance v1, Lcom/navdy/obd/command/BatchCommand;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/navdy/obd/command/ICommand;

    invoke-direct {v1, v2}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 94
    .local v1, "setupMonitoringCommand":Lcom/navdy/obd/command/BatchCommand;
    new-instance v2, Lcom/navdy/obd/command/ObdCommand;

    const-string v3, "DUMMY"

    const-string v4, "\n"

    invoke-direct {v2, v3, v4}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 95
    new-instance v2, Lcom/navdy/obd/command/ObdCommand;

    const-string v3, "STM\r"

    invoke-direct {v2, v3}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 96
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TURN_OFF_SILENT_MONITORING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 97
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->CLEAR_ALL_PASS_FILTERS:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 98
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->parameterGroupsToMonitor:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 99
    .local v9, "parameter":Ljava/lang/Integer;
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 100
    .local v10, "pgnHex":Ljava/lang/String;
    new-instance v8, Lcom/navdy/obd/command/ObdCommand;

    const-string v3, "STFPGA %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v8, v3}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 101
    .local v8, "filterCommand":Lcom/navdy/obd/command/ObdCommand;
    sget-object v3, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Adding PGN filter with command "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/navdy/obd/command/ObdCommand;->getCommand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {v1, v8}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    goto :goto_1

    .line 104
    .end local v8    # "filterCommand":Lcom/navdy/obd/command/ObdCommand;
    .end local v9    # "parameter":Ljava/lang/Integer;
    .end local v10    # "pgnHex":Ljava/lang/String;
    :cond_2
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TURN_ON_HEADERS:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v1, v2}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 105
    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->channel:Lcom/navdy/obd/io/IChannel;

    sget-object v3, Lcom/navdy/obd/Protocol;->SAE_J1939_CAN_PROTOCOL:Lcom/navdy/obd/Protocol;

    new-instance v4, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;

    invoke-direct {v4, p0, v1}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;-><init>(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;Lcom/navdy/obd/command/BatchCommand;)V

    iget-object v5, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;Z)V

    .line 121
    .local v0, "applyFiltersJob":Lcom/navdy/obd/ObdJob;
    invoke-virtual {v0}, Lcom/navdy/obd/ObdJob;->run()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
