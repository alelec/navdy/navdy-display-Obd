.class public Lcom/navdy/obd/j1939/J1939Profile;
.super Ljava/lang/Object;
.source "J1939Profile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/j1939/J1939Profile$Column;
    }
.end annotation


# static fields
.field public static final SPN_ENGINE_AVERAGE_FUEL_ECONOMY:J = 0xb9L

.field public static final SPN_ENGINE_OIL_PRESSURE:J = 0x64L

.field public static final SPN_ENGINE_TRIP_FUEL:J = 0x13bdL

.field public static final SPN_TOTAL_DISTANCE_TRAVELLED:J = 0x395L

.field public static final TAG:Ljava/lang/String;

.field public static obdPidToJ1939ParameterMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/j1939/Parameter;",
            ">;>;"
        }
    .end annotation
.end field

.field public static parameterToCustomPidMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    const-class v0, Lcom/navdy/obd/j1939/J1939Profile;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/obd/j1939/J1939Profile;->obdPidToJ1939ParameterMapping:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    .line 48
    sget-object v0, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    const-wide/16 v2, 0x64

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0x102

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    const-wide/16 v2, 0xb9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    const-wide/16 v2, 0x13bd

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0x103

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    const-wide/16 v2, 0x395

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0x104

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getParameters(I)Ljava/util/List;
    .locals 2
    .param p1, "pid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/j1939/Parameter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lcom/navdy/obd/j1939/J1939Profile;->obdPidToJ1939ParameterMapping:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public load(Ljava/io/InputStream;)V
    .locals 28
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 59
    const/16 v21, 0x0

    .line 61
    .local v21, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v22, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    .end local v21    # "reader":Ljava/io/BufferedReader;
    .local v22, "reader":Ljava/io/BufferedReader;
    :try_start_1
    sget-object v10, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/commons/csv/CSVFormat;->withHeader([Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v10

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Lorg/apache/commons/csv/CSVFormat;->parse(Ljava/io/Reader;)Lorg/apache/commons/csv/CSVParser;

    move-result-object v24

    .line 63
    .local v24, "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    invoke-interface/range {v24 .. v24}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_0
    :goto_0
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/commons/csv/CSVRecord;

    .line 64
    .local v23, "record":Lorg/apache/commons/csv/CSVRecord;
    const-string v10, "Name"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 65
    .local v3, "name":Ljava/lang/String;
    const-string v10, "SPN"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 66
    .local v4, "spn":J
    const-string v10, "PGN"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 67
    .local v6, "pgn":I
    const-string v10, "ByteOffset"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 68
    .local v7, "byteOffset":I
    const-string v10, "BitOffset"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 69
    .local v8, "bitOffset":I
    const-string v10, "BitLength"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 70
    .local v9, "bitLength":I
    const-string v10, "ValueOffset"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v25

    .line 71
    .local v25, "valueOffset":I
    const-string v10, "Resolution"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 72
    .local v12, "resolution":D
    const-string v10, "MinValue"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 73
    .local v14, "minValue":D
    const-string v10, "MaxValue"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    .line 74
    .local v16, "maxValue":D
    const-string v10, "OBD_Pid"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/commons/csv/CSVRecord;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 75
    .local v19, "obd2Pid":I
    new-instance v2, Lcom/navdy/obd/j1939/Parameter;

    move/from16 v0, v25

    int-to-long v10, v0

    invoke-direct/range {v2 .. v17}, Lcom/navdy/obd/j1939/Parameter;-><init>(Ljava/lang/String;JIIIIJDDD)V

    .line 76
    .local v2, "parameter":Lcom/navdy/obd/j1939/Parameter;
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Parameter "

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v27, ", PID : "

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 v10, -0x1

    move/from16 v0, v19

    if-ne v0, v10, :cond_1

    .line 78
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "PID is not defined for parameter :"

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 80
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->parameterToCustomPidMapping:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 81
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Custom PID found "

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_1
    :goto_1
    const/4 v10, -0x1

    move/from16 v0, v19

    if-eq v0, v10, :cond_0

    .line 87
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->obdPidToJ1939ParameterMapping:Ljava/util/HashMap;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/List;

    .line 88
    .local v20, "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    if-nez v20, :cond_2

    .line 89
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .restart local v20    # "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->obdPidToJ1939ParameterMapping:Ljava/util/HashMap;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v10, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    :cond_2
    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    .line 95
    .end local v2    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "spn":J
    .end local v6    # "pgn":I
    .end local v7    # "byteOffset":I
    .end local v8    # "bitOffset":I
    .end local v9    # "bitLength":I
    .end local v12    # "resolution":D
    .end local v14    # "minValue":D
    .end local v16    # "maxValue":D
    .end local v19    # "obd2Pid":I
    .end local v20    # "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    .end local v23    # "record":Lorg/apache/commons/csv/CSVRecord;
    .end local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    .end local v25    # "valueOffset":I
    :catch_0
    move-exception v18

    move-object/from16 v21, v22

    .line 96
    .end local v22    # "reader":Ljava/io/BufferedReader;
    .local v18, "e":Ljava/lang/RuntimeException;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_2
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "RunTimeException during loading the profile"

    move-object/from16 v0, v18

    invoke-static {v10, v11, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    if-eqz v21, :cond_3

    .line 102
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 108
    .end local v18    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :goto_3
    return-void

    .line 83
    .end local v21    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v4    # "spn":J
    .restart local v6    # "pgn":I
    .restart local v7    # "byteOffset":I
    .restart local v8    # "bitOffset":I
    .restart local v9    # "bitLength":I
    .restart local v12    # "resolution":D
    .restart local v14    # "minValue":D
    .restart local v16    # "maxValue":D
    .restart local v19    # "obd2Pid":I
    .restart local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v23    # "record":Lorg/apache/commons/csv/CSVRecord;
    .restart local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    .restart local v25    # "valueOffset":I
    :cond_4
    :try_start_4
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "No custom PID found either"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 97
    .end local v2    # "parameter":Lcom/navdy/obd/j1939/Parameter;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "spn":J
    .end local v6    # "pgn":I
    .end local v7    # "byteOffset":I
    .end local v8    # "bitOffset":I
    .end local v9    # "bitLength":I
    .end local v12    # "resolution":D
    .end local v14    # "minValue":D
    .end local v16    # "maxValue":D
    .end local v19    # "obd2Pid":I
    .end local v23    # "record":Lorg/apache/commons/csv/CSVRecord;
    .end local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    .end local v25    # "valueOffset":I
    :catch_1
    move-exception v18

    move-object/from16 v21, v22

    .line 98
    .end local v22    # "reader":Ljava/io/BufferedReader;
    .local v18, "e":Ljava/io/IOException;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    :goto_4
    :try_start_5
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "IOException during loading the profile"

    move-object/from16 v0, v18

    invoke-static {v10, v11, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 100
    if-eqz v21, :cond_3

    .line 102
    :try_start_6
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    .line 103
    :catch_2
    move-exception v18

    .line 104
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "IOException closing the reader"

    move-object/from16 v0, v18

    invoke-static {v10, v11, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 100
    .end local v18    # "e":Ljava/io/IOException;
    .end local v21    # "reader":Ljava/io/BufferedReader;
    .restart local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :cond_5
    if-eqz v22, :cond_7

    .line 102
    :try_start_7
    invoke-virtual/range {v22 .. v22}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    move-object/from16 v21, v22

    .line 105
    .end local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 103
    .end local v21    # "reader":Ljava/io/BufferedReader;
    .restart local v22    # "reader":Ljava/io/BufferedReader;
    :catch_3
    move-exception v18

    .line 104
    .restart local v18    # "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "IOException closing the reader"

    move-object/from16 v0, v18

    invoke-static {v10, v11, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v21, v22

    .line 105
    .end local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 103
    .end local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    .local v18, "e":Ljava/lang/RuntimeException;
    :catch_4
    move-exception v18

    .line 104
    .local v18, "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v11, "IOException closing the reader"

    move-object/from16 v0, v18

    invoke-static {v10, v11, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 100
    .end local v18    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    :goto_5
    if-eqz v21, :cond_6

    .line 102
    :try_start_8
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 105
    :cond_6
    :goto_6
    throw v10

    .line 103
    :catch_5
    move-exception v18

    .line 104
    .restart local v18    # "e":Ljava/io/IOException;
    sget-object v11, Lcom/navdy/obd/j1939/J1939Profile;->TAG:Ljava/lang/String;

    const-string v26, "IOException closing the reader"

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-static {v11, v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 100
    .end local v18    # "e":Ljava/io/IOException;
    .end local v21    # "reader":Ljava/io/BufferedReader;
    .restart local v22    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v10

    move-object/from16 v21, v22

    .end local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 97
    :catch_6
    move-exception v18

    goto :goto_4

    .line 95
    :catch_7
    move-exception v18

    goto :goto_2

    .end local v21    # "reader":Ljava/io/BufferedReader;
    .restart local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v24    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/commons/csv/CSVRecord;>;"
    :cond_7
    move-object/from16 v21, v22

    .end local v22    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "reader":Ljava/io/BufferedReader;
    goto :goto_3
.end method
