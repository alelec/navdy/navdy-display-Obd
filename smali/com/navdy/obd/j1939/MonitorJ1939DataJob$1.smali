.class Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;
.super Ljava/lang/Object;
.source "MonitorJ1939DataJob.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

.field final synthetic val$setupMonitoringCommand:Lcom/navdy/obd/command/BatchCommand;


# direct methods
.method constructor <init>(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;Lcom/navdy/obd/command/BatchCommand;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;->this$0:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    iput-object p2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;->val$setupMonitoringCommand:Lcom/navdy/obd/command/BatchCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 6
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 108
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ApplyFilter complete , Success :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    if-eqz p2, :cond_1

    .line 110
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;->val$setupMonitoringCommand:Lcom/navdy/obd/command/BatchCommand;

    invoke-virtual {v2}, Lcom/navdy/obd/command/BatchCommand;->getCommands()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ICommand;

    .local v0, "command":Lcom/navdy/obd/command/ICommand;
    move-object v1, v0

    .line 111
    check-cast v1, Lcom/navdy/obd/command/ObdCommand;

    .line 112
    .local v1, "obdCommand":Lcom/navdy/obd/command/ObdCommand;
    sget-object v3, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Response for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getCommand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getResponse()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    .end local v0    # "command":Lcom/navdy/obd/command/ICommand;
    .end local v1    # "obdCommand":Lcom/navdy/obd/command/ObdCommand;
    :cond_0
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;->this$0:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    invoke-static {v2}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->access$000(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;)V

    .line 119
    :goto_1
    return-void

    .line 116
    :cond_1
    iget-object v2, p0, Lcom/navdy/obd/j1939/MonitorJ1939DataJob$1;->this$0:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    invoke-static {v2}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->access$100(Lcom/navdy/obd/j1939/MonitorJ1939DataJob;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v2

    const-string v3, "Failed to apply PGN filters"

    invoke-virtual {v2, v3}, Lcom/navdy/obd/ObdDataObserver;->onError(Ljava/lang/String;)V

    .line 117
    sget-object v2, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->TAG:Ljava/lang/String;

    const-string v3, "Failed to apply the PGN filters, not initiating the monitoring"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
