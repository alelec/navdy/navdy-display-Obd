.class public Lcom/navdy/obd/j1939/J1939ObdJobAdapter;
.super Ljava/lang/Object;
.source "J1939ObdJobAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;
    }
.end annotation


# static fields
.field public static J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private channel:Lcom/navdy/obd/io/IChannel;

.field private listener:Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;

.field private monitorJ1939DataJob:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

.field private monitoredPidsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field private obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

.field public parameterToPidMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field private profile:Lcom/navdy/obd/j1939/J1939Profile;

.field private started:Z

.field private vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-class v0, Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->TAG:Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/navdy/obd/PidSet;

    invoke-direct {v0}, Lcom/navdy/obd/PidSet;-><init>()V

    sput-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    .line 71
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 72
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 73
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 74
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 75
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 76
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 77
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x102

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 78
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x103

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 79
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->J1939SupportedObdPids:Lcom/navdy/obd/PidSet;

    const/16 v1, 0x104

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/j1939/J1939Profile;Ljava/util/List;Lcom/navdy/obd/VehicleStateManager;Lcom/navdy/obd/ObdDataObserver;)V
    .locals 10
    .param p1, "channel"    # Lcom/navdy/obd/io/IChannel;
    .param p2, "profile"    # Lcom/navdy/obd/j1939/J1939Profile;
    .param p4, "vehicleStateManager"    # Lcom/navdy/obd/VehicleStateManager;
    .param p5, "logger"    # Lcom/navdy/obd/ObdDataObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/obd/io/IChannel;",
            "Lcom/navdy/obd/j1939/J1939Profile;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Lcom/navdy/obd/VehicleStateManager;",
            "Lcom/navdy/obd/ObdDataObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p3, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->started:Z

    .line 83
    iput-object p5, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    .line 84
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->parameterToPidMapping:Ljava/util/HashMap;

    .line 85
    iput-object p3, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->monitoredPidsList:Ljava/util/List;

    .line 86
    iput-object p4, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    .line 87
    iput-object p1, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->channel:Lcom/navdy/obd/io/IChannel;

    .line 88
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v2, "parametersToMonitor":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/obd/Pid;

    .line 90
    .local v3, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v3}, Lcom/navdy/obd/Pid;->getId()I

    move-result v5

    invoke-virtual {p2, v5}, Lcom/navdy/obd/j1939/J1939Profile;->getParameters(I)Ljava/util/List;

    move-result-object v1

    .line 91
    .local v1, "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    if-eqz v1, :cond_0

    .line 92
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 93
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/j1939/Parameter;

    .line 94
    .local v0, "param":Lcom/navdy/obd/j1939/Parameter;
    iget-object v6, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->parameterToPidMapping:Ljava/util/HashMap;

    iget-wide v8, v0, Lcom/navdy/obd/j1939/Parameter;->spn:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 98
    .end local v0    # "param":Lcom/navdy/obd/j1939/Parameter;
    .end local v1    # "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    .end local v3    # "pid":Lcom/navdy/obd/Pid;
    :cond_1
    new-instance v4, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    iget-object v5, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->obdDataObserver:Lcom/navdy/obd/ObdDataObserver;

    invoke-direct {v4, p1, v2, v5, p0}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;-><init>(Lcom/navdy/obd/io/IChannel;Ljava/util/List;Lcom/navdy/obd/ObdDataObserver;Lcom/navdy/obd/j1939/MonitorJ1939DataJob$IDataObserver;)V

    iput-object v4, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->monitorJ1939DataJob:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    .line 99
    return-void
.end method


# virtual methods
.method public getMonitoredPidsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->monitoredPidsList:Ljava/util/List;

    return-object v0
.end method

.method public onParameterGroupData(ILjava/util/List;)V
    .locals 14
    .param p1, "pgn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/j1939/Parameter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "parameters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/j1939/Parameter;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/j1939/Parameter;

    .line 39
    .local v2, "params":Lcom/navdy/obd/j1939/Parameter;
    iget-object v9, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->parameterToPidMapping:Ljava/util/HashMap;

    iget-wide v10, v2, Lcom/navdy/obd/j1939/Parameter;->spn:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/obd/Pid;

    .line 40
    .local v3, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v2}, Lcom/navdy/obd/j1939/Parameter;->getRecentData()[B

    move-result-object v0

    .line 41
    .local v0, "data":[B
    if-eqz v0, :cond_0

    .line 42
    iget v9, v2, Lcom/navdy/obd/j1939/Parameter;->lengthInBits:I

    const/16 v10, 0x8

    if-lt v9, v10, :cond_1

    iget v9, v2, Lcom/navdy/obd/j1939/Parameter;->lengthInBits:I

    div-int/lit8 v1, v9, 0x8

    .line 43
    .local v1, "dataLength":I
    :goto_1
    const/4 v9, 0x1

    if-lt v1, v9, :cond_0

    .line 44
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v0, v9, v1, v10}, Lcom/navdy/obd/Utility;->bytesToInt([BIIZ)I

    move-result v9

    int-to-double v4, v9

    .line 45
    .local v4, "rawData":D
    iget-wide v10, v2, Lcom/navdy/obd/j1939/Parameter;->resolution:D

    mul-double/2addr v10, v4

    iget-wide v12, v2, Lcom/navdy/obd/j1939/Parameter;->valueOffset:J

    long-to-double v12, v12

    add-double v6, v10, v12

    .line 46
    .local v6, "value":D
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 47
    invoke-virtual {v3, v6, v7}, Lcom/navdy/obd/Pid;->setValue(D)V

    goto :goto_0

    .line 42
    .end local v1    # "dataLength":I
    .end local v4    # "rawData":D
    .end local v6    # "value":D
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 51
    .end local v0    # "data":[B
    .end local v2    # "params":Lcom/navdy/obd/j1939/Parameter;
    .end local v3    # "pid":Lcom/navdy/obd/Pid;
    :cond_2
    iget-boolean v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->started:Z

    if-nez v8, :cond_3

    .line 52
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->started:Z

    .line 53
    iget-object v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    iget-object v9, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->monitoredPidsList:Ljava/util/List;

    invoke-virtual {v8, v9}, Lcom/navdy/obd/VehicleStateManager;->update(Ljava/util/List;)V

    .line 55
    :cond_3
    iget-object v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->vehicleStateManager:Lcom/navdy/obd/VehicleStateManager;

    invoke-virtual {v8}, Lcom/navdy/obd/VehicleStateManager;->onScanComplete()V

    .line 56
    iget-object v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->listener:Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;

    if-eqz v8, :cond_4

    .line 57
    iget-object v8, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->listener:Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;

    invoke-interface {v8}, Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;->onNewDataAvailable()V

    .line 59
    :cond_4
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->monitorJ1939DataJob:Lcom/navdy/obd/j1939/MonitorJ1939DataJob;

    invoke-virtual {v0}, Lcom/navdy/obd/j1939/MonitorJ1939DataJob;->run()V

    .line 108
    sget-object v0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->TAG:Ljava/lang/String;

    const-string v1, "Monitoring finished, disconnecting the channel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->channel:Lcom/navdy/obd/io/IChannel;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->channel:Lcom/navdy/obd/io/IChannel;

    invoke-interface {v0}, Lcom/navdy/obd/io/IChannel;->disconnect()V

    .line 112
    :cond_0
    return-void
.end method

.method public setListener(Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/obd/j1939/J1939ObdJobAdapter;->listener:Lcom/navdy/obd/j1939/J1939ObdJobAdapter$J1939ObdJobListener;

    .line 103
    return-void
.end method
