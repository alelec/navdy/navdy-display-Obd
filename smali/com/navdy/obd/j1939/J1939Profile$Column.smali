.class Lcom/navdy/obd/j1939/J1939Profile$Column;
.super Ljava/lang/Object;
.source "J1939Profile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/j1939/J1939Profile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Column"
.end annotation


# static fields
.field public static final BIT_LENGTH:Ljava/lang/String; = "BitLength"

.field public static final BIT_OFFSET:Ljava/lang/String; = "BitOffset"

.field public static final BYTE_OFFSET:Ljava/lang/String; = "ByteOffset"

.field public static final MAX_VALUE:Ljava/lang/String; = "MaxValue"

.field public static final MIN_VALUE:Ljava/lang/String; = "MinValue"

.field public static final NAME:Ljava/lang/String; = "Name"

.field public static final OBD_PID:Ljava/lang/String; = "OBD_Pid"

.field public static final PGN:Ljava/lang/String; = "PGN"

.field public static final RESOLUTION:Ljava/lang/String; = "Resolution"

.field public static final SPN:Ljava/lang/String; = "SPN"

.field public static final VALUE_OFFSET:Ljava/lang/String; = "ValueOffset"


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/j1939/J1939Profile;


# direct methods
.method constructor <init>(Lcom/navdy/obd/j1939/J1939Profile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/j1939/J1939Profile;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/navdy/obd/j1939/J1939Profile$Column;->this$0:Lcom/navdy/obd/j1939/J1939Profile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
