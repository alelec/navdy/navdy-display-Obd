.class public Lcom/navdy/obd/converters/TemperatureConversion;
.super Ljava/lang/Object;
.source "TemperatureConversion.java"

# interfaces
.implements Lcom/navdy/obd/converters/AbstractConversion;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public convert([B)D
    .locals 2
    .param p1, "rawData"    # [B

    .prologue
    .line 10
    if-eqz p1, :cond_0

    .line 11
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v0, v0, -0x28

    int-to-double v0, v0

    .line 13
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
