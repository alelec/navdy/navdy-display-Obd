.class public Lcom/navdy/obd/converters/LinearCombinationConversion;
.super Ljava/lang/Object;
.source "LinearCombinationConversion.java"

# interfaces
.implements Lcom/navdy/obd/converters/AbstractConversion;


# instance fields
.field private aBias:I

.field private aFactor:I

.field private bBias:I

.field private bFactor:I

.field private denominator:D


# direct methods
.method public constructor <init>(IIID)V
    .locals 0
    .param p1, "aFactor"    # I
    .param p2, "aBias"    # I
    .param p3, "bFactor"    # I
    .param p4, "denominator"    # D

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->aFactor:I

    .line 18
    iput p2, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->aBias:I

    .line 19
    iput p3, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->bFactor:I

    .line 20
    iput-wide p4, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->denominator:D

    .line 21
    return-void
.end method


# virtual methods
.method public convert([B)D
    .locals 6
    .param p1, "rawData"    # [B

    .prologue
    const/4 v3, 0x3

    .line 26
    if-eqz p1, :cond_0

    array-length v2, p1

    if-ge v2, v3, :cond_1

    .line 27
    :cond_0
    const-wide/16 v2, 0x0

    .line 34
    :goto_0
    return-wide v2

    .line 29
    :cond_1
    const/4 v2, 0x2

    aget-byte v2, p1, v2

    and-int/lit16 v0, v2, 0xff

    .line 30
    .local v0, "A":I
    const/4 v1, 0x0

    .line 31
    .local v1, "B":I
    iget v2, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->bFactor:I

    if-eqz v2, :cond_2

    array-length v2, p1

    if-le v2, v3, :cond_2

    .line 32
    aget-byte v2, p1, v3

    and-int/lit16 v1, v2, 0xff

    .line 34
    :cond_2
    iget v2, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->aFactor:I

    iget v3, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->aBias:I

    add-int/2addr v3, v0

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->bFactor:I

    mul-int/2addr v3, v1

    add-int/2addr v2, v3

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/navdy/obd/converters/LinearCombinationConversion;->denominator:D

    div-double/2addr v2, v4

    goto :goto_0
.end method
