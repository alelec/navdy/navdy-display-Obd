.class public Lcom/navdy/obd/converters/CustomConversion;
.super Ljava/lang/Object;
.source "CustomConversion.java"

# interfaces
.implements Lcom/navdy/obd/converters/AbstractConversion;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/converters/CustomConversion$VariableOffset;
    }
.end annotation


# instance fields
.field private expression:Lparsii/eval/Expression;

.field private offsets:[Lcom/navdy/obd/converters/CustomConversion$VariableOffset;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 11
    .param p1, "expression"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lparsii/tokenizer/ParseException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lparsii/eval/Scope;->create()Lparsii/eval/Scope;

    move-result-object v5

    .line 30
    .local v5, "scope":Lparsii/eval/Scope;
    invoke-static {p1, v5}, Lparsii/eval/Parser;->parse(Ljava/lang/String;Lparsii/eval/Scope;)Lparsii/eval/Expression;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/obd/converters/CustomConversion;->expression:Lparsii/eval/Expression;

    .line 32
    invoke-virtual {v5}, Lparsii/eval/Scope;->getLocalVariables()Ljava/util/Collection;

    move-result-object v7

    .line 33
    .local v7, "variables":Ljava/util/Collection;, "Ljava/util/Collection<Lparsii/eval/Variable;>;"
    invoke-interface {v7}, Ljava/util/Collection;->size()I

    move-result v8

    new-array v8, v8, [Lcom/navdy/obd/converters/CustomConversion$VariableOffset;

    iput-object v8, p0, Lcom/navdy/obd/converters/CustomConversion;->offsets:[Lcom/navdy/obd/converters/CustomConversion$VariableOffset;

    .line 35
    const/4 v1, 0x0

    .line 38
    .local v1, "i":I
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lparsii/eval/Variable;

    .line 39
    .local v6, "variable":Lparsii/eval/Variable;
    invoke-virtual {v6}, Lparsii/eval/Variable;->getName()Ljava/lang/String;

    move-result-object v3

    .line 41
    .local v3, "name":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/navdy/obd/converters/CustomConversion;->offset(Ljava/lang/String;)I

    move-result v4

    .line 43
    .local v4, "offset":I
    if-gez v4, :cond_0

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v0, "errors":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    sget-object v8, Lparsii/tokenizer/Position;->UNKNOWN:Lparsii/tokenizer/Position;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid variable "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 47
    invoke-virtual {v6}, Lparsii/eval/Variable;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 45
    invoke-static {v8, v9}, Lparsii/tokenizer/ParseError;->error(Lparsii/tokenizer/Position;Ljava/lang/String;)Lparsii/tokenizer/ParseError;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-static {v0}, Lparsii/tokenizer/ParseException;->create(Ljava/util/List;)Lparsii/tokenizer/ParseException;

    move-result-object v8

    throw v8

    .line 51
    .end local v0    # "errors":Ljava/util/List;, "Ljava/util/List<Lparsii/tokenizer/ParseError;>;"
    :cond_0
    iget-object v9, p0, Lcom/navdy/obd/converters/CustomConversion;->offsets:[Lcom/navdy/obd/converters/CustomConversion$VariableOffset;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    new-instance v10, Lcom/navdy/obd/converters/CustomConversion$VariableOffset;

    invoke-direct {v10, v6, v4}, Lcom/navdy/obd/converters/CustomConversion$VariableOffset;-><init>(Lparsii/eval/Variable;I)V

    aput-object v10, v9, v1

    move v1, v2

    .line 52
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 54
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "offset":I
    .end local v6    # "variable":Lparsii/eval/Variable;
    :cond_1
    return-void
.end method

.method private offset(Ljava/lang/String;)I
    .locals 7
    .param p1, "variableName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 71
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-gt v5, v6, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 72
    :cond_0
    const/4 v3, -0x1

    .line 78
    :goto_0
    return v3

    .line 74
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_2

    move v2, v3

    .line 76
    .local v2, "twoDigit":Z
    :goto_1
    if-eqz v2, :cond_3

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/lit8 v5, v5, -0x41

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v1, v5, 0x1a

    .line 77
    .local v1, "tens":I
    :goto_2
    if-eqz v2, :cond_4

    :goto_3
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v0, v3, -0x41

    .line 78
    .local v0, "digit":I
    add-int v3, v1, v0

    goto :goto_0

    .end local v0    # "digit":I
    .end local v1    # "tens":I
    .end local v2    # "twoDigit":Z
    :cond_2
    move v2, v4

    .line 75
    goto :goto_1

    .restart local v2    # "twoDigit":Z
    :cond_3
    move v1, v4

    .line 76
    goto :goto_2

    .restart local v1    # "tens":I
    :cond_4
    move v3, v4

    .line 77
    goto :goto_3
.end method


# virtual methods
.method public convert([B)D
    .locals 8
    .param p1, "rawData"    # [B

    .prologue
    .line 58
    iget-object v2, p0, Lcom/navdy/obd/converters/CustomConversion;->offsets:[Lcom/navdy/obd/converters/CustomConversion$VariableOffset;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 59
    .local v0, "var":Lcom/navdy/obd/converters/CustomConversion$VariableOffset;
    invoke-static {v0}, Lcom/navdy/obd/converters/CustomConversion$VariableOffset;->access$000(Lcom/navdy/obd/converters/CustomConversion$VariableOffset;)Lparsii/eval/Variable;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 60
    invoke-static {v0}, Lcom/navdy/obd/converters/CustomConversion$VariableOffset;->access$000(Lcom/navdy/obd/converters/CustomConversion$VariableOffset;)Lparsii/eval/Variable;

    move-result-object v4

    invoke-static {v0}, Lcom/navdy/obd/converters/CustomConversion$VariableOffset;->access$100(Lcom/navdy/obd/converters/CustomConversion$VariableOffset;)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    int-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Lparsii/eval/Variable;->setValue(D)V

    .line 58
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    .end local v0    # "var":Lcom/navdy/obd/converters/CustomConversion$VariableOffset;
    :cond_1
    iget-object v1, p0, Lcom/navdy/obd/converters/CustomConversion;->expression:Lparsii/eval/Expression;

    invoke-virtual {v1}, Lparsii/eval/Expression;->evaluate()D

    move-result-wide v2

    return-wide v2
.end method
