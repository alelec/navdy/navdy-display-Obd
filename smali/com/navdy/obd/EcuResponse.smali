.class public Lcom/navdy/obd/EcuResponse;
.super Ljava/lang/Object;
.source "EcuResponse.java"


# instance fields
.field public data:[B

.field public final ecu:I

.field private frames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field public length:I

.field public multiline:Z

.field private offset:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ecu"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    .line 38
    iput p1, p0, Lcom/navdy/obd/EcuResponse;->ecu:I

    .line 39
    iput-boolean v0, p0, Lcom/navdy/obd/EcuResponse;->multiline:Z

    .line 40
    return-void
.end method

.method public constructor <init>(IZI)V
    .locals 1
    .param p1, "ecu"    # I
    .param p2, "multiline"    # Z
    .param p3, "length"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    .line 25
    iput p1, p0, Lcom/navdy/obd/EcuResponse;->ecu:I

    .line 26
    iput p3, p0, Lcom/navdy/obd/EcuResponse;->length:I

    .line 27
    iput-boolean p2, p0, Lcom/navdy/obd/EcuResponse;->multiline:Z

    .line 28
    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 1
    .param p1, "ecu"    # I
    .param p2, "data"    # [B

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    .line 31
    iput p1, p0, Lcom/navdy/obd/EcuResponse;->ecu:I

    .line 32
    iput-object p2, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/obd/EcuResponse;->multiline:Z

    .line 34
    array-length v0, p2

    iput v0, p0, Lcom/navdy/obd/EcuResponse;->length:I

    .line 35
    return-void
.end method


# virtual methods
.method public addFrame(Ljava/lang/String;)V
    .locals 3
    .param p1, "hexString"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v1, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    if-nez v1, :cond_0

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    .line 47
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    new-array v0, v1, [B

    .line 48
    .local v0, "bytes":[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p1, v0, v1, v2}, Lcom/navdy/obd/util/HexUtil;->parseHexString(Ljava/lang/String;[BII)V

    .line 49
    iget-object v1, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 5
    .param p1, "hexString"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    if-nez v1, :cond_0

    .line 85
    iget v1, p0, Lcom/navdy/obd/EcuResponse;->length:I

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 87
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    .line 89
    .local v0, "srcLen":I
    iget-object v1, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    iget v2, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    iget v3, p0, Lcom/navdy/obd/EcuResponse;->length:I

    iget v4, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/navdy/obd/util/HexUtil;->parseHexString(Ljava/lang/String;[BII)V

    .line 90
    iget v1, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/navdy/obd/EcuResponse;->offset:I

    .line 91
    return-void
.end method

.method public flatten()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 54
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 59
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    iput-object v3, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 79
    :goto_1
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    array-length v3, v3

    iput v3, p0, Lcom/navdy/obd/EcuResponse;->length:I

    goto :goto_0

    .line 69
    :cond_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 70
    .local v2, "stream":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 71
    iget-object v3, p0, Lcom/navdy/obd/EcuResponse;->frames:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 72
    .local v0, "frame":[B
    if-nez v1, :cond_3

    .line 73
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v5, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 75
    :cond_3
    const/4 v3, 0x3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x4

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 77
    .end local v0    # "frame":[B
    :cond_4
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/obd/EcuResponse;->data:[B

    goto :goto_1
.end method
