.class public Lcom/navdy/obd/PidListener;
.super Ljava/lang/Object;
.source "PidListener.java"


# instance fields
.field private currentState:Lcom/navdy/obd/CarState;

.field public final listener:Lcom/navdy/obd/IPidListener;

.field public final monitoredPids:Lcom/navdy/obd/PidSet;

.field public final schedule:Lcom/navdy/obd/ScanSchedule;


# direct methods
.method public constructor <init>(Lcom/navdy/obd/IPidListener;Lcom/navdy/obd/ScanSchedule;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/obd/IPidListener;
    .param p2, "schedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/navdy/obd/CarState;

    invoke-direct {v0}, Lcom/navdy/obd/CarState;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/PidListener;->currentState:Lcom/navdy/obd/CarState;

    .line 19
    iput-object p1, p0, Lcom/navdy/obd/PidListener;->listener:Lcom/navdy/obd/IPidListener;

    .line 20
    iput-object p2, p0, Lcom/navdy/obd/PidListener;->schedule:Lcom/navdy/obd/ScanSchedule;

    .line 21
    new-instance v0, Lcom/navdy/obd/PidSet;

    invoke-direct {v0, p2}, Lcom/navdy/obd/PidSet;-><init>(Lcom/navdy/obd/ScanSchedule;)V

    iput-object v0, p0, Lcom/navdy/obd/PidListener;->monitoredPids:Lcom/navdy/obd/PidSet;

    .line 22
    return-void
.end method


# virtual methods
.method public recordReadings(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v0, 0x0

    .line 28
    .local v0, "changedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/Pid;

    .line 29
    .local v1, "pid":Lcom/navdy/obd/Pid;
    iget-object v3, p0, Lcom/navdy/obd/PidListener;->monitoredPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v3, v1}, Lcom/navdy/obd/PidSet;->contains(Lcom/navdy/obd/Pid;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 30
    iget-object v3, p0, Lcom/navdy/obd/PidListener;->currentState:Lcom/navdy/obd/CarState;

    invoke-virtual {v3, v1}, Lcom/navdy/obd/CarState;->recordReading(Lcom/navdy/obd/Pid;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 31
    if-nez v0, :cond_1

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "changedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .restart local v0    # "changedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    .end local v1    # "pid":Lcom/navdy/obd/Pid;
    :cond_2
    if-eqz v0, :cond_3

    .line 39
    iget-object v2, p0, Lcom/navdy/obd/PidListener;->listener:Lcom/navdy/obd/IPidListener;

    invoke-interface {v2, v0}, Lcom/navdy/obd/IPidListener;->pidsChanged(Ljava/util/List;)V

    .line 41
    :cond_3
    iget-object v2, p0, Lcom/navdy/obd/PidListener;->listener:Lcom/navdy/obd/IPidListener;

    invoke-interface {v2, p1, v0}, Lcom/navdy/obd/IPidListener;->pidsRead(Ljava/util/List;Ljava/util/List;)V

    .line 42
    return-void
.end method
