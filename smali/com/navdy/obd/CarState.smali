.class public Lcom/navdy/obd/CarState;
.super Ljava/lang/Object;
.source "CarState.java"


# instance fields
.field lastReadings:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/CarState;->lastReadings:Landroid/util/SparseArray;

    .line 17
    return-void
.end method


# virtual methods
.method public getReadings(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v2, "readings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/Pid;

    .line 52
    .local v1, "pid":Lcom/navdy/obd/Pid;
    iget-object v4, p0, Lcom/navdy/obd/CarState;->lastReadings:Landroid/util/SparseArray;

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 53
    .local v0, "lastReading":Lcom/navdy/obd/Pid;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v4

    const-wide/high16 v6, -0x3e20000000000000L    # -2.147483648E9

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_0

    .line 54
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    .end local v0    # "lastReading":Lcom/navdy/obd/Pid;
    .end local v1    # "pid":Lcom/navdy/obd/Pid;
    :cond_1
    return-object v2
.end method

.method public recordReading(Lcom/navdy/obd/Pid;)Z
    .locals 12
    .param p1, "pid"    # Lcom/navdy/obd/Pid;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 27
    invoke-virtual {p1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v1

    .line 28
    .local v1, "id":I
    invoke-virtual {p1}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v4

    .line 29
    .local v4, "newValue":D
    invoke-virtual {p1}, Lcom/navdy/obd/Pid;->getTimeStamp()J

    move-result-wide v2

    .line 30
    .local v2, "newTimeStamp":J
    const-wide/high16 v10, -0x3e20000000000000L    # -2.147483648E9

    cmpl-double v9, v4, v10

    if-nez v9, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v7

    .line 33
    :cond_1
    iget-object v9, p0, Lcom/navdy/obd/CarState;->lastReadings:Landroid/util/SparseArray;

    invoke-virtual {v9, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/Pid;

    .line 34
    .local v6, "oldValue":Lcom/navdy/obd/Pid;
    if-nez v6, :cond_2

    .line 36
    new-instance v0, Lcom/navdy/obd/Pid;

    invoke-direct {v0, v1}, Lcom/navdy/obd/Pid;-><init>(I)V

    .line 37
    .local v0, "copy":Lcom/navdy/obd/Pid;
    invoke-virtual {v0, v2, v3}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 38
    invoke-virtual {v0, v4, v5}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 39
    iget-object v7, p0, Lcom/navdy/obd/CarState;->lastReadings:Landroid/util/SparseArray;

    invoke-virtual {v7, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v7, v8

    .line 40
    goto :goto_0

    .line 41
    .end local v0    # "copy":Lcom/navdy/obd/Pid;
    :cond_2
    invoke-virtual {v6}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    cmpl-double v9, v4, v10

    if-eqz v9, :cond_0

    .line 42
    invoke-virtual {v6, v2, v3}, Lcom/navdy/obd/Pid;->setTimeStamp(J)V

    .line 43
    invoke-virtual {v6, v4, v5}, Lcom/navdy/obd/Pid;->setValue(D)V

    move v7, v8

    .line 44
    goto :goto_0
.end method

.method public recordReadings(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 21
    .local v0, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/CarState;->recordReading(Lcom/navdy/obd/Pid;)Z

    goto :goto_0

    .line 23
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_0
    return-void
.end method
