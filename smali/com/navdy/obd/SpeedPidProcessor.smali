.class public Lcom/navdy/obd/SpeedPidProcessor;
.super Lcom/navdy/obd/PidProcessor;
.source "SpeedPidProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/navdy/obd/PidProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public isSupported(Lcom/navdy/obd/PidSet;)Z
    .locals 1
    .param p1, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 9
    if-eqz p1, :cond_0

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processPidValue(Lcom/navdy/obd/PidLookupTable;)Z
    .locals 5
    .param p1, "vehicleState"    # Lcom/navdy/obd/PidLookupTable;

    .prologue
    const/16 v4, 0xd

    .line 14
    invoke-virtual {p1, v4}, Lcom/navdy/obd/PidLookupTable;->getPidValue(I)D

    move-result-wide v0

    const-wide v2, 0x406fe00000000000L    # 255.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 15
    const-wide/high16 v0, -0x3e20000000000000L    # -2.147483648E9

    invoke-virtual {p1, v4, v0, v1}, Lcom/navdy/obd/PidLookupTable;->updatePid(ID)Z

    move-result v0

    .line 17
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resolveDependencies()Lcom/navdy/obd/PidSet;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return-object v0
.end method
