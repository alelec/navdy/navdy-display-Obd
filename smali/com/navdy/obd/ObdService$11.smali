.class Lcom/navdy/obd/ObdService$11;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->scanVIN()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 1266
    iput-object p1, p0, Lcom/navdy/obd/ObdService$11;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1268
    iget-object v3, p0, Lcom/navdy/obd/ObdService$11;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v4

    monitor-enter v4

    .line 1269
    :try_start_7
    iget-object v3, p0, Lcom/navdy/obd/ObdService$11;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v3

    if-eqz v3, :cond_3f

    iget-object v3, p0, Lcom/navdy/obd/ObdService$11;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v3

    iget-object v2, v3, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    .line 1270
    .local v2, "vin":Ljava/lang/String;
    :goto_17
    iget-object v3, p0, Lcom/navdy/obd/ObdService$11;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_41

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/IObdServiceListener;
    :try_end_2d
    .catchall {:try_start_7 .. :try_end_2d} :catchall_3c

    .line 1272
    .local v1, "listener":Lcom/navdy/obd/IObdServiceListener;
    :try_start_2d
    invoke-interface {v1, v2}, Lcom/navdy/obd/IObdServiceListener;->scannedVIN(Ljava/lang/String;)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_30} :catch_31
    .catchall {:try_start_2d .. :try_end_30} :catchall_3c

    goto :goto_21

    .line 1273
    :catch_31
    move-exception v0

    .line 1274
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_32
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error notifying listener"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21

    .line 1277
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "listener":Lcom/navdy/obd/IObdServiceListener;
    .end local v2    # "vin":Ljava/lang/String;
    :catchall_3c
    move-exception v3

    monitor-exit v4
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_3c

    throw v3

    .line 1269
    :cond_3f
    const/4 v2, 0x0

    goto :goto_17

    .line 1277
    .restart local v2    # "vin":Ljava/lang/String;
    :cond_41
    :try_start_41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_41 .. :try_end_42} :catchall_3c

    .line 1278
    return-void
.end method
