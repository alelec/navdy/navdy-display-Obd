.class public Lcom/navdy/obd/command/CheckDTCCommand;
.super Lcom/navdy/obd/command/ObdCommand;
.source "CheckDTCCommand.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "0101"

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 11
    .local p1, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    const-string v0, "0101"

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 12
    if-eqz p1, :cond_14

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_14

    .line 13
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/CheckDTCCommand;->setExpectedResponses(I)V

    .line 15
    :cond_14
    return-void
.end method


# virtual methods
.method public getNumberOfTroubleCodes()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 40
    invoke-virtual {p0}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponseCount()I

    move-result v5

    .line 41
    .local v5, "responseCount":I
    const/4 v3, 0x0

    .line 42
    .local v3, "numberOfTroubleCodes":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_7
    if-ge v1, v5, :cond_55

    .line 43
    invoke-virtual {p0, v1}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v0

    .line 44
    .local v0, "ecuResponse":Lcom/navdy/obd/EcuResponse;
    if-eqz v0, :cond_52

    .line 45
    invoke-virtual {p0, v1}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v6

    iget-object v4, v6, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 46
    .local v4, "response":[B
    if-eqz v4, :cond_41

    array-length v6, v4

    const/4 v7, 0x3

    if-lt v6, v7, :cond_41

    const/4 v6, 0x0

    aget-byte v6, v4, v6

    const/16 v7, 0x41

    if-ne v6, v7, :cond_41

    aget-byte v6, v4, v10

    if-ne v6, v10, :cond_41

    .line 47
    const/4 v6, 0x2

    aget-byte v6, v4, v6

    and-int/lit16 v2, v6, 0xff

    .line 48
    .local v2, "num":I
    and-int/lit8 v6, v2, 0x7f

    add-int/2addr v3, v6

    .line 49
    sget-object v6, Lcom/navdy/obd/command/CheckDTCCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Check engine light is on , ECU {}, Number of DTCs set {}"

    iget v8, v0, Lcom/navdy/obd/EcuResponse;->ecu:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    add-int/lit8 v9, v2, -0x80

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v7, v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    .end local v2    # "num":I
    :cond_41
    sget-object v6, Lcom/navdy/obd/command/CheckDTCCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Response of CheckDTCCommand, ECU {} , Data {}"

    iget v8, v0, Lcom/navdy/obd/EcuResponse;->ecu:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v4}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v7, v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    .end local v4    # "response":[B
    :cond_52
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 54
    .end local v0    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    :cond_55
    return v3
.end method

.method public isCheckEngineLightOn()Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 21
    invoke-virtual {p0}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponseCount()I

    move-result v4

    .line 22
    .local v4, "responseCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_7
    if-ge v1, v4, :cond_56

    .line 23
    invoke-virtual {p0, v1}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v0

    .line 24
    .local v0, "ecuResponse":Lcom/navdy/obd/EcuResponse;
    if-eqz v0, :cond_53

    .line 25
    invoke-virtual {p0, v1}, Lcom/navdy/obd/command/CheckDTCCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v7

    iget-object v3, v7, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 26
    .local v3, "response":[B
    if-eqz v3, :cond_42

    array-length v7, v3

    const/4 v8, 0x3

    if-lt v7, v8, :cond_42

    aget-byte v7, v3, v6

    const/16 v8, 0x41

    if-ne v7, v8, :cond_42

    aget-byte v7, v3, v5

    if-ne v7, v5, :cond_42

    .line 27
    const/4 v7, 0x2

    aget-byte v7, v3, v7

    and-int/lit16 v2, v7, 0xff

    .line 28
    .local v2, "num":I
    and-int/lit16 v7, v2, 0x80

    if-lez v7, :cond_42

    .line 29
    sget-object v6, Lcom/navdy/obd/command/CheckDTCCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Check engine light is on , ECU {}, Number of DTCs set {}"

    iget v8, v0, Lcom/navdy/obd/EcuResponse;->ecu:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    add-int/lit8 v9, v2, -0x80

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v7, v8, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 36
    .end local v0    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .end local v2    # "num":I
    .end local v3    # "response":[B
    :goto_41
    return v5

    .line 33
    .restart local v0    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .restart local v3    # "response":[B
    :cond_42
    sget-object v7, Lcom/navdy/obd/command/CheckDTCCommand;->Log:Lorg/slf4j/Logger;

    const-string v8, "Response of CheckDTCCommand, ECU {} , Data {}"

    iget v9, v0, Lcom/navdy/obd/EcuResponse;->ecu:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    .end local v3    # "response":[B
    :cond_53
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .end local v0    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    :cond_56
    move v5, v6

    .line 36
    goto :goto_41
.end method
