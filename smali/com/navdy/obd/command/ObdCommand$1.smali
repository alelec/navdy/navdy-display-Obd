.class Lcom/navdy/obd/command/ObdCommand$1;
.super Ljava/lang/Object;
.source "ObdCommand.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/command/ObdCommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/command/ObdCommand;

.field final synthetic val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

.field final synthetic val$inputStream:Ljava/io/InputStream;

.field final synthetic val$outputStream:Ljava/io/OutputStream;

.field final synthetic val$protocol2:Lcom/navdy/obd/Protocol;


# direct methods
.method constructor <init>(Lcom/navdy/obd/command/ObdCommand;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/command/IObdDataObserver;Lcom/navdy/obd/Protocol;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    iput-object p2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$inputStream:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$outputStream:Ljava/io/OutputStream;

    iput-object p4, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    iput-object p5, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$protocol2:Lcom/navdy/obd/Protocol;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 183
    :try_start_1
    new-instance v1, Lcom/navdy/obd/io/ObdCommandInterpreter;

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$inputStream:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$outputStream:Ljava/io/OutputStream;

    invoke-direct {v1, v2, v3}, Lcom/navdy/obd/io/ObdCommandInterpreter;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 184
    .local v1, "obdCommandInterpreter":Lcom/navdy/obd/io/ObdCommandInterpreter;
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/obd/command/ObdCommand;->access$002(Lcom/navdy/obd/command/ObdCommand;[Lcom/navdy/obd/EcuResponse;)[Lcom/navdy/obd/EcuResponse;

    .line 185
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/obd/command/ObdCommand;->access$102(Lcom/navdy/obd/command/ObdCommand;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$200(Lcom/navdy/obd/command/ObdCommand;)[B

    move-result-object v2

    if-nez v2, :cond_b7

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v3}, Lcom/navdy/obd/command/ObdCommand;->access$300(Lcom/navdy/obd/command/ObdCommand;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    if-eq v2, v6, :cond_b4

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    add-int/lit8 v2, v2, 0x30

    int-to-char v2, v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    :goto_42
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->write(Ljava/lang/String;)V

    .line 188
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v1}, Lcom/navdy/obd/io/ObdCommandInterpreter;->getCommandBytes()[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/obd/command/ObdCommand;->access$202(Lcom/navdy/obd/command/ObdCommand;[B)[B

    .line 192
    :goto_56
    sget-object v3, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v4, "wrote: {}{} "

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$300(Lcom/navdy/obd/command/ObdCommand;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    if-eq v2, v6, :cond_d5

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_72
    invoke-interface {v3, v4, v5, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 193
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    sget-object v3, Lcom/navdy/obd/command/ObdCommand;->LogRaw:Lorg/slf4j/Logger;

    const-string v4, "wrote: {}{} "

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$300(Lcom/navdy/obd/command/ObdCommand;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    if-eq v2, v6, :cond_d8

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$400(Lcom/navdy/obd/command/ObdCommand;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_93
    invoke-interface {v3, v4, v5, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 194
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    if-eqz v2, :cond_aa

    .line 195
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v4}, Lcom/navdy/obd/command/ObdCommand;->access$200(Lcom/navdy/obd/command/ObdCommand;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v2, v3}, Lcom/navdy/obd/command/IObdDataObserver;->onCommand(Ljava/lang/String;)V

    .line 197
    :cond_aa
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    iget-object v3, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$protocol2:Lcom/navdy/obd/Protocol;

    iget-object v4, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    invoke-virtual {v2, v3, v1, v4}, Lcom/navdy/obd/command/ObdCommand;->processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 204
    .end local v1    # "obdCommandInterpreter":Lcom/navdy/obd/io/ObdCommandInterpreter;
    :cond_b3
    :goto_b3
    return-void

    .line 187
    .restart local v1    # "obdCommandInterpreter":Lcom/navdy/obd/io/ObdCommandInterpreter;
    :cond_b4
    const-string v2, ""

    goto :goto_42

    .line 190
    :cond_b7
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2}, Lcom/navdy/obd/command/ObdCommand;->access$200(Lcom/navdy/obd/command/ObdCommand;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->write([B)V
    :try_end_c0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c0} :catch_c1

    goto :goto_56

    .line 198
    .end local v1    # "obdCommandInterpreter":Lcom/navdy/obd/io/ObdCommandInterpreter;
    :catch_c1
    move-exception v0

    .line 199
    .local v0, "ie":Ljava/io/IOException;
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->this$0:Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v2, v0}, Lcom/navdy/obd/command/ObdCommand;->access$502(Lcom/navdy/obd/command/ObdCommand;Ljava/io/IOException;)Ljava/io/IOException;

    .line 200
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    if-eqz v2, :cond_b3

    .line 201
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand$1;->val$iObdDataObserver:Lcom/navdy/obd/command/IObdDataObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/obd/command/IObdDataObserver;->onError(Ljava/lang/String;)V

    goto :goto_b3

    .line 192
    .end local v0    # "ie":Ljava/io/IOException;
    .restart local v1    # "obdCommandInterpreter":Lcom/navdy/obd/io/ObdCommandInterpreter;
    :cond_d5
    :try_start_d5
    const-string v2, ""

    goto :goto_72

    .line 193
    :cond_d8
    const-string v2, ""
    :try_end_da
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_da} :catch_c1

    goto :goto_93
.end method
