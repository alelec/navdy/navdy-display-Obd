.class public Lcom/navdy/obd/command/ReadVoltageCommand;
.super Lcom/navdy/obd/command/ObdCommand;
.source "ReadVoltageCommand.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "atrv"

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 9
    return-void
.end method


# virtual methods
.method public getVoltage()F
    .locals 5

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/navdy/obd/command/ReadVoltageCommand;->getResponse()Ljava/lang/String;

    move-result-object v1

    .line 13
    .local v1, "response":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 14
    const-string v3, "V"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 15
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 17
    :cond_0
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 19
    .local v2, "voltage":Ljava/lang/Float;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 23
    :goto_0
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    return v3

    .line 20
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/navdy/obd/command/ReadVoltageCommand;->Log:Lorg/slf4j/Logger;

    const-string v4, "Unable to parse ({}) as float voltage"

    invoke-interface {v3, v4, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
