.class public Lcom/navdy/obd/command/BatchCommand;
.super Ljava/lang/Object;
.source "BatchCommand.java"

# interfaces
.implements Lcom/navdy/obd/command/ICommand;


# instance fields
.field protected commands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/obd/command/ICommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/ICommand;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/command/ICommand;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    .line 23
    iget-object v0, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 24
    return-void
.end method

.method public varargs constructor <init>([Lcom/navdy/obd/command/ICommand;)V
    .locals 1
    .param p1, "commands"    # [Lcom/navdy/obd/command/ICommand;

    .prologue
    .line 19
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/BatchCommand;-><init>(Ljava/util/List;)V

    .line 20
    return-void
.end method


# virtual methods
.method public add(Lcom/navdy/obd/command/ICommand;)V
    .locals 1
    .param p1, "command"    # Lcom/navdy/obd/command/ICommand;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 3
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v1, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ICommand;

    .line 47
    .local v0, "command":Lcom/navdy/obd/command/ICommand;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/navdy/obd/command/ICommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    goto :goto_0

    .line 49
    .end local v0    # "command":Lcom/navdy/obd/command/ICommand;
    :cond_0
    return-void
.end method

.method public getCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/ICommand;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDoubleValue()D
    .locals 2

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getIntValue()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "batch("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 36
    .local v1, "names":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ICommand;

    .line 37
    .local v0, "command":Lcom/navdy/obd/command/ICommand;
    invoke-interface {v0}, Lcom/navdy/obd/command/ICommand;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 40
    .end local v0    # "command":Lcom/navdy/obd/command/ICommand;
    :cond_0
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getResponse()Ljava/lang/String;
    .locals 5

    .prologue
    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .local v2, "responses":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/navdy/obd/command/BatchCommand;->commands:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/ICommand;

    .line 65
    .local v0, "command":Lcom/navdy/obd/command/ICommand;
    invoke-interface {v0}, Lcom/navdy/obd/command/ICommand;->getResponse()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "response":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 67
    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 66
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 70
    .end local v0    # "command":Lcom/navdy/obd/command/ICommand;
    .end local v1    # "response":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
