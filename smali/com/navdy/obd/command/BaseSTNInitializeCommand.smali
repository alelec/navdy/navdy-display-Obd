.class public Lcom/navdy/obd/command/BaseSTNInitializeCommand;
.super Lcom/navdy/obd/command/BatchCommand;
.source "BaseSTNInitializeCommand.java"


# instance fields
.field private deviceInfoCommand:Lcom/navdy/obd/command/ObdCommand;


# direct methods
.method public varargs constructor <init>([Lcom/navdy/obd/command/ICommand;)V
    .locals 1
    .param p1, "commands"    # [Lcom/navdy/obd/command/ICommand;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 8
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->DEVICE_INFO_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    iput-object v0, p0, Lcom/navdy/obd/command/BaseSTNInitializeCommand;->deviceInfoCommand:Lcom/navdy/obd/command/ObdCommand;

    .line 13
    iget-object v0, p0, Lcom/navdy/obd/command/BaseSTNInitializeCommand;->deviceInfoCommand:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/BaseSTNInitializeCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getConfigurationName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17
    iget-object v1, p0, Lcom/navdy/obd/command/BaseSTNInitializeCommand;->deviceInfoCommand:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v1}, Lcom/navdy/obd/command/ObdCommand;->getResponse()Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, "response":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 21
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
