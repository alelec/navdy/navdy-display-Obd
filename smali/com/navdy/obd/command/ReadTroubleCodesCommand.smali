.class public Lcom/navdy/obd/command/ReadTroubleCodesCommand;
.super Lcom/navdy/obd/command/ObdCommand;
.source "ReadTroubleCodesCommand.java"


# static fields
.field private static final TROUBLE_CATEGORY:[Ljava/lang/String;


# instance fields
.field public active:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "P"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "C"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "B"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "U"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->TROUBLE_CATEGORY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    invoke-static {v1}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->pid(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 11
    iput-boolean v1, p0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;-><init>(Ljava/util/List;Z)V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 5
    .param p2, "active"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    invoke-static {p2}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->pid(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 11
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    .line 19
    iput-boolean p2, p0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    .line 20
    const/4 v0, -0x1

    .line 21
    .local v0, "num":I
    if-eqz p1, :cond_19

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_19

    .line 22
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 24
    :cond_19
    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->setExpectedResponses(I)V

    .line 25
    sget-object v3, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->Log:Lorg/slf4j/Logger;

    const-string v4, "Reading {} DTC\'s for {} ECUs"

    iget-boolean v1, p0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    if-eqz v1, :cond_30

    const-string v1, "active"

    move-object v2, v1

    :goto_27
    const/4 v1, -0x1

    if-ne v0, v1, :cond_34

    const-string v1, "all"

    :goto_2c
    invoke-interface {v3, v4, v2, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    return-void

    .line 25
    :cond_30
    const-string v1, "pending"

    move-object v2, v1

    goto :goto_27

    .line 26
    :cond_34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2c
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 34
    invoke-static {p1}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->pid(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    .line 35
    return-void
.end method

.method private static pid(Z)Ljava/lang/String;
    .locals 1
    .param p0, "active"    # Z

    .prologue
    .line 14
    if-eqz p0, :cond_5

    const-string v0, "03"

    :goto_4
    return-object v0

    :cond_5
    const-string v0, "07"

    goto :goto_4
.end method


# virtual methods
.method public getTroubleCodes()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v14, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->Log:Lorg/slf4j/Logger;

    const-string v15, "Raw Response of {} READTroubleCodesCommand, Data {}"

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    if-eqz v13, :cond_9d

    const-string v13, "active"

    .line 43
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getResponse()Ljava/lang/String;

    move-result-object v16

    .line 42
    move-object/from16 v0, v16

    invoke-interface {v14, v15, v13, v0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getResponseCount()I

    move-result v9

    .line 45
    .local v9, "responseCount":I
    sget-object v14, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->Log:Lorg/slf4j/Logger;

    const-string v15, "Raw Response of {} READTroubleCodesCommand, Data {}, Count {}"

    const/4 v13, 0x3

    new-array v0, v13, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    if-eqz v13, :cond_a1

    const-string v13, "active"

    :goto_2c
    aput-object v13, v16, v17

    const/4 v13, 0x1

    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getResponse()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v16, v13

    const/4 v13, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v16, v13

    .line 45
    invoke-interface/range {v14 .. v16}, Lorg/slf4j/Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v12, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_45
    if-ge v5, v9, :cond_110

    .line 49
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v3

    .line 50
    .local v3, "ecuResponse":Lcom/navdy/obd/EcuResponse;
    if-eqz v3, :cond_9a

    .line 51
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v13

    iget-object v7, v13, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 52
    .local v7, "response":[B
    invoke-static {v7}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v8

    .line 53
    .local v8, "responseAsString":Ljava/lang/String;
    sget-object v14, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->Log:Lorg/slf4j/Logger;

    const-string v15, "Response of {} READTroubleCodesCommand, ECU {} , Data {}"

    const/4 v13, 0x3

    new-array v0, v13, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->active:Z

    if-eqz v13, :cond_a4

    const-string v13, "active"

    :goto_6e
    aput-object v13, v16, v17

    const/4 v13, 0x1

    iget v0, v3, Lcom/navdy/obd/EcuResponse;->ecu:I

    move/from16 v17, v0

    .line 54
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v16, v13

    const/4 v13, 0x2

    aput-object v8, v16, v13

    .line 53
    invoke-interface/range {v14 .. v16}, Lorg/slf4j/Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    if-eqz v7, :cond_9a

    array-length v13, v7

    const/4 v14, 0x3

    if-lt v13, v14, :cond_9a

    .line 56
    const/4 v6, 0x1

    .local v6, "j":I
    :goto_88
    array-length v13, v7

    add-int/lit8 v13, v13, -0x1

    if-ge v6, v13, :cond_9a

    .line 57
    aget-byte v13, v7, v6

    and-int/lit16 v4, v13, 0xff

    .line 58
    .local v4, "firstHex":I
    shr-int/lit8 v2, v4, 0x4

    .line 59
    .local v2, "categoryIdentifierNibble":I
    and-int/lit8 v11, v4, 0xf

    .line 60
    .local v11, "secondNibble":I
    add-int/lit8 v13, v6, 0x1

    array-length v14, v7

    if-lt v13, v14, :cond_a7

    .line 48
    .end local v2    # "categoryIdentifierNibble":I
    .end local v4    # "firstHex":I
    .end local v6    # "j":I
    .end local v7    # "response":[B
    .end local v8    # "responseAsString":Ljava/lang/String;
    .end local v11    # "secondNibble":I
    :cond_9a
    add-int/lit8 v5, v5, 0x1

    goto :goto_45

    .line 42
    .end local v3    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .end local v5    # "i":I
    .end local v9    # "responseCount":I
    .end local v12    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_9d
    const-string v13, "pending"

    goto/16 :goto_c

    .line 45
    .restart local v9    # "responseCount":I
    :cond_a1
    const-string v13, "pending"

    goto :goto_2c

    .line 53
    .restart local v3    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .restart local v5    # "i":I
    .restart local v7    # "response":[B
    .restart local v8    # "responseAsString":Ljava/lang/String;
    .restart local v12    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a4
    const-string v13, "pending"

    goto :goto_6e

    .line 63
    .restart local v2    # "categoryIdentifierNibble":I
    .restart local v4    # "firstHex":I
    .restart local v6    # "j":I
    .restart local v11    # "secondNibble":I
    :cond_a7
    add-int/lit8 v13, v6, 0x1

    aget-byte v13, v7, v13

    and-int/lit16 v10, v13, 0xff

    .line 64
    .local v10, "secondHex":I
    const/16 v13, 0xf

    if-gt v2, v13, :cond_10c

    .line 65
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/navdy/obd/command/ReadTroubleCodesCommand;->TROUBLE_CATEGORY:[Ljava/lang/String;

    div-int/lit8 v15, v2, 0x4

    aget-object v14, v14, v15

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    rem-int/lit8 v17, v2, 0x4

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "categoryIdentifier":Ljava/lang/String;
    if-nez v4, :cond_df

    if-eqz v10, :cond_9a

    .line 69
    :cond_df
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "%x%02x"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    .end local v1    # "categoryIdentifier":Ljava/lang/String;
    :cond_10c
    add-int/lit8 v6, v6, 0x2

    goto/16 :goto_88

    .line 75
    .end local v2    # "categoryIdentifierNibble":I
    .end local v3    # "ecuResponse":Lcom/navdy/obd/EcuResponse;
    .end local v4    # "firstHex":I
    .end local v6    # "j":I
    .end local v7    # "response":[B
    .end local v8    # "responseAsString":Ljava/lang/String;
    .end local v10    # "secondHex":I
    .end local v11    # "secondNibble":I
    :cond_110
    return-object v12
.end method
