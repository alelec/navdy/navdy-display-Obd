.class public Lcom/navdy/obd/command/WriteConfigurationCommand;
.super Lcom/navdy/obd/command/BatchCommand;
.source "WriteConfigurationCommand.java"


# static fields
.field static final Log:Lorg/slf4j/Logger;

.field public static final SET_DEVICE_INFO_COMMAND:Ljava/lang/String; = "STSATI"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/navdy/obd/command/WriteConfigurationCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/WriteConfigurationCommand;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "configuration"    # Ljava/lang/String;

    .prologue
    .line 15
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v3}, Lcom/navdy/obd/command/BatchCommand;-><init>(Ljava/util/List;)V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Configuration file cannot be empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 19
    :cond_0
    const-string v3, "STSATI"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 20
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Configuration should set the device information to identify the configuration"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 22
    :cond_1
    const-string v3, "\n"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, "commands":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 24
    .local v2, "i":I
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v0, v1, v3

    .line 25
    .local v0, "command":Ljava/lang/String;
    new-instance v5, Lcom/navdy/obd/command/ObdCommand;

    invoke-direct {v5, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/navdy/obd/command/WriteConfigurationCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 24
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 27
    .end local v0    # "command":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public getResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/navdy/obd/command/BatchCommand;->getResponse()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
