.class public Lcom/navdy/obd/command/ValidPidsCommand;
.super Ljava/lang/Object;
.source "ValidPidsCommand.java"

# interfaces
.implements Lcom/navdy/obd/command/ICommand;


# static fields
.field static final Log:Lorg/slf4j/Logger;

.field public static MAX_ADDRESS:I


# instance fields
.field private supportedPids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/ObdCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/navdy/obd/command/ValidPidsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ValidPidsCommand;->Log:Lorg/slf4j/Logger;

    .line 20
    const/16 v0, 0x80

    sput v0, Lcom/navdy/obd/command/ValidPidsCommand;->MAX_ADDRESS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/command/ValidPidsCommand;->supportedPids:Ljava/util/List;

    return-void
.end method

.method private anyEcuSupportsMorePids(Lcom/navdy/obd/command/ObdCommand;)Z
    .locals 2
    .param p1, "command"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 52
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/navdy/obd/command/ObdCommand;->getResponseCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 53
    invoke-virtual {p1, v0}, Lcom/navdy/obd/command/ObdCommand;->getResponse(I)Lcom/navdy/obd/EcuResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/obd/EcuResponse;->data:[B

    invoke-direct {p0, v1}, Lcom/navdy/obd/command/ValidPidsCommand;->supportsMorePids([B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    const/4 v1, 0x1

    .line 56
    :goto_1
    return v1

    .line 52
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private supportsMorePids([B)Z
    .locals 3
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x0

    .line 63
    if-eqz p1, :cond_0

    array-length v1, p1

    if-le v1, v2, :cond_0

    .line 64
    aget-byte v1, p1, v2

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 66
    :cond_0
    return v0
.end method


# virtual methods
.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 11
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 35
    sget-object v5, Lcom/navdy/obd/command/ValidPidsCommand;->Log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OBD - Scanning for valid pids with protocol = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "address":I
    const/4 v3, 0x0

    .line 38
    .local v3, "done":Z
    :goto_0
    if-nez v3, :cond_2

    sget v5, Lcom/navdy/obd/command/ValidPidsCommand;->MAX_ADDRESS:I

    if-gt v0, v5, :cond_2

    .line 39
    const-string v5, "01%02x"

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "cmd":Ljava/lang/String;
    sget-object v5, Lcom/navdy/obd/command/ValidPidsCommand;->Log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OBD - Reading "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 41
    new-instance v2, Lcom/navdy/obd/command/ObdCommand;

    invoke-direct {v2, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 42
    .local v2, "command":Lcom/navdy/obd/command/ObdCommand;
    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/navdy/obd/command/ObdCommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 43
    invoke-virtual {v2}, Lcom/navdy/obd/command/ObdCommand;->getResponse()Ljava/lang/String;

    move-result-object v4

    .line 44
    .local v4, "response":Ljava/lang/String;
    sget-object v8, Lcom/navdy/obd/command/ValidPidsCommand;->Log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OBD - Response: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v4, :cond_0

    const/16 v5, 0xd

    const/16 v10, 0xa

    invoke-virtual {v4, v5, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 45
    iget-object v5, p0, Lcom/navdy/obd/command/ValidPidsCommand;->supportedPids:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v0, v0, 0x20

    .line 47
    invoke-direct {p0, v2}, Lcom/navdy/obd/command/ValidPidsCommand;->anyEcuSupportsMorePids(Lcom/navdy/obd/command/ObdCommand;)Z

    move-result v5

    if-nez v5, :cond_1

    move v3, v6

    .line 48
    :goto_2
    goto :goto_0

    .line 44
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    move v3, v7

    .line 47
    goto :goto_2

    .line 49
    .end local v1    # "cmd":Ljava/lang/String;
    .end local v2    # "command":Lcom/navdy/obd/command/ObdCommand;
    .end local v4    # "response":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public getDoubleValue()D
    .locals 2

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getIntValue()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "validPids"

    return-object v0
.end method

.method public getResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method public supportedPids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/ObdCommand;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/obd/command/ValidPidsCommand;->supportedPids:Ljava/util/List;

    return-object v0
.end method
