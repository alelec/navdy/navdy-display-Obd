.class public final Lcom/navdy/obd/command/Sample;
.super Ljava/lang/Object;
.source "Sample.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0010\u0010\u0002\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/navdy/obd/command/Sample;",
        "",
        "pid",
        "",
        "scanInterval",
        "(II)V",
        "lastSampleTimestamp",
        "",
        "lastSamplingTime",
        "getLastSamplingTime",
        "()J",
        "setLastSamplingTime",
        "(J)V",
        "Lcom/navdy/obd/Pid;",
        "samplingTimeStats",
        "Lcom/navdy/util/RunningStats;",
        "getScanInterval",
        "()I",
        "updated",
        "",
        "obd-service_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field public lastSampleTimestamp:J
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field private lastSamplingTime:J

.field public final pid:Lcom/navdy/obd/Pid;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public final samplingTimeStats:Lcom/navdy/util/RunningStats;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final scanInterval:I

.field public updated:Z
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "scanInterval"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/navdy/obd/command/Sample;->scanInterval:I

    .line 11
    new-instance v0, Lcom/navdy/obd/Pid;

    invoke-direct {v0, p1}, Lcom/navdy/obd/Pid;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/obd/command/Sample;->pid:Lcom/navdy/obd/Pid;

    .line 13
    new-instance v0, Lcom/navdy/util/RunningStats;

    invoke-direct {v0}, Lcom/navdy/util/RunningStats;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/command/Sample;->samplingTimeStats:Lcom/navdy/util/RunningStats;

    return-void
.end method


# virtual methods
.method public final getLastSamplingTime()J
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/navdy/obd/command/Sample;->lastSamplingTime:J

    return-wide v0
.end method

.method public final getScanInterval()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lcom/navdy/obd/command/Sample;->scanInterval:I

    return v0
.end method

.method public final setLastSamplingTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 15
    iput-wide p1, p0, Lcom/navdy/obd/command/Sample;->lastSamplingTime:J

    return-void
.end method
