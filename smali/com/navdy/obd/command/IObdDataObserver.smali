.class public interface abstract Lcom/navdy/obd/command/IObdDataObserver;
.super Ljava/lang/Object;
.source "IObdDataObserver.java"


# virtual methods
.method public abstract onCommand(Ljava/lang/String;)V
.end method

.method public abstract onError(Ljava/lang/String;)V
.end method

.method public abstract onRawCanBusMessage(Ljava/lang/String;)V
.end method

.method public abstract onResponse(Ljava/lang/String;)V
.end method
