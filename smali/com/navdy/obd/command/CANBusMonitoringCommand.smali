.class public Lcom/navdy/obd/command/CANBusMonitoringCommand;
.super Lcom/navdy/obd/command/BatchCommand;
.source "CANBusMonitoringCommand.java"


# static fields
.field public static final CAN_BUS_MONITOR_COMMAND_PID:I = 0x3e9


# instance fields
.field private canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

.field monitoredPids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/can/CANBusDataDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    const/4 v2, 0x0

    new-array v2, v2, [Lcom/navdy/obd/command/ICommand;

    invoke-direct {p0, v2}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 21
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->monitoredPids:Ljava/util/ArrayList;

    .line 23
    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v2}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 24
    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->CLEAR_ALL_PASS_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v2}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 25
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 26
    .local v1, "uniqueHeaders":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/can/CANBusDataDescriptor;

    .line 27
    .local v0, "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    iget-object v3, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->monitoredPids:Ljava/util/ArrayList;

    iget v4, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->obdPid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    iget-object v3, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 29
    iget-object v3, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 30
    iget-object v3, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 31
    new-instance v3, Lcom/navdy/obd/command/ObdCommand;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "STFAP "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",7FF"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    goto :goto_0

    .line 35
    .end local v0    # "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    :cond_1
    new-instance v2, Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-direct {v2, p1}, Lcom/navdy/obd/command/CANBusDataProcessor;-><init>(Ljava/util/List;)V

    iput-object v2, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    .line 36
    iget-object v2, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {p0, v2}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 37
    new-instance v2, Lcom/navdy/obd/command/CANBusMonitoringCommand$1;

    const-string v3, "Turn on formating"

    const-string v4, "ATCAF1"

    invoke-direct {v2, p0, v3, v4}, Lcom/navdy/obd/command/CANBusMonitoringCommand$1;-><init>(Lcom/navdy/obd/command/CANBusMonitoringCommand;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 44
    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->CLEAR_ALL_PASS_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v2}, Lcom/navdy/obd/command/CANBusMonitoringCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getMonitoredPidsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->monitoredPids:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isMonitoringFailed()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0}, Lcom/navdy/obd/command/CANBusDataProcessor;->isMonitoringFailed()Z

    move-result v0

    return v0
.end method

.method public isSamplingComplete()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0}, Lcom/navdy/obd/command/CANBusDataProcessor;->isSamplingComplete()Z

    move-result v0

    return v0
.end method

.method public isSamplingSuccessful()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0}, Lcom/navdy/obd/command/CANBusDataProcessor;->isSamplingSuccessful()Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0}, Lcom/navdy/obd/command/CANBusDataProcessor;->reset()V

    .line 53
    return-void
.end method

.method public setCANBusDataListener(Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;)V
    .locals 1
    .param p1, "dataListener"    # Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/command/CANBusDataProcessor;->setCANBusDataListener(Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;)V

    .line 49
    return-void
.end method

.method public startMonitoring()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/obd/command/CANBusMonitoringCommand;->canbusDataProcessor:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-virtual {v0}, Lcom/navdy/obd/command/CANBusDataProcessor;->startMonitoring()V

    .line 61
    return-void
.end method
