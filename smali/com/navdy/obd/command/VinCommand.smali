.class public Lcom/navdy/obd/command/VinCommand;
.super Lcom/navdy/obd/command/ObdCommand;
.source "VinCommand.java"


# static fields
.field private static final DATA_LEN:I = 0x4

.field private static final HEADER_LEN:I = 0x3

.field static final Log:Lorg/slf4j/Logger;

.field private static final VIN_LENGTH:I = 0x14


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/navdy/obd/command/VinCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/VinCommand;->Log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "0902"

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getVIN()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 22
    invoke-virtual {p0}, Lcom/navdy/obd/command/VinCommand;->getByteResponse()[B

    move-result-object v2

    .line 24
    .local v2, "response":[B
    if-nez v2, :cond_0

    .line 50
    :goto_0
    return-object v4

    .line 30
    :cond_0
    const/4 v5, 0x0

    :try_start_0
    aget-byte v5, v2, v5

    const/16 v6, 0x49

    if-ne v5, v6, :cond_3

    .line 31
    array-length v5, v2

    const/16 v6, 0x17

    if-le v5, v6, :cond_2

    .line 32
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .local v3, "vin":Ljava/lang/StringBuilder;
    const/4 v1, 0x3

    .line 34
    .local v1, "pos":I
    :goto_1
    add-int/lit8 v5, v1, 0x4

    array-length v6, v2

    if-gt v5, v6, :cond_1

    .line 35
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x4

    invoke-direct {v5, v2, v1, v6}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    add-int/lit8 v1, v1, 0x7

    goto :goto_1

    .line 38
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 40
    .end local v1    # "pos":I
    .end local v3    # "vin":Ljava/lang/StringBuilder;
    :cond_2
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x3

    array-length v7, v2

    add-int/lit8 v7, v7, -0x3

    invoke-direct {v5, v2, v6, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 44
    :cond_3
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/navdy/obd/command/VinCommand;->Log:Lorg/slf4j/Logger;

    const-string v6, "Failed to parse vin"

    invoke-interface {v5, v6, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
