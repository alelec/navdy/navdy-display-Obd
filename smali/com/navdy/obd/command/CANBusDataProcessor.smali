.class public Lcom/navdy/obd/command/CANBusDataProcessor;
.super Lcom/navdy/obd/command/ObdCommand;
.source "CANBusDataProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;,
        Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;
    }
.end annotation


# static fields
.field public static final MINIMUM_SAMPLES_NEEDED:I = 0x3

.field public static final MONITORING_DURATION:I = 0x2710

.field public static final READ_TIME_OUT_MILLIS:I = 0x3e8

.field public static final SAMPLING_DURATION:I = 0xfa


# instance fields
.field baos:Ljava/io/ByteArrayOutputStream;

.field buffer:[B

.field private canBusDataListener:Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;

.field private headerToDataMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;",
            ">;"
        }
    .end annotation
.end field

.field private isSampling:Z

.field private monitorDurationMilliseconds:J

.field private monitorDurationNanos:J

.field private monitoringFailed:Z

.field private numberOfSuccessSamples:I

.field private samplingFailed:Z


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/can/CANBusDataDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "canBusDataDescriptors":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    const/4 v4, 0x0

    .line 50
    const-string v3, "STM"

    invoke-direct {p0, v3}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    .line 28
    iput v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    .line 36
    iput-boolean v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    .line 37
    iput-boolean v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->monitoringFailed:Z

    .line 38
    const/16 v3, 0x2000

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->buffer:[B

    .line 40
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const v4, 0x7d000

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v3, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->baos:Ljava/io/ByteArrayOutputStream;

    .line 51
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    .line 52
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/can/CANBusDataDescriptor;

    .line 53
    .local v0, "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    iget-object v1, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->header:Ljava/lang/String;

    .line 55
    .local v1, "header":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 56
    new-instance v2, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;

    invoke-direct {v2, p0}, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;-><init>(Lcom/navdy/obd/command/CANBusDataProcessor;)V

    .line 57
    .local v2, "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0x8

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, v2, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->descriptorsList:Ljava/util/ArrayList;

    .line 58
    iget-object v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :goto_1
    iget-object v4, v2, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->descriptorsList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    iget v4, v0, Lcom/navdy/obd/can/CANBusDataDescriptor;->dataFrequency:I

    iput v4, v2, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->interval:I

    goto :goto_0

    .line 60
    .end local v2    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    :cond_0
    iget-object v4, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;

    .restart local v2    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    goto :goto_1

    .line 65
    .end local v0    # "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    .end local v1    # "header":Ljava/lang/String;
    .end local v2    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    :cond_1
    return-void
.end method

.method public static bytesToInt([BIIZ)I
    .locals 6
    .param p0, "byteArray"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "bigEndian"    # Z

    .prologue
    .line 200
    if-eqz p0, :cond_2

    if-lez p2, :cond_2

    const/4 v4, 0x4

    if-gt p2, v4, :cond_2

    array-length v4, p0

    add-int v5, p1, p2

    if-lt v4, v5, :cond_2

    .line 201
    const/4 v1, 0x0

    .line 202
    .local v1, "data":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, p2, :cond_3

    .line 203
    if-eqz p3, :cond_1

    move v4, v3

    :goto_1
    add-int v2, p1, v4

    .line 204
    .local v2, "effectiveOffset":I
    aget-byte v4, p0, v2

    and-int/lit16 v0, v4, 0xff

    .line 205
    .local v0, "byteData":I
    add-int/2addr v1, v0

    .line 206
    add-int/lit8 v4, p2, -0x1

    if-ge v3, v4, :cond_0

    .line 207
    shl-int/lit8 v1, v1, 0x8

    .line 202
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "byteData":I
    .end local v2    # "effectiveOffset":I
    :cond_1
    sub-int v4, p2, v3

    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 212
    .end local v1    # "data":I
    .end local v3    # "i":I
    :cond_2
    new-instance v4, Ljava/lang/NumberFormatException;

    const-string v5, "Incorrect data"

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 210
    .restart local v1    # "data":I
    .restart local v3    # "i":I
    :cond_3
    return v1
.end method

.method public static parseHexArray([CII)[B
    .locals 6
    .param p0, "hexArray"    # [C
    .param p1, "position"    # I
    .param p2, "length"    # I

    .prologue
    .line 217
    div-int/lit8 v4, p2, 0x2

    new-array v0, v4, [B

    .line 218
    .local v0, "byteData":[B
    const/4 v1, 0x0

    .line 219
    .local v1, "bytePosition":I
    move v3, p1

    .local v3, "i":I
    move v2, v1

    .end local v1    # "bytePosition":I
    .local v2, "bytePosition":I
    :goto_0
    add-int v4, p1, p2

    if-ge v3, v4, :cond_0

    .line 220
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePosition":I
    .restart local v1    # "bytePosition":I
    aget-char v4, p0, v3

    invoke-static {v4}, Lcom/navdy/obd/util/HexUtil;->parseHexDigit(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v3, 0x1

    aget-char v5, p0, v5

    invoke-static {v5}, Lcom/navdy/obd/util/HexUtil;->parseHexDigit(I)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v2

    .line 219
    add-int/lit8 v3, v3, 0x2

    move v2, v1

    .end local v1    # "bytePosition":I
    .restart local v2    # "bytePosition":I
    goto :goto_0

    .line 222
    :cond_0
    return-object v0
.end method

.method private processData(Ljava/lang/String;[CI)V
    .locals 16
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "data"    # [C
    .param p3, "dataLength"    # I

    .prologue
    .line 166
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 167
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;

    .line 168
    .local v8, "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    iget v9, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    .line 169
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    if-nez v9, :cond_4

    .line 170
    iget-wide v12, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->lastProcessedTime:J

    const-wide/16 v14, 0x0

    cmp-long v9, v12, v14

    if-eqz v9, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    iget-wide v14, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->lastProcessedTime:J

    sub-long/2addr v12, v14

    iget v9, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->interval:I

    int-to-long v14, v9

    cmp-long v9, v12, v14

    if-lez v9, :cond_4

    .line 171
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    iput-wide v12, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->lastProcessedTime:J

    .line 173
    iget-object v7, v8, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->descriptorsList:Ljava/util/ArrayList;

    .line 174
    .local v7, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    if-eqz v7, :cond_4

    .line 175
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/obd/can/CANBusDataDescriptor;

    .line 176
    .local v6, "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    iget v5, v6, Lcom/navdy/obd/can/CANBusDataDescriptor;->byteOffset:I

    .line 177
    .local v5, "byteOffset":I
    iget v2, v6, Lcom/navdy/obd/can/CANBusDataDescriptor;->lengthInBits:I

    .line 178
    .local v2, "bitLength":I
    div-int/lit8 v4, v2, 0x8

    .line 179
    .local v4, "byteLength":I
    add-int v9, v5, v4

    mul-int/lit8 v9, v9, 0x2

    move/from16 v0, p3

    if-ge v9, v0, :cond_1

    .line 180
    mul-int/lit8 v9, v5, 0x2

    mul-int/lit8 v13, v4, 0x2

    move-object/from16 v0, p2

    invoke-static {v0, v9, v13}, Lcom/navdy/obd/command/CANBusDataProcessor;->parseHexArray([CII)[B

    move-result-object v3

    .line 181
    .local v3, "byteData":[B
    if-eqz v3, :cond_1

    .line 182
    const-wide/high16 v10, -0x3e20000000000000L    # -2.147483648E9

    .line 183
    .local v10, "value":D
    array-length v9, v3

    const/4 v13, 0x1

    if-ne v9, v13, :cond_2

    .line 184
    const/4 v9, 0x0

    aget-byte v9, v3, v9

    and-int/lit16 v9, v9, 0xff

    int-to-double v10, v9

    .line 188
    :goto_1
    iget-wide v14, v6, Lcom/navdy/obd/can/CANBusDataDescriptor;->resolution:D

    mul-double/2addr v10, v14

    .line 189
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->canBusDataListener:Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;

    iget v13, v6, Lcom/navdy/obd/can/CANBusDataDescriptor;->obdPid:I

    invoke-interface {v9, v13, v10, v11}, Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;->onCanBusDataRead(ID)V

    goto :goto_0

    .line 186
    :cond_2
    const/4 v13, 0x0

    array-length v14, v3

    iget-boolean v9, v6, Lcom/navdy/obd/can/CANBusDataDescriptor;->littleEndian:Z

    if-nez v9, :cond_3

    const/4 v9, 0x1

    :goto_2
    invoke-static {v3, v13, v14, v9}, Lcom/navdy/obd/command/CANBusDataProcessor;->bytesToInt([BIIZ)I

    move-result v9

    int-to-double v10, v9

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 197
    .end local v2    # "bitLength":I
    .end local v3    # "byteData":[B
    .end local v4    # "byteLength":I
    .end local v5    # "byteOffset":I
    .end local v6    # "descriptor":Lcom/navdy/obd/can/CANBusDataDescriptor;
    .end local v7    # "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/can/CANBusDataDescriptor;>;"
    .end local v8    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    .end local v10    # "value":D
    :cond_4
    return-void
.end method

.method private resetHeaderStatsForSampling()V
    .locals 4

    .prologue
    .line 155
    iget-object v2, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 156
    .local v1, "headerDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;

    .line 157
    .local v0, "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    const/4 v3, 0x0

    iput v3, v0, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    goto :goto_0

    .line 159
    .end local v0    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    :cond_0
    return-void
.end method


# virtual methods
.method protected getCommandExecutionTimeOutInMillis()J
    .locals 2

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2710

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x4e20

    goto :goto_0
.end method

.method public isMonitoringFailed()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->monitoringFailed:Z

    return v0
.end method

.method public isSamplingComplete()Z
    .locals 2

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSamplingSuccessful()Z
    .locals 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 33
    .param p1, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p2, "obdCommandInterpreter"    # Lcom/navdy/obd/io/ObdCommandInterpreter;
    .param p3, "dataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    move/from16 v27, v0

    if-eqz v27, :cond_4

    const-wide/16 v28, 0xfa

    :goto_0
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/obd/command/CANBusDataProcessor;->monitorDurationMilliseconds:J

    .line 70
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->monitorDurationMilliseconds:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x3e8

    mul-long v28, v28, v30

    const-wide/16 v30, 0x3e8

    mul-long v28, v28, v30

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/obd/command/CANBusDataProcessor;->monitorDurationNanos:J

    .line 71
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v22

    .line 72
    .local v22, "start":J
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Starting the CAN bus monitoring"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 73
    const/16 v27, 0x3e8

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/navdy/obd/io/ObdCommandInterpreter;->setReadTimeOut(I)V

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->baos:Ljava/io/ByteArrayOutputStream;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 75
    const-wide/16 v8, 0x0

    .line 76
    .local v8, "elapsedTime":J
    const/16 v16, 0x0

    .line 77
    .local v16, "index":I
    const/16 v25, 0x0

    .line 78
    .local v25, "totalLength":I
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v27, 0x3

    move/from16 v0, v27

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 79
    .local v12, "headerBuilder":Ljava/lang/StringBuilder;
    const/16 v19, 0x0

    .line 80
    .local v19, "lineLength":I
    const/16 v27, 0x20

    move/from16 v0, v27

    new-array v6, v0, [C

    .line 81
    .local v6, "dataArray":[C
    invoke-direct/range {p0 .. p0}, Lcom/navdy/obd/command/CANBusDataProcessor;->resetHeaderStatsForSampling()V

    .line 82
    :cond_0
    :goto_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v28

    sub-long v8, v28, v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->monitorDurationNanos:J

    move-wide/from16 v28, v0

    cmp-long v27, v8, v28

    if-gez v27, :cond_b

    .line 84
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->buffer:[B

    move-object/from16 v27, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/navdy/obd/io/ObdCommandInterpreter;->read([B)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v18

    .line 85
    .local v18, "length":I
    if-lez v18, :cond_0

    .line 86
    add-int v25, v25, v18

    .line 88
    const/4 v15, 0x0

    .local v15, "i":I
    move/from16 v20, v19

    .end local v19    # "lineLength":I
    .local v20, "lineLength":I
    :goto_2
    move/from16 v0, v18

    if-ge v15, v0, :cond_13

    .line 89
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->buffer:[B

    move-object/from16 v27, v0

    aget-byte v27, v27, v15

    move/from16 v0, v27

    int-to-char v5, v0

    .line 90
    .local v5, "ch":C
    const/16 v27, 0x30

    move/from16 v0, v27

    if-lt v5, v0, :cond_1

    const/16 v27, 0x39

    move/from16 v0, v27

    if-le v5, v0, :cond_3

    :cond_1
    const/16 v27, 0x41

    move/from16 v0, v27

    if-lt v5, v0, :cond_2

    const/16 v27, 0x46

    move/from16 v0, v27

    if-le v5, v0, :cond_3

    :cond_2
    const/16 v27, 0x61

    move/from16 v0, v27

    if-lt v5, v0, :cond_5

    const/16 v27, 0x66

    move/from16 v0, v27

    if-gt v5, v0, :cond_5

    :cond_3
    const/16 v26, 0x1

    .line 91
    .local v26, "validHexDigit":Z
    :goto_3
    if-nez v26, :cond_6

    const/16 v27, 0xd

    move/from16 v0, v27

    if-eq v5, v0, :cond_6

    const/16 v27, 0xa

    move/from16 v0, v27

    if-eq v5, v0, :cond_6

    .line 92
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Received an invalid character, ending monitoring , Char"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move/from16 v19, v20

    .line 93
    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    goto :goto_1

    .line 69
    .end local v5    # "ch":C
    .end local v6    # "dataArray":[C
    .end local v8    # "elapsedTime":J
    .end local v12    # "headerBuilder":Ljava/lang/StringBuilder;
    .end local v15    # "i":I
    .end local v16    # "index":I
    .end local v18    # "length":I
    .end local v19    # "lineLength":I
    .end local v22    # "start":J
    .end local v25    # "totalLength":I
    .end local v26    # "validHexDigit":Z
    :cond_4
    const-wide/16 v28, 0x2710

    goto/16 :goto_0

    .line 90
    .restart local v5    # "ch":C
    .restart local v6    # "dataArray":[C
    .restart local v8    # "elapsedTime":J
    .restart local v12    # "headerBuilder":Ljava/lang/StringBuilder;
    .restart local v15    # "i":I
    .restart local v16    # "index":I
    .restart local v18    # "length":I
    .restart local v20    # "lineLength":I
    .restart local v22    # "start":J
    .restart local v25    # "totalLength":I
    :cond_5
    const/16 v26, 0x0

    goto :goto_3

    .line 95
    .restart local v26    # "validHexDigit":Z
    :cond_6
    const/16 v27, 0xd

    move/from16 v0, v27

    if-eq v5, v0, :cond_7

    const/16 v27, 0xa

    move/from16 v0, v27

    if-ne v5, v0, :cond_9

    .line 96
    :cond_7
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v27

    const/16 v28, 0x3

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    const/16 v27, 0x5

    move/from16 v0, v20

    move/from16 v1, v27

    if-le v0, v1, :cond_8

    .line 97
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    add-int/lit8 v28, v20, -0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/obd/command/CANBusDataProcessor;->processData(Ljava/lang/String;[CI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 99
    :cond_8
    const/16 v19, 0x0

    .line 100
    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    const/16 v27, 0x0

    :try_start_2
    move/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 88
    :goto_4
    add-int/lit8 v15, v15, 0x1

    move/from16 v20, v19

    .end local v19    # "lineLength":I
    .restart local v20    # "lineLength":I
    goto/16 :goto_2

    .line 101
    :cond_9
    const/16 v27, 0x3

    move/from16 v0, v20

    move/from16 v1, v27

    if-ge v0, v1, :cond_a

    .line 102
    :try_start_3
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 103
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    goto :goto_4

    .line 105
    .end local v19    # "lineLength":I
    .restart local v20    # "lineLength":I
    :cond_a
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    add-int/lit8 v27, v20, -0x3

    :try_start_4
    aput-char v5, v6, v27
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    .line 109
    .end local v5    # "ch":C
    .end local v15    # "i":I
    .end local v18    # "length":I
    .end local v26    # "validHexDigit":Z
    :catch_0
    move-exception v7

    .line 110
    .local v7, "e":Ljava/lang/InterruptedException;
    :goto_5
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Interrupted exception {}"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v7}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :cond_b
    :goto_6
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v28

    sub-long v28, v28, v22

    const-wide/32 v30, 0xf4240

    div-long v10, v28, v30

    .line 118
    .local v10, "elapsedTimeMillis":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->headerToDataMapping:Ljava/util/HashMap;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v14

    .line 119
    .local v14, "headerDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;>;"
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_c
    :goto_7
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_10

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;

    .line 120
    .local v13, "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    iget v0, v13, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    move/from16 v28, v0

    if-nez v28, :cond_e

    const/16 v17, 0x0

    .line 121
    .local v17, "interval":I
    :goto_8
    sget-object v28, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v29, "Header {}, Count {}, Freq :{}"

    const/16 v30, 0x3

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    aput-object v32, v30, v31

    const/16 v31, 0x1

    iget v0, v13, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    const/16 v31, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    invoke-interface/range {v28 .. v30}, Lorg/slf4j/Logger;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    if-eqz v17, :cond_d

    iget v0, v13, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->interval:I

    move/from16 v28, v0

    move/from16 v0, v17

    move/from16 v1, v28

    if-le v0, v1, :cond_c

    .line 123
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    move/from16 v28, v0

    if-eqz v28, :cond_f

    .line 124
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    goto :goto_7

    .line 112
    .end local v10    # "elapsedTimeMillis":J
    .end local v13    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    .end local v14    # "headerDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;>;"
    .end local v17    # "interval":I
    :catch_1
    move-exception v7

    .line 113
    .local v7, "e":Ljava/io/IOException;
    :goto_9
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "IOException exception {}"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v7}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 120
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v10    # "elapsedTimeMillis":J
    .restart local v13    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    .restart local v14    # "headerDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;>;"
    :cond_e
    iget v0, v13, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    div-long v28, v10, v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v17, v0

    goto :goto_8

    .line 126
    .restart local v17    # "interval":I
    :cond_f
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/obd/command/CANBusDataProcessor;->monitoringFailed:Z

    goto/16 :goto_7

    .line 130
    .end local v13    # "headerData":Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
    .end local v17    # "interval":I
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    move/from16 v27, v0

    if-nez v27, :cond_11

    .line 131
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    move/from16 v27, v0

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    .line 133
    :cond_11
    const/16 v24, 0x0

    .line 135
    .local v24, "terminalClear":Z
    :goto_a
    if-nez v24, :cond_12

    .line 137
    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v4, v0, [B

    fill-array-data v4, :array_0

    .line 138
    .local v4, "at":[B
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/navdy/obd/io/ObdCommandInterpreter;->write([B)V

    .line 140
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Clearing the terminal"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    const/16 v27, 0x0

    const/16 v28, 0x1

    :try_start_5
    move-object/from16 v0, p2

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->clearTerminalBulk(ZZ)V

    .line 143
    const/16 v24, 0x1

    .line 144
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Terminal clear"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_a

    .line 145
    :catch_2
    move-exception v7

    .line 146
    .local v7, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 147
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Interrupted while clearing terminal"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_a

    .line 148
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v21

    .line 149
    .local v21, "te":Ljava/util/concurrent/TimeoutException;
    sget-object v27, Lcom/navdy/obd/command/CANBusDataProcessor;->Log:Lorg/slf4j/Logger;

    const-string v28, "Clearing the terminal timed out"

    invoke-interface/range {v27 .. v28}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_a

    .line 152
    .end local v4    # "at":[B
    .end local v21    # "te":Ljava/util/concurrent/TimeoutException;
    :cond_12
    return-void

    .line 112
    .end local v10    # "elapsedTimeMillis":J
    .end local v14    # "headerDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;>;"
    .end local v19    # "lineLength":I
    .end local v24    # "terminalClear":Z
    .restart local v15    # "i":I
    .restart local v18    # "length":I
    .restart local v20    # "lineLength":I
    :catch_4
    move-exception v7

    move/from16 v19, v20

    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    goto/16 :goto_9

    .line 109
    .end local v19    # "lineLength":I
    .restart local v20    # "lineLength":I
    :catch_5
    move-exception v7

    move/from16 v19, v20

    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    goto/16 :goto_5

    .end local v19    # "lineLength":I
    .restart local v20    # "lineLength":I
    :cond_13
    move/from16 v19, v20

    .end local v20    # "lineLength":I
    .restart local v19    # "lineLength":I
    goto/16 :goto_1

    .line 137
    nop

    :array_0
    .array-data 1
        0x41t
        0x54t
        0x49t
        0x49t
        0x49t
        0xdt
    .end array-data
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 226
    iput-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->samplingFailed:Z

    .line 227
    iput-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->monitoringFailed:Z

    .line 228
    iput v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->numberOfSuccessSamples:I

    .line 229
    invoke-direct {p0}, Lcom/navdy/obd/command/CANBusDataProcessor;->resetHeaderStatsForSampling()V

    .line 230
    return-void
.end method

.method public setCANBusDataListener(Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;)V
    .locals 0
    .param p1, "canBusDataListener"    # Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->canBusDataListener:Lcom/navdy/obd/command/CANBusDataProcessor$ICanBusDataListener;

    .line 163
    return-void
.end method

.method public startMonitoring()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor;->isSampling:Z

    .line 242
    return-void
.end method
