.class public Lcom/navdy/obd/command/ObdCommand;
.super Ljava/lang/Object;
.source "ObdCommand.java"

# interfaces
.implements Lcom/navdy/obd/command/ICommand;


# static fields
.field public static final ALLOW_LONG_MESSAGES:Lcom/navdy/obd/command/ObdCommand;

.field public static final ANY_ECU:I = -0x1

.field public static final AUTO_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field private static final CARRIAGE_RETURN:I = 0xd

.field public static final CLEAR_ALL_PASS_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final DETECT_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final DEVICE_INFO_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final HEADERS_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final HEADERS_ON_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field static final Log:Lorg/slf4j/Logger;

.field static final LogRaw:Lorg/slf4j/Logger;

.field public static final NANOS_PER_MS:J = 0xf4240L

.field public static final NO_DATA:I = -0x80000000

.field public static final NO_FILTER:I = 0x0

.field public static final OK:Ljava/lang/String; = "OK"

.field public static final READ_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final SCAN_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final SLEEP_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final SPACES_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final TIMEOUT_MILLS:J = 0x2710L

.field public static final TURN_OFF_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final TURN_OFF_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

.field public static final TURN_OFF_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

.field public static final TURN_ON_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

.field public static final TURN_ON_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

.field public static final TURN_ON_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

.field public static final UNKNOWN:I = -0x1

.field public static final WAIT:Ljava/lang/String; = "WAIT"

.field private static singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private command:Ljava/lang/String;

.field private commandBytes:[B

.field private conversion:Lcom/navdy/obd/converters/AbstractConversion;

.field private error:Ljava/io/IOException;

.field private expectedResponses:I

.field private name:Ljava/lang/String;

.field private response:Ljava/lang/String;

.field private responses:[Lcom/navdy/obd/EcuResponse;

.field private targetEcu:I

.field private timeoutMillis:J

.field private timeoutNanos:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "ATAL"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->ALLOW_LONG_MESSAGES:Lcom/navdy/obd/command/ObdCommand;

    .line 24
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "SP"

    const-string v2, "atsp A6"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->AUTO_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 26
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STFCP"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->CLEAR_ALL_PASS_FILTERS_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 27
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "Detect"

    const-string v2, "0100"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->DETECT_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 28
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "ATI"

    const-string v2, "ATI"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->DEVICE_INFO_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 29
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "ECHO OFF"

    const-string v2, "ate0"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 30
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "HDR OFF"

    const-string v2, "ath0"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->HEADERS_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 31
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "HDR ON"

    const-string v2, "ath1"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->HEADERS_ON_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 32
    const-class v0, Lcom/navdy/obd/command/ObdCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    .line 33
    const-string v0, "ObdRaw"

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->LogRaw:Lorg/slf4j/Logger;

    .line 38
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "DP"

    const-string v2, "atdpn"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->READ_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 39
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "INIT"

    const-string v2, "atws"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 40
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "SCAN"

    const-string v2, "atma"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->SCAN_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 41
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STSLEEP"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->SLEEP_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 42
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "SPACE"

    const-string v2, "ats0"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->SPACES_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 44
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "CAN Formatting OFF"

    const-string v2, "ATCAF0"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 45
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STSLVG off"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    .line 46
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STSLVL off,off"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    .line 47
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "CAN Formatting OFF"

    const-string v2, "ATCAF1"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    .line 48
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STSLVG on"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    .line 49
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "STSLVL off,on"

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    .line 52
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, p1, p1, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "conversion"    # Lcom/navdy/obd/converters/AbstractConversion;

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput v0, p0, Lcom/navdy/obd/command/ObdCommand;->expectedResponses:I

    .line 77
    iput v0, p0, Lcom/navdy/obd/command/ObdCommand;->targetEcu:I

    .line 78
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutMillis:J

    .line 79
    const-wide v0, 0x2540be400L

    iput-wide v0, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutNanos:J

    .line 80
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->name:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/navdy/obd/command/ObdCommand;->conversion:Lcom/navdy/obd/converters/AbstractConversion;

    .line 82
    invoke-virtual {p0, p2}, Lcom/navdy/obd/command/ObdCommand;->setCommand(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "conversion"    # Lcom/navdy/obd/converters/AbstractConversion;
    .param p4, "timeOut"    # J

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;)V

    .line 71
    iput-wide p4, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutMillis:J

    .line 72
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p4

    iput-wide v0, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutNanos:J

    .line 73
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/obd/command/ObdCommand;[Lcom/navdy/obd/EcuResponse;)[Lcom/navdy/obd/EcuResponse;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;
    .param p1, "x1"    # [Lcom/navdy/obd/EcuResponse;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    return-object p1
.end method

.method static synthetic access$102(Lcom/navdy/obd/command/ObdCommand;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->response:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/obd/command/ObdCommand;)[B
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->commandBytes:[B

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/obd/command/ObdCommand;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;
    .param p1, "x1"    # [B

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->commandBytes:[B

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/obd/command/ObdCommand;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->command:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/obd/command/ObdCommand;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;

    .prologue
    .line 21
    iget v0, p0, Lcom/navdy/obd/command/ObdCommand;->expectedResponses:I

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/obd/command/ObdCommand;Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/command/ObdCommand;
    .param p1, "x1"    # Ljava/io/IOException;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->error:Ljava/io/IOException;

    return-object p1
.end method

.method public static createSetVoltageLevelWakeupTriggerCommand(ZFI)Lcom/navdy/obd/command/ObdCommand;
    .locals 3
    .param p0, "below"    # Z
    .param p1, "voltage"    # F
    .param p2, "seconds"    # I

    .prologue
    .line 66
    new-instance v1, Lcom/navdy/obd/command/ObdCommand;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STSLVLW "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p0, :cond_33

    const-string v0, "<"

    :goto_11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_33
    const-string v0, ">"

    goto :goto_11
.end method


# virtual methods
.method public clone()Lcom/navdy/obd/command/ObdCommand;
    .locals 6

    .prologue
    .line 99
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    iget-object v1, p0, Lcom/navdy/obd/command/ObdCommand;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand;->command:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/obd/command/ObdCommand;->conversion:Lcom/navdy/obd/converters/AbstractConversion;

    iget-wide v4, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutMillis:J

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/obd/converters/AbstractConversion;J)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/navdy/obd/command/ObdCommand;->clone()Lcom/navdy/obd/command/ObdCommand;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 11
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->error:Ljava/io/IOException;

    .line 176
    move-object v2, p1

    .line 177
    .local v2, "inputStream":Ljava/io/InputStream;
    move-object v3, p2

    .line 178
    .local v3, "outputStream":Ljava/io/OutputStream;
    move-object v4, p4

    .line 179
    .local v4, "iObdDataObserver":Lcom/navdy/obd/command/IObdDataObserver;
    move-object v5, p3

    .line 180
    .local v5, "protocol2":Lcom/navdy/obd/Protocol;
    sget-object v10, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/navdy/obd/command/ObdCommand$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/command/ObdCommand$1;-><init>(Lcom/navdy/obd/command/ObdCommand;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/command/IObdDataObserver;Lcom/navdy/obd/Protocol;)V

    invoke-interface {v10, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v9

    .line 207
    .local v9, "future":Ljava/util/concurrent/Future;
    :try_start_13
    invoke-virtual {p0}, Lcom/navdy/obd/command/ObdCommand;->getCommandExecutionTimeOutInMillis()J

    move-result-wide v0

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v9, v0, v1, v10}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->error:Ljava/io/IOException;

    if-eqz v0, :cond_8e

    .line 209
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->error:Ljava/io/IOException;

    throw v0
    :try_end_23
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_23} :catch_23
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_13 .. :try_end_23} :catch_54
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_13 .. :try_end_23} :catch_5f

    .line 211
    :catch_23
    move-exception v6

    .line 212
    .local v6, "e":Ljava/lang/InterruptedException;
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Trying to cancel the task, Success : "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 213
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 214
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    .line 215
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Command IO thread was interrupted"

    invoke-direct {v0, v1, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 216
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catch_54
    move-exception v7

    .line 217
    .local v7, "e2":Ljava/util/concurrent/ExecutionException;
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {v7}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 218
    .end local v7    # "e2":Ljava/util/concurrent/ExecutionException;
    :catch_5f
    move-exception v8

    .line 219
    .local v8, "e3":Ljava/util/concurrent/TimeoutException;
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Trying to cancel the task, Success : "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 220
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 221
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/ObdCommand;->singleThreadedExecutor:Ljava/util/concurrent/ExecutorService;

    .line 222
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v8}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 224
    .end local v8    # "e3":Ljava/util/concurrent/TimeoutException;
    :cond_8e
    return-void
.end method

.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/navdy/obd/command/ObdCommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 172
    return-void
.end method

.method public getByteResponse()[B
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/ObdCommand;->getByteResponse(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getByteResponse(I)[B
    .locals 1
    .param p1, "filter"    # I

    .prologue
    .line 133
    iget v0, p0, Lcom/navdy/obd/command/ObdCommand;->targetEcu:I

    invoke-virtual {p0, v0, p1}, Lcom/navdy/obd/command/ObdCommand;->getEcuResponse(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->command:Ljava/lang/String;

    return-object v0
.end method

.method protected getCommandExecutionTimeOutInMillis()J
    .locals 2

    .prologue
    .line 274
    iget-wide v0, p0, Lcom/navdy/obd/command/ObdCommand;->timeoutMillis:J

    return-wide v0
.end method

.method public getDoubleValue()D
    .locals 5

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/navdy/obd/command/ObdCommand;->getByteResponse()[B

    move-result-object v0

    .line 279
    .local v0, "data":[B
    if-eqz v0, :cond_36

    .line 281
    const/4 v2, 0x0

    :try_start_7
    aget-byte v2, v0, v2

    const/16 v3, 0x41

    if-ne v2, v3, :cond_36

    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand;->conversion:Lcom/navdy/obd/converters/AbstractConversion;

    if-eqz v2, :cond_36

    .line 282
    iget-object v2, p0, Lcom/navdy/obd/command/ObdCommand;->conversion:Lcom/navdy/obd/converters/AbstractConversion;

    invoke-interface {v2, v0}, Lcom/navdy/obd/converters/AbstractConversion;->convert([B)D
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_16} :catch_18

    move-result-wide v2

    .line 288
    :goto_17
    return-wide v2

    .line 284
    :catch_18
    move-exception v1

    .line 285
    .local v1, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception converting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 288
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_36
    const-wide/high16 v2, -0x3e20000000000000L    # -2.147483648E9

    goto :goto_17
.end method

.method public getEcuResponse(I)[B
    .locals 1
    .param p1, "ecu"    # I

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/obd/command/ObdCommand;->getEcuResponse(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getEcuResponse(II)[B
    .locals 8
    .param p1, "ecu"    # I
    .param p2, "filter"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 141
    iget-object v3, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    if-nez v3, :cond_8

    move-object v0, v2

    .line 155
    :cond_7
    :goto_7
    return-object v0

    .line 144
    :cond_8
    iget-object v5, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    array-length v6, v5

    move v3, v4

    :goto_c
    if-ge v3, v6, :cond_24

    aget-object v1, v5, v3

    .line 145
    .local v1, "response":Lcom/navdy/obd/EcuResponse;
    const/4 v7, -0x1

    if-eq p1, v7, :cond_17

    iget v7, v1, Lcom/navdy/obd/EcuResponse;->ecu:I

    if-ne v7, p1, :cond_21

    .line 146
    :cond_17
    iget-object v0, v1, Lcom/navdy/obd/EcuResponse;->data:[B

    .line 147
    .local v0, "data":[B
    if-eqz p2, :cond_7

    .line 150
    if-eqz v0, :cond_21

    aget-byte v7, v0, v4

    if-eq v7, p2, :cond_7

    .line 144
    .end local v0    # "data":[B
    :cond_21
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .end local v1    # "response":Lcom/navdy/obd/EcuResponse;
    :cond_24
    move-object v0, v2

    .line 155
    goto :goto_7
.end method

.method public getIntValue()I
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/navdy/obd/command/ObdCommand;->getDoubleValue()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getResponse(I)Lcom/navdy/obd/EcuResponse;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->response:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseCount()I
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    array-length v0, v0

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public getTargetEcu()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/navdy/obd/command/ObdCommand;->targetEcu:I

    return v0
.end method

.method protected processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 9
    .param p1, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p2, "obdCommandInterpreter"    # Lcom/navdy/obd/io/ObdCommandInterpreter;
    .param p3, "dataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->SLEEP_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    if-ne v6, p0, :cond_46

    .line 229
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Reading response of Sleep command "

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 231
    :try_start_b
    invoke-virtual {p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "response":Ljava/lang/String;
    if-eqz v3, :cond_17

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_2f

    .line 233
    :cond_17
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Empty response for SLEEP command"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 234
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Invalid response while putting chip to sleep"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_26} :catch_26

    .line 240
    .end local v3    # "response":Ljava/lang/String;
    :catch_26
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Interrupted while reading response for sleep command"

    invoke-interface {v6, v7, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 271
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_2e
    :goto_2e
    return-void

    .line 236
    .restart local v3    # "response":Ljava/lang/String;
    :cond_2f
    :try_start_2f
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Response {}"

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 237
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "OK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_43
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_43} :catch_26

    move-result v6

    if-nez v6, :cond_2e

    .line 245
    .end local v3    # "response":Ljava/lang/String;
    :cond_46
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v4, "responseBuilder":Ljava/lang/StringBuilder;
    :try_start_4b
    invoke-virtual {p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->readResponse()Ljava/lang/String;

    move-result-object v3

    .line 248
    .restart local v3    # "response":Ljava/lang/String;
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->LogRaw:Lorg/slf4j/Logger;

    const-string v7, "read: {} "

    invoke-interface {v6, v7, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    invoke-virtual {p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse()Z

    move-result v5

    .line 250
    .local v5, "textResponse":Z
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    if-ne p0, v6, :cond_73

    .line 252
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Reset Command, Clearing the terminal"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V
    :try_end_68
    .catch Ljava/lang/InterruptedException; {:try_start_4b .. :try_end_68} :catch_89

    .line 254
    :try_start_68
    invoke-virtual {p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->clearTerminal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->didGetTextResponse()Z
    :try_end_72
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_68 .. :try_end_72} :catch_8b
    .catch Ljava/lang/Throwable; {:try_start_68 .. :try_end_72} :catch_94
    .catch Ljava/lang/InterruptedException; {:try_start_68 .. :try_end_72} :catch_89

    move-result v5

    .line 264
    :cond_73
    :try_start_73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/obd/command/ObdCommand;->response:Ljava/lang/String;

    .line 265
    if-eqz p3, :cond_80

    .line 266
    iget-object v6, p0, Lcom/navdy/obd/command/ObdCommand;->response:Ljava/lang/String;

    invoke-interface {p3, v6}, Lcom/navdy/obd/command/IObdDataObserver;->onResponse(Ljava/lang/String;)V

    .line 268
    :cond_80
    iget-object v6, p0, Lcom/navdy/obd/command/ObdCommand;->response:Ljava/lang/String;

    invoke-static {v5, v6, p1}, Lcom/navdy/obd/ResponseParser;->parse(ZLjava/lang/String;Lcom/navdy/obd/Protocol;)[Lcom/navdy/obd/EcuResponse;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/obd/command/ObdCommand;->responses:[Lcom/navdy/obd/EcuResponse;

    goto :goto_2e

    .line 269
    .end local v3    # "response":Ljava/lang/String;
    .end local v5    # "textResponse":Z
    :catch_89
    move-exception v6

    goto :goto_2e

    .line 256
    .restart local v3    # "response":Ljava/lang/String;
    .restart local v5    # "textResponse":Z
    :catch_8b
    move-exception v2

    .line 257
    .local v2, "e3":Ljava/util/concurrent/TimeoutException;
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Clearing terminal timed out"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2e

    .line 259
    .end local v2    # "e3":Ljava/util/concurrent/TimeoutException;
    :catch_94
    move-exception v1

    .line 260
    .local v1, "e2":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/obd/command/ObdCommand;->Log:Lorg/slf4j/Logger;

    const-string v7, "Interrupted while clearing the terminal "

    invoke-interface {v6, v7, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9c
    .catch Ljava/lang/InterruptedException; {:try_start_73 .. :try_end_9c} :catch_89

    goto :goto_2e
.end method

.method public setCommand(Ljava/lang/String;)V
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/obd/command/ObdCommand;->command:Ljava/lang/String;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->commandBytes:[B

    .line 96
    return-void
.end method

.method public setExpectedResponses(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 107
    const/16 v0, 0x9

    if-gt p1, v0, :cond_9

    if-eqz p1, :cond_9

    const/4 v0, -0x1

    if-ge p1, v0, :cond_22

    .line 108
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid expected count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_22
    iput p1, p0, Lcom/navdy/obd/command/ObdCommand;->expectedResponses:I

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/command/ObdCommand;->commandBytes:[B

    .line 112
    return-void
.end method

.method public setTargetEcu(I)V
    .locals 1
    .param p1, "ecuAddress"    # I

    .prologue
    .line 115
    iget v0, p0, Lcom/navdy/obd/command/ObdCommand;->targetEcu:I

    if-eq p1, v0, :cond_6

    .line 116
    iput p1, p0, Lcom/navdy/obd/command/ObdCommand;->targetEcu:I

    .line 118
    :cond_6
    return-void
.end method
