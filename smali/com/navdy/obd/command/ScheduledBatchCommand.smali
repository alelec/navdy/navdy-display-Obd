.class public Lcom/navdy/obd/command/ScheduledBatchCommand;
.super Lcom/navdy/obd/command/BatchCommand;
.source "ScheduledBatchCommand.java"


# instance fields
.field public samples:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/command/Sample;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;[Lcom/navdy/obd/command/ICommand;)V
    .locals 6
    .param p2, "commands"    # [Lcom/navdy/obd/command/ICommand;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ScanSchedule$Scan;",
            ">;[",
            "Lcom/navdy/obd/command/ICommand;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "scanList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ScanSchedule$Scan;>;"
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/obd/command/BatchCommand;-><init>(Ljava/util/List;)V

    .line 26
    if-eqz p1, :cond_0

    array-length v2, p2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 27
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "scanList length must match commands length"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 30
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    .line 31
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 32
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 33
    .local v1, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    iget-object v2, p0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    new-instance v3, Lcom/navdy/obd/command/Sample;

    iget v4, v1, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    iget v5, v1, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    invoke-direct {v3, v4, v5}, Lcom/navdy/obd/command/Sample;-><init>(II)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    .end local v1    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_2
    return-void
.end method


# virtual methods
.method public execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 26
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p4, "commandObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    const-wide/16 v14, 0x2710

    .line 48
    .local v14, "nextSampleTime":J
    const/4 v12, 0x1

    .line 50
    .local v12, "needToSleep":Z
    :cond_0
    :goto_0
    if-eqz v12, :cond_5

    .line 51
    const/4 v8, 0x0

    .local v8, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/ScheduledBatchCommand;->commands:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "l":I
    :goto_1
    if-ge v8, v9, :cond_4

    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/navdy/obd/command/Sample;

    .line 53
    .local v13, "sample":Lcom/navdy/obd/command/Sample;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/obd/command/ScheduledBatchCommand;->commands:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/obd/command/ICommand;

    .line 54
    .local v4, "command":Lcom/navdy/obd/command/ICommand;
    iget-wide v10, v13, Lcom/navdy/obd/command/Sample;->lastSampleTimestamp:J

    .line 55
    .local v10, "lastReading":J
    invoke-virtual {v13}, Lcom/navdy/obd/command/Sample;->getScanInterval()I

    move-result v16

    .line 56
    .local v16, "scanInterval":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 57
    .local v18, "startTime":J
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v22, v0

    sub-long v24, v18, v10

    sub-long v20, v22, v24

    .line 58
    .local v20, "timeToWait":J
    const-wide/16 v22, 0x0

    cmp-long v17, v10, v22

    if-eqz v17, :cond_1

    const-wide/16 v22, 0x0

    cmp-long v17, v20, v22

    if-gtz v17, :cond_2

    .line 59
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-interface {v4, v0, v1, v2, v3}, Lcom/navdy/obd/command/ICommand;->execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 61
    .local v6, "finishTime":J
    move-wide/from16 v0, v18

    iput-wide v0, v13, Lcom/navdy/obd/command/Sample;->lastSampleTimestamp:J

    .line 62
    sub-long v22, v6, v18

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Lcom/navdy/obd/command/Sample;->setLastSamplingTime(J)V

    .line 63
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v13, Lcom/navdy/obd/command/Sample;->updated:Z

    .line 64
    const/4 v12, 0x0

    .line 51
    .end local v6    # "finishTime":J
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 66
    :cond_2
    cmp-long v17, v20, v14

    if-gez v17, :cond_3

    .line 67
    move-wide/from16 v14, v20

    .line 69
    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v13, Lcom/navdy/obd/command/Sample;->updated:Z

    goto :goto_2

    .line 72
    .end local v4    # "command":Lcom/navdy/obd/command/ICommand;
    .end local v10    # "lastReading":J
    .end local v13    # "sample":Lcom/navdy/obd/command/Sample;
    .end local v16    # "scanInterval":I
    .end local v18    # "startTime":J
    .end local v20    # "timeToWait":J
    :cond_4
    if-eqz v12, :cond_0

    .line 74
    :try_start_0
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v5

    .line 76
    .local v5, "e":Ljava/lang/InterruptedException;
    const/4 v12, 0x0

    .line 77
    goto/16 :goto_0

    .line 80
    .end local v5    # "e":Ljava/lang/InterruptedException;
    .end local v8    # "i":I
    .end local v9    # "l":I
    :cond_5
    return-void
.end method

.method public getPids()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 39
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 40
    iget-object v2, p0, Lcom/navdy/obd/command/ScheduledBatchCommand;->samples:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/command/Sample;

    iget-object v2, v2, Lcom/navdy/obd/command/Sample;->pid:Lcom/navdy/obd/Pid;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    return-object v1
.end method
