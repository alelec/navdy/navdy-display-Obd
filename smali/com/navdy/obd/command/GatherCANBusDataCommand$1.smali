.class final Lcom/navdy/obd/command/GatherCANBusDataCommand$1;
.super Lcom/navdy/obd/command/ObdCommand;
.source "GatherCANBusDataCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/command/GatherCANBusDataCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 1
    .param p1, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p2, "obdCommandInterpreter"    # Lcom/navdy/obd/io/ObdCommandInterpreter;
    .param p3, "dataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p2, Lcom/navdy/obd/io/ObdCommandInterpreter;->debug:Z

    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/obd/command/ObdCommand;->processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V

    .line 59
    return-void
.end method
