.class public Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;
.super Ljava/lang/Object;
.source "GatherCANBusDataCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/command/GatherCANBusDataCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LastSampleStats"
.end annotation


# instance fields
.field public dataSizeCaptured:J

.field public succeeded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->dataSizeCaptured:J

    .line 41
    return-void
.end method
