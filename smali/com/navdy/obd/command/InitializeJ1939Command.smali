.class public Lcom/navdy/obd/command/InitializeJ1939Command;
.super Lcom/navdy/obd/command/BaseSTNInitializeCommand;
.source "InitializeJ1939Command.java"


# static fields
.field private static setProtocolCommand:Lcom/navdy/obd/command/ObdCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 10
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "SetProtocol"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "atsp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/obd/command/InitializeJ1939Command;->setProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/obd/command/ICommand;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->SPACES_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/BaseSTNInitializeCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 17
    sget-object v0, Lcom/navdy/obd/command/InitializeJ1939Command;->setProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/InitializeJ1939Command;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 18
    return-void
.end method
