.class public Lcom/navdy/obd/command/InitializeCommand;
.super Lcom/navdy/obd/command/BaseSTNInitializeCommand;
.source "InitializeCommand.java"


# instance fields
.field private readProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

.field private vinCommand:Lcom/navdy/obd/command/VinCommand;


# direct methods
.method public constructor <init>(Lcom/navdy/obd/Protocol;)V
    .locals 4
    .param p1, "protocol"    # Lcom/navdy/obd/Protocol;

    .prologue
    .line 13
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/obd/command/ICommand;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_DELTA_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->RESET_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->ECHO_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->HEADERS_ON_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->SPACES_OFF_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->TURN_ON_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->TURN_OFF_VOLTAGE_LEVEL_WAKEUP:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/obd/command/ObdCommand;->ALLOW_LONG_MESSAGES:Lcom/navdy/obd/command/ObdCommand;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/navdy/obd/command/BaseSTNInitializeCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    .line 9
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->READ_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    iput-object v0, p0, Lcom/navdy/obd/command/InitializeCommand;->readProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

    .line 10
    new-instance v0, Lcom/navdy/obd/command/VinCommand;

    invoke-direct {v0}, Lcom/navdy/obd/command/VinCommand;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/command/InitializeCommand;->vinCommand:Lcom/navdy/obd/command/VinCommand;

    .line 22
    if-eqz p1, :cond_0

    .line 23
    new-instance v0, Lcom/navdy/obd/command/ObdCommand;

    const-string v1, "SetProtocol"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "atsp A"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/obd/Protocol;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/InitializeCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 27
    :goto_0
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->DETECT_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/InitializeCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 28
    iget-object v0, p0, Lcom/navdy/obd/command/InitializeCommand;->readProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/InitializeCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 29
    return-void

    .line 25
    :cond_0
    sget-object v0, Lcom/navdy/obd/command/ObdCommand;->AUTO_PROTOCOL_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {p0, v0}, Lcom/navdy/obd/command/InitializeCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    goto :goto_0
.end method


# virtual methods
.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/obd/command/InitializeCommand;->readProtocolCommand:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v0}, Lcom/navdy/obd/command/ObdCommand;->getResponse()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
