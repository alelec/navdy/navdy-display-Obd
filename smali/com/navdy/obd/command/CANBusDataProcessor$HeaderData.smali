.class Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;
.super Ljava/lang/Object;
.source "CANBusDataProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/command/CANBusDataProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HeaderData"
.end annotation


# instance fields
.field count:I

.field descriptorsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/obd/can/CANBusDataDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field header:Ljava/lang/String;

.field interval:I

.field lastProcessedTime:J

.field length:I

.field final synthetic this$0:Lcom/navdy/obd/command/CANBusDataProcessor;


# direct methods
.method constructor <init>(Lcom/navdy/obd/command/CANBusDataProcessor;)V
    .locals 2
    .param p1, "this$0"    # Lcom/navdy/obd/command/CANBusDataProcessor;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->this$0:Lcom/navdy/obd/command/CANBusDataProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->count:I

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/obd/command/CANBusDataProcessor$HeaderData;->lastProcessedTime:J

    return-void
.end method
