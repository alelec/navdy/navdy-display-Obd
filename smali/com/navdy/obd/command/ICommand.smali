.class public interface abstract Lcom/navdy/obd/command/ICommand;
.super Ljava/lang/Object;
.source "ICommand.java"


# virtual methods
.method public abstract execute(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/command/IObdDataObserver;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubleValue()D
.end method

.method public abstract getIntValue()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getResponse()Ljava/lang/String;
.end method
