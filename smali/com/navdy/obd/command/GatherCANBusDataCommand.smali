.class public Lcom/navdy/obd/command/GatherCANBusDataCommand;
.super Lcom/navdy/obd/command/ObdCommand;
.source "GatherCANBusDataCommand.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;
    }
.end annotation


# static fields
.field public static final DEFAULT_MONITORING_DURATION:I = 0x7d0

.field public static final DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

.field public static final GATHER_CAN_BUS_DATA_PID:I = 0x3e8

.field static final Log:Lorg/slf4j/Logger;

.field public static final MAX_RETRY_COUNT:I = 0x3

.field public static final MINIMUM_DATA_REQUIRED:I = 0x64

.field public static final MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

.field public static final MONITOR_COMMAND_EXECUTION_TIMEOUT:I = 0x1388

.field public static final READ_TIME_OUT_MILLIS:I = 0x3e8

.field public static final TIME_OUT_TO_:I = 0x7d0


# instance fields
.field baos:Ljava/io/ByteArrayOutputStream;

.field buffer:[B

.field private errorData:Ljava/lang/String;

.field private failureCount:I

.field private lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

.field private monitorDurationMilliseconds:J

.field private monitorDurationNanos:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 20
    const-class v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    .line 48
    new-instance v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;

    const-wide/16 v2, 0x7d0

    invoke-direct {v0, v2, v3}, Lcom/navdy/obd/command/GatherCANBusDataCommand;-><init>(J)V

    sput-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    .line 49
    new-instance v0, Lcom/navdy/obd/command/BatchCommand;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/navdy/obd/command/ICommand;

    invoke-direct {v0, v1}, Lcom/navdy/obd/command/BatchCommand;-><init>([Lcom/navdy/obd/command/ICommand;)V

    sput-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

    .line 52
    sget-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

    sget-object v1, Lcom/navdy/obd/command/GatherCANBusDataCommand;->TURN_OFF_FORMATTING_COMMAND:Lcom/navdy/obd/command/ObdCommand;

    invoke-virtual {v0, v1}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 53
    sget-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

    sget-object v1, Lcom/navdy/obd/command/GatherCANBusDataCommand;->DEFAULT_MONITOR_COMMAND:Lcom/navdy/obd/command/GatherCANBusDataCommand;

    invoke-virtual {v0, v1}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 54
    sget-object v0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->MONITOR_COMMAND:Lcom/navdy/obd/command/BatchCommand;

    new-instance v1, Lcom/navdy/obd/command/GatherCANBusDataCommand$1;

    const-string v2, "Turn on formating"

    const-string v3, "ATCAF1"

    invoke-direct {v1, v2, v3}, Lcom/navdy/obd/command/GatherCANBusDataCommand$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/obd/command/BatchCommand;->add(Lcom/navdy/obd/command/ICommand;)V

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 64
    const-wide/16 v0, 0x7d0

    invoke-direct {p0, v0, v1}, Lcom/navdy/obd/command/GatherCANBusDataCommand;-><init>(J)V

    .line 65
    return-void
.end method

.method public constructor <init>(J)V
    .locals 5
    .param p1, "durationMillis"    # J

    .prologue
    const-wide/16 v2, 0x3e8

    .line 68
    const-string v0, "CAN Monitoring"

    const-string v1, "ATMA"

    invoke-direct {p0, v0, v1}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->errorData:Ljava/lang/String;

    .line 44
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->buffer:[B

    .line 46
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const v1, 0x7d000

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->baos:Ljava/io/ByteArrayOutputStream;

    .line 69
    new-instance v0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    invoke-direct {v0}, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    .line 70
    iput-wide p1, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->monitorDurationMilliseconds:J

    .line 71
    iget-wide v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->monitorDurationMilliseconds:J

    mul-long/2addr v0, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->monitorDurationNanos:J

    .line 72
    return-void
.end method


# virtual methods
.method protected getCommandExecutionTimeOutInMillis()J
    .locals 2

    .prologue
    .line 144
    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method public getErrorData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->errorData:Ljava/lang/String;

    return-object v0
.end method

.method public getLastSampleDataSize()J
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    iget-wide v0, v0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->dataSizeCaptured:J

    return-wide v0
.end method

.method public getLastSampleSucceeded()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    iget-boolean v0, v0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    return v0
.end method

.method public isMonitoringFailureDetected()Z
    .locals 2

    .prologue
    .line 148
    iget v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected processCommandResponse(Lcom/navdy/obd/Protocol;Lcom/navdy/obd/io/ObdCommandInterpreter;Lcom/navdy/obd/command/IObdDataObserver;)V
    .locals 15
    .param p1, "protocol"    # Lcom/navdy/obd/Protocol;
    .param p2, "obdCommandInterpreter"    # Lcom/navdy/obd/io/ObdCommandInterpreter;
    .param p3, "dataObserver"    # Lcom/navdy/obd/command/IObdDataObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 77
    .local v8, "start":J
    const-wide/16 v4, 0x0

    .line 78
    .local v4, "elapsedTime":J
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Starting the CAN bus monitoring"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 79
    const/16 v11, 0x3e8

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/navdy/obd/io/ObdCommandInterpreter;->setReadTimeOut(I)V

    .line 80
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 81
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    .line 82
    const/4 v10, 0x0

    .line 83
    .local v10, "totalLength":I
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    sub-long v4, v12, v8

    iget-wide v12, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->monitorDurationNanos:J

    cmp-long v11, v4, v12

    if-gez v11, :cond_1

    .line 85
    :try_start_0
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->buffer:[B

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/navdy/obd/io/ObdCommandInterpreter;->read([B)I

    move-result v6

    .line 86
    .local v6, "length":I
    if-lez v6, :cond_0

    .line 87
    add-int/2addr v10, v6

    .line 88
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->baos:Ljava/io/ByteArrayOutputStream;

    iget-object v12, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->buffer:[B

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 90
    .end local v6    # "length":I
    :catch_0
    move-exception v3

    .line 91
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Interrupted exception {}"

    invoke-interface {v11, v12, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :goto_1
    iget v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    if-lez v11, :cond_2

    const/16 v11, 0x64

    if-le v10, v11, :cond_2

    .line 100
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->errorData:Ljava/lang/String;

    .line 101
    const/4 v11, 0x0

    iput v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    .line 103
    :cond_2
    const/16 v11, 0x64

    if-ge v10, v11, :cond_4

    .line 104
    if-nez v10, :cond_3

    .line 105
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "No data read from the CAN bus, marking as failed"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 113
    :goto_2
    iget v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    .line 121
    :goto_3
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 139
    :goto_4
    return-void

    .line 93
    :catch_1
    move-exception v3

    .line 94
    .local v3, "e":Ljava/io/IOException;
    iget v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    .line 95
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "IOException exception {}"

    invoke-interface {v11, v12, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    .line 108
    .local v7, "newData":Ljava/lang/String;
    const/16 v11, 0xd

    const/16 v12, 0xa

    invoke-virtual {v7, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    .line 109
    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Lcom/navdy/obd/command/IObdDataObserver;->onRawCanBusMessage(Ljava/lang/String;)V

    .line 110
    iput-object v7, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->errorData:Ljava/lang/String;

    .line 111
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Very less data read from the CAN bus, marking as failed {}"

    invoke-interface {v11, v12, v7}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 115
    .end local v7    # "newData":Ljava/lang/String;
    :cond_4
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    .line 116
    .restart local v7    # "newData":Ljava/lang/String;
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Total length {}, String length : {}"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v11, v12, v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 117
    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Lcom/navdy/obd/command/IObdDataObserver;->onRawCanBusMessage(Ljava/lang/String;)V

    .line 118
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    .line 119
    iget-object v11, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    int-to-long v12, v10

    iput-wide v12, v11, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->dataSizeCaptured:J

    goto :goto_3

    .line 125
    .end local v7    # "newData":Ljava/lang/String;
    :cond_5
    const/4 v11, 0x6

    new-array v2, v11, [B

    fill-array-data v2, :array_0

    .line 126
    .local v2, "at":[B
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->write([B)V

    .line 128
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Clearing the terminal"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 130
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Lcom/navdy/obd/io/ObdCommandInterpreter;->clearTerminalBulk()V

    .line 131
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Terminal clear"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_4

    .line 132
    :catch_2
    move-exception v3

    .line 133
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 134
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Interrupted while clearing terminal"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_4

    .line 135
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v3

    .line 136
    .local v3, "e":Ljava/util/concurrent/TimeoutException;
    invoke-virtual {v3}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    .line 137
    sget-object v11, Lcom/navdy/obd/command/GatherCANBusDataCommand;->Log:Lorg/slf4j/Logger;

    const-string v12, "Timed out while clearing terminal"

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 125
    :array_0
    .array-data 1
        0x41t
        0x54t
        0x49t
        0x49t
        0x49t
        0xdt
    .end array-data
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    iput v1, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->failureCount:I

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->errorData:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    iput-boolean v1, v0, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->succeeded:Z

    .line 155
    return-void
.end method

.method public resetLastSampleStats()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/navdy/obd/command/GatherCANBusDataCommand;->lastSampleStats:Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;

    invoke-virtual {v0}, Lcom/navdy/obd/command/GatherCANBusDataCommand$LastSampleStats;->reset()V

    .line 167
    return-void
.end method
