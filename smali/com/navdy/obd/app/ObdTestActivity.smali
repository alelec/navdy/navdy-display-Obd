.class public Lcom/navdy/obd/app/ObdTestActivity;
.super Landroid/app/Activity;
.source "ObdTestActivity.java"


# static fields
.field private static final D:Z = true

.field private static final DISCONNECT_CMD:Ljava/lang/String; = "disconnect"

.field private static final LOG:Lorg/slf4j/Logger;

.field public static final MESSAGE_READ:I = 0x2

.field public static final MESSAGE_STATE_CHANGE:I = 0x1

.field public static final MESSAGE_TOAST:I = 0x4

.field public static final MESSAGE_WRITE:I = 0x3

.field private static final RAW_CMD:Ljava/lang/String; = "raw"

.field private static final READ_CMD:Ljava/lang/String; = "read "

.field private static final REQUEST_CONNECT_DEVICE:I = 0x1

.field private static final REQUEST_ENABLE_BT:I = 0x2

.field private static final RESET_CMD:Ljava/lang/String; = "reset"

.field private static final SCAN_CMD:Ljava/lang/String; = "scan"

.field private static final TAG:Ljava/lang/String; = "ObdTestApp"

.field public static final TOAST:Ljava/lang/String; = "toast"


# instance fields
.field private carApi:Lcom/navdy/obd/ICarService;

.field private carServiceConnection:Landroid/content/ServiceConnection;

.field private mConversationArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mConversationView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0a000e
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field protected mOutEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0a000f
    .end annotation
.end field

.field private mOutStringBuffer:Ljava/lang/StringBuffer;

.field protected mSendButton:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0a0010
    .end annotation
.end field

.field private mWriteListener:Landroid/widget/TextView$OnEditorActionListener;

.field private obdApi:Lcom/navdy/obd/IObdService;

.field private obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

.field private serviceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/app/ObdTestActivity;->LOG:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutStringBuffer:Ljava/lang/StringBuffer;

    .line 119
    new-instance v0, Lcom/navdy/obd/app/ObdTestActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/ObdTestActivity$1;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    .line 172
    new-instance v0, Lcom/navdy/obd/app/ObdTestActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/ObdTestActivity$2;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->serviceConnection:Landroid/content/ServiceConnection;

    .line 185
    new-instance v0, Lcom/navdy/obd/app/ObdTestActivity$3;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/ObdTestActivity$3;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->carServiceConnection:Landroid/content/ServiceConnection;

    .line 320
    new-instance v0, Lcom/navdy/obd/app/ObdTestActivity$5;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/ObdTestActivity$5;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mWriteListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 334
    new-instance v0, Lcom/navdy/obd/app/ObdTestActivity$6;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/ObdTestActivity$6;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/obd/app/ObdTestActivity;Lcom/navdy/obd/IObdService;)Lcom/navdy/obd/IObdService;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/app/ObdTestActivity;
    .param p1, "x1"    # Lcom/navdy/obd/IObdService;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    return-object p1
.end method

.method static synthetic access$202(Lcom/navdy/obd/app/ObdTestActivity;Lcom/navdy/obd/ICarService;)Lcom/navdy/obd/ICarService;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/app/ObdTestActivity;
    .param p1, "x1"    # Lcom/navdy/obd/ICarService;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity;->carApi:Lcom/navdy/obd/ICarService;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/obd/app/ObdTestActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/app/ObdTestActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/obd/app/ObdTestActivity;->sendMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mConversationArrayAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private sendMessage(Ljava/lang/String;)V
    .locals 10
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    .line 269
    const/4 v5, 0x0

    .line 272
    .local v5, "state":I
    :try_start_0
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v6}, Lcom/navdy/obd/IObdService;->getState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 276
    :goto_0
    if-eq v5, v8, :cond_1

    .line 277
    invoke-static {p0}, Lcom/navdy/obd/app/Utility;->toastNotConnectedMessage(Landroid/content/Context;)V

    .line 317
    :cond_0
    :goto_1
    return-void

    .line 273
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Landroid/os/RemoteException;
    const-string v6, "ObdTestApp"

    const-string v7, "Failed to read service state"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 284
    :try_start_1
    const-string v6, "scan"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 285
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 286
    .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v6, Lcom/navdy/obd/Pid;

    const/16 v7, 0xd

    invoke-direct {v6, v7}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    new-instance v6, Lcom/navdy/obd/Pid;

    const/4 v7, 0x5

    invoke-direct {v6, v7}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    new-instance v6, Lcom/navdy/obd/Pid;

    const/16 v7, 0xc

    invoke-direct {v6, v7}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    new-instance v6, Lcom/navdy/obd/Pid;

    const/16 v7, 0x10

    invoke-direct {v6, v7}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    const/4 v7, 0x1

    invoke-interface {v6, v3, v7}, Lcom/navdy/obd/IObdService;->scanPids(Ljava/util/List;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 314
    .end local v3    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutStringBuffer:Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 315
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutStringBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 292
    :cond_3
    :try_start_2
    const-string v6, "raw"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 293
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v6}, Lcom/navdy/obd/IObdService;->startRawScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 309
    :catch_1
    move-exception v1

    .line 310
    .restart local v1    # "e":Landroid/os/RemoteException;
    const-string v6, "ObdTestApp"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to send "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " command"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 294
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_4
    :try_start_3
    const-string v6, "read "

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 295
    const-string v6, "read "

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 296
    .local v2, "pid":I
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v6, v2}, Lcom/navdy/obd/IObdService;->readPid(I)Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, "response":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 298
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 299
    .local v0, "bytes":[B
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    array-length v8, v0

    const/4 v9, -0x1

    invoke-virtual {v6, v7, v8, v9, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 300
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 302
    .end local v0    # "bytes":[B
    .end local v2    # "pid":I
    .end local v4    # "response":Ljava/lang/String;
    :cond_5
    const-string v6, "reset"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 303
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v6}, Lcom/navdy/obd/IObdService;->reset()V

    goto :goto_2

    .line 304
    :cond_6
    const-string v6, "disconnect"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 305
    iget-object v6, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v6}, Lcom/navdy/obd/IObdService;->disconnect()V

    goto/16 :goto_2

    .line 307
    :cond_7
    const-string v6, "ObdTestApp"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown command "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2
.end method

.method private setupChat()V
    .locals 2

    .prologue
    .line 213
    const-string v0, "ObdTestApp"

    const-string v1, "setupChat()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030005

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mConversationArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 217
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mConversationView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/navdy/obd/app/ObdTestActivity;->mConversationArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 220
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/obd/app/ObdTestActivity;->mWriteListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 223
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mSendButton:Landroid/widget/Button;

    new-instance v1, Lcom/navdy/obd/app/ObdTestActivity$4;

    invoke-direct {v1, p0}, Lcom/navdy/obd/app/ObdTestActivity$4;-><init>(Lcom/navdy/obd/app/ObdTestActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutStringBuffer:Ljava/lang/StringBuffer;

    .line 234
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, -0x1

    .line 375
    const-string v2, "ObdTestApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    packed-switch p1, :pswitch_data_0

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 379
    :pswitch_0
    if-ne p2, v5, :cond_0

    .line 381
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lcom/navdy/obd/app/DeviceListActivity;->EXTRA_DEVICE_ADDRESS:Ljava/lang/String;

    .line 382
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "address":Ljava/lang/String;
    const-string v2, "ObdTestApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Attempting to connect to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :try_start_0
    iget-object v2, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    iget-object v3, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    invoke-interface {v2, v0, v3}, Lcom/navdy/obd/IObdService;->connect(Ljava/lang/String;Lcom/navdy/obd/IObdServiceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "ObdTestApp"

    const-string v3, "Failed to add listener"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 394
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_1
    if-ne p2, v5, :cond_1

    .line 396
    invoke-direct {p0}, Lcom/navdy/obd/app/ObdTestActivity;->setupChat()V

    goto :goto_0

    .line 399
    :cond_1
    const-string v2, "ObdTestApp"

    const-string v3, "BT not enabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    const v2, 0x7f070003

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 401
    invoke-virtual {p0}, Lcom/navdy/obd/app/ObdTestActivity;->finish()V

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const-string v0, "ObdTestApp"

    const-string v1, "+++ ON CREATE +++"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    sget-object v0, Lcom/navdy/obd/app/ObdTestActivity;->LOG:Lorg/slf4j/Logger;

    const-string v1, "Testing logback logging"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 100
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/ObdTestActivity;->setContentView(I)V

    .line 101
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/app/Activity;)V

    .line 117
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/navdy/obd/app/ObdTestActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 409
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f090001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 410
    const/4 v1, 0x1

    return v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 250
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 253
    const-string v1, "ObdTestApp"

    const-string v2, "--- ON DESTROY ---"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdApi:Lcom/navdy/obd/IObdService;

    iget-object v2, p0, Lcom/navdy/obd/app/ObdTestActivity;->obdListener:Lcom/navdy/obd/IObdServiceListener$Stub;

    invoke-interface {v1, v2}, Lcom/navdy/obd/IObdService;->removeListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 257
    iget-object v1, p0, Lcom/navdy/obd/app/ObdTestActivity;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/navdy/obd/app/ObdTestActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "ObdTestApp"

    const-string v2, "Failed to disconnect from service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 415
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v2, v3

    .line 436
    :goto_0
    return v2

    .line 418
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/navdy/obd/app/DeviceListActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    .local v1, "serverIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1, v2}, Lcom/navdy/obd/app/ObdTestActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 423
    .end local v1    # "serverIntent":Landroid/content/Intent;
    :pswitch_1
    invoke-static {p0}, Lcom/navdy/obd/ObdServiceInterface;->startObdService(Landroid/content/Context;)V

    goto :goto_0

    .line 426
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/navdy/obd/app/UpgradeObdActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 427
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/ObdTestActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 430
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_3
    invoke-static {p0, v2}, Lcom/navdy/obd/app/UpgradeObdActivity;->flash(Landroid/content/Context;Z)V

    goto :goto_0

    .line 433
    :pswitch_4
    invoke-static {p0, v3}, Lcom/navdy/obd/app/UpgradeObdActivity;->flash(Landroid/content/Context;Z)V

    goto :goto_0

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0012
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public declared-synchronized onPause()V
    .locals 2

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 239
    const-string v0, "ObdTestApp"

    const-string v1, "- ON PAUSE -"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    monitor-exit p0

    return-void

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 2

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 209
    const-string v0, "ObdTestApp"

    const-string v1, "+ ON RESUME +"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 201
    const-string v0, "ObdTestApp"

    const-string v1, "++ ON START ++"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutStringBuffer:Ljava/lang/StringBuffer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/obd/app/ObdTestActivity;->setupChat()V

    .line 204
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 244
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 245
    const-string v0, "ObdTestApp"

    const-string v1, "-- ON STOP --"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    return-void
.end method
