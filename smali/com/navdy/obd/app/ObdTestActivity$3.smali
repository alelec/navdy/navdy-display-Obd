.class Lcom/navdy/obd/app/ObdTestActivity$3;
.super Ljava/lang/Object;
.source "ObdTestActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/app/ObdTestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/app/ObdTestActivity;


# direct methods
.method constructor <init>(Lcom/navdy/obd/app/ObdTestActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity$3;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 188
    const-string v0, "ObdTestApp"

    const-string v1, "CarService connection established"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity$3;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {p2}, Lcom/navdy/obd/ICarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/ICarService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/obd/app/ObdTestActivity;->access$202(Lcom/navdy/obd/app/ObdTestActivity;Lcom/navdy/obd/ICarService;)Lcom/navdy/obd/ICarService;

    .line 190
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 194
    const-string v0, "ObdTestApp"

    const-string v1, "CarService disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    return-void
.end method
