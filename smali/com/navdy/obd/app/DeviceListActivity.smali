.class public Lcom/navdy/obd/app/DeviceListActivity;
.super Landroid/app/Activity;
.source "DeviceListActivity.java"

# interfaces
.implements Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;


# static fields
.field private static final D:Z = true

.field public static EXTRA_DEVICE_ADDRESS:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "DeviceListActivity"


# instance fields
.field private discoveredDevices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mNewDevicesArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPairedDevicesArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/navdy/obd/io/ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private scanner:Lcom/navdy/obd/discovery/GroupScanner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "device_address"

    sput-object v0, Lcom/navdy/obd/app/DeviceListActivity;->EXTRA_DEVICE_ADDRESS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 133
    new-instance v0, Lcom/navdy/obd/app/DeviceListActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/obd/app/DeviceListActivity$2;-><init>(Lcom/navdy/obd/app/DeviceListActivity;)V

    iput-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/obd/app/DeviceListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/obd/app/DeviceListActivity;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/navdy/obd/app/DeviceListActivity;->doDiscovery()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/obd/app/DeviceListActivity;)Lcom/navdy/obd/discovery/GroupScanner;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/obd/app/DeviceListActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    return-object v0
.end method

.method private doDiscovery()V
    .locals 2

    .prologue
    .line 120
    const-string v0, "DeviceListActivity"

    const-string v1, "doDiscovery()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 124
    const v0, 0x7f07000e

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->setTitle(I)V

    .line 127
    const v0, 0x7f0a000a

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v0}, Lcom/navdy/obd/discovery/GroupScanner;->startScan()Z

    .line 130
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f0a000b

    const v9, 0x7f0a0008

    const v8, 0x7f030003

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const/4 v7, 0x5

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->requestWindowFeature(I)Z

    .line 66
    const v7, 0x7f030002

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->setContentView(I)V

    .line 69
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->setResult(I)V

    .line 72
    const v7, 0x7f0a000d

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 73
    .local v6, "scanButton":Landroid/widget/Button;
    new-instance v7, Lcom/navdy/obd/app/DeviceListActivity$1;

    invoke-direct {v7, p0}, Lcom/navdy/obd/app/DeviceListActivity$1;-><init>(Lcom/navdy/obd/app/DeviceListActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->discoveredDevices:Ljava/util/HashSet;

    .line 84
    new-instance v7, Landroid/widget/ArrayAdapter;

    invoke-direct {v7, p0, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mPairedDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 85
    new-instance v7, Landroid/widget/ArrayAdapter;

    invoke-direct {v7, p0, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mNewDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 88
    invoke-virtual {p0, v9}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 89
    .local v5, "pairedListView":Landroid/widget/ListView;
    iget-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mPairedDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    iget-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    invoke-virtual {p0, v10}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 94
    .local v2, "newDevicesListView":Landroid/widget/ListView;
    iget-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mNewDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    iget-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 98
    new-instance v7, Lcom/navdy/obd/discovery/GroupScanner;

    invoke-direct {v7, p0, p0}, Lcom/navdy/obd/discovery/GroupScanner;-><init>(Landroid/content/Context;Lcom/navdy/obd/discovery/ObdChannelScanner$Listener;)V

    iput-object v7, p0, Lcom/navdy/obd/app/DeviceListActivity;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    .line 101
    const v7, 0x7f0a0009

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 102
    .local v4, "pairedEmptyView":Landroid/view/View;
    invoke-virtual {p0, v9}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 103
    .local v3, "pairedDeviceList":Landroid/widget/ListView;
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 104
    const v7, 0x7f0a000c

    invoke-virtual {p0, v7}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 105
    .local v1, "empty":Landroid/view/View;
    invoke-virtual {p0, v10}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 106
    .local v0, "discoveredDeviceList":Landroid/widget/ListView;
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 107
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 113
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->scanner:Lcom/navdy/obd/discovery/GroupScanner;

    invoke-virtual {v0}, Lcom/navdy/obd/discovery/GroupScanner;->stopScan()Z

    .line 114
    return-void
.end method

.method public onDiscovered(Lcom/navdy/obd/discovery/ObdChannelScanner;Lcom/navdy/obd/io/ChannelInfo;)V
    .locals 2
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;
    .param p2, "device"    # Lcom/navdy/obd/io/ChannelInfo;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->discoveredDevices:Ljava/util/HashSet;

    invoke-virtual {p2}, Lcom/navdy/obd/io/ChannelInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {p2}, Lcom/navdy/obd/io/ChannelInfo;->isBonded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->mPairedDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 167
    const v0, 0x7f0a0007

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->mPairedDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 177
    :cond_1
    :goto_0
    return-void

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/navdy/obd/app/DeviceListActivity;->mNewDevicesArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onScanStarted(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
    .locals 0
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;

    .prologue
    .line 154
    return-void
.end method

.method public onScanStopped(Lcom/navdy/obd/discovery/ObdChannelScanner;)V
    .locals 1
    .param p1, "scanner"    # Lcom/navdy/obd/discovery/ObdChannelScanner;

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 159
    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Lcom/navdy/obd/app/DeviceListActivity;->setTitle(I)V

    .line 160
    return-void
.end method
