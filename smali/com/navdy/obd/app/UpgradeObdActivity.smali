.class public Lcom/navdy/obd/app/UpgradeObdActivity;
.super Landroid/app/Activity;
.source "UpgradeObdActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mTxtCurrentVersion:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0a0001
    .end annotation
.end field

.field protected mTxtUpgradeVersion:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0a0003
    .end annotation
.end field

.field private obdApi:Lcom/navdy/obd/IObdService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/navdy/obd/app/UpgradeObdActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static flash(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recover"    # Z

    .prologue
    .line 134
    new-instance v0, Lcom/navdy/obd/app/UpgradeObdActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/obd/app/UpgradeObdActivity$1;-><init>(Landroid/content/Context;Z)V

    .line 166
    .local v0, "flashTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 167
    return-void
.end method

.method private initView()V
    .locals 5

    .prologue
    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v2}, Lcom/navdy/obd/IObdService;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "firmwareVersion":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->mTxtCurrentVersion:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v1    # "firmwareVersion":Ljava/lang/String;
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while getting firmware version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private update()V
    .locals 6

    .prologue
    .line 52
    iget-object v3, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->obdApi:Lcom/navdy/obd/IObdService;

    if-nez v3, :cond_0

    .line 53
    invoke-static {p0}, Lcom/navdy/obd/app/Utility;->toastNotConnectedMessage(Landroid/content/Context;)V

    .line 76
    :goto_0
    return-void

    .line 57
    :cond_0
    const/4 v2, 0x0

    .line 59
    .local v2, "state":I
    :try_start_0
    iget-object v3, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->obdApi:Lcom/navdy/obd/IObdService;

    invoke-interface {v3}, Lcom/navdy/obd/IObdService;->getState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 63
    :goto_1
    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 64
    invoke-static {p0}, Lcom/navdy/obd/app/Utility;->toastNotConnectedMessage(Landroid/content/Context;)V

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    const-string v4, "Failed to read service state"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 69
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/obd/app/UpgradeObdActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "update.bin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "outputPath":Ljava/lang/String;
    const v3, 0x7f050003

    invoke-static {p0, v3, v1}, Lcom/navdy/obd/app/UpgradeObdActivity;->writeResourceToFile(Landroid/content/Context;ILjava/lang/String;)V

    .line 71
    iget-object v3, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->obdApi:Lcom/navdy/obd/IObdService;

    const-string v4, "3.2.0"

    invoke-interface {v3, v4, v1}, Lcom/navdy/obd/IObdService;->updateDeviceFirmware(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 72
    .end local v1    # "outputPath":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 73
    .restart local v0    # "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception updating the firmware "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static writeResourceToFile(Landroid/content/Context;ILjava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "outputPath"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 80
    .local v4, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 85
    :cond_0
    :try_start_0
    invoke-virtual {v4}, Ljava/io/InputStream;->available()I

    move-result v5

    new-array v0, v5, [B

    .line 86
    .local v0, "buffer":[B
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    .line 87
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 88
    .local v3, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 89
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 90
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 91
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "buffer":[B
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/io/IOException;
    sget-object v5, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error writing resource to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/navdy/obd/app/UpgradeObdActivity;->update()V

    .line 99
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/navdy/obd/app/UpgradeObdActivity;->setContentView(I)V

    .line 45
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/app/Activity;)V

    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/obd/ObdService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/navdy/obd/IObdService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p0, v1}, Lcom/navdy/obd/app/UpgradeObdActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 49
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 112
    sget-object v0, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    const-string v1, "Obd service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static {p2}, Lcom/navdy/obd/IObdService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/obd/app/UpgradeObdActivity;->obdApi:Lcom/navdy/obd/IObdService;

    .line 114
    invoke-direct {p0}, Lcom/navdy/obd/app/UpgradeObdActivity;->initView()V

    .line 115
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 119
    sget-object v0, Lcom/navdy/obd/app/UpgradeObdActivity;->TAG:Ljava/lang/String;

    const-string v1, "Obd service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-void
.end method
