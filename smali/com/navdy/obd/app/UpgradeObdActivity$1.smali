.class final Lcom/navdy/obd/app/UpgradeObdActivity$1;
.super Landroid/os/AsyncTask;
.source "UpgradeObdActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/app/UpgradeObdActivity;->flash(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$recover:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$recover:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 134
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/obd/app/UpgradeObdActivity$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v9, 0x0

    .line 138
    const-string v2, "/dev/ttymxc3"

    .line 139
    .local v2, "port":Ljava/lang/String;
    new-instance v3, Lcom/navdy/obd/io/SerialChannel;

    iget-object v6, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$context:Landroid/content/Context;

    new-instance v7, Lcom/navdy/obd/app/UpgradeObdActivity$1$1;

    invoke-direct {v7, p0}, Lcom/navdy/obd/app/UpgradeObdActivity$1$1;-><init>(Lcom/navdy/obd/app/UpgradeObdActivity$1;)V

    invoke-direct {v3, v6, v7}, Lcom/navdy/obd/io/SerialChannel;-><init>(Landroid/content/Context;Lcom/navdy/obd/io/IChannelSink;)V

    .line 150
    .local v3, "serialChannel":Lcom/navdy/obd/io/SerialChannel;
    invoke-virtual {v3, v2}, Lcom/navdy/obd/io/SerialChannel;->connect(Ljava/lang/String;)V

    .line 151
    new-instance v4, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;

    invoke-direct {v4, v9, v3}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;-><init>(Lcom/navdy/obd/ObdService;Lcom/navdy/obd/io/IChannel;)V

    .line 154
    .local v4, "stnSerialDeviceFirmwareManager":Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "update.bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "outputPath":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$context:Landroid/content/Context;

    const v7, 0x7f050003

    invoke-static {v6, v7, v1}, Lcom/navdy/obd/app/UpgradeObdActivity;->writeResourceToFile(Landroid/content/Context;ILjava/lang/String;)V

    .line 156
    new-instance v5, Lcom/navdy/obd/update/Update;

    invoke-direct {v5}, Lcom/navdy/obd/update/Update;-><init>()V

    .line 157
    .local v5, "update":Lcom/navdy/obd/update/Update;
    const-string v6, ""

    iput-object v6, v5, Lcom/navdy/obd/update/Update;->mVersion:Ljava/lang/String;

    .line 158
    iput-object v1, v5, Lcom/navdy/obd/update/Update;->mUpdateFilePath:Ljava/lang/String;

    .line 159
    invoke-virtual {v3}, Lcom/navdy/obd/io/SerialChannel;->getSerialPort()Lcom/navdy/hardware/SerialPort;

    move-result-object v7

    iget-boolean v6, p0, Lcom/navdy/obd/app/UpgradeObdActivity$1;->val$recover:Z

    if-nez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v4, v5, v7, v6}, Lcom/navdy/obd/update/STNSerialDeviceFirmwareManager;->update(Lcom/navdy/obd/update/Update;Lcom/navdy/hardware/SerialPort;Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v1    # "outputPath":Ljava/lang/String;
    .end local v5    # "update":Lcom/navdy/obd/update/Update;
    :goto_1
    return-object v9

    .line 159
    .restart local v1    # "outputPath":Ljava/lang/String;
    .restart local v5    # "update":Lcom/navdy/obd/update/Update;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 160
    .end local v1    # "outputPath":Ljava/lang/String;
    .end local v5    # "update":Lcom/navdy/obd/update/Update;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/obd/app/UpgradeObdActivity;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception updating the firmware "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
