.class public Lcom/navdy/obd/app/ObdTestActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "ObdTestActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/obd/app/ObdTestActivity;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/obd/app/ObdTestActivity;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0a000e

    const-string v2, "field \'mConversationView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ListView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/obd/app/ObdTestActivity;->mConversationView:Landroid/widget/ListView;

    .line 12
    const v1, 0x7f0a000f

    const-string v2, "field \'mOutEditText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/obd/app/ObdTestActivity;->mOutEditText:Landroid/widget/EditText;

    .line 14
    const v1, 0x7f0a0010

    const-string v2, "field \'mSendButton\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Button;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/obd/app/ObdTestActivity;->mSendButton:Landroid/widget/Button;

    .line 16
    return-void
.end method

.method public static reset(Lcom/navdy/obd/app/ObdTestActivity;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mConversationView:Landroid/widget/ListView;

    .line 20
    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mOutEditText:Landroid/widget/EditText;

    .line 21
    iput-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity;->mSendButton:Landroid/widget/Button;

    .line 22
    return-void
.end method
