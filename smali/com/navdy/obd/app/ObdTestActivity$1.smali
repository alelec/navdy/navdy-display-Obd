.class Lcom/navdy/obd/app/ObdTestActivity$1;
.super Lcom/navdy/obd/IObdServiceListener$Stub;
.source "ObdTestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/app/ObdTestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/app/ObdTestActivity;


# direct methods
.method constructor <init>(Lcom/navdy/obd/app/ObdTestActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-direct {p0}, Lcom/navdy/obd/IObdServiceListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionStateChange(I)V
    .locals 3
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    const-string v0, "ObdTestApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateChange:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v0}, Lcom/navdy/obd/app/ObdTestActivity;->access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 124
    return-void
.end method

.method public onRawData(Ljava/lang/String;)V
    .locals 5
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 141
    .local v0, "buffer":[B
    iget-object v1, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v1}, Lcom/navdy/obd/app/ObdTestActivity;->access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    array-length v3, v0

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 143
    return-void
.end method

.method public onStatusMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v2, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v2}, Lcom/navdy/obd/app/ObdTestActivity;->access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 129
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "toast"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 133
    iget-object v2, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v2}, Lcom/navdy/obd/app/ObdTestActivity;->access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 134
    return-void
.end method

.method public scannedPids(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v1, "message":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/Pid;

    .line 149
    .local v2, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 152
    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 154
    .end local v2    # "pid":Lcom/navdy/obd/Pid;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 157
    .local v0, "buffer":[B
    iget-object v3, p0, Lcom/navdy/obd/app/ObdTestActivity$1;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v3}, Lcom/navdy/obd/app/ObdTestActivity;->access$000(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x2

    array-length v5, v0

    const/4 v6, -0x1

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 158
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 159
    return-void
.end method

.method public scannedVIN(Ljava/lang/String;)V
    .locals 0
    .param p1, "vin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 169
    return-void
.end method

.method public supportedPids(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method
