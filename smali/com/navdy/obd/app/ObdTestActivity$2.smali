.class Lcom/navdy/obd/app/ObdTestActivity$2;
.super Ljava/lang/Object;
.source "ObdTestActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/app/ObdTestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/app/ObdTestActivity;


# direct methods
.method constructor <init>(Lcom/navdy/obd/app/ObdTestActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity$2;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 175
    const-string v0, "ObdTestApp"

    const-string v1, "Service connection established"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/navdy/obd/app/ObdTestActivity$2;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {p2}, Lcom/navdy/obd/IObdService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/obd/app/ObdTestActivity;->access$102(Lcom/navdy/obd/app/ObdTestActivity;Lcom/navdy/obd/IObdService;)Lcom/navdy/obd/IObdService;

    .line 177
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 181
    const-string v0, "ObdTestApp"

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    return-void
.end method
