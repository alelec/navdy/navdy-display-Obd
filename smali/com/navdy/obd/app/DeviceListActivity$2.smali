.class Lcom/navdy/obd/app/DeviceListActivity$2;
.super Ljava/lang/Object;
.source "DeviceListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/app/DeviceListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/app/DeviceListActivity;


# direct methods
.method constructor <init>(Lcom/navdy/obd/app/DeviceListActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/app/DeviceListActivity;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/obd/app/DeviceListActivity$2;->this$0:Lcom/navdy/obd/app/DeviceListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "av":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/navdy/obd/app/DeviceListActivity$2;->this$0:Lcom/navdy/obd/app/DeviceListActivity;

    invoke-static {v2}, Lcom/navdy/obd/app/DeviceListActivity;->access$100(Lcom/navdy/obd/app/DeviceListActivity;)Lcom/navdy/obd/discovery/GroupScanner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/obd/discovery/GroupScanner;->stopScan()Z

    .line 138
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/io/ChannelInfo;

    .line 141
    .local v0, "info":Lcom/navdy/obd/io/ChannelInfo;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 142
    .local v1, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/navdy/obd/app/DeviceListActivity;->EXTRA_DEVICE_ADDRESS:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/navdy/obd/io/ChannelInfo;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    iget-object v2, p0, Lcom/navdy/obd/app/DeviceListActivity$2;->this$0:Lcom/navdy/obd/app/DeviceListActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Lcom/navdy/obd/app/DeviceListActivity;->setResult(ILandroid/content/Intent;)V

    .line 146
    iget-object v2, p0, Lcom/navdy/obd/app/DeviceListActivity$2;->this$0:Lcom/navdy/obd/app/DeviceListActivity;

    invoke-virtual {v2}, Lcom/navdy/obd/app/DeviceListActivity;->finish()V

    .line 147
    return-void
.end method
