.class Lcom/navdy/obd/app/ObdTestActivity$6;
.super Landroid/os/Handler;
.source "ObdTestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/app/ObdTestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/app/ObdTestActivity;


# direct methods
.method constructor <init>(Lcom/navdy/obd/app/ObdTestActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/app/ObdTestActivity;

    .prologue
    .line 334
    iput-object p1, p0, Lcom/navdy/obd/app/ObdTestActivity$6;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    .line 337
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 371
    :goto_0
    :pswitch_0
    return-void

    .line 339
    :pswitch_1
    const-string v4, "ObdTestApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MESSAGE_STATE_CHANGE: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget v4, p1, Landroid/os/Message;->arg1:I

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 344
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/obd/app/ObdTestActivity$6;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v4}, Lcom/navdy/obd/app/ObdTestActivity;->access$400(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->clear()V

    goto :goto_0

    .line 355
    :pswitch_3
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [B

    move-object v2, v4

    check-cast v2, [B

    .line 357
    .local v2, "writeBuf":[B
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    .line 358
    .local v3, "writeMessage":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/obd/app/ObdTestActivity$6;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v4}, Lcom/navdy/obd/app/ObdTestActivity;->access$400(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Me:  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 361
    .end local v2    # "writeBuf":[B
    .end local v3    # "writeMessage":Ljava/lang/String;
    :pswitch_4
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [B

    move-object v0, v4

    check-cast v0, [B

    .line 363
    .local v0, "readBuf":[B
    new-instance v1, Ljava/lang/String;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-direct {v1, v0, v7, v4}, Ljava/lang/String;-><init>([BII)V

    .line 364
    .local v1, "readMessage":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/obd/app/ObdTestActivity$6;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-static {v4}, Lcom/navdy/obd/app/ObdTestActivity;->access$400(Lcom/navdy/obd/app/ObdTestActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 367
    .end local v0    # "readBuf":[B
    .end local v1    # "readMessage":Ljava/lang/String;
    :pswitch_5
    iget-object v4, p0, Lcom/navdy/obd/app/ObdTestActivity$6;->this$0:Lcom/navdy/obd/app/ObdTestActivity;

    invoke-virtual {v4}, Lcom/navdy/obd/app/ObdTestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "toast"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 368
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 337
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch

    .line 340
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
