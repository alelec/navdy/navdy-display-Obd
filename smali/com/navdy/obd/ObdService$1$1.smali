.class Lcom/navdy/obd/ObdService$1$1;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService$1;->updateDeviceFirmware(Ljava/lang/String;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/obd/ObdService$1;

.field final synthetic val$update:Lcom/navdy/obd/update/Update;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService$1;Lcom/navdy/obd/update/Update;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/obd/ObdService$1;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iput-object p2, p0, Lcom/navdy/obd/ObdService$1$1;->val$update:Lcom/navdy/obd/update/Update;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 148
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v2, v2, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$500(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/update/ObdDeviceFirmwareManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/obd/ObdService$1$1;->val$update:Lcom/navdy/obd/update/Update;

    invoke-interface {v2, v3}, Lcom/navdy/obd/update/ObdDeviceFirmwareManager;->updateFirmware(Lcom/navdy/obd/update/Update;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v0, v0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$602(Lcom/navdy/obd/ObdService;Z)Z

    .line 150
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v0, v0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$702(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v0, v0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/navdy/obd/ObdService;->access$802(Lcom/navdy/obd/ObdService;Ljava/lang/String;)Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/navdy/obd/ObdService$1$1;->this$1:Lcom/navdy/obd/ObdService$1;

    iget-object v0, v0, Lcom/navdy/obd/ObdService$1;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v0}, Lcom/navdy/obd/ObdService;->forceReconnect()V

    .line 153
    return-void
.end method
