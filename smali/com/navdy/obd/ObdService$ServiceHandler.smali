.class final Lcom/navdy/obd/ObdService$ServiceHandler;
.super Landroid/os/Handler;
.source "ObdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/ObdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method private constructor <init>(Lcom/navdy/obd/ObdService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 449
    iput-object p1, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    .line 450
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 451
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/obd/ObdService;Landroid/os/Looper;Lcom/navdy/obd/ObdService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/obd/ObdService;
    .param p2, "x1"    # Landroid/os/Looper;
    .param p3, "x2"    # Lcom/navdy/obd/ObdService$1;

    .prologue
    .line 448
    invoke-direct {p0, p1, p2}, Lcom/navdy/obd/ObdService$ServiceHandler;-><init>(Lcom/navdy/obd/ObdService;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 454
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_fc

    .line 505
    :cond_8
    :goto_8
    return-void

    .line 456
    :pswitch_9
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2, v3}, Lcom/navdy/obd/ObdService;->access$3200(Lcom/navdy/obd/ObdService;I)V

    goto :goto_8

    .line 459
    :pswitch_11
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MESSAGE_TOAST: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$000(Lcom/navdy/obd/ObdService;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/IObdServiceListener;

    .line 462
    .local v1, "listener":Lcom/navdy/obd/IObdServiceListener;
    :try_start_43
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "toast"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/navdy/obd/IObdServiceListener;->onStatusMessage(Ljava/lang/String;)V
    :try_end_50
    .catch Landroid/os/RemoteException; {:try_start_43 .. :try_end_50} :catch_51

    goto :goto_37

    .line 463
    :catch_51
    move-exception v0

    .line 464
    .local v0, "e":Landroid/os/RemoteException;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to notify listener of statusMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_37

    .line 470
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "listener":Lcom/navdy/obd/IObdServiceListener;
    :pswitch_5c
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v2

    if-ne v2, v6, :cond_8

    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$2600(Lcom/navdy/obd/ObdService;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 471
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Rescanning for ECUs"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/navdy/obd/ObdService;->access$3200(Lcom/navdy/obd/ObdService;I)V

    goto :goto_8

    .line 477
    :pswitch_7c
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Disabling PIDs scan"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    iget-object v2, v2, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ObdCommunicationEnabled"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 479
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2, v5}, Lcom/navdy/obd/ObdService;->access$2602(Lcom/navdy/obd/ObdService;Z)Z

    .line 480
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$300(Lcom/navdy/obd/ObdService;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    .line 481
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$900(Lcom/navdy/obd/ObdService;)V

    .line 482
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2, v6}, Lcom/navdy/obd/ObdService;->access$3200(Lcom/navdy/obd/ObdService;I)V

    goto/16 :goto_8

    .line 487
    :pswitch_b0
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Enabling PIDs scan"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    iget-object v2, v2, Lcom/navdy/obd/ObdService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ObdCommunicationEnabled"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 489
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2, v4}, Lcom/navdy/obd/ObdService;->access$2602(Lcom/navdy/obd/ObdService;Z)Z

    goto/16 :goto_8

    .line 492
    :pswitch_d1
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdService;->resetChannel()V

    goto/16 :goto_8

    .line 495
    :pswitch_d8
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$900(Lcom/navdy/obd/ObdService;)V

    .line 496
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2, v4}, Lcom/navdy/obd/ObdService;->access$602(Lcom/navdy/obd/ObdService;Z)Z

    .line 497
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdService;->forceReconnect()V

    goto/16 :goto_8

    .line 500
    :pswitch_e9
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$900(Lcom/navdy/obd/ObdService;)V

    .line 501
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2, v3}, Lcom/navdy/obd/ObdService;->access$3300(Lcom/navdy/obd/ObdService;I)V

    .line 502
    iget-object v2, p0, Lcom/navdy/obd/ObdService$ServiceHandler;->this$0:Lcom/navdy/obd/ObdService;

    invoke-virtual {v2}, Lcom/navdy/obd/ObdService;->forceReconnect()V

    goto/16 :goto_8

    .line 454
    :pswitch_data_fc
    .packed-switch 0x1
        :pswitch_9
        :pswitch_11
        :pswitch_5c
        :pswitch_5c
        :pswitch_7c
        :pswitch_b0
        :pswitch_d1
        :pswitch_d8
        :pswitch_e9
    .end packed-switch
.end method
