.class Lcom/navdy/obd/ObdService$7;
.super Ljava/lang/Object;
.source "ObdService.java"

# interfaces
.implements Lcom/navdy/obd/ObdJob$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/obd/ObdService;->monitorPendingCodes()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/obd/ObdService;


# direct methods
.method constructor <init>(Lcom/navdy/obd/ObdService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/obd/ObdService;

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/obd/ObdJob;Z)V
    .locals 12
    .param p1, "job"    # Lcom/navdy/obd/ObdJob;
    .param p2, "success"    # Z

    .prologue
    .line 1003
    if-eqz p2, :cond_15

    .line 1004
    iget-object v0, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1900(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/VehicleInfo;

    move-result-object v0

    iget-object v11, v0, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 1006
    .local v11, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v11, :cond_16

    .line 1007
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v2, "No DTC"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    .end local v11    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_15
    :goto_15
    return-void

    .line 1009
    .restart local v11    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_16
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_41

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1010
    .local v10, "troubleCode":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current DTC: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a

    .line 1013
    .end local v10    # "troubleCode":Ljava/lang/String;
    :cond_41
    const-string v6, "P0401"

    .line 1014
    .local v6, "code":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$4000(Lcom/navdy/obd/ObdService;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_15

    const/4 v0, 0x0

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1024
    invoke-static {}, Lcom/navdy/obd/ObdService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Will clear DTC 0401"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    new-instance v9, Lcom/navdy/obd/command/ObdCommand;

    const-string v0, "Set Header"

    const-string v2, "ATSH8110F1"

    invoke-direct {v9, v0, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    .local v9, "setHeader":Lcom/navdy/obd/command/ObdCommand;
    new-instance v8, Lcom/navdy/obd/command/ObdCommand;

    const-string v0, "Clear Codes"

    const-string v2, "14FF001"

    invoke-direct {v8, v0, v2}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    .local v8, "sendClear":Lcom/navdy/obd/command/ObdCommand;
    new-instance v1, Lcom/navdy/obd/command/ObdCommand;

    const-string v0, "04"

    invoke-direct {v1, v0}, Lcom/navdy/obd/command/ObdCommand;-><init>(Ljava/lang/String;)V

    .line 1030
    .local v1, "clearDTCCommand":Lcom/navdy/obd/command/ObdCommand;
    iget-object v0, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v0}, Lcom/navdy/obd/ObdService;->access$1200(Lcom/navdy/obd/ObdService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    .line 1031
    .local v7, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v0, Lcom/navdy/obd/ObdJob;

    iget-object v2, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    .line 1034
    invoke-static {v2}, Lcom/navdy/obd/ObdService;->access$200(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/io/IChannel;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    invoke-static {v3}, Lcom/navdy/obd/ObdService;->access$1600(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/Protocol;

    move-result-object v3

    new-instance v4, Lcom/navdy/obd/ObdService$7$1;

    invoke-direct {v4, p0}, Lcom/navdy/obd/ObdService$7$1;-><init>(Lcom/navdy/obd/ObdService$7;)V

    iget-object v5, p0, Lcom/navdy/obd/ObdService$7;->this$0:Lcom/navdy/obd/ObdService;

    .line 1044
    invoke-static {v5}, Lcom/navdy/obd/ObdService;->access$1700(Lcom/navdy/obd/ObdService;)Lcom/navdy/obd/ObdDataObserver;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/ObdJob;-><init>(Lcom/navdy/obd/command/ICommand;Lcom/navdy/obd/io/IChannel;Lcom/navdy/obd/Protocol;Lcom/navdy/obd/ObdJob$IListener;Lcom/navdy/obd/command/IObdDataObserver;)V

    const-wide/16 v2, 0x64

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1031
    invoke-interface {v7, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_15
.end method
