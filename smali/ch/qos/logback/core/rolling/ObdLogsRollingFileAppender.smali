.class public Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;
.super Lch/qos/logback/core/rolling/RollingFileAppender;
.source "ObdLogsRollingFileAppender.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lch/qos/logback/core/rolling/RollingFileAppender",
        "<TE;>;"
    }
.end annotation


# instance fields
.field loggerName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    .local p0, "this":Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;, "Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender<TE;>;"
    invoke-direct {p0}, Lch/qos/logback/core/rolling/RollingFileAppender;-><init>()V

    return-void
.end method


# virtual methods
.method protected append(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;, "Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender<TE;>;"
    .local p1, "eventObject":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->loggerName:Ljava/lang/String;

    if-nez v0, :cond_0

    instance-of v0, p1, Lch/qos/logback/classic/spi/ILoggingEvent;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 31
    check-cast v0, Lch/qos/logback/classic/spi/ILoggingEvent;

    invoke-interface {v0}, Lch/qos/logback/classic/spi/ILoggingEvent;->getLoggerName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->loggerName:Ljava/lang/String;

    .line 33
    :cond_0
    invoke-super {p0, p1}, Lch/qos/logback/core/rolling/RollingFileAppender;->append(Ljava/lang/Object;)V

    .line 34
    return-void
.end method

.method public rollover()V
    .locals 4

    .prologue
    .line 20
    .local p0, "this":Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;, "Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender<TE;>;"
    invoke-super {p0}, Lch/qos/logback/core/rolling/RollingFileAppender;->rollover()V

    .line 21
    iget-object v2, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->rollingPolicy:Lch/qos/logback/core/rolling/RollingPolicy;

    check-cast v2, Lch/qos/logback/core/rolling/RollingPolicyBase;

    iget-object v2, v2, Lch/qos/logback/core/rolling/RollingPolicyBase;->fileNamePattern:Lch/qos/logback/core/rolling/helper/FileNamePattern;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lch/qos/logback/core/rolling/helper/FileNamePattern;->convertInt(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/obd/ObdService;->getObdService()Lcom/navdy/obd/ObdService;

    move-result-object v1

    .line 23
    .local v1, "service":Lcom/navdy/obd/ObdService;
    if-eqz v1, :cond_0

    .line 24
    iget-object v2, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->loggerName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/obd/ObdService;->onLogFileRollOver(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    :cond_0
    return-void
.end method

.method protected subAppend(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;, "Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender<TE;>;"
    .local p1, "event":Ljava/lang/Object;, "TE;"
    invoke-static {}, Lcom/navdy/obd/ObdService;->getObdService()Lcom/navdy/obd/ObdService;

    move-result-object v0

    .line 39
    .local v0, "service":Lcom/navdy/obd/ObdService;
    if-eqz v0, :cond_0

    .line 40
    iget-boolean v1, v0, Lcom/navdy/obd/ObdService;->monitoringCanBusLimitReached:Z

    if-eqz v1, :cond_0

    .line 41
    const-string v1, "com.navdy.obd.CanBusRaw"

    iget-object v2, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->loggerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v2, p0, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->triggeringPolicy:Lch/qos/logback/core/rolling/TriggeringPolicy;

    monitor-enter v2

    .line 43
    :try_start_0
    invoke-virtual {p0}, Lch/qos/logback/core/rolling/ObdLogsRollingFileAppender;->rollover()V

    .line 44
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_0
    invoke-super {p0, p1}, Lch/qos/logback/core/rolling/RollingFileAppender;->subAppend(Ljava/lang/Object;)V

    .line 49
    return-void

    .line 44
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
